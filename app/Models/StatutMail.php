<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StatutMail extends Model {
	protected $table = 'statut_mail';
    protected $primaryKey = 'id_statut';
}
