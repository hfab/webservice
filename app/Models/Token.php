<?php namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Token extends Model {

    protected $table = 'tokens';

    protected $fillable = ['destinataire_id', 'campagne_id', 'base_id', 'fai_id', 'priority', 'date_active', 'sender_id', 'md_list_id'];

    function scopeByBase($query, $base) {
        return $query->where('base', $base);
    }

    function scopeByFAI($query, $fai) {
        return $query->where('fai', $fai);
    }

    function scopeIsAvailable($query) {
        return $query->whereRaw('campagne_id IS null')->today();
    }

    function scopeToday($query) {
        $today = Carbon::today()->format('Y-m-d');
        return $query->where('date_active', $today);
    }

    function scopeYesterday($query) {
        $yesterday = Carbon::yesterday()->format('Y-m-d');
        return $query->where('date_active', $yesterday);
    }

    function destinataire() {
        return $this->belongsTo('\App\Models\Destinataire', 'destinataire_id', 'id');
    }

    function fai() {
        return $this->belongsTo('\App\Models\Fai', 'fai_id', 'id');
    }

    function base() {
        return $this->belongsTo('\App\Models\Base', 'base_id', 'id');
    }

    function sender() {
        return $this->hasOne('\App\Models\Sender', 'id', 'sender_id');
    }
}
