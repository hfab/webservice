<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Segment extends Model {
    
    protected $fillable = ['condition_type', 'condition_table', 'condition_column', 'condition_operator', 'condition_value'];
    
}
