<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fai extends Model {
    protected $fillable = ['nom', 'code'];

    public $timestamps = false;


    function get_domains() {
        if (strpos($this->domains, '|') !== false) {
            return explode('|', $this->domains);
        }
        return [$this->domains];
    }

    static function quotas() {
        $fais = Fai::all();

        $return = array();

        foreach($fais as $fai) {
            $return[$fai->id] = $fai->quota_pression;
        }

        return $return;
    }

    static function domains() {
        $fais = Fai::all();

        $return = array();

        foreach($fais as $fai) {
            $return[$fai->id] = explode('|', $fai->domains);
        }

        return $return;
    }
}
