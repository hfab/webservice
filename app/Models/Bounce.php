<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bounce extends Model {

    protected $table = 'bounces';

    protected $fillable = array('id', 'destinataire_id', 'campagne_id', 'message');

    function scopeByDate($query, $date)
    {
        return $query->where('created_at', $date);
    }

    function destinataire()
    {
        return $this->belongsTo('\App\Models\Destinataire', 'destinataire_id', 'id');
    }

    function campagne()
    {
        return $this->belongsTo('\App\Models\Campagne', 'campagne_id', 'id');
    }


}
