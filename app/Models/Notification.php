<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model {

    protected $fillable = ['message', 'type', "user_id", 'is_important'];

	function user() {
        return $this->belongsTo('\App\Models\User', 'user_id', 'id');
    }

    static function add($type, $title, $message, $important = false, $url = null) {
        $types = ['info', 'error'];

        if (!in_array($type, $types)) {
            echo "TYPE DE NOTIF NON RECONNU ".PHP_EOL;
            return false;
        }

        $fields = [
            'user_id' => Auth::User()->id,
            'type' => $type,
            'message' => $message,
            'is_important' => $important,
            'url' => $url
            ];

        self::create($fields);
    }

    function getColorAttribute() {
        switch($this->level) {
            case 'DEBUG':
                return 'yellow-gold';
            case 'INFO':
                return 'green-meadow';
            case 'ERROR':
                return 'red-thunderbird';
            default :
                return 'blue-madison';
        }

    }

    function getIconAttribute() {

        return 'flash';
    }
}
