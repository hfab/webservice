<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blacklist extends Model {
	protected $table = 'blacklistes';
    public $timestamps = false;

    protected $fillable = ['email', 'base_id'];

    function base() {
        return $this->hasOne('\App\Models\Base', 'id', 'base_id');
    }
}

