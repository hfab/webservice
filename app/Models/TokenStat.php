<?php namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class TokenStat extends Model {

    protected $table = 'tokens_stats';

    protected $fillable = ['base_id', 'count'];

    function base() {
        return $this->belongsTo('\App\Models\Base', 'base_id', 'id');
    }
}
