<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class CampagneBase extends Model
{
    protected $table = 'campagne_base';

    public function campagne()
    {
        return $this->hasOne('\App\Models\Campagne', 'id', 'campagne_id');
    }
}
