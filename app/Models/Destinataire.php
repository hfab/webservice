<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Fai;

class Destinataire extends Model {
	protected $fillable = ['mail', 'base_id', 'fai_id', 'liste', 'origine', 'tokenized_at', 'sender_id'];

    function scopeMail($query, $mail) {
        return $query->where('mail', $mail);
    }

    function scopeBaseId($query, $base) {
        return $query->where('base_id', $base);
    }

    function scopeFAI($query, $fai) {
        return $query->where('fai', $fai);
    }

    function getAdresseAttribute() {
        return self::decrypt($this->m_mail);
    }

    static function getFai($mail = null)
    {
        $mail = explode('@', $mail);
        if (!isset($mail[1])) {
            return false;
        }
        $mail = $mail[1];

        $fais = \Cache::remember('fais', 60, function()
        {
            return \App\Models\Fai::all();
        });

        foreach($fais as $fai) {
            if (strpos($fai->domains, $mail) !== false) {
                return $fai;
            }
        }
        return Fai::find(6);
    }

    function fai() {
        return $this->belongsTo('\App\Models\Fai', 'fai_id', 'id');
    }

}
