<?php namespace App\Observers;

use App\Models\Notification;

class CampagneObserver {
    public function saving($model)
    {
        $fields = [
            'is_important' => false,
            'user_id' => \Auth::User()->id,
            'level' => 'info',
            'message' => 'Campagne '.$model->nom.' créée.'
        ];

        Notification::create($fields);
    }
}

class BaseObserver {
    public function saving($model)
    {
        $fields = [
            'is_important' => false,
            'user_id' => \Auth::User()->id,
            'level' => 'info',
            'message' => 'Base '.$model->nom.' créée.'
        ];

        Notification::create($fields);
    }
}