<?php
Route::group(['prefix' => ''], function() {
    define('DESTINATAIRE_ACTIVE',       '0');
    define('DESTINATAIRE_OPTOUT',       '3');
    define('DESTINATAIRE_BOUNCE',       '4');
    define('DESTINATAIRE_ABUSED',       '5');
    define('BASE_DEACTIVATED',          '6');
    define('DESTINATAIRE_BLACKLISTED',  '666');
});

Route::get              ('motdepasse/new',                                  'UserController@indexNewMdp');
Route::post             ('motdepasse/new',                                  'UserController@NewMdp');

Route::controllers([
    'auth' =>       'Auth\AuthController',
    'password' =>   'Auth\PasswordController',
]);

Route::get('webservice-import-lead','ApiController@ve_api');

Route::group(['middleware' => 'auth'], function () {

    // Partie mobile
    Route::get('mobilehome', function () {
        return view('mobile.index');
    });

    Route::resource     ('bat',                                             'BatController');

    Route::get          ('/',                                               'HomeController@index');
    Route::get          ('/home',                                           'HomeController@index');

    Route::get          ('/liste',                                          'HomeController@liste');

    Route::get          ('/gen-envoi',                                      'ListeController@gen_envoi');
    Route::post         ('/gen-envoi',                                      'ListeController@gen_envoi_submit');

    Route::get          ('campagnes/mail',                                  'CampagneController@create');
    Route::post         ('ajax/similitude',                                 'AjaxController@getSimilarDossier');

    Route::post         ('ajax/c_stats_ouv',                                'AjaxController@getOuvertures');
    Route::post         ('ajax/c_stats_clicks',                             'AjaxController@getClicks');
    Route::post         ('ajax/c_stats_bounces',                            'AjaxController@getBounces');
    Route::post         ('ajax/ca',                                         'AjaxController@getRecordFront');
    Route::post         ('ajax/ca/setaaf',                                  'AjaxController@setaaf');

    Route::get          ('campagnes/fetchBat',                              'CampagneController@fetchBat');

    Route::get          ('base/add',                                        'BasesController@add');
    Route::post         ('base/add_type',                                   'BasesController@inject_file_type');

    Route::post         ('base/{id}/add_destinataires',                     'BasesController@inject_file');
    Route::get          ('base/{id}/add_destinataires',                     'BasesController@add_destinataires');
    Route::get          ('base/{id}/stats',                                 'BasesController@stats');

    Route::get          ('base/{id}/compare_upload',                        'BasesController@compare_upload');
    Route::post         ('base/{id}/compare_mails',                         'BasesController@compare_mails');

    Route::get          ('campagne/unsubscribers',                          'CampagneController@extractUnsubscribers');
    Route::post         ('campagne/unsubscribers',                          'CampagneController@extractUnsubscribers');

// ajout fab1 for capdecision
    Route::get          ('campagne/{id}/voir',                              'CampagneController@previsualisation');
// plac� avant "resource" car sinon bug avec la ligne ressource


    Route::post         ('base/checkemail',                                 'BasesController@check_desabo');
    Route::resource     ('base/searchmailall',                              'BasesController@searchall');
    Route::get          ('base/searchmailall/desabo/{recherche}',           'BasesController@desabo_fct_domainName');
    Route::resource     ('base',                                            'BasesController');

    Route::get          ('campagne/{id}/md5',                               'CampagneController@choixfichiermd5');
    Route::post         ('campagne/md5/action',                             'CampagneController@add_repoussoirs');

    Route::get          ('csv',                                             'CsvController@index');

    Route::get          ('csv/base',                                        'CsvController@choixextrairebase');
    Route::post         ('csv/base',                                        'CsvController@extrairebase');

    Route::get          ('csv/campagne',                                    'CsvController@choixextrairecampagne');
    Route::post         ('csv/campagne',                                    'CsvController@extrairecampagne');


    Route::get          ('csv/theme',                                       'CsvController@choixextrairetheme');
    Route::post         ('csv/theme',                                       'CsvController@extrairetheme');

    Route::get          ('csv/periode',                                     'CsvController@choixextraireperiode');
    Route::post         ('csv/periode',                                     'CsvController@extraireperiode');

// stats diff�rence entre destinataire et repoussoir
    Route::get          ('stats/email',                                     'HomeController@choixstatsemail');
    Route::post         ('stats/email/base',                                'HomeController@statsbaseemail');

    Route::get          ('deconnexion',                                     'HomeController@deconnexion');
// fin fab1

    Route::get          ('campagne',                                        'CampagneController@index');
    Route::resource     ('campagne',                                        'CampagneController');

    Route::get          ('campagne/{id}/dupliquercampagne',                 'CampagneController@dupliquercampagne');
    Route::get          ('campagne/{id}/edit/info',                         'CampagneController@editmessage');
    Route::post         ('campagne/{id}/send_bat_md',                       'CampagneController@send_bat_maildrop');
	Route::post         ('campagne/{id}/send_bat_sm',                       'CampagneController@send_bat_smessage');
	Route::post         ('campagne/{id}/send_bat_mb',                       'CampagneController@send_bat_mindbaz');
    Route::post         ('campagne/{id}/send_bat_edatis',                   'CampagneController@send_bat_edatis');

    Route::get          ('campagne/{id}/choix_compte_bat_m4y',              'CampagneController@choix_compte_bat_m4y');
    Route::post         ('campagne/send_bat_mailforyou_choix',              'CampagneController@send_bat_mailforyou_choix');
    // old
    Route::get          ('campagne/{id}/send_bat_mailforyou',               'CampagneController@send_bat_mailforyou');

    Route::resource     ('campagne/{id}/stats',                             'CampagneController@showStats');
    Route::get          ('campagne/{id}/html',                              'CampagneController@showHtml');
    Route::get          ('campagne/{id}/html/check',                        'HtmlController@check_kit');

    Route::get          ('planning/{id}/senders',                           'PlanningController@show_used_senders');
    Route::get          ('planning/{id}/send',                              'PlanningController@send');

    Route::resource     ('campagne/{id}/planning',                          'CampagneController@editPlanning');
    Route::post         ('campagne/storePlanning',      ['uses' =>          'CampagneController@storePlanning', 'as' =>'storePlanning' ] );
    Route::get          ('campagne/{id}/planning/routeurerror',             'CampagneController@editPlanning');
    //MINDBAZ : planifications méthode listes
    Route::get          ('campagne/{campagne_id}/mindbaz_planning',         'PlanningController@edit_mindbaz');
    Route::post         ('campagne/{campagne_id}/mindbaz_planning',         'PlanningController@schedule_mindbaz');

    Route::post         ('planning/{id}/calculatespamscore',                'PlanningController@calculatespamscore');
    Route::post         ('planning/{id}/validate',                          'PlanningController@set_valid');
    Route::post         ('planning/{id}/delete',                            'PlanningController@delete');
    Route::post         ('planning/{id}/sendbatmb',                         'PlanningController@send_bat_mb');

    Route::post         ('sender/lists/ajax',                               'SenderController@ajaxGetLists');

    Route::get          ('volumetotal',                                     'CampagneController@volumetotal');

    Route::post         ('apply/upload',                                    'CampagneController@upload_repoussoirs');
    Route::get          ('dezip',                                           'CampagneController@upload_import_img_server');
    Route::post         ('dezip',                                           'CampagneController@import_img_server');

    Route::get          ('/sender/multidomain',                             'SenderController@multi_edit_ovh');
    Route::post         ('/sender/multidomain',                             'SenderController@multi_edit_ovh');

    Route::get          ('sender/change_volume_new',                             'SenderController@change_volume');
    Route::post         ('sender/cchange_volume_new',                             'SenderController@change_volume_store');
    Route::get          ('sender/change_volume_new/{id}',                        'SenderController@change_volume_tri');

    Route::get          ('sender/{id}/enable',                              'SenderController@enable_sender');
    Route::get          ('sender/{id}/disable',                             'SenderController@disable_sender');

    Route::resource     ('sender',                                          'SenderController');

    //Special functions for Mindbaz
    Route::post         ('sender/{id}/uploadsubscribers',                   'SenderController@upload_subscribers');
    Route::get          ('sender/{id}/lists',                               'SenderController@show_lists');
    Route::resource     ('sender/{id}/createlist',                          'SenderController@create_list');

    Route::get          ('sender/{id}/consulter',                           'SenderController@consulter');

    Route::resource     ('affiche',                                         'HtmlController');
    Route::resource     ('theme',                                           'ThemeController');

// ajout rendre theme inactif/actif
    Route::get          ('theme/{id}/inactif',                              'ThemeController@makeinactif');
    Route::get          ('theme/{id}/activer',                              'ThemeController@makeactiver');

    Route::resource     ('user',                                            'UserController');

    Route::get          ('deduplicate/import',                              'ListeController@showDeduplicate');
    Route::post         ('deduplicate/import',                              'ListeController@ajaxDeduplicate');

    Route::get          ('blacklist/import',                                'ListeController@showBlacklist');
    Route::post         ('blacklist/import',                                'ListeController@ajaxBlacklist');
    Route::get          ('kit',                                             'KitController@test');
    Route::get          ('compare',                                         'ListeController@showCompare');
    Route::post         ('compare',                                         'ListeController@ajaxCompare');

    Route::get          ('ouv',                                             'OuvertureController@store');

    Route::get          ('testClicks',                                      'HomeController@testClicks');

    Route::get          ('stats',                                           'StatsController@stats');
    Route::get          ('stats/fai/{id}',                                  'StatsController@statsfai');
    Route::get          ('stats/optout',                                    'StatsController@statsoptout');
    Route::get          ('stats/ouvertures',                                'StatsController@statsouvertures');

    Route::get          ('stats/bounces',                                   'HomeController@bounces');
    Route::get          ('planning',                                        'HomeController@planning');
    Route::post          ('planning',                                       'HomeController@planning');

    Route::get          ('notifications',                                   'HomeController@notifications');
    Route::get          ('unsubscribe',                                     'HomeController@unsubscribe');
    Route::post         ('unsubscribeById',                                 'HomeController@unsubscribeById');

    Route::resource     ('fai',                                             'FaisController');

    // PARTIE STATS FAB
    Route::get          ('stats/routeur',                                   'StatsController@index');

    Route::get          ('stats/routeur/maildrop',                          'StatsController@maildrop');
    Route::get          ('stats/routeur/smessage',                          'StatsController@smessage');
    Route::get          ('stats/routeur/phoenix',                           'StatsController@phoenix');
    Route::get          ('stats/routeur/mailforyou',                        'StatsController@mailforyou');
    Route::get          ('stats/routeur/mindbaz',                           'StatsController@mindbaz');
    Route::get          ('stats/routeur/edatis',                            'StatsController@edatis');

    Route::get          ('stats/routeurs/',                                 'StatsController@statsrouteurs');
    Route::post         ('stats/routeurs/',                                 'StatsController@statsrouteurs');

    Route::get          ('stats/routeur/maildrop/{ref}',                    'StatsController@stats_detail_md');
    Route::get          ('stats/routeur/smessage/{ref}',                    'StatsController@stats_detail_smessage');
    Route::get          ('stats/routeur/phoenix/{ref}',                     'StatsController@stats_detail_phoenix');
    Route::get          ('stats/routeur/phoenix/{ref}',                     'StatsController@stats_detail_mindbaz');

    Route::group        (['middleware' => 'plan'], function () {
        Route::resource ('tag',                                             'TagController');
    });

    Route::get          ('logs',                                            'HomeController@logs');
    Route::get          ('sender/create/error',                             'SenderController@create');
    Route::get          ('sender/{id}/edit/error',                          'SenderController@edit');

    Route::get          ('upload', function() {
        return View::make('campagne.upload_repoussoir');
    });

    Route::post         ('segment/ajax',                                    'SegmentController@ajaxShowVolume');
    Route::resource     ('segment',                                         'SegmentController');

    Route::post         ('routeur/senders/ajax',                            'RouteurController@ajaxGetSenders');
    Route::post         ('planning/defaut/ajax',                            'CampagneController@ajaxGetPlanningDefaut');
    Route::get          ('planning/defaut',                                 'CampagneController@config_defaut_planning');
    Route::get          ('planning/defaut/{id}/edit',                       'CampagneController@config_defaut_edit');
    Route::post         ('planning/defaut/{id}/edit',                       'CampagneController@config_defaut_edit_store');
    Route::get          ('planning/defaut/{id}/delete',                     'CampagneController@config_defaut_edit_delete');
    Route::get          ('planning/defaut/add',                             'CampagneController@config_defaut_planning_add');
    Route::post         ('planning/defaut/add',                             'CampagneController@config_defaut_planning_add_store');


    Route::get          ('base/{id}/destinataire',                          'BasesController@show_base_destinataires');
    Route::get          ('base/{id}/destinataire/{destinataire_id}/edit',   'BasesController@edit_destinataire');
    Route::post         ('base/destinataire/store',                         'BasesController@store_destinataire');
    Route::get          ('base/{id}/destinataire/{typecrit}/{valuecrit}',   'BasesController@show_base_destinataires_crit');
    Route::post         ('base/destinataire/search',                        'BasesController@show_search_destinataires_results');
    Route::post         ('base/{id}/destinataire/ajax',                     'BasesController@get_search_destinataires_ajax');
    Route::resource     ('base/{base_id}/destinataire/search',              'BasesController@show_search_destinataires_results');

    Route::get          ('base/{baseid}/add_destinataires_perso',           'BasesController@add_destinataires_perso');
    Route::post         ('base/{baseid}/add_destinataires_perso',           'BasesController@import_desti_champs_perso_prepare');
    Route::post         ('base/{baseid}/add_destinataires_perso/import',    'BasesController@import_desti_champs_perso_run');


    Route::get     ('ca','CampagneController@CampagneCa_show');
    Route::get     ('ca/mois/{id}','CampagneController@CampagneCa_show_mois');
    Route::get     ('ca/add/{id}','CampagneController@CampagneCa_add_mois');
    Route::get     ('ca/tri/base/{id}', 'CampagneController@CampagneCa_showByBase');
    Route::get     ('ca/tri/plateforme/{id}', 'CampagneController@CampagneCa_showByPlateforme');

    Route::post     ('ca/tri/search', 'CampagneController@CampagneCa_search');
    Route::post     ('ca/force', 'CampagneController@CampagneCaForce');


    Route::get     ('plateforme', 'CampagneController@plateforme_affi_Show');
    Route::get     ('plateforme/add', 'CampagneController@plateforme_affi_Add');
    Route::post     ('plateforme/add', 'CampagneController@plateforme_affi_Store');


    //Super Admin = dev , desactiver de routeurs etc..
    Route::group        (['middleware' => 'superadmin'], function () {
        Route::get      ('/admin',                                          'AdminController@index');
        Route::resource ('/admin/routeur',                                  'RouteurController@index');
        Route::get      ('/admin/routeur/{id}/enable',                      'RouteurController@changeStatusRouteurEnable');
        Route::get      ('/admin/routeur/{id}/disable',                     'RouteurController@changeStatusRouteurDisable');

        Route::get      ('/admin/auto/enable',                              'AdminController@envoiEnable');
        Route::get      ('/admin/auto/disable',                             'AdminController@envoiDisable');

        Route::get      ('/admin/dns',                                      'AdminController@dns_record_index');
        Route::post     ('/admin/dns',                                      'AdminController@dns_record_result');

        Route::resource ('/admin/branch',                                   'BranchController');
        Route::get      ('/admin/branch/{id}/enable',                       'BranchController@enable');
        Route::get      ('/admin/branch/{id}/disable',                      'BranchController@disable');

        Route::get      ('/admin/cleanrelaunch/enable',                     'AdminController@cleanRelaunchEnable');
        Route::get      ('/admin/cleanrelaunch/disable',                    'AdminController@cleanRelaunchDisable');


        Route::get      ('/admin/routeur/{id}/enable_segment',              'RouteurController@segmentDisable');
        Route::get      ('/admin/routeur/{id}/disable_segment',             'RouteurController@segmentEnable');

        Route::get      ('/admin/pixel/',                                   'AdminController@pixelIndex');
        Route::get      ('/admin/pixel/{id}',                               'AdminController@pixelEdit');
        Route::post     ('/admin/pixel/{id}',                               'AdminController@pixelEdit');


        Route::get     ('downl','ListeController@download_base_index');
        Route::get     ('downl/base/{base_id}','ListeController@download_base_email_go');
        Route::get     ('downl/basedesabo/{base_id}','ListeController@download_basedesabo_email_go');
        Route::get     ('/downl/basemd5/{base_id}','ListeController@download_basehash_email_go');
        Route::get     ('/downl/champs/{base_id}','ListeController@download_champs_email_go');

        Route::get     ('downl/files','ListeController@download_file_list');
        Route::get     ('downl/file/{fichier}','ListeController@dwnl_file');
        Route::get     ('downl/deletefile/{fichier}','ListeController@dwnl_deletefile');

        // Partie Outils
        Route::get          ('outils/bl/add',                                   'AdminController@add_check_bl_manuel');
        Route::get          ('outils/bl',                                       'AdminController@bl_status');
        Route::get          ('outils/bl/{id_bl}',                               'AdminController@bl_details');

        Route::post         ('outils/bl/add',                                   'AdminController@add_check_bl_manuel_store');

        Route::get          ('outils/listemanager',                             'ListeController@indexoutils');
    //    Route::get          ('outils/listemanager/assembler',                   'ListeController@assembler');

        Route::get          ('outils/listemanager/upload',                      'ListeController@indexupload');
        Route::post         ('outils/listemanager/upload',                      'ListeController@upload');

        Route::get          ('outils/listemanager/supprimer/{fichier}',         'ListeController@supprimerfichier');
        Route::get          ('outils/listemanager/telecharger/{fichier}',       'ListeController@telechargerfichier');

        Route::get          ('outils/listemanager/assembler',                   'ListeController@indexassembler');
        Route::post         ('outils/listemanager/assembler',                   'ListeController@assembler');

        Route::get          ('outils/listemanager/decoupage',                   'ListeController@indexdecoupage');
        Route::post         ('outils/listemanager/decoupage',                   'ListeController@decoupage');

        Route::get          ('outils/listemanager/statliste',                   'ListeController@indexstatliste');
        Route::post         ('outils/listemanager/statliste',                   'ListeController@statliste');

        Route::get          ('outils/listemanager/modifier',                    'ListeController@indexmodifierliste');
        Route::post         ('outils/listemanager/modifier',                    'ListeController@modifierliste');

        Route::get          ('outils/listemanager/nddfr',                       'ListeController@nddfrindex');
        Route::post         ('outils/listemanager/nddfr',                       'ListeController@nddfr');

        Route::get          ('tool/filterblacklist',                            'ListeController@filterblacklist');
        Route::get          ('tool/filterblacklist/{file}',                     'ListeController@filterblacklist_go');

        Route::get          ('tool/filterbdd',                                  'ListeController@filterbdd');
        Route::get          ('tool/filterbdd/{base_id}/{file}',                 'ListeController@filterbdd_go');

        Route::get          ('base/extract/{id}/extractmd5',                    'BasesController@extract_md5');
        Route::get          ('base/{id}/extractcontacts',                       'BasesController@extract_contacts');

    });

    Route::get          ('/faq',                                            'HomeController@faq');

    Route::get          ('/planning/set_defaut_planning',                   'PlanningController@set_defaut');
//    Route::get          ('/planning/delete_defaut_planning/{id}',           'PlanningController@set_defaut');
    Route::get          ('/planning/liste_defaut',                          'PlanningController@list_planning_defaut');
    Route::post         ('/planning/set_defaut_planning',                   'PlanningController@set_defaut_store');

});
// FIN PARTIE

// Route nEcessitant d'etre publiques
//Affichage du HTML des kits
Route::get('html', function() {
    $routeur = \App\Models\Routeur::find(1);
    $campagne = \App\Models\Campagne::find(3);
    echo $campagne->generateHtml($routeur);

});

//Affichage des kits en fonction des routeurs / type
Route::get              ('mailexport/{routeur}/{fichier}',                  'CampagneController@emailexport');
// route pour recuperer le contenu de la campagne
Route::get              ('campagne/{id}/routeur_{routeur_id}',              'CampagneController@showRouteurHtml');
Route::get              ('campagne/{id}/routeura_{routeur_id}',             'CampagneController@showRouteurHtmlAnonyme');


//Partie pixel de tracking
Route::get              ('/pixel/{email}/{cmp}',                            'OuvertureController@store');

//Partie wizweb
if(getenv('WIZWEB_API') == true) {
    Route::post         ('ws_wizweb',                                       'WizwebApiController@index');
}

// Route pour se passer les desabo entre tor
Route::get              ('mailexport/unsubscribe/maildrop/{fichier}',       'ListeController@url_desabo');
Route::get              ('mailexport/unsubscribe/phoenix/{fichier}',        'ListeController@url_desabo');
Route::get              ('mailexport/unsubscribe/smessage/{fichier}',       'ListeController@url_desabo');

Route::get              ('m4y/envoi',                                       'StatsController@etat_envoi_mailforyou');

///////////////////////////////
/* Routes suivantes : TESTS */

/////////////////////////////

Route::get              ('test',                                            'TestController@testapi');
//Route::get('ws_wizweb','WizwebApiController@index'); //tester facilement si l'api pr wizweb fonctionne
