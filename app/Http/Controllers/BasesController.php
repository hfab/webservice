<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Jobs\BaseImportDestinataires;
use App\Jobs\BaseImportDesinscrits;
use App\Jobs\BaseImportBlacklistes;
use App\Jobs\BaseImportBounces;
use App\Jobs\BaseCompareDestinataires;
use App\Jobs\BaseImportMindbaz;
use App\Models\Base;
use App\Models\Campagne;
use App\Models\Destinataire;
use App\Models\Fai;
use App\Models\Planning;
use Illuminate\Http\Request;
use Illuminate\Pagination\PaginationServiceProvider;
// fab1 pour le crawldb
use App\Http\Controllers\DB;
// use DB;
use App\Quotation;
use Carbon\Carbon;

use App\Jobs\BaseImportDestinatairesPerso;

class BasesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        $bases = Base::with('ViewCountDestinataires')->get();

        return view('base.index_new')
            ->withMenu('bases')
            ->withBases($bases);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('base.create_new')
            ->withMenu('bases')
            ->withTrigram('');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $message = 'La variable [unsubscribelink] du lien de désinscription est manquante.';

        if((strlen(\Input::get('header_html')) > 0 or strlen(\Input::get('header_html')) > 0) and (stripos(\Input::get('footer_html'),'[unsubscribelink]') === false)) {
            return view('base.create_new')
                ->withMenu('bases')
                ->withError($message);
        }

        $base = new Base();
        $base->nom = \Input::get('nom');
        $base->code = \Input::get('code');
        $base->mentions_legales = \Input::get('mentions_legales');
        $base->mentions_header = \Input::get('mentions_header');

        $base->is_active = 1;
        //info si l'on doit rajouter les tags partenaires
        if(\Input::get('is_active') != 'on') {
            $base->is_active = 0;
        }

        $message = "La base a été mise à jour avec succès.";

        $base->header_html = \Input::get('header_html');
        $base->footer_html = \Input::get('footer_html');

        $base->nom_reply = \Input::get('nom_reply');
        $base->email_reply = \Input::get('email_reply');

        $base->save();

        $trigram =  \Input::get('trigram');
        \DB::table('base_wizweb')->where('base_id', $base->id)->delete();
        if(!empty($trigram)) {
            \DB::table('base_wizweb')->insert(['base_id' => $base->id, 'trigram' => $trigram]);
        }

        return \redirect('base')->withMessage($message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $base = Base::find($id);

        //Facultatif : Trigram pour wizweb
        $select_trigram = \DB::table('base_wizweb')
            ->select('trigram')
            ->where('base_id', $base->id)
            ->first();

        $trigram = "";
        if(!empty($select_trigram)){
            $trigram = $select_trigram->trigram;
        }

        return view('base.edit_new')
            ->withMenu('bases')
            ->withBase($base)
            ->withTrigram($trigram);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $message = "La base a été mise à jour avec succès.";

        $base = Base::find($id);
        $base->nom = \Input::get('nom');
        $base->code = \Input::get('code');
        $base->mentions_legales = \Input::get('mentions_legales');
        $base->mentions_header = \Input::get('mentions_header');

        $base->is_active = 1;
        //info si l'on doit rajouter les tags partenaires
        if(\Input::get('is_active') != 'on') {
            $base->is_active = 0;
        }

        if(stripos(\Input::get('header_html'),'[unsubscribelink]') === false and stripos(\Input::get('footer_html'),'[unsubscribelink]') === false) {
            $message = 'La variable [unsubscribelink] du lien de désinscription est manquante.';
        }

        $base->nom_reply = \Input::get('nom_reply');
		$base->email_reply = \Input::get('email_reply');

        $base->header_html = \Input::get('header_html');
        $base->footer_html = \Input::get('footer_html');
        $base->save();

        $trigram =  \Input::get('trigram');
        \DB::table('base_wizweb')->where('base_id', $base->id)->delete();
        if(!empty($trigram)) {
            \DB::table('base_wizweb')->insert(['base_id' => $base->id, 'trigram' => $trigram]);
        }

        return redirect('base/'.$base->id.'/edit')
            ->withMenu('bases')
            ->withBase($base)
            ->withTrigram($trigram)
            ->withError($message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        \DB::statement('DELETE FROM tokens where base_id ='.$id);

        $campagnes_ids = array_pluck(\DB::table('campagnes')->where('base_id',$id)->get(), 'id');

        \DB::table('campagnes_routeurs')->whereIn('campagne_id', $campagnes_ids)->delete();
        \DB::table('tokens_stats')->where('base_id', $id)->delete();
        \DB::table('ouvertures_stats_bases')->where('base_id', $id)->delete();

        \DB::table('campagnes')->where('base_id',$id)->delete();
        \DB::table('destinataires')->where('base_id',$id)->delete();
        \DB::table('plannings')->whereIn('campagne_id', $campagnes_ids)->delete();
        Base::find($id)->delete();

        return \redirect('base');
    }

    public function add()
    {
        return view('base.add_new')
            ->withMenu('bases');
    }

    public function add_destinataires($id)
    {
        $base = Base::find($id);

        return view('base.add_destinataires_new')
            ->withMenu('bases')
            ->withBase($base);
    }

    public function inject_file($base_id)
    {
        $name = \Input::file('file')->getClientOriginalName();
        \Input::file('file')->move(storage_path() . '/listes/', $name);

        $this->dispatch (new BaseImportDestinataires($base_id, \Input::file('file')->getClientOriginalName()));

        return "OK";

    }

    public function inject_file_type()
    {
        $name = \Input::file('file')->getClientOriginalName();
        $type = \Input::get('type');
        \Input::file('file')->move(storage_path() . "/$type/", $name);

        if($type == 'bounces') {
            $this->dispatch(new BaseImportBounces(\Input::file('file')->getClientOriginalName()));
        }
        elseif($type == 'blacklistes'){
            $this->dispatch(new BaseImportBlacklistes(\Input::file('file')->getClientOriginalName()));
        }
        elseif ($type == 'desinscrits'){
            $this->dispatch(new BaseImportDesinscrits(\Input::file('file')->getClientOriginalName()));
        }
        else{
            return false;
        }
        return "OK";
    }

    function stats($id)
    {
        $base = Base::find($id);

        $fais = Destinataire::where('statut', 0)
            ->where('base_id', $base->id)
            ->selectRaw('fai_id, count(*) as num')
            ->groupBy('fai_id')
            ->orderByRaw('num desc')
            ->get();

        return view('base.stats')
            ->withPageTitle('Statistiques sur la base ' . $base->nom)
            ->withMenu('bases')
            ->withFais($fais)
            ->withBase($base);
    }

    function choixBaseExtract()
    {
        $bases = Base::with('ViewCountDestinataires')->get();
        return view('base.extract')
            ->withMenu('bases')
            ->withBases($bases);
    }

    function extract_md5($id)
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $today =  Carbon::today()->format('Ymd');
        $filename = storage_path() . "/listes/md5-base-$id-$today.csv";
        $ressource_fichier = fopen($filename, 'w+');
        \DB::table('destinataires')
                        ->select('hash')
                        ->where('base_id', $id)
                        ->where('statut', '<', 3)
                        ->chunk(50000, function ($emailbase) use ($ressource_fichier) {
                            foreach ($emailbase as $e) {
                                fputs($ressource_fichier, $e->hash . "\n");
                            }
                        });

        return response()->download($filename);
    }

    function compare_upload($base_id)
    {
        $base = Base::find($base_id);

        return view('base.compare_upload')
            ->with('base',$base);
    }

    function compare_mails($base_id, $type='md5')
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        \DB::disableQueryLog();

        $name = \Input::file('file')->getClientOriginalName();
        $base_id = \Input::get('base_id');
        $type = \Input::get('type');

        \Input::file('file')->move(storage_path() . "/compare_$type/", $name);

        $this->dispatch (new BaseCompareDestinataires($base_id, $type, \Input::file('file')->getClientOriginalName()));
    }

    function extract_contacts($id)
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        \DB::disableQueryLog();

        $today =  Carbon::today()->format('Ymd');
        $filename = storage_path() . "/listes/base-$id-$today.csv";
        $ressource_fichier = fopen($filename, 'w+');
        \DB::table('destinataires')
            ->select('mail')
            ->where('base_id', $id)
            ->where('statut', '<', 3)
            ->chunk(50000, function ($emailbase) use ($ressource_fichier) {
                foreach ($emailbase as $e) {
                    fputs($ressource_fichier, $e->mail . "\n");
                }
            });

        return response()->download($filename);
    }

    function add_blacklist(){
    }

    function check_desabo(){
      $tabcheck = array();
      $check = array();
      $etat_fr = 'Actif';

      $checkemail = \Input::get('email');
      $resultatcheck = \DB::table('destinataires')->where('mail', $checkemail)->get();

      foreach ($resultatcheck as $v) {
        $labase = \DB::table('bases')->where('id', $v->base_id)->first();

        // var_dump($labase);

         if($labase == null){

          $message = "La base de ce destinataire n'existe plus";
          return view('base.checkemailstatut')->with('checkemail',$checkemail)->with('tabcheck',$tabcheck)->with('message',$message);

         }

        if($v->statut > 2){
          $etat_fr = 'Non actif';
          $datedesabo = $v->optout_at;
          $from_file = $v->optout_file;

        } else {
          $etat_fr = 'Actif';
          $datedesabo = null;
          $from_file = null;

        }

        $check = [
          'base_id' => $labase->id,
          'base_nom' => $labase->nom,
          'email' => $v->mail,
          'statut' => $v->statut,
          'etat_fr' => $etat_fr,
          'date_desabo' => $v->optout_at,
          'from' => $v->optout_file

        ];

        $tabcheck[] = $check;
        $check = array();
      }

      $message = null;

      return view('base.checkemailstatut')->with('checkemail',$checkemail)->with('tabcheck',$tabcheck)->with('message',$message);

    }

    function searchall(){
      $fields = array_except(\Input::all(), '_token');
      $recherche = '';
      // var_dump($fields['searchall']);
      if(isset($fields['searchall'])){
          $recherche = $fields['searchall'];
      }

      if(isset($fields['recherche'])){
        $recherche = $fields['recherche'];
      }
      if(isset($fields['export'])){
          $today =  Carbon::today()->format('YmdHis');
          $filename = storage_path() . "/listes/export-results-$today.csv";
          $ressource_fichier = fopen($filename, 'w+');
          \DB::table('destinataires')
              ->select('mail')
              ->where('mail', 'LIKE', '%'.$recherche.'%')
              ->chunk(50000, function ($emailbase) use ($ressource_fichier) {
                  foreach ($emailbase as $e) {
                      fputs($ressource_fichier, $e->mail . "\n");
                  }
              });

          return response()->download($filename);
      }

      $desti = Destinataire::where('mail','LIKE','%'.$recherche.'%')->paginate(100);
      return view('base.show_desti_all_new', ['recherche' => $recherche])
      //->with('base',$base)
      //->with('lesbases',$lesbases)
      ->with('desti',$desti)
      ->with('recherche',$recherche);

    }

    function exportresults(){
        $fields = array_except(\Input::all(), '_token');
        $exportall = \Input::get('exportall');
        // var_dump($fields['searchall']);
        if(isset($fields['searchall'])){
            $recherche = $fields['searchall'];
        }

        if(isset($fields['recherche'])){
            $recherche = $fields['recherche'];
        }

        $desti = Destinataire::where('mail','LIKE','%'.$recherche.'%')->paginate(100);
        return view('base.show_desti_all', ['recherche' => $recherche])
            //->with('base',$base)
            //->with('lesbases',$lesbases)
            ->with('desti',$desti)
            ->with('recherche',$recherche);
    }

    function show_base_destinataires($id){

      $recherche = '';
      if(!empty(\Input::get('recherche')) ){
          // recup de la ref et suppression de l'espace ajouté pour affichage
          $recherche = \Input::get('recherche');
          $query->where('mail', 'LIKE', '%' . $recherche . '%');
      }

      $fai = \DB::table('fais')->get();
      $desti = Destinataire::where('base_id',$id)->paginate(100);
      $base = \DB::table('bases')->where('id',$id)->first();

      $lesbases = \DB::table('bases')->get();

      return view('base.show_desti', ['desti' => $desti])
      ->with('base',$base)
      ->with('lesbases',$lesbases)
      ->with('fai',$fai)
      ->with('recherche',$recherche);

    }

    function edit_destinataire($id,$destinataire_id){

      $base = \DB::table('bases')->where('id',$id)->first();
      $desti = \DB::table('destinataires')->where('id',$destinataire_id)->first();
      return view('base.edit_desti')->with('desti',$desti)->with('base',$base);

    }

    function store_destinataire(){

      $fields = array_except(\Input::all(), '_token');

      \DB::table('destinataires')
      ->where('id', $fields['desti_id'])
      ->update([
        'mail' => $fields['mail'],
        'nom' => $fields['nom'],
        'prenom' => $fields['prenom'],
        'datenaissance' => $fields['datenaissance'],
        'ville' => $fields['ville'],
        'departement' => $fields['departement'],
        'civilite' => $fields['sexe'],
      ]);

      return \redirect('base/'.$fields['base_id'].'/destinataire');

    }

    function show_base_destinataires_crit($id,$typecrit,$valuecrit){

      $recherche = '';
      $lesbases = \DB::table('bases')->get();
      $fai = \DB::table('fais')->get();
      $base = \DB::table('bases')->where('id',$id)->first();

      if($typecrit == 'fai'){
        $desti = Destinataire::where('base_id',$id)->where('fai_id',$valuecrit)->paginate(100);
        return view('base.show_desti', ['desti' => $desti])
        ->with('base',$base)
        ->with('lesbases',$lesbases)
        ->with('fai',$fai)
        ->with('recherche',$recherche);
      }

      if($typecrit == 'abo'){

        if($valuecrit == 0){
          // echo 'les desinscrits';
          $desti = Destinataire::where('base_id',$id)->where('statut','>','0')->paginate(100);
          return view('base.show_desti', ['desti' => $desti])
          ->with('base',$base)
          ->with('lesbases',$lesbases)
          ->with('fai',$fai)
          ->with('recherche',$recherche);
        }

        if($valuecrit == 1){
          // echo 'les abonnés';
          $desti = Destinataire::where('base_id',$id)->where('statut','0')->paginate(100);
          return view('base.show_desti', ['desti' => $desti])
          ->with('base',$base)
          ->with('lesbases',$lesbases)
          ->with('fai',$fai)
          ->with('recherche',$recherche);
        }


      }

    }

    function show_search_destinataires_results($id){

      $recherche = '';
      $fields = array_except(\Input::all(), '_token');
      if(isset($fields['recherche'])){
        $recherche = $fields['recherche'];

      }

      $fai = \DB::table('fais')->get();

      $lesbases = \DB::table('bases')->get();
      $base = \DB::table('bases')->where('id',$id)->first();
      // renvoyer les resultats dans la blade sur la bonne base
      $desti = Destinataire::where('base_id',$id)->where('mail','LIKE','%'.$fields['recherche'].'%')->paginate(100);
      return view('base.show_desti', ['recherche' => $recherche])
      ->with('base',$base)
      ->with('lesbases',$lesbases)
      ->with('fai',$fai)
      ->with('recherche',$recherche)
      ->with('desti',$desti);

    }

    public function get_search_destinataires_ajax() {

        $base_id = \Input::get('base_id');
        $recherche = \Input::get('recherche');

        $source = \DB::table('destinataires')
            ->select('mail')
            ->where('base_id', $base_id)
            ->where('mail', 'like', "%$recherche%")
            ->orderBy('mail')
            ->take(20)
            ->lists('mail');

        return json_encode($source);
    }

    public function add_destinataires_perso($id)
    {
        $base = Base::find($id);

        return view('base.choix_champs_perso_new')
            ->withMenu('bases')
            ->withBase($base);
    }


    function import_desti_champs_perso_prepare($id){

      $base = Base::find($id);
      $champsperso = array_except(\Input::all(), '_token');

      $content = array();
      $listechamp = $champsperso['result'];
      // fichier not null
      $name = \Input::file('fichier')->getClientOriginalName();
			\Input::file('fichier')->move(storage_path() . '/listes/perso/', $name);

      $handle = fopen(storage_path('listes/perso/'. $name), 'r');

      // param nombre de lignes
      $countapercu = 3;
      while($ligne = fgets($handle))
        {
          if($ligne == '' || $ligne == "\n"){
              // on fait rien
          } else {

            if($countapercu == 0){

              break;
            } else {
              // echo $ligne;
              $content[]= $ligne;

            }

            $countapercu = $countapercu - 1;

          }
        }

        return view('base.apercu_champ_perso_new')
            ->withMenu('bases')
            ->withBase($base)
            ->with('content',$content)
            ->with('name', $name)
            ->with('listechamp',$listechamp);

    }

    function import_desti_champs_perso_run($id){

      $form = array_except(\Input::all(), '_token');
      $this->dispatch(new BaseImportDestinatairesPerso($id,$form['lefichier'], $form['champs']));
      return \redirect("base/".$id."/add_destinataires_perso");

    }

    public function desabo_fct_domainName($recherche){
     \DB::table('destinataires')->where('mail','LIKE','%'.$recherche.'%')->update(['statut' => 3]);
    return redirect('base/searchmailall');
    }
 
}
