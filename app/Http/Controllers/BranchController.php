<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Branch;

use Illuminate\Http\Request;

class BranchController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $branches = Branch::all();
        return view('admin.branch.index_new')
            ->withMenu('admin')
            ->withBranches($branches);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return view('admin.branch.create')
            ->withMenu('admin');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $branch = new Branch();
        $branch->name = \Input::get('name');
        $branch->save();

        return \redirect('/admin/branch')->withMessage('SAVE OK');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $branch = Branch::find($id);
        $branches = Branch::all();

        return view('admin.branch.edit')
            ->withMenu('admin')
            ->withBranch($branch)
            ->withBranches($branches);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $branch = Branch::find($id);
        $branch->name = \Input::get('name');
        $branch->save();

        return \redirect('/admin/branch')
            ->withMessage('UPDATE OK');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$branch = Branch::find($id);
        $branch->delete();

        return \redirect('/admin/branch/');
	}

    public function enable($id){

        $branch = Branch::find($id);
        $branch->is_active = 1;
        $branch->save();

        return \redirect('/admin/branch');
    }

    public function disable($id){

        $branch = Branch::find($id);
        $branch->is_active = 0;
        $branch->save();

        return \redirect('/admin/branch');
    }

}
