<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Classes\Routeurs\RouteurSmessage;
use App\Models\TokenStat;
use App\Models\Routeur;
use App\Models\Planning;

use Carbon\Carbon;

class StatsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $active_routeurs = \DB::table('routeurs')
            ->where('is_active', 1)
            ->get();

        return view('stats.index')
            ->with('active_routeurs', $active_routeurs);
    }

    public function mindbaz()
    {
        $lastmaj = \DB::table('stats_mindbaz')->max('bloc_maj');

        $statsrouteurv2 = \DB::table('stats_mindbaz_total')
            ->join('campagnes', 'stats_mindbaz_total.reference', '=', 'campagnes.ref')
            ->where('bloc_maj',$lastmaj)
            ->orderBy('created_at','desc')
            ->get();

        return view('stats.mindbaz')
            ->with('statsrouteurv2',$statsrouteurv2);
    }

    public function mailforyou()
    {
      $timemaj = \DB::table('stats_m4y')->max('updated_at');
      $lastmaj = \DB::table('stats_m4y')->max('bloc_maj');

      $ds = \DB::table('stats_m4y')
          ->where('bloc_maj',$lastmaj)
          ->orderBy('id_m4y','desc')
          ->get();

      return view('stats.m4y')
          ->with('ds',$ds)
          ->with('timemaj',$timemaj);
    }

    public function etat_envoi_mailforyou()
    {
      $timemaj = \DB::table('etat_envoi_m4y')->max('updated_at');
      $lastmaj = \DB::table('etat_envoi_m4y')->max('bloc_maj');
      $ds = \DB::table('etat_envoi_m4y')
          ->where('bloc_maj',$lastmaj)
          ->orderBy('id','desc')
          ->get();

      return view('stats.etatenvoim4y')
          ->with('ds',$ds)
          ->with('timemaj',$timemaj);
    }

    public function smessage()
    {
      $routeur = new RouteurSmessage();
      $lerouteurid = \DB::table('routeurs')
          ->where('nom','Smessage')
          ->first();
      $smessage = \DB::table('senders')
          ->where('routeur_id', $lerouteurid->id)
          ->get();

      $mintimestamp = time() - 3600*24*10;

      // je selectionne la maj max
      $lastmaj = \DB::table('stats_smessage')->max('bloc_maj');
      $statsrouteur = \DB::table('stats_smessage_total')
          ->where('bloc_maj',$lastmaj)
          ->where('date_creation','>',$mintimestamp)
          ->where('total_mail','>',1)
          ->orderBy('date_creation','desc')
          ->get();

      return view('stats.smessage')
          ->with('smessage',$smessage)
          ->with('routeur',$routeur)
          ->with('statsrouteur',$statsrouteur);

    }

    public function maildrop()
    {
      $lerouteurid = \DB::table('routeurs')
          ->where('nom','Maildrop')
          ->first();
      $comptemaildrop = \DB::table('senders')
          ->where('routeur_id', $lerouteurid->id)
          ->get();

      $lastmaj = \DB::table('stats_maildrop')->max('bloc_maj');

      $statsrouteurv2 = \DB::table('stats_maildrop_total')
      ->join('campagnes', 'stats_maildrop_total.reference', '=', 'campagnes.ref')
      ->where('bloc_maj',$lastmaj)
      ->orderBy('created_at','desc')
      ->get();

      return view('stats.maildrop')
      ->with('comptemaildrop',$comptemaildrop)
      ->with('statsrouteurv2',$statsrouteurv2);

    }

    public function phoenix()
    {
      // charger la table
      $lastmaj = \DB::table('stats_phoenix_total')->max('bloc_maj');
      $phoenixstats = \DB::table('stats_phoenix_total')->where('bloc_maj',$lastmaj)
          ->join('campagnes', 'stats_phoenix_total.reference', '=', 'campagnes.ref')
          ->orderBy('created_at','desc')
          ->get();

      return view('stats.phoenix')->with('phoenixstats',$phoenixstats);
    }

    public function stats_detail_phoenix($ref)
    {
        $statscampagne = \DB::table('stats_phoenix_total')
            ->where('reference', $ref)
            ->orderBy('date_maj','asc')
            ->get();

        $arrayouv = array();
        $arrayclic = array();
        $arraydesinscriptions = array();
        $arraylabel = array();
        $arrayerror = array();

        foreach ($statscampagne as $k => $v)
        {
            $date = new \DateTime();
            $date->setTimestamp($v->date_maj);
            $arraylabel[] = addslashes($date->format('Y-m-d H:i:s'));
            $arrayouv[]=$v->ouvreurs;
            $arrayclic[]=$v->cliqueurs;
            $arraydesinscriptions[]=$v->desabo;
            $arrayerror[]=$v->erreurs;

            return view('stats.phoenixdetail')
            ->with('ref',$ref)->with('statscampagne',$statscampagne)
            ->with('arrayouv',$arrayouv)
            ->with('arraylabel',$arraylabel)
            ->with('arrayclic',$arrayclic)
            ->with('arraydesinscriptions',$arraydesinscriptions)
            ->with('arrayerror',$arrayerror);
        }
    }

    function stats_detail_md($ref)
    {

      $statscampagne = \DB::table('stats_maildrop_total')
          ->where('reference', $ref)
          ->orderBy('date_maj','asc')
          ->get();

      $arrayouv = array();
      $arrayclic = array();
      $arraylabel = array();

      foreach ($statscampagne as $counter)
      {
          $date = new \DateTime();
          $date->setTimestamp($counter->date_maj);
          $arraylabel[] = addslashes($date->format('Y-m-d H:i:s'));
          $arrayouv[]=$counter->ouvreurs;
          $arrayclic[]=$counter->cliqueurs;
          $arraydesinscriptions[]=$counter->desinscriptions;

      }

      return view('stats.maildropdetail')
      ->with('ref',$ref)->with('statscampagne',$statscampagne)
      ->with('arrayouv',$arrayouv)
      ->with('arraylabel',$arraylabel)
      ->with('arrayclic',$arrayclic)
      ->with('arraydesinscriptions',$arraydesinscriptions);

    }

    function stats_detail_smessage($ref)
    {
        $statscampagne = \DB::table('stats_smessage_total')
          ->where('reference', $ref)
          ->orderBy('date_maj','asc')
          ->get();

        $arrayouv = array();
        $arrayclic = array();
        $arraydesabo = array();
        $arraylabel = array();

        foreach ($statscampagne as $counter)
        {
          $date = new \DateTime();
          $date->setTimestamp($counter->date_maj);
          $arraylabel[] = addslashes($date->format('Y-m-d H:i:s'));
          $arrayouv[]=$counter->ouvreurs;
          $arrayclic[]=$counter->cliqueurs;
          $arrayinactif[]=$counter->inactifs;
          $arraydesabo[]=$counter->desinscrits;

        }

      return view('stats.smessagedetail')
      ->with('ref',$ref)->with('statscampagne',$statscampagne)
      ->with('arrayouv',$arrayouv)
      ->with('arraylabel',$arraylabel)
      ->with('arrayclic',$arrayclic)
      ->with('arrayinactif',$arrayinactif)
      ->with('arraydesabo',$arraydesabo);

    }

    function stats_detail_mindbaz($ref)
    {
        $statscampagne = \DB::table('stats_mindbaz_total')
            ->where('reference', $ref)
            ->orderBy('date_maj','asc')
            ->get();

        $arrayouv = array();
        $arrayclic = array();
        $arraydesinscriptions = array();
        $arraylabel = array();

        foreach ($statscampagne as $counter)
        {
            $date = new \DateTime();
            $date->setTimestamp($counter->date_maj);
            $arraylabel[] = addslashes($date->format('Y-m-d H:i:s'));
            $arrayouv[]=$counter->openers;
            $arrayclic[]=$counter->clickers;
            $arraydesinscriptions[]=$counter->unsubscribers;

        }

        return view('stats.mindbazdetail')
            ->with('ref',$ref)->with('statscampagne',$statscampagne)
            ->with('arrayouv',$arrayouv)
            ->with('arraylabel',$arraylabel)
            ->with('arrayclic',$arrayclic)
            ->with('arraydesinscriptions',$arraydesinscriptions);

    }


    public function edatis(){

      $timemaj = \DB::table('stats_edatis')->max('updated_at');
      $lastmaj = \DB::table('stats_edatis')->max('bloc_maj');

      $ds = \DB::table('stats_edatis')
          ->where('bloc_maj',$lastmaj)
          ->orderBy('id','desc')
          ->get();

      return view('stats.edatis')
          ->with('ds',$ds)
          ->with('timemaj',$timemaj);

    }

    public function stats()
    {
        $tokens = [];
		$today = date('Y-m-d 00:00:00');
        $senders = \App\Models\Sender::all();
        $bases = \App\Models\Base::all();

        $tokenStats = TokenStat::all();
        foreach($tokenStats as $token_stat) {
            $tokens[$token_stat->base_id] = $token_stat->count;
        }

        // $tokens_today_total = \DB::table('tokens')->where('date_active', date('Y-m-d'))->count();
        // $tokens_today_used = \DB::table('tokens')->where('date_active', date('Y-m-d'))->whereRaw('campagne_id is not null')->count();
        // $tokens_today_left = \DB::table('tokens')->where('date_active', date('Y-m-d'))->whereRaw('campagne_id is null')->count();

        $tokensTot = \DB::table('tokens_stats_general')
        			->select(\DB::raw('COALESCE(volume , 0) as total'))
					->first(); // pour le volume tokens global

		$tokensTotal = 0;

		if(!empty($tokensTot->total)){
			$tokensTotal = $tokensTot->total;
		}

        $tokensUsed = \DB::table('tokens_stats')
        				->select(\DB::raw('COALESCE(sum(count_used),0) as total_used'))
						->where('created_at', '>', $today)
						->first();  // pour le volume tokens utilise

        // $ouv_24 = \App\Models\Ouverture::where('created_at','>',date('Y-m-d H:i:s',time()-86400))->count();
        // $clic_24 = \App\Models\Clic::where('created_at','>',date('Y-m-d H:i:s',time()-86400))->count();
         $optout_24 = \App\Models\Destinataire::where('statut', DESTINATAIRE_OPTOUT)->count();

        $optout_24 = \DB::table('optout_stats')
        ->select(\DB::raw('SUM(count) as total_optout'))
        ->first();

        $ouv_24 = \DB::table('ouvertures_stats')
        ->where('id', \DB::raw("(select max(`id`) from ouvertures_stats)"))
        ->where('updated_at','LIKE', date('Y-m-d') . '%')
        ->first();

        // $bounce_24 = \App\Models\Bounce::where('created_at','>',date('Y-m-d H:i:s',time()-86400))->count();

        $statstokens = \DB::table('tokens_stats')
                ->select('base_id', \DB::raw('count as volumetotal'), \DB::raw('SUM(count_used) as volumeused'))
                ->where('updated_at','LIKE', date('Y-m-d') . '%')
                ->groupBy('base_id')
                ->havingRaw('SUM(count) > 1')
                ->get();

        $planningsrouteurs_24 = Planning::select('routeur_id', \DB::raw('COALESCE(SUM(volume_selected), 0) as total'))
            ->where('updated_at','LIKE', date('Y-m-d') . '%')
            ->groupBy('routeur_id')
            ->get();

        return view('stats')
            ->with('menu', 'stats')
            ->with('tokens', $tokens)
            ->with('tokensUsed', $tokensUsed)
            ->with('tokensTotal', $tokensTotal)
            ->with('ouv_24', $ouv_24)
            // ->with('clic_24', $clic_24)
            ->with('optout_24', $optout_24)
            // ->with('bounce_24', $bounce_24)
            ->with('statstokens', $statstokens)
            ->with('planningsrouteurs_24', $planningsrouteurs_24)
            ->withSenders($senders);
    }

    public function statsfai($id)
    {

      $statsfais = \DB::table('fais_stats')
      ->select('fai_id',\DB::raw('volume as volume'), \DB::raw('SUM(volume_used) as volume_used'))
      ->where('base_id',$id)
      ->where('updated_at','LIKE', date('Y-m-d') . '%')
      ->groupBy('fai_id')
      ->get();

      return view('stats.fai')->with('statsfais',$statsfais);
    }

    public function statsoptout(){

      $optout = \DB::table('optout_stats')->get();

      return view('stats.optout')->with('optout',$optout);
    }

    public function statsouvertures()
    {

      $dateDuJour = date('Y-m-d H:i:s');
      $dateLimite = date('Y-m-d H:i:s', strtotime('-30 days'));

       $ouvertures = \DB::table('ouvertures_stats')
           ->select('id', 'updated_at', \DB::raw('max(count) as max_count'))
           ->where('updated_at','>',$dateLimite)
           ->groupBy('updated_at')
           ->get();

      $ouverturesbases = \DB::table('ouvertures_stats_bases')->get();

      return view('stats.ouvertures')->with('ouvertures',$ouvertures)->with('ouverturesbases',$ouverturesbases);

    }

    public function statsrouteurs()
    {
        $routeurs = Routeur::where('is_active', 1)->get();

        $routeur = \Input::get('routeur');
        $from = \Input::get('from');
        $to = \Input::get('to');

        $planningsQuery = Planning::select('routeur_id',\DB::raw('COALESCE(SUM(volume_selected), 0) as total'));

//        var_dump($routeur);

        if(!empty($routeur)) {
            $planningsQuery->where('routeur_id', $routeur);

        }
        if(!empty($from)){
            $planningsQuery->where('created_at', '>=', $from);
        }
        if(!empty($to)){
            $planningsQuery->where('created_at', '<=', $to);
        } else{
            $to = date('Y-m-d') ;
        }
        $plannings = $planningsQuery->groupBy('routeur_id')->get();

        return view('stats.routeurs')
            ->with([
                'from' => $from,
                'to' => $to,
                'plannings' => $plannings,
                'routeurs' => $routeurs,
                'routeur' => $routeur
            ]);
    }
}
