<?php namespace App\Http\Controllers;

use App\Http\Requests;
use Carbon\Carbon;
use App\Http\Controllers\Controller;

use App\Models\Base;
use App\Models\Routeur;

use Illuminate\Http\Request;

class AdminController extends Controller {

    public function __construct()
    {
        $allbases = Base::select('id', 'nom')->where('is_active', 1)->get();
        $bases = array();
        foreach($allbases as $b){
            $bases[$b->id]=$b->nom;
        }

        $this->bases = $bases;

        $allrouteurs = Routeur::select('id', 'nom')->where('is_active', 1)->get();
        $routeurs = array();
        foreach($allrouteurs as $r){
            $routeurs[$r->id]=$r->nom;
        }

        $this->routeurs = $routeurs;
    }

    public function index()
    {
        $autostatus = \DB::table('settings')->where('parameter', 'is_auto')->first();
        $cleanrelaunchstatus = \DB::table('settings')->where('parameter', 'clean_relaunch')->first();

        return view('admin.index_new')
            ->withTitle('Admin - TOR')
            ->with('autostatus', $autostatus)
            ->with('cleanrelaunchstatus', $cleanrelaunchstatus);
    }

    public function envoiEnable()
    {
        \DB::table('settings')
            ->where('parameter', 'is_auto')
            ->update(['value' => '1']);
        return \redirect('/admin');
    }

    public function envoiDisable()
    {
        \DB::table('settings')
            ->where('parameter', 'is_auto')
            ->update(['value' => '0']);
        return \redirect('/admin');
    }

    public function cleanRelaunchEnable()
    {
        \DB::table('settings')
            ->where('parameter', 'clean_relaunch')
            ->update(['value' => '1']);
        return \redirect('/admin');
    }

    public function cleanRelaunchDisable()
    {
        \DB::table('settings')
            ->where('parameter', 'clean_relaunch')
            ->update(['value' => '0']);
        return \redirect('/admin');
    }

    public function dns_record_index()
    {

        return view('admin.dns.index')
            ->withTitle('Admin - TOR');

    }

    public function dns_record_result()
    {
        $fields = array_except(\Input::all(), '_token');
        var_dump($fields);

        /*
  $TTL 60
  @	IN SOA dns16.ovh.net. tech.ovh.net. (2017010603 86400 3600 3600000 300)
                            IN NS     ns16.ovh.net.
                            IN NS     dns16.ovh.net.
                            IN MX 1   redirect.ovh.net.
                            IN A      213.186.33.5
                            IN TXT    "v=spf1 ip4:212.18.245.0/24 a mx  ~all"
  abuse.at              600 IN TXT    "yveschaponic@gmail.com"
  autoconfig                IN CNAME  mailconfig.ovh.net.
  autodiscover              IN CNAME  mailconfig.ovh.net.
  bounces.at            600 IN TXT    "bounces_mfu@sc2consulting.fr"
  ftp                       IN CNAME  lespromosanepasrater.com.
  imap                      IN CNAME  ssl0.ovh.net.
  mail                      IN CNAME  ssl0.ovh.net.
  news.at               600 IN TXT    "reply_news@sc2consulting.fr"
  pop3                      IN CNAME  ssl0.ovh.net.
  reply.at              600 IN TXT    "bounces_mfu@sc2consulting.fr"
  s2cdkim._domainkey        IN TXT    ( "k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDr4X7d6uC6+SN6JYkroIvtSAC0cKZk77j6ZwEpr7ShRxz2x62hGw6hg/nrazbBIwKCo0gxz4aPIW3+gPzG9jlNr/K5v7VstfVQljimQPbIGT2SEQbEqmajkPRtwsFqYDtBX+kG0GhqP8ttJxgMHx8KsZn6sWPqUFe+fbWVYkH7DwIDAQAB" )
  smtp                      IN CNAME  ssl0.ovh.net.
  www                       IN MX 1   redirect.ovh.net.
  www                       IN A      213.186.33.5

  // foreach here
  info13                    IN A      212.18.245.13
  info14                    IN A      212.18.245.14
  info15                    IN A      212.18.245.15
  info16                    IN A      212.18.245.16
  info17                    IN A      212.18.245.17
  info18                    IN A      212.18.245.18
  info19                    IN A      212.18.245.19
  info20                    IN A      212.18.245.20
  info21                    IN A      212.18.245.21
  info22                    IN A      212.18.245.22
  info23                    IN A      212.18.245.23
  info24                    IN A      212.18.245.24

        */

        /*


        */

        $dns = '$TTL 60
      @	IN SOA dns'.$fields['dnsovh'].'.ovh.net. tech.ovh.net. (2017010603 86400 3600 3600000 300)
                                IN NS     ns'.$fields['dnsovh'].'.ovh.net.
                                IN NS     dns'.$fields['dnsovh'].'.ovh.net.
                                IN MX 1   redirect.ovh.net.
                                IN A      '.$fields['ipa'].'
                                IN TXT    "v=spf1 ip4:'.$fields['spf'].'/24 a mx  ~all"
      abuse.at              600 IN TXT    "yveschaponic@gmail.com"
      autoconfig                IN CNAME  mailconfig.ovh.net.
      autodiscover              IN CNAME  mailconfig.ovh.net.
      bounces.at            600 IN TXT    "bounces_mfu@sc2consulting.fr"
      ftp                       IN CNAME  '.$fields['ndd'].'.
      imap                      IN CNAME  ssl0.ovh.net.
      mail                      IN CNAME  ssl0.ovh.net.
      news.at               600 IN TXT    "reply_news@sc2consulting.fr"
      pop3                      IN CNAME  ssl0.ovh.net.
      reply.at              600 IN TXT    "bounces_mfu@sc2consulting.fr"
      s2cdkim._domainkey        IN TXT    ( "k=rsa; p='.$fields['dkim'].'" )
      smtp                      IN CNAME  ssl0.ovh.net.
      www                       IN MX 1   redirect.ovh.net.
      www                       IN A      '.$fields['ipa'];

        $records = "\n";
        for($i=$fields['ipstart'];$i<$fields['ipend'];$i++)
        {
            // info13                    IN A      212.18.245.13
            $records = $records . $fields['sndd'].$i."                    IN A      ".$fields['plageip'].$i."\n";
        }

        // var_dump($records);
        $dns = $dns . $records;

        // $parts = explode('.',$ip);
        // $reverse_ip = implode('.', array_reverse($parts));

        /*
        1 IN PTR info1.lesoffresinmanquables.com.
        2 IN PTR info2.lesoffresinmanquables.com.
        3 IN PTR info3.lesoffresinmanquables.com.
        4 IN PTR info4.lesoffresinmanquables.com.
        5 IN PTR info5.lesoffresinmanquables.com.
        6 IN PTR info6.lesoffresinmanquables.com.
        7 IN PTR info7.lesoffresinmanquables.com.
        8 IN PTR info8.lesoffresinmanquables.com.
        9 IN PTR info9.lesoffresinmanquables.com.
        10 IN PTR info10.lesoffresinmanquables.com.
        11 IN PTR info11.lesoffresinmanquables.com.
        12 IN PTR info12.lesoffresinmanquables.com.

        */

        $rdns = '';

        for($i=$fields['ipstart'];$i<$fields['ipend'];$i++)
        {
            // $rdns .= "\ninfo$i IN A 212.18.245.$i";
            $rdns .= "\n $i IN PTR ". $fields['sndd']."$i.".$fields['ndd'].".";
        }

        // var_dump($rdns);

        return view('admin.dns.result')
            ->withTitle('Admin - TOR')
            ->with('dns',$dns)
            ->with('rdns',$rdns);

    }

    public function bl_status()
    {
        // select de la totalité
        $bl_data = \DB::table('blacklist_lookup')->get();
        return view('admin.bl.index')->with('bl_data',$bl_data);
    }

    public function bl_details($id_bl)
    {
        // select de la totalité
        $bl_data_record = \DB::table('blacklist_lookup_records')->where('id_blacklist_lookup',$id_bl)->get();
        return view('admin.bl.details')->with('bl_data_record',$bl_data_record);
    }

    public function add_check_bl_manuel()
    {
        return view('admin.bl.add');
    }

    public function add_check_bl_manuel_store()
    {
        $new_ndd = \Input::get('ndd_mano');
        \DB::table('blacklist_lookup')
            ->insert(
                [   'domain' => $new_ndd,
                    'domain_reverse' => null,
                    'ip' => null, 'is_bl' => 0,
                    'start_bl_date' => null,
                    'end_bl_date' => null,
                    'created_at' => new \DateTime(),
                    'updated_at' => new \DateTime(),
                    'is_manuel' => 1
                ]
            );
        return \redirect('/outils/bl');
    }

    public function segmentDisable($routeur_id)
    {
        $routeur = \DB::table('routeurs')
            ->select('nom')
            ->where('id', $routeur_id )
            ->first();

        $routeur_name = strtolower($routeur->nom);

        \DB::table('settings')
            ->where('parameter', $routeur_name."_mode")
            ->update(['value' => 'list']);

        return \redirect('/admin');
    }

    public function segmentEnable($routeur_id)
    {
        $routeur = \DB::table('routeurs')
            ->select('nom')
            ->where('id', $routeur_id )
            ->first();

        $routeur_name = strtolower($routeur->nom);

        \DB::table('settings')
            ->where('parameter', $routeur_name."_mode")
            ->update(['value' => 'segment']);
    }

    public function pixelIndex(){
        $pixels = \DB::table('pixel_domain_base_routeur')->get();
        return view('admin.pixel.index')
            ->with('bases', $this->bases)
            ->with('routeurs', $this->routeurs)
            ->with('pixels', $pixels);
    }

    public function pixelEdit($id){
        $pixel = \DB::table('pixel_domain_base_routeur')->where('id', $id)->first();

        $inputs = \Input::all();
        if(!empty($inputs) && !empty($pixel)){
            \DB::table('pixel_domain_base_routeur')
                ->where('id', $id)
                ->update(
                [
                    'domain' => $inputs['domain'],
                    'base_id' => $inputs['base_id'],
                    'routeur_id' => $inputs['routeur_id'],
                ]
            );
        }

        return view('admin.pixel.edit')
            ->with('pixel', $pixel)
            ->with('bases', $this->bases)
            ->with('routeurs', $this->routeurs);
    }

}
