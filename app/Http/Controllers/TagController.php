<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Tag;

class TagController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {

        $tags = Tag::all();

        return view('tag.index')
            ->withMenu('campagne')
            ->withTags($tags);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        return view('tag.create')
            ->withMenu('campagne');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        $newtag = new Tag();

        $newtag->nom = \Input::get('nom');
        $newtag->url = trim(\Input::get('url'));
        $newtag->save();

        return redirect('/tag');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        $tag = Tag::find($id);
        return view('tag.edit')
            ->withMenu('campagne')
            ->withTag($tag);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        $tag = Tag::find($id);

        $tag->nom = \Input::get('nom');
        $tag->url = trim(\Input::get('url'));
        $tag->save();

        return redirect('tag')->withMessage('UPDATE OK');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        $tag = Tag::find($id);
        $tag->delete();
        return \redirect('tag');
    }
}