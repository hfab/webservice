<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Campagne;
use App\Models\CampagneStat;
use App\Models\Ouverture;
use App\Models\Clic;
use Carbon\Carbon;
use App\Models\Bounce;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class AjaxController extends Controller {

    private $repertoire_serveur;

    public function __construct() {
//        $this->middleware('auth');
        //TODO remplacer middleware par if !nolog retourne erreur

        $this->repertoire_serveur = storage_path().'/campagnes';
    }

    public function similaire($str1, $str2)
    {

        $strlen1=strlen($str1);
        $strlen2=strlen($str2);


        $max=max($strlen1, $strlen2);


        $splitSize=250;

        if($max>$splitSize)		{

            $lev=0;

            for($cont=0;$cont<$max;$cont+=$splitSize)	{

                if($strlen1<=$cont || $strlen2<=$cont)		{

                    $lev=$lev/($max/min($strlen1,$strlen2));

                    break;

                }

                $lev+=levenshtein(substr($str1,$cont,$splitSize), substr($str2,$cont,$splitSize));

            }

        }    else    $lev=levenshtein(strtolower($str1), strtolower($str2));



        $porcentage= -100*$lev/$max+100;

        if($porcentage>75)		similar_text(strtolower($str1),strtolower($str2),$porcentage);

        return $porcentage;

    }

    public function getSimilarDossier()
    {
        $dossier = \Input::get('dossier');
        //$base = \Input::get('base');

        $affiche="";

        $liste = scandir($this->repertoire_serveur);

        foreach($liste as $fichier){

            $fichier=trim($fichier);
            if($fichier != '.' and $fichier !='..'){
                $unpour = $this->similaire($dossier, $fichier);

                if(preg_match("/$dossier/i", $fichier)){
                    $unpour = 50;
                }

                if($unpour == 100){
                    echo "<script>  ajout(); </script>";
                    break;
                }

                if(round($unpour) >= 50 )
                {
                    $date=date('Y-m-d');
                    $refe= $fichier."_".$date;
                    //."_".$base;
                    $affiche = $affiche. " <span> $fichier <input  name='folder' onClick=\"modif('$fichier','$refe'); ajout(); couleur(); \" type='radio' value='".$fichier."' </span>  ";
                }

            }

        }

//        if(!file_exists($this->repertoire_serveur."/$base/$dossier")){
        if(!file_exists($this->repertoire_serveur."/$dossier")){

            $affiche = $affiche.  " <p> Nouveau dossier <input onClick=\"var ch1 =  Math.floor (Math.random() * ( 20 - 1 )); var ch2 =  Math.floor (Math.random() * ( 20 - 1 )); var res = ch1+ch2; var a=prompt('Si vous êtes sur de créer un nouveau dossier, veuillez indiquer la réponse à la question : '+ch1+'+'+ch2+'=???',''); if(a==res){ajout();}\" name='folder' type='radio' value='Nouveau dossier' </p>  ";
        }

        echo $affiche;
    }

    public function getOuvertures() {
        $campagne = Campagne::find(\Input::get('campagne_id'));
        $today = Carbon::today();

        $stats= CampagneStat::firstOrCreate(['campagne_id' => $campagne->id, 'date' => $today]);
        $stats->ouvertures = Ouverture::where('campagne_id', $campagne->id)->where('created_at', '>', $today)->count();
        $stats->save();
    }

    public function getClicks() {
        $campagne = Campagne::find(\Input::get('campagne_id'));
        $today = Carbon::today();

        \Artisan::call('campagne:clicks', ['campagne_id' => $campagne->id]);

        $stats= CampagneStat::firstOrCreate(['campagne_id' => $campagne->id, 'date' => $today]);
        $stats->clicks = Clic::where('campagne_id', $campagne->id)->where('created_at', '>', $today)->count();
        $stats->save();
    }

    public function getBounces() {
        $campagne = Campagne::find(\Input::get('campagne_id'));
        $today = Carbon::today();

        \Artisan::call('campagne:bounces', ['campagne_id' => $campagne->id]);

        $stats= CampagneStat::firstOrCreate(['campagne_id' => $campagne->id, 'date' => $today]);
        $stats->bounces = Bounce::where('campagne_id', $campagne->id)->where('created_at', '>', $today)->count();
        $stats->save();
    }


    public function getRecordFront(){

      $id_ca = \Input::get('id');
      $column = \Input::get('type');
      $value = \Input::get('valeur');
      $mois = \Input::get('mois');

      if($column == 'ca_brut'){
        \DB::table('campagnes_ca_concat')
        ->where('id', $id_ca)
        ->where('mois_compta', $mois)
        ->update([$column => $value,
        'ca_net' =>$value]);
      } else {
        \DB::table('campagnes_ca_concat')
        ->where('id', $id_ca)
        ->where('mois_compta', $mois)
        ->update([$column => $value]);
      }
      
      return response()->json('true');

    }



    public function setaaf(){

      $id_ca = \Input::get('id');
      $column = \Input::get('type');
      $value = \Input::get('valeur');
      $mois = \Input::get('mois');

      \DB::table('campagnes_ca_concat')
      ->where('id', $id_ca)
      ->where('mois_compta', $mois)
      ->update([$column => $value]);

      return response()->json('true');

    }



}
