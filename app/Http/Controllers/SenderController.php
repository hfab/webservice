<?php namespace App\Http\Controllers;

use App\Models\Fai;
use App\Models\Sender;
use App\Models\Routeur;

use App\Classes\Routeurs\RouteurPhoenix;
use App\Classes\Routeurs\RouteurSmessage;
use App\Classes\Routeurs\RouteurMaildrop;
use App\Classes\Routeurs\RouteurMindbaz;

use \Ovh\Api;

class SenderController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
        $this->routeur = new RouteurMaildrop();
    }

    function index()
    {

        $recherche = "";
        $offsetPage = 30;

        $routeurinfo = \DB::table('routeurs')->get();
        $senders = Sender::all();

        $routeur_id = null;

        $query = Sender::orderBy('id', 'asc');

        if( !empty(\Input::get('routeur_id')) ){
            // recup de la ref et suppression de l'espace ajouté pour affichage
            $routeur_id = \Input::get('routeur_id');
            $query->where('routeur_id', $routeur_id);

        }

        if( !empty($recherche) or !empty(\Input::get('recherche')) ){
            $recherche = \Input::get('recherche');
            $query->where('nom', 'LIKE', "%$recherche%")
                ->OrWhere('domaine', 'LIKE', "%$recherche%");
        }

        $senders = $query->paginate($offsetPage);

        return view('sender.index_new', ['senders' => $senders])
            ->withMenu('senders')
            ->with('recherche', $recherche)
            ->with('routeurinfo',$routeurinfo)
            ->with('routeur_id', $routeur_id);
    }

    function consulter($id)
    {

        $tri = true;

        $routeurinfo = \DB::table('routeurs')->get();
        $routeurtableau = \DB::table('routeurs')->where('id',$id)->first();
        $senders = \DB::table('senders')->where('routeur_id',$id)->get();
        // var_dump($senders);
        return view('sender.index')
            ->withMenu('senders')
            ->withSenders($senders)
            ->with('routeurinfo',$routeurinfo)
            ->with('routeurtableau',$routeurtableau)
            ->with('tri', $tri);
    }

    function create()
    {
        $fais = Fai::all();
        return view('sender.create_new')
            ->withMenu('senders')
            ->withFais($fais);
    }

    function edit($id)
    {
        $sender = Sender::find($id);
        $fais = Fai::all();

        return view('sender.edit_new')
            ->withMenu('senders')
            ->withSender($sender)
            ->withFais($fais);
    }

    function update($id)
    {
        $sender = Sender::find($id);

        $fields = array_except(\Input::all(), ['_method', '_token', '_fai']);

        \DB::table('senders_history')
        ->insert(
        ['quota_before' => $sender->quota,
        'sender_id' => $id,
        'quota' => $fields['quota'],
        // 'quota_left' => $fai_volume
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
        ]);

        // nouvelle securité
        $lerouteurid = \DB::table('routeurs')->where('nom','Maildrop')->first();
        if($lerouteurid->id == $fields['routeur_id']){
        //   \Log::info('Un sender Maildrop a été modifié');
          $retour = $this->routeur->CheckAccount($fields['password']);
          if($retour === false){
            return redirect('/sender/'. $id .'/edit/error');
          }
        }

        $fields['is_bat'] = true;
        if( ! \Input::get('is_bat') ) {
            $fields['is_bat'] = false;
        }

        if(empty($fields['routeur_id'])){
          // Protection de base contre l'erreur
          // par défaut routeur = maildrop si non précisé
          $fields['routeur_id'] = '1';
        }

        if($sender->quota == $sender->quota_left && $fields['quota'] != $sender->quota){
            $fields['quota_left'] = $fields['quota'];
        } else {
            unset($fields['quota']);
            unset($fields['quota_left']);
        }

        if($sender->nb_campagnes == $sender->nb_campagnes_left && $fields['nb_campagnes'] != $sender->nb_campagnes){
            $fields['nb_campagnes_left'] = $fields['nb_campagnes'];
        } else{
            unset($fields['nb_campagnes']);
            unset($fields['nb_campagnes_left']);
        }

        $routeurinfo = \DB::table('routeurs')->where('id',$fields['routeur_id'])->first();
        if($routeurinfo->nom == 'Phoenix'){

          $routeurphoenix = new RouteurPhoenix();
          $vardomaine = $routeurphoenix->router_get($fields['password']);
            if($vardomaine) {
                $domaine = $vardomaine->routers[0]->name;
                $fields['domaine'] = $domaine;
            }
        }
        $sender->update($fields);

        if (is_array(\Input::get('fai'))) {
            $fais = \Input::get('fai');
        } else {
            $fais = array();
        }
        $sender->fais()->sync($fais);

        if (is_array(\Input::get('fai_quota'))) {
            $fai_quotas = \Input::get('fai_quota');
            foreach($fai_quotas as $fai_id => $fq){
                $fs = \DB::table('fai_sender')
                    ->where('fai_id',$fai_id)
                    ->where('sender_id',$sender->id)
                    ->first();

                if(empty($fs)){
                    continue;
                }
                if($fs->quota == $fq or $fs->quota != $fs->quota_left){
                    continue;
                }

                $oldquota = \DB::table('fai_sender')
                    ->where('fai_id',$fai_id)
                    ->where('sender_id',$sender->id)->first();

                \DB::table('senders_history')
                    ->insert(
                      [
                      'quota_before' => $oldquota->quota,
                      'sender_id' => $id,
                      'fai_id' => $fai_id,
                      'quota' => $fq,
                      // 'quota_left' => $fai_volume
                      'created_at' => date("Y-m-d H:i:s"),
                      'updated_at' => date("Y-m-d H:i:s")
                      ]
                    );

                \DB::table('fai_sender')
                    ->where('fai_id',$fai_id)
                    ->where('sender_id',$sender->id)
                    ->update(['quota'=>$fq, 'quota_left'=>$fq]);
            }
        }

        if (\Auth::check())
        {
          $user = \Auth::user();
          \Log::info($user['original']['name'] . ' vient de modifier le sender [sender_id]->['. $id .'] [user_id]->['. $user['original']['id'] .']');
        }

        return redirect('/sender');
    }

    function store() {
        $fields = array_except(\Input::all(), ['_method', '_token']);
        // nouvelle securité
        $lerouteurid = \DB::table('routeurs')->where('nom','Maildrop')->first();
        if($lerouteurid->id == $fields['routeur_id']){
          // \Log::info('MD Sender Store');
          $retour = $this->routeur->CheckAccount($fields['password']);
          if($retour === false){
            return redirect('/sender/create/error');
          }
        }

        $fields['quota_left'] = $fields['quota'];
        $fields['nb_campagnes_left'] = $fields['nb_campagnes'];

        $fields['is_bat'] = true;
        if( ! \Input::get('is_bat') ) {
            $fields['is_bat'] = false;
        }

        if(empty($fields['routeur_id'])){
            // Protection de base contre l'erreur
            // par défaut routeur = maildrop si non précisé
            $fields['routeur_id'] = '1';
        }

        //FIX temporaire pour prendre en compte les FAIS lors de la création d'un sender
        $sender = Sender::create($fields);

        if (is_array(\Input::get('fai'))) {
            $fais = \Input::get('fai');
        } else {
            $fais = array();
        }
        $sender->fais()->sync($fais);

        if (is_array(\Input::get('fai_quota'))) {
            $fai_quotas = \Input::get('fai_quota');

            foreach($fai_quotas as $fai_id => $fq){
                \DB::table('fai_sender')
                    ->where('fai_id',$fai_id)
                    ->where('sender_id',$sender->id)
                    ->update(['quota'=>$fq, 'quota_left'=>$fq]);

                    /*
                    \DB::table('senders_history')
                    ->insert(['quota_before' => $fai_id,
                    'sender_id' => $s,
                    'quota' => $fai_volume,
                    'quota_left' => $fai_volume
                    ]);
                    */

            }
        }

        if (\Auth::check())
        {
          $user = \Auth::user();
          \Log::info($user['original']['name'] . ' vient de modifier le sender [user_id]->['. $user['original']['id'] .']');
        }

        return redirect('/sender');
    }

    public function destroy($id)
    {
        $sender = Sender::find($id);
        $sender->delete();

        \DB::table('fai_sender')
            ->where('sender_id', $id)
            ->delete();

        if (\Auth::check()) {
          $user = \Auth::user();
          \Log::info($user['original']['name'] . ' vient de supprimer le sender [sender_id]->['. $id .'] [user_id]->['. $user['original']['id'] .']');
        }

        return \redirect('sender');
    }

    public function change_volume()
    {

      $tri = false;
      $faiselect = array('' => 'Choisir');
      $routeurinfo = \DB::table('routeurs')->get();
      $faiinfo = \DB::table('fais')->get();

      foreach ($faiinfo as $v) {
        $faiselect[$v->id] = $v->nom;
      }

      $senders = Sender::all();

      return view('sender.change_volume_new')
          ->withMenu('senders')
          ->withSenders($senders)
          ->with('routeurinfo',$routeurinfo)
          ->with('faiinfo',$faiinfo)
          ->with('faiselect',$faiselect)
          ->with('tri', $tri);

      // je liste les comptes par routeur id

      // j'affiche les informations de chague sender vite fai_quota
      // case a cocher pour choisir un volume commun
      // un formulaire avec les volumes à appliquer sur les cochés


    }

    public function change_volume_tri($id)
    {
      $tri = true;
      $routeurtableau = \DB::table('routeurs')->where('id',$id)->first();
      $faiselect = array('' => 'Choisir');
      $routeurinfo = \DB::table('routeurs')->get();
      $faiinfo = \DB::table('fais')->get();

      foreach ($faiinfo as $v) {
        $faiselect[$v->id] = $v->nom;
      }

      $senders = \DB::table('senders')->where('routeur_id',$id)->get();

      return view('sender.change_volume_new')
          ->withMenu('senders')
          ->withSenders($senders)
          ->with('routeurinfo',$routeurinfo)
          ->with('faiinfo',$faiinfo)
          ->with('faiselect',$faiselect)
          ->with('tri', $tri)
          ->with('routeurtableau', $routeurtableau);

      // je liste les comptes par routeur id

      // j'affiche les informations de chague sender vite fai_quota
      // case a cocher pour choisir un volume commun
      // un formulaire avec les volumes à appliquer sur les cochés


    }

    public function change_volume_store()
    {
      $sender_ids = \Input::get('sender_id');
      $fai_id = \Input::get('fai_id');
      $fai_volume = \Input::get('fai_volume');
      $volume_global = \Input::get('volume_global');

      if($fai_id === ''){
        echo 'je fais rien';

      } else {

      foreach ($sender_ids as $s) {
        // var_dump($s);
        $rowu = \DB::table('fai_sender')->where('fai_id', $fai_id)->where('sender_id', $s)->update(['quota' => $fai_volume, 'quota_left' => $fai_volume]);

        if($rowu === 0){
           \DB::table('fai_sender')->insert(['fai_id' => $fai_id, 'sender_id' => $s, 'quota' => $fai_volume, 'quota_left' => $fai_volume ]);
        }
      }
      }
      // $input = array_except(\Input::all(), ['_token', 'sender_id']);;
      // // ar_dump($input);
      //
      // foreach ($input as $k => $i) {
      //   var_dump($k);
      //   var_dump($i);
      // }


      // prevoir modif volume global
      if($volume_global > 0) {
        \DB::table('senders')->whereIn('id', $sender_ids)->update(['quota' => $volume_global, 'quota_left' => $volume_global]);

      }

      // si un truc est entré je grise les autres
      return redirect('sender/change_volume_new');
    }

    public function multi_edit_ovh()
    {
        $message = "";
        $selected_domains = \Input::get('domains');
        $action = \Input::get('action');

        if(empty($selected_domains)){
            $selected_domains = array();
        }

        $domains_to_edit = array();
        /**
         * Instanciate an OVH Client.
         * You can generate new credentials with full access to your account on
         * the token creation page
         */
        $ovh = new Api(
            getenv('OVH_API_KEY'),  // Application Key
            getenv('OVH_API_SECRET'),  // Application Secret
            'ovh-eu',      // Endpoint of API OVH Europe (List of available endpoints)
            getenv('OVH_API_CONSUMER')
        ); // Consumer Key

        $result2 = $ovh->get('/domain/zone');
        \Log::info(json_encode($result2));
//        print_r( $result2 );

        foreach($result2 as $r){
            $sender = Sender::where('domaine', 'LIKE', "%$r%")->first();

            if(empty($sender)){
                continue;
            }
            $domains_to_edit [$r] = $sender;
        }

        if(!empty($selected_domains)){
            foreach($selected_domains as $r) {
                if ($action == 'zones') {

                } elseif ($action == 'redirections') {

                    $result = $ovh->get("/domain/zone/$r/record");

                    \Log::info(json_encode($result));

                    foreach($result as $id){
                        $result = $ovh->get("/domain/zone/$r/record/$id");
//                        print_r('<br/> '.$result['subDomain']);
                        if( $result['subDomain'] == 'www'){
                            print_r($result);
                            $result = $ovh->delete("/domain/zone/$r/record/$id");
                        }
                    }

                    $ovh->post("/domain/zone/$r/refresh");
//
                    $result = $ovh->get("/domain/zone/$r/redirection");
//                    print_r($result);
                    \Log::info(json_encode($result));
//
                    $result = $ovh->post("/domain/zone/$r/redirection",
                        array(
                            'subDomain' => 'www', // Subdomain (type: string)
                            'target' => \Input::get('site'), // Required: Target of the redirection (type: string)
                            'type' => 'invisible', // Required: Redirection type (type: zone.RedirectionTypeEnum)
                        )
                    );

                    $ovh->post("/domain/zone/$r/refresh");
                }
                $message = true;
            }
        }

        return view('sender.multi_edit_ovh')
            ->with('domains', $domains_to_edit)
            ->with('selected_domains', $selected_domains)
            ->with('message', $message);
    }

    public function enable_sender($id)
    {
      \DB::table('senders')->where('id', $id)->update(['is_enable' => 1]);
      return redirect('sender');

    }

    public function disable_sender($id){
      \DB::table('senders')->where('id', $id)->update(['is_enable' => 0]);
      return redirect('sender');

    }

    public function show_lists($id)
    {
        $sender = Sender::find($id);

        $mindbaz = new RouteurMindbaz();

        $lists = array();

        $lists = $mindbaz->show_lists($sender);

        return  view('sender.showlists')
            ->with('sender', $sender)
            ->with('lists', $lists);
    }


    public function upload_subscribers($id)
    {
        $sender = Sender::find($id);
        $mindbaz = new RouteurMindbaz();

        $name = \Input::file('file')->getClientOriginalName();
        \Input::file('file')->move(storage_path() . '/listes/', $name);

        $imported = $mindbaz->import_list($sender, storage_path() . '/listes/'.$name);

        return $imported;
    }

    public function ajaxGetLists()
    {
        $routeur_id = \Input::get('routeur_id');
        $sender_id = \Input::get('sender_id');

        $routeur = Routeur::find($routeur_id);
        $sender = Sender::find($sender_id);

        $lists = array();

        if($routeur->nom == 'Mindbaz') {
            $mindbaz = new RouteurMindbaz();
            $lists = $mindbaz->show_lists($sender);
        }
        return json_encode($lists);
    }

    public function create_list($id)
    {
        $sender = Sender::find($id);

        $mindbaz = new RouteurMindbaz();

//        $lists = array();
//
//        $lists = $mindbaz->create_list();

        return  view('sender.createlist')
            ->with('sender', $sender);

    }
}
