<?php namespace App\Http\Controllers;

use App\Models\Blacklist;
use App\Models\Fai;
use Illuminate\Support\Facades\File;
use \App\Models\Base;

use App\Jobs\DownloadBaseEmailJ;
use App\Jobs\DownloadBaseEmailDesaboJ;
use App\Jobs\DownloadBaseEmailMd5J;
use App\Jobs\DownloadBaseEmailAllJ;
use App\Jobs\CleanListeBlackliste;
use App\Jobs\DedupFichierBase;




class ListeController extends Controller {

	public function __construct()
	{

		$this->middleware('auth', ['except' => ['url_desabo']]);
		$this->middleware('admin', ['except' => ['url_desabo']]);
		$this->destinataires = [];
	}

		public function url_desabo($fichier){

			$file = storage_path("desinscrits/$fichier");
        $fp = fopen($file,"r");
        while(!feof($fp)) {
          $email = trim(fgets($fp, 1024));
           echo $email . "\n";
        }

			// return \File::get(storage_path("desinscrits/$fichier"));

		}


    public function showCompare()
    {
        return view('outils.compare' );
    }

    public function ajaxCompare()
    {
        echo '0';
    }

    public function showDeduplicate()
    {
        $bases = \App\Models\Base::getGroups();
        return view('outils.deduplicate', ['bases' => $bases] );
    }

    public function ajaxDeduplicate()
    {
        echo '0';
    }


    public function showBlacklist()
    {
        $bases = \App\Models\Base::getGroups();
        return view('outils.blacklist', ['bases' => $bases] );
    }

    public function ajaxBlacklist() {
        $base_id = \Input::get('base_id');
        $base = \App\Models\Base::find($base_id);

        $file = \Input::file('file');

        $f = fopen($file->getRealPath(), 'r');

        $bl_already = 0;
        $bl_inserted = 0;

        while($row = fgets($f, 1024)) {

            $row = trim($row, "\r\t\n;| ");

            if (strlen($row) < 3) {
                continue;
            }

            $email = filter_var($row, FILTER_VALIDATE_EMAIL);
            if ($email === false) {
                return response( json_encode( ['error' => 'format'] ) );
            }

            $bl = \App\Models\Blacklist::firstOrNew( ['email' => $email, 'base_id' => $base_id ] );
            if ($bl->id !== null) {
                $bl_already++;
            } else {
                $bl_inserted++;
                $bl->save();
            }
        }

        fclose($f);

        return response( json_encode(
            [
                'status' => 'ok',
                'inserted' => $bl_inserted,
                'already' => $bl_already,
            ]
            ) );
    }

		public function indexoutils(){

			return view('outils.index_outils_new');
		}


		public function indexupload(){
			// lister les fichiers déjà présents
			$directory = storage_path('listes/listmanager');
			$fichiers = \File::allFiles($directory);
			return view('outils.indexupload_new')->with('fichiers', $fichiers);
		}

		public function upload(){
			$name = \Input::file('fichier')->getClientOriginalName();
			\Input::file('fichier')->move(storage_path() . '/listes/listmanager/', $name);

			$directory = storage_path('listes/listmanager');
			$fichiers = \File::allFiles($directory);
			return view('outils.indexupload_new')->with('fichiers', $fichiers);
		}

		public function supprimerfichier($fichier){
			// rajouter sweet alert
			$name = basename($fichier);
			\File::delete(storage_path() . '/listes/listmanager/'. $name);

			$directory = storage_path('listes/listmanager');
			$fichiers = \File::allFiles($directory);
			return redirect()->action('ListeController@upload');
		}

		public function telechargerfichier($fichier){
			// rajouter sweet alert
			$name = basename($fichier);
			return response()->download(storage_path('listes/listmanager/' . $name));
		}

		function supprimer_caract($string){
			$string = str_replace(' ', '', $string);
			$string = str_replace("\n", '', $string);
			$string = str_replace(chr(10), '', $string);
			$string = str_replace(chr(13), '', $string);
			return $string;
		}

		function retirerAccent($chaine){
    $tabremplace = array("¥" => "Y", "µ" => "u", "À" => "A", "Á" => "A",
    "Â" => "A", "Ã" => "A", "Ä" => "A", "Å" => "A",
    "Æ" => "A", "Ç" => "C", "È" => "E", "É" => "E",
    "Ê" => "E", "Ë" => "E", "Ì" => "I", "Í" => "I",
    "Î" => "I", "Ï" => "I", "Ð" => "D", "Ñ" => "N",
    "Ò" => "O", "Ó" => "O", "Ô" => "O", "Õ" => "O",
    "Ö" => "O", "Ø" => "O", "Ù" => "U", "Ú" => "U",
    "Û" => "U", "Ü" => "U", "Ý" => "Y", "ß" => "s",
    "à" => "a", "á" => "a", "â" => "a", "ã" => "a",
    "ä" => "a", "å" => "a", "æ" => "a", "ç" => "c",
    "è" => "e", "é" => "e", "ê" => "e", "ë" => "e",
    "ì" => "i", "í" => "i", "î" => "i", "ï" => "i",
    "ð" => "o", "ñ" => "n", "ò" => "o", "ó" => "o",
    "ô" => "o", "õ" => "o", "ö" => "o", "ø" => "o",
    "ù" => "u", "ú" => "u", "û" => "u", "ü" => "u",
    "ý" => "y", "ÿ" => "y");

    $chaine = strtr($chaine, $tabremplace);
    return $chaine;
    }

		function get_fai_mail($mail)
		{
			$decoup = explode('@',$mail);
			if(isset($decoup[1])) {
				return $decoup[1];
			}
		}


		public function indexassembler(){

			// je presente les fichiers a assembler
			$directory = storage_path('listes/listmanager');
			$fichiers = \File::allFiles($directory);

			return view('outils.indexassemblage_new')->with('fichiers', $fichiers);
		}


		public function assembler(){

					$contenu = array();
					$file = \Input::get('file');
					$nomliste = \Input::get('listename');
					foreach ($file as $fichier) {

					$handle = fopen(storage_path('listes/listmanager/' . $fichier), 'r');
					while($ligne = fgets($handle))
						{
							if($ligne == '' || $ligne == "\n"){
									// on fait rien
							} else {
								$contenu[] = $this->supprimer_caract($ligne);
							}
						}
					}
					$listefinale = fopen(storage_path('listes/listmanager/f_' . $this->retirerAccent($nomliste) . '.csv'), 'a+');
					foreach ($contenu as $case) {
						fputs($listefinale, $case . "\n");
					}
					return response()->download(storage_path('listes/listmanager/f_' . $this->retirerAccent($nomliste) . '.csv'));
		}

		public function indexdecoupage(){
			// choix de la liste à découper

			// volume des fai par fichier
			// nombre de fichier
			$directory = storage_path('listes/listmanager');
			$fichiers = \File::allFiles($directory);
			return view('outils.indexdecoupage_new')->with('fichiers', $fichiers);

		}

		public function decoupage(){

			$tabliste = array();
			$reste = array();
			$fichierdecoupe = array();
			$lemaxI = 0;

			$free = array();
			$sfr = array();
			$laposte = array();
			$hotmail = array();
			$yahoo = array();
			$aol = array();
			$neuf = array();
			$gmail = array();
			$autre = array();

			$orange = array();
			$wanadoo = array();

			$chuncklaposte = array();
			$chunckfree = array();
			$chunckautre = array();
			$chuncksfr = array();
			$chunckaol = array();
			$chunckgmail = array();
			$chunckhotmail = array();
			$chunckyahoo = array();

			$fichier = \Input::get('file');
			$nomdecoupage = \Input::get('nomdecoupage');

			$demandenombrefichier = \Input::get('nbrfichier');
			$demandenombrefree = \Input::get('nbrfree');
			$demandenombreautre = \Input::get('nbrautre');
			$demandenombresfrneuf = \Input::get('nbrsfrneuf');
			$demandenombrelaposte = \Input::get('nbrlaposte');
			$demandenombreaol = \Input::get('nbraol');
			$demandenombregmail = \Input::get('nbrgmail');
			$demandenombrehotmail = \Input::get('nbrhotmail');
			$demandenombreyahoo = \Input::get('nbryahoo');

			$handle = fopen(storage_path('listes/listmanager/' . $fichier), 'r');
			 while($ligne = fgets($handle))
				 {
					 $tabliste[] = trim($ligne);
				 }

			for ($i=0; $i < $demandenombrefichier; $i++) {
				$handle =	fopen(storage_path('listes/listmanager/decoupe_' . $i . '_'. $nomdecoupage .'.csv'), 'a+');
			}

			foreach ($tabliste as $key => $valeurmail) {

				if($valeurmail == '' || $valeurmail == "\n"){
						// on fait rien
				} else {

					$valeurmail = strtolower($valeurmail);

				if($this->get_fai_mail($valeurmail) == 'free.fr'){
						$free[] = $valeurmail;
				}elseif($this->get_fai_mail($valeurmail) == 'orange.fr'){
					$orange[] = $valeurmail;
				}elseif($this->get_fai_mail($valeurmail) == 'sfr.fr'){
					$sfr[] = $valeurmail;
				}elseif($this->get_fai_mail($valeurmail) == 'sfr.com'){
					$sfr[] = $valeurmail;
				}elseif($this->get_fai_mail($valeurmail) == 'laposte.net'){
					$laposte[] = $valeurmail;
				}elseif($this->get_fai_mail($valeurmail) == 'hotmail.com'){
					$hotmail[] = $valeurmail;
				}elseif($this->get_fai_mail($valeurmail) == 'hotmail.fr'){
					$hotmail[] = $valeurmail;
				}elseif($this->get_fai_mail($valeurmail) == 'live.com'){
					$hotmail[] = $valeurmail;
				}elseif($this->get_fai_mail($valeurmail) == 'live.fr'){
					$hotmail[] = $valeurmail;
				}elseif($this->get_fai_mail($valeurmail) == 'yahoo.fr'){
					$yahoo[] = $valeurmail;
				}elseif($this->get_fai_mail($valeurmail) == 'yahoo.com'){
					$yahoo[] = $valeurmail;
				}elseif($this->get_fai_mail($valeurmail) == 'aol.com'){
					$aol[] = $valeurmail;
				}elseif($this->get_fai_mail($valeurmail) == 'neuf.fr'){
					$sfr[] = $valeurmail;
				}elseif($this->get_fai_mail($valeurmail) == 'gmail.com'){
					$gmail[] = $valeurmail;
				}elseif($this->get_fai_mail($valeurmail) == 'wanadoo.fr'){
					$orange[] = $valeurmail;
				}elseif($this->get_fai_mail($valeurmail) == 'wanadoo.co.uk'){
					$orange[] = $valeurmail;

				}else{
					// autre
					$autre[] = $valeurmail;
				}
			 }
			}

			if($demandenombrefree != 0){
				$chunckfree = array_chunk($free,$demandenombrefree);
			}
			if($demandenombreautre != 0){
				$chunckautre = array_chunk($autre,$demandenombreautre);
			}
			if($demandenombresfrneuf != 0){
				$chuncksfr = array_chunk($sfr,$demandenombresfrneuf);
			}
			if($demandenombrelaposte != 0){
				$chuncklaposte = array_chunk($laposte,$demandenombrelaposte);
			}
			if($demandenombreaol != 0){
				$chunckaol = array_chunk($aol,$demandenombreaol);
			}
			if($demandenombregmail != 0){
				$chunckgmail = array_chunk($gmail,$demandenombregmail);
			}
			if($demandenombrehotmail != 0){
				$chunckhotmail = array_chunk($hotmail,$demandenombregmail);
			}
			if($demandenombreyahoo != 0){
				$chunckyahoo = array_chunk($yahoo,$demandenombreyahoo);
			}

			// if($demandenombreorange != 0){
			// 	$chunckorange = array_chunk($orange,$demandenombreorange);
			// }


			for ($i=0; $i < $demandenombrefichier; $i++) {

				$fichierdecoupe[] = storage_path('listes/listmanager/decoupe_' . $i . '_'. $nomdecoupage .'.csv');

				$handle =	fopen(storage_path('listes/listmanager/decoupe_' . $i . '_'. $nomdecoupage .'.csv'), 'a+');
				if(array_key_exists($i,$chuncklaposte)){
					foreach ($chuncklaposte[$i] as $case) {
						fputs($handle, $case . "\n");
					}
					// unset($chuncklaposte[$i]);
			 	}
				if(array_key_exists($i,$chunckfree)){
					foreach ($chunckfree[$i] as $case) {
						fputs($handle, $case . "\n");
					}
				}
				if(array_key_exists($i,$chuncksfr)){
					foreach ($chuncksfr[$i] as $case) {
						fputs($handle, $case . "\n");
					}
				}
				if(array_key_exists($i,$chunckautre)){
					foreach ($chunckautre[$i] as $case) {
						fputs($handle, $case . "\n");
					}
				}
				if(array_key_exists($i,$chunckaol)){
					foreach ($chunckaol[$i] as $case) {
						fputs($handle, $case . "\n");
					}
				}
				if(array_key_exists($i,$chunckgmail)){
					foreach ($chunckgmail[$i] as $case) {
						fputs($handle, $case . "\n");
					}
				}
				if(array_key_exists($i,$chunckhotmail)){
					foreach ($chunckhotmail[$i] as $case) {
						fputs($handle, $case . "\n");
					}
				}
				if(array_key_exists($i,$chunckyahoo)){
					foreach ($chunckyahoo[$i] as $case) {
						fputs($handle, $case . "\n");
					}
				}
			}

			$lemaxI = count($chuncklaposte);
			if($lemaxI < count($chunckfree)){
				$lemaxI = count($chunckfree);
			}
			if($lemaxI < count($chuncksfr)){
				$lemaxI = count($chuncksfr);
			}
			if($lemaxI < count($chunckautre)){
				$lemaxI = count($chunckautre);
			}
			if($lemaxI < count($chunckaol)){
				$lemaxI = count($chunckaol);
			}
			if($lemaxI < count($chunckgmail)){
				$lemaxI = count($chunckgmail);
			}
			if($lemaxI < count($chunckhotmail)){
				$lemaxI = count($chunckhotmail);
			}
			if($lemaxI < count($chunckyahoo)){
				$lemaxI = count($chunckyahoo);
			}

			for ($i=$demandenombrefichier; $i <$lemaxI ; $i++) {
				if(array_key_exists($i,$chuncklaposte)){
					foreach ($chuncklaposte[$i] as $case) {
						$reste[] = $case;
					}
					// unset($chuncklaposte[$i]);
				}
				if(array_key_exists($i,$chunckfree)){
					foreach ($chunckfree[$i] as $case) {
						$reste[] = $case;
					}
				}
				if(array_key_exists($i,$chuncksfr)){
					foreach ($chuncksfr[$i] as $case) {
						$reste[] = $case;
					}
				}
				if(array_key_exists($i,$chunckautre)){
					foreach ($chunckautre[$i] as $case) {
						$reste[] = $case;
					}
				}
				if(array_key_exists($i,$chunckaol)){
					foreach ($chunckaol[$i] as $case) {
						$reste[] = $case;
					}
				}
				if(array_key_exists($i,$chunckgmail)){
					foreach ($chunckgmail[$i] as $case) {
						$reste[] = $case;
					}
				}
				if(array_key_exists($i,$chunckhotmail)){
					foreach ($chunckhotmail[$i] as $case) {
						$reste[] = $case;
					}
				}
				if(array_key_exists($i,$chunckyahoo)){
					foreach ($chunckyahoo[$i] as $case) {
						$reste[] = $case;
					}
				}
			}

			// !!§§ Atention cela range seulement ceux qui ont eu un nombre demandé au debut §§!! //
			// var_dump($reste);

			foreach ($reste as $casereste) {
				$handle =	fopen(storage_path('listes/listmanager/decoupe_reste_'. $nomdecoupage .'.csv'), 'a+');
				fputs($handle, $casereste . "\n");

			}

			$fichierdecoupe[] = storage_path('listes/listmanager/decoupe_reste_'. $nomdecoupage .'.csv');
			return view('outils.decoupage')->with('fichierdecoupe', $fichierdecoupe);
		}

		public function indexstatliste(){

			$directory = storage_path('listes/listmanager');
			$fichiers = \File::allFiles($directory);
			return view('outils.indexstatsliste_new')->with('fichiers', $fichiers);
		}

		public function statliste(){
			$file = \Input::get('file');
			$nomfichier = $file[0];
			// var_dump($file);
			// var_dump($nomfichier);
			$free = array();
			$sfr = array();
			$laposte = array();
			$orange = array();
			$hotmail = array();
			$yahoo = array();
			$aol = array();
			$neuf = array();
			$gmail = array();
			$autre = array();

			 $handle = fopen(storage_path('listes/listmanager/' . $nomfichier), 'r');
				while($ligne = fgets($handle))
					{
						$ligne = $this->supprimer_caract($ligne);

						// sécurité pour les fichiers avec plusieurs colonne
						if($ligne == '' || $ligne == "\n"){
								// on fait rien
						} else {

						$ligne = strtolower($ligne);
						if($this->get_fai_mail($ligne) == 'free.fr'){
							$free[] = $ligne;
						}elseif($this->get_fai_mail($ligne) == 'orange.fr'){
							$orange[] = $ligne;
						}elseif($this->get_fai_mail($ligne) == 'sfr.fr'){
							$sfr[] = $ligne;
						}elseif($this->get_fai_mail($ligne) == 'sfr.com'){
							$sfr[] = $ligne;
						}elseif($this->get_fai_mail($ligne) == 'laposte.net'){
							$laposte[] = $ligne;
						}elseif($this->get_fai_mail($ligne) == 'hotmail.com'){
							$hotmail[] = $ligne;
						}elseif($this->get_fai_mail($ligne) == 'hotmail.fr'){
							$hotmail[] = $ligne;
						}elseif($this->get_fai_mail($ligne) == 'live.com'){
							$hotmail[] = $ligne;
						}elseif($this->get_fai_mail($ligne) == 'live.fr'){
							$hotmail[] = $ligne;
						}elseif($this->get_fai_mail($ligne) == 'msn.fr'){
							$hotmail[] = $ligne;
						}elseif($this->get_fai_mail($ligne) == 'msn.com'){
							$hotmail[] = $ligne;
						}elseif($this->get_fai_mail($ligne) == 'outlook.fr'){
							$hotmail[] = $ligne;
						}elseif($this->get_fai_mail($ligne) == 'outlook.com'){
							$hotmail[] = $ligne;
						}elseif($this->get_fai_mail($ligne) == 'yahoo.fr'){
							$yahoo[] = $ligne;
						}elseif($this->get_fai_mail($ligne) == 'yahoo.com'){
							$yahoo[] = $ligne;
						}elseif($this->get_fai_mail($ligne) == 'aol.com'){
							$aol[] = $ligne;
						}elseif($this->get_fai_mail($ligne) == 'neuf.fr'){
							$sfr[] = $ligne;
						}elseif($this->get_fai_mail($ligne) == 'gmail.com'){
							$gmail[] = $ligne;
						}elseif($this->get_fai_mail($ligne) == 'wanadoo.fr'){
							$orange[] = $ligne;
						}elseif($this->get_fai_mail($ligne) == 'voila.fr'){
							$orange[] = $ligne;

						}else{
							// autre
							$autre[] = $ligne;
						}
						}

						$nombrefree = count($free);
						$nombresfr = count($sfr);
						$nombrelaposte = count($laposte);
						$nombreorange = count($orange);
						$nombrehotmail = count($hotmail);
						$nombreyahoo = count($yahoo);
						$nombreaol = count($aol);
						// $nombreneuf = count($neuf);
						$nombregmail = count($gmail);
						$nombreautre = count($autre);

						$totalmail = count($free) + count($sfr) + count($laposte) + count($orange) + count($hotmail) + count($yahoo) + count($aol) + count($gmail) + count($autre);
					}

			$directory = storage_path('listes/listmanager');
			$fichiers = \File::allFiles($directory);
			return view('outils.statsliste')->with('fichiers', $fichiers)
			->with('nombrefree', $nombrefree)
			->with('nombresfr', $nombresfr)
			->with('nombrelaposte', $nombrelaposte)
			->with('nombrehotmail', $nombrehotmail)
			->with('nombreyahoo', $nombreyahoo)
			->with('nombreaol', $nombreaol)
			->with('nombreorange', $nombreorange)
			->with('nombregmail', $nombregmail)
			->with('nombreautre', $nombreautre)
			->with('totalmail', $totalmail);
			// ->with('fichiers', $fichiers);
		}

		public function indexmodifierliste(){
			$directory = storage_path('listes/listmanager');
			$fichiers = \File::allFiles($directory);
			return view('outils.indexmodifierliste_new')->with('fichiers', $fichiers);

		}

		public function modifierliste(){
			$choix = \Input::get('choix');
			$fichier = \Input::get('file');

			if($choix == 'supprimeradresse'){
				return view('outils.listesupprimeradresse')->with('fichier',$fichier);
			}

			if($choix == 'supprimerfai'){
				return view('outils.listesupprimerfai')->with('fichier',$fichier);
			}

			if($choix == 'faiparam'){

				$fai = \Input::get('lefai');
				$liste = \Input::get('varfichier');

				$tableauliste = array();
				$tableauresultat = array();
				$handle = fopen(storage_path('listes/listmanager/' . $liste), 'r');
 				while($ligne = fgets($handle))
 					{
						$tableauliste[] = trim($ligne);
					}

					// var_dump($tableauliste);
					foreach ($tableauliste as $maildelaliste) {
						// var_dump($maildelaliste);

						if($fai == 'orange'){
							if($this->get_fai_mail($maildelaliste) == 'orange.fr' || $this->get_fai_mail($maildelaliste) == 'wanadoo.fr'){
								// temporaire à changer
							} else {
								// on garde
								$tableauresultat[] = $maildelaliste;
							}
						}

						if($fai == 'free'){
							if($this->get_fai_mail($maildelaliste) == 'free.fr'){
								// temporaire à changer
							} else {
								// on garde
								$tableauresultat[] = $maildelaliste;
							}
						}

						if($fai == 'sfr'){
							if($this->get_fai_mail($maildelaliste) == 'sfr.fr' || $this->get_fai_mail($maildelaliste) == 'sfr.com'){
								// temporaire à changer
							} else {
								// on garde
								$tableauresultat[] = $maildelaliste;
							}
						}

						if($fai == 'laposte'){
							if($this->get_fai_mail($maildelaliste) == 'laposte.net'){
								// temporaire à changer
							} else {
								// on garde
								$tableauresultat[] = $maildelaliste;
							}
						}

						if($fai == 'aol'){
							if($this->get_fai_mail($maildelaliste) == 'aol.com'){
								// temporaire à changer
							} else {
								// on garde
								$tableauresultat[] = $maildelaliste;
							}
						}

						if($fai == 'gmail'){
							if($this->get_fai_mail($maildelaliste) == 'gmail.com'){
								// temporaire à changer
							} else {
								// on garde
								$tableauresultat[] = $maildelaliste;
							}
						}

						if($fai == 'hotmail'){
							if($this->get_fai_mail($maildelaliste) == 'hotmail.fr' || $this->get_fai_mail($maildelaliste) == 'hotmail.com' || $this->get_fai_mail($maildelaliste) == 'live.fr' || $this->get_fai_mail($maildelaliste) == 'live.com'){
								// temporaire à changer
							} else {
								// on garde
								$tableauresultat[] = $maildelaliste;
							}
						}

						if($fai == 'yahoo'){
							if($this->get_fai_mail($maildelaliste) == 'yahoo.fr' || $this->get_fai_mail($maildelaliste) == 'yahoo.com'){
								// temporaire à changer
							} else {
								// on garde
								$tableauresultat[] = $maildelaliste;
							}
						}
					}

					foreach ($tableauresultat as $leresultat) {
						$handle =	fopen(storage_path('listes/listmanager/supprfai' . $fai . '_'. $liste), 'a+');
						fputs($handle, $leresultat . "\n");
						fclose($handle);
					}
					return response()->download(storage_path('listes/listmanager/supprfai' . $fai . '_'. $liste));
			}

			if($choix == 'supprimercaract'){
				// pas prio, a finir
			}

			if($choix == 'filtrerliste'){
			  $directory = storage_path('listes/listmanager');

				$fichier = \Input::get('file');
				$fichiers = \File::allFiles($directory);

				return view('outils.indexfiltrerliste')->with('fichier',$fichier)->with('fichiers',$fichiers);
			}

			if($choix == 'filtrerlistedb'){
				$bases = Base::all();
				$blacklistes = \DB::table('blacklistes')
					->select('liste')
					->distinct()
					->get();
				$directory = storage_path('listes/listmanager');

				$fichier = \Input::get('file');
				$fichiers = \File::allFiles($directory);

				return view('outils.indexmodifierlistedb')
					->with('fichier',$fichier)
					->with('bases', $bases)
					->with('blacklistes',$blacklistes)
					->with('fichiers',$fichiers);
			}

			if($choix == 'filtrerlisteaction'){
				$directory = storage_path('listes/listmanager');

				$fichier1 = \Input::get('varfichierbase');
				$fichier2 = \Input::get('file');

				$tabliste1 = array();
				$tabliste2 = array();

				$handle = fopen(storage_path('listes/listmanager/' . $fichier1), 'r');
 				while($ligne = fgets($handle))
				{
					$tabliste1[trim($ligne)] = "";
				}

				$handle = fopen(storage_path('listes/listmanager/' . $fichier2), 'r');

	 			while($ligne = fgets($handle))
				{
					$tabliste2[trim($ligne)]="";
				}

				$uniques = array_diff_key($tabliste1, $tabliste2);
				$doublons = array_intersect_key($tabliste1, $tabliste2);

//				echo "<br/> COMPTE -- ".count($tabliste1)." - ".count($tabliste2);
//				echo "<br/> COUNT -- uniques ".count($uniques). " -- doublons -- ".count($doublons);

				$filenameUniques = 'listes/listmanager/filtrage_'. $fichier1."_".count($uniques)."uniques.txt";
				$lefichier =	fopen(storage_path($filenameUniques), 'w');

				foreach ($uniques as $lemail => $rien) {
					fputs($lefichier, $lemail . "\n");

				}
				fclose($lefichier);

				$filenameDoublons = 'listes/listmanager/filtrage_'. $fichier1."_".count($uniques)."doublons.txt";
				$lefichier =	fopen(storage_path($filenameDoublons), 'w');

				foreach ($doublons as $lemail => $rien) {
					fputs($lefichier, $lemail . "\n");
				}
				fclose($lefichier);

		  		//retourner download
				return response()->download(storage_path($filenameUniques));
			}

			if($choix == 'filtrerlistedbaction'){
				$directory = storage_path('listes/listmanager');
				$stats = array();
				$fais = Fai::all();
				$default_fai = Fai::where('nom','autre')->first();

				$fichier1 = \Input::get('varfichierbase');
				$base = \Input::get('base');
				$blackliste = \Input::get('blackliste');

				$blacklistes = \DB::table('blacklistes')
									->select('email')
									->where('liste',$blackliste)
									->get();

				$base = Base::find($base);


				\DB::table('destinataires')
					->select('mail')
					->where('base_id',$base->id)
					// ->where('statut','<',3)
					->chunk(20000, function($base_dest){
						foreach($base_dest as $bd){
							$this->destinataires[$bd->mail]="";
						}
					});

				$tabliste1 = array();
				$tabliste2 = array();
				$tabliste3 = array();

				$original = storage_path('listes/listmanager/' . $fichier1);
				$handle = fopen($original, 'r');
				while($ligne = fgets($handle))
				{
					$tabliste1[trim($ligne)] = "";
//					$fai = $default_fai;
//					foreach($fais as $k_fai) {
//						foreach($k_fai->get_domains() as $dom) {
//							if (strpos($ligne,$dom) !== false) {
//								$fai = $k_fai;
//								break 2;
//							}
//						}
//					}
//					if(!isset($stats[$original][$fai->id])){
//						$stats[$original][$fai->id] = 0;
//					}
//					$stats[$original][$fai->id]++;
				}

//				$stats[$original]['uniques'] = 0;
//				$stats[$original]['doublons'] = 0;

				foreach($blacklistes as $bl){
					$tabliste3[$bl->email] = "";
				}

				$uniques = array_diff_key($tabliste1, $this->destinataires);
				$doublons = array_intersect_key($tabliste1, $this->destinataires);

				$uniques_from_bl = array_diff_key($uniques, $tabliste3);
				$doublons_from_bl = array_intersect_key($uniques, $tabliste3);

//				echo "<br/> COMPTE -- ".count($tabliste1)." - ".count($tabliste2);
//				echo "<br/> COUNT -- uniques ".count($uniques). " -- doublons -- ".count($doublons);
//				echo "<br/> COUNT -- uniques ".count($uniques_from_bl). " -- doublons -- ".count($doublons);

				//UNIQUES BASE//
				$filename = 'filtrage_'. $fichier1."_".count($uniques)."uniques-base-$base->nom.txt";

				$stats[$filename]['uniques'] = count($uniques);
				$stats[$filename]['doublons'] = count($doublons);

				$filepathUniques = 'listes/listmanager/'.$filename;
				$lefichier =	fopen(storage_path($filepathUniques), 'w');
				foreach ($uniques as $lemail => $rien) {
					fputs($lefichier, $lemail . "\n");
//					$fai = $default_fai;
//					foreach($fais as $k_fai) {
//						foreach($k_fai->get_domains() as $dom) {
//							if (strpos($lemail,$dom) !== false) {
//								$fai = $k_fai;
//								break 2;
//							}
//						}
//					}
//					if(!isset($stats[$filenameUniques][$fai->id])){
//						$stats[$filenameUniques][$fai->id] = 0;
//					}
//					$stats[$filenameUniques][$fai->id]++;
				}
				fclose($lefichier);
				//DOUBLONS BASE//
				$filename = 'filtrage_'. $fichier1."_".count($doublons)."doublons-base-$base->nom.txt";
				$filepathDoublons = 'listes/listmanager/'.$filename;
				$lefichier =	fopen(storage_path($filepathDoublons), 'w');
				foreach ($doublons as $lemail => $rien) {
					fputs($lefichier, $lemail . "\n");
				}
				fclose($lefichier);

				//UNIQUES BLACKLISTE//
				$filename = 'filtrage_'. $fichier1."_".count($uniques_from_bl)."uniques-base-$base->nom-BL-$blackliste.txt";
				$filepathUniques = 'listes/listmanager/'.$filename;

				$stats[$filename]['uniques'] = count($uniques_from_bl);
				$stats[$filename]['doublons'] = count($doublons_from_bl);

				$lefichier =	fopen(storage_path($filepathUniques), 'w');
				foreach ($uniques_from_bl as $lemail => $rien) {
					fputs($lefichier, $lemail . "\n");
//					$fai = $default_fai;
//					foreach($fais as $k_fai) {
//						foreach($k_fai->get_domains() as $dom) {
//							if (strpos($lemail,$dom) !== false) {
//								$fai = $k_fai;
//								break 2;
//							}
//						}
//					}
//					if(!isset($stats[$filenameUniques][$fai->id])){
//						$stats[$filenameUniques][$fai->id] = 0;
//					}
//					$stats[$filenameUniques][$fai->id]++;
				}
				fclose($lefichier);
				//DOUBLONS BLACKLISTE//
				$filepathDoublons = 'listes/listmanager/filtrage_'. $fichier1."_".count($doublons_from_bl)."doublons-base-$base->nom-BL-$blackliste.txt";
				$lefichier =	fopen(storage_path($filepathDoublons), 'w');
				foreach ($doublons_from_bl as $lemail => $rien) {
					fputs($lefichier, $lemail . "\n");
				}
				fclose($lefichier);

				$bases = Base::all();
				$blacklistes = \DB::table('blacklistes')
					->select('liste')
					->distinct()
					->get();
				$directory = storage_path('listes/listmanager');
				$fichiers = \File::allFiles($directory);

				$fichier[] = $fichier1;

				//retourner download
				return view('outils.indexmodifierlistedb')
					->with('fichier',$fichier)
					->with('bases', $bases)
					->with('base', $base)
					->with('blacklistes',$blacklistes)
					->with('blackliste',$blackliste)
					->with('fichiers',$fichiers)
					->with('stats',$stats)
					->with('fais',$fais);

				//retourner download
				return response()->download(storage_path($filenameUniques));
			}

			if($choix == 'filtrerblacklistglobal'){

				$directory = storage_path('listes/listmanager');
				$stats = array();
				$fais = Fai::all();

				$fichiertab = \Input::get('file');

				foreach ($fichiertab as $fichier) {
					$fichier1 = $fichier;
				}

				\DB::table('blacklistes')
					->select('email')
					->chunk(20000, function($base_dest){
						foreach($base_dest as $bd){
							$this->destinataires[$bd->email]="";
						}
					});

				$tabliste1 = array();
				$tabliste2 = array();
				$tabliste3 = array();

				$original = storage_path('listes/listmanager/' . $fichier1);

				$handle = fopen($original, 'r');
				while($ligne = fgets($handle))
				{
					$tabliste1[trim($ligne)] = "";


				$uniques = array_diff_key($tabliste1, $this->destinataires);
				$doublons = array_intersect_key($tabliste1, $this->destinataires);

				$uniques_from_bl = array_diff_key($uniques, $tabliste3);
				$doublons_from_bl = array_intersect_key($uniques, $tabliste3);

				}


				//UNIQUES BASE//
				$filename = 'filtrage_'. $fichier1."_".count($uniques)."uniques-base-blacklistes.txt";

				$stats[$filename]['uniques'] = count($uniques);
				$stats[$filename]['doublons'] = count($doublons);

				$filepathUniques = 'listes/listmanager/'.$filename;
				$lefichier =	fopen(storage_path($filepathUniques), 'w');
				// $fichier[] = array($filepathUniques);
				foreach ($uniques as $lemail => $rien) {
					fputs($lefichier, $lemail . "\n");
				}
				fclose($lefichier);
				//DOUBLONS BASE//
				$filename = 'filtrage_'. $fichier1."_".count($doublons)."doublons-base-blacklistes.txt";
				$filepathDoublons = 'listes/listmanager/'.$filename;
				$lefichier =	fopen(storage_path($filepathDoublons), 'w');
				foreach ($doublons as $lemail => $rien) {
					fputs($lefichier, $lemail . "\n");
				}
				fclose($lefichier);
				// $fichier[] = array($filepathDoublons);

				$directory = storage_path('listes/listmanager');
				$fichiers = \File::allFiles($directory);

				return response()->download(storage_path($filepathUniques));

			}

			if($choix == 'deletemail'){
				$mail = \Input::get('deletea');
				$liste = \Input::get('varfichier');
				$tableauliste = array();

				$handle = fopen(storage_path('listes/listmanager/' . $liste), 'r');
 				while($ligne = fgets($handle))
 					{
						$tableauliste[] = trim($ligne);
					}

			foreach ($tableauliste as $tabmail) {

				if($tabmail == $mail){
						// adresse trouvé on fait rien
					} else {
					$handle =	fopen(storage_path('listes/listmanager/new_'. $liste), 'a+');
					fputs($handle, $tabmail . "\n");
					fclose($handle);
					}
			}
			return response()->download(storage_path('listes/listmanager/new_'. $liste));
		}

		if($choix == 'extrairefai'){

			$lefichier = \Input::get('file');
			$fichier = $lefichier[0];
			return view('outils.indexextractfai')->with('fichier',$fichier);
		}

		if($choix == 'extrairefaiaction'){
			$lefichier = \Input::get('varfichierbase');
			$fai = \Input::get('lefai');
			$tableauresultat = array();
			$handle = fopen(storage_path('listes/listmanager/' . $lefichier), 'r');

			while($ligne = fgets($handle))
				{
					$tableauliste[] = trim($ligne);
				}

			foreach ($tableauliste as $maildelaliste) {


			if($fai == 'orange'){
				if($this->get_fai_mail($maildelaliste) == 'orange.fr' || $this->get_fai_mail($maildelaliste) == 'wanadoo.fr' || $this->get_fai_mail($maildelaliste) == 'voila.fr'){
					$tableauresultat[] = $maildelaliste;
				}
			}

			if($fai == 'free'){
				if($this->get_fai_mail($maildelaliste) == 'free.fr'){
					$tableauresultat[] = $maildelaliste;
				}
			}

			if($fai == 'sfr'){
				if($this->get_fai_mail($maildelaliste) == 'sfr.fr' || $this->get_fai_mail($maildelaliste) == 'sfr.com'){
					$tableauresultat[] = $maildelaliste;
				}
			}

			if($fai == 'laposte'){
				if($this->get_fai_mail($maildelaliste) == 'laposte.net'){
				$tableauresultat[] = $maildelaliste;
				}
			}

			if($fai == 'aol'){
				if($this->get_fai_mail($maildelaliste) == 'aol.com'){
					$tableauresultat[] = $maildelaliste;
				}
			}

			if($fai == 'gmail'){
				if($this->get_fai_mail($maildelaliste) == 'gmail.com'){
					$tableauresultat[] = $maildelaliste;
				}
			}

			if($fai == 'hotmail'){
				if($this->get_fai_mail($maildelaliste) == 'hotmail.fr' || $this->get_fai_mail($maildelaliste) == 'hotmail.com' || $this->get_fai_mail($maildelaliste) == 'live.fr' || $this->get_fai_mail($maildelaliste) == 'live.com'){
					$tableauresultat[] = $maildelaliste;
				}
			}

			if($fai == 'yahoo'){
				if($this->get_fai_mail($maildelaliste) == 'yahoo.fr' || $this->get_fai_mail($maildelaliste) == 'yahoo.com'){
					$tableauresultat[] = $maildelaliste;
				}
			}

			}

			if(count($tableauresultat) > 0){

			foreach ($tableauresultat as $lemail) {
				$handle =	fopen(storage_path('listes/listmanager/extractfai_'. $fai . '_' . $lefichier), 'a+');
				fputs($handle, $lemail . "\n");
				fclose($handle);
			}
				return response()->download(storage_path('listes/listmanager/extractfai_'. $fai . '_' . $lefichier));
		} else {
				echo "Aucune adresse de ce FAI : " . $fai;
		}

		}

		if($choix == 'convertmd5'){
			$lefichier = \Input::get('file');
			$fichier = $lefichier[0];
			$tableauliste = array();
			$tableaumd5 = array();
			$chainemd5 = '';
			$handle = fopen(storage_path('listes/listmanager/' . $fichier), 'r');

			$check = 0;

			while($ligne = fgets($handle))
				{
					$chainemd5 = $chainemd5 . md5(trim($ligne)) . "\n";
				}

					$handle =	fopen(storage_path('listes/listmanager/md5_' . $fichier), 'a+');
					fputs($handle, $chainemd5);
					fclose($handle);

				return response()->download(storage_path('listes/listmanager/md5_' . $fichier));

		}

	}

		public function nddfrindex(){
			$directory = storage_path('listes/listmanager');
		$fichiers = \File::allFiles($directory);
		// retourn view choix
		return view('outils.indexnddfr_new')->with('fichiers', $fichiers);

		}

		public function nddfr(){
			$emailfr = array();
			$emailetranger = array();
			$chainemail = '';

			$lefichier = \Input::get('file');
			$handle = fopen(storage_path('listes/listmanager/' . $lefichier), 'r');
			while($ligne = fgets($handle))
				{
					$email = trim($ligne);
					if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
							 if(strripos($email,".fr")){
								 $chainemail .= $email . "\n";
						 	} elseif(strripos($email,".com")) {
								$chainemail .= $email . "\n";
							} elseif(strripos($email,".net")) {
								$chainemail .= $email . "\n";
							} elseif(strripos($email,".org")) {
								$chainemail .= $email . "\n";
							}
					}
				}

				$handle = fopen(storage_path('listes/listmanager/FR_' . $lefichier), 'a+');
				fputs($handle, $chainemail);
				fclose($handle);

				// renvoyer le fichier
				return response()->download(storage_path('listes/listmanager/FR_' . $lefichier));
				// ajouter bouton blade

		}

		// newww

		public function download_base_index(){

			// table avec les bases
			// télécharger simple - désabo

			$bases = \DB::table('bases')->get();
			// echo 'hello';
			// die();

			return view('outils.dl_base_menu_new')
			->with('bases', $bases);


		}


		public function download_file_list(){


						$directory = storage_path('listes/download/');
						$fichiers = \File::allFiles($directory);
						return view('outils.getfile_new')->with('fichiers', $fichiers);


		}

		public function dwnl_file($fichier){
			$name = basename($fichier);
			return response()->download(storage_path('listes/download/' . $name));

		}

		public function dwnl_deletefile($fichier){

			$name = basename($fichier);
			\File::delete(storage_path() . '/listes/download/'. $name);

			$directory = storage_path('listes/download');
			$fichiers = \File::allFiles($directory);
			return redirect()->action('ListeController@download_file_list');

		}


		public function download_base_email(){

			return view('outils.dl_base_menu')
			->with('bases', $bases);


		}

		public function download_base_email_go($base_id){
			$this->dispatch(new DownloadBaseEmailJ($base_id));
			return \redirect('/downl');
		}

		public function download_basedesabo_email_go($base_id){
			$this->dispatch(new DownloadBaseEmailDesaboJ($base_id));
			return \redirect('/downl');
		}

		public function download_basehash_email_go($base_id){
			$this->dispatch(new DownloadBaseEmailMd5J($base_id));
			return \redirect('/downl');
		}

		public function download_champs_email_go($base_id){
			$this->dispatch(new DownloadBaseEmailAllJ($base_id));
			return \redirect('/downl');
		}


		public function download_desabo(){

			$directory = storage_path('listes/download/');
			$fichiers = \File::allFiles($directory);
			return view('outils.indexupload')->with('fichiers', $fichiers);
		}


		public function filterblacklist(){
			$directory = storage_path('listes/listmanager/');
			$fichiers = \File::allFiles($directory);
			return view('outils.tool_filterblacklist_new')->with('fichiers', $fichiers);
		}

		public function filterblacklist_go($file){
			$this->dispatch(new CleanListeBlackliste($file));
			return \redirect('/tool/filtrerblacklist');

		}

		// penser à faire un clean

		public function filterbdd(){
			$directory = storage_path('listes/listmanager/');
			$fichiers = \File::allFiles($directory);
			$bases = \DB::table('bases')->get();

			return view('outils.tool_filterbdd_new')
			->with('fichiers', $fichiers)
			->with('bases',$bases);



		}

		public function filterbdd_go($base_id,$file){

			// \Log::info('Fichier > ' . $file . ' base_id > ' . $base_id );
			$this->dispatch(new DedupFichierBase($base_id,$file));
			return \redirect('/tool/filtrerblacklist');
		}


}
