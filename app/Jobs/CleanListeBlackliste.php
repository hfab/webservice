<?php

namespace App\Jobs;

use App\Jobs\Job;

use App\Models\Notification;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

class CleanListeBlackliste extends Job implements SelfHandling
{


    use InteractsWithQueue, SerializesModels;
    public $lefichier;

    /**
     * Create a new job instance.
     *
     * @return void
     */
     public function __construct($lefichier)
     {
         $this->lefichier = $lefichier;
     }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $monfichier = $this->lefichier;

        \Log::info('Fichier > ' . $monfichier . ' > debut export filter blacklist : ' . date('Ymd H:i:s'));
        $original = storage_path('listes/listmanager/' . $monfichier);
        $handle = fopen($original, 'r');

        \DB::table('blacklistes')
					->select('email')
					->chunk(20000, function($base_dest){

					foreach($base_dest as $bd){
							$this->destinataires[$bd->email]=null;
						}
					});

          while($ligne = fgets($handle))
          {
            $tablisteorigine[trim($ligne)] = null;

          }

        $uniques = array_diff_key($tablisteorigine, $this->destinataires);

        $contentuniques = '';
        foreach ($uniques as $key => $value) {
          $contentuniques.= $key ."; \n";
        }

        $filename = storage_path('listes/download/') . 'filtrage_'. $monfichier ."_".count($uniques)."_uniques-base-blacklistes.txt";

        $fp = fopen($filename,"w+");
        fwrite($fp, $contentuniques);

        \Log::info('Fichier > ' . $monfichier . ' > fin export filter blacklist : ' . date('Ymd H:i:s'));
        Notification::create([
            'user_id' => 1,
            'level' => 'info',
            'is_important' => true,
            'message' => "Fichier filter blacklist " .$monfichier. " success"
        ]);


    }
}
