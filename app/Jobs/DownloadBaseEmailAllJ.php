<?php

namespace App\Jobs;

use App\Jobs\Job;

use App\Models\Notification;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

class DownloadBaseEmailAllJ extends Job implements SelfHandling
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
     public function __construct($base_id)
     {
         $this->base_id = $base_id;
     }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      $labaseid = $this->base_id;

      $baseobject = \DB::table('bases')
      ->where('id', $labaseid)
      ->first();

      \Log::info('Base > ' . $baseobject->nom . ' > debut export all fields : ' . date('Ymd H:i:s'));

      $today = date('Ymd');

      $offset = 100000;

      $base_counter = \DB::table('destinataires')
      ->where('base_id',$labaseid)
      ->count();

      // supprimer ancien fichier si il y a
      $exportname = 'export_base_champall_' .$baseobject->code .'_'. $today . '.csv';
      $pathname = storage_path() . '/listes/download/';
      $file = $pathname.$exportname;
      $fp = fopen($file,"w+");

      fclose($fp);

      $long = strlen($base_counter) - substr_count($offset,'0');
      $nbretape = substr($base_counter,0,$long);

      $skip = 0;

      for ($i = 1; $i <= $nbretape; $i++) {

        echo "Etape ". $i . ' / '. $nbretape . "\n";
        $arraymail=array();

        $lignedb = \DB::table('destinataires')
        ->select('mail','nom','prenom','datenaissance','ville','departement')
        ->where('base_id',$labaseid)
        ->where('statut','<',3)
        ->skip($skip)
        ->take($offset)
        ->get();

        if(isset($lignedb)){
          $content = '';

          foreach ($lignedb as $db) {
          $content .= $db->mail . ";" . $db->nom . ";" . $db->prenom .";" . $db->datenaissance . ";" . $db->ville . ";" . $db->departement . "; \n";
          }

          $fp = fopen($file,"a+");
          fwrite($fp, $content);

        }

        $skip = $skip + $offset;

        unset($content);
        unset($arraymail);

      }

      $arraymail=array();

      $lignedb = \DB::table('destinataires')
      ->select('mail','nom','prenom','datenaissance','ville','departement')
      ->where('base_id',$labaseid)
      ->skip($skip)
      ->take($offset)
      ->get();

      if(isset($lignedb)){
        $content = '';

        foreach ($lignedb as $db) {
        $content .= $db->mail . ";" . $db->nom . ";" . $db->prenom .";" . $db->datenaissance . ";" . $db->ville . ";" . $db->departement . "; \n";
        }

        $fp = fopen($file,"a+");
        fwrite($fp, $content);

      }


      \Log::info('Base > ' . $baseobject->nom . ' > fin export all fields : ' . date('Ymd H:i:s'));
      Notification::create([
          'user_id' => 1,
          'level' => 'info',
          'is_important' => true,
          'message' => "Bdd export champs " .$baseobject->nom. " success"
      ]);
    }
}
