<?php

namespace App\Jobs;

use App\Models\Notification;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

class BaseImportBlacklistes extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $file;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($file)
    {
        $this->file = storage_path().'/blacklistes/'.$file;
        $this->bases = array();
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        $this->bases = \DB::table('bases')->select('id')->get();
        $file = $this->file;

        if (!is_file($file)) {
            $this->error('File not found : '.$file);
        }

        \Log::info("Import blackliste a partir fichier -- $file");

        $ts = date('Y-m-d H:i:s');

        \App\Helpers\Profiler::start('import_blackliste');

        $pdo = \DB::connection()->getPdo();
        $sql = "INSERT IGNORE INTO blacklistes (email, base_id, liste, created_at, updated_at) VALUES (:email, :base_id, :liste, :ts1, :ts2)";
        $stmt = $pdo->prepare($sql);
        $pdo->beginTransaction();
        $bulk_count = 0;

        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $extension = \File::extension($file);
        $name = \File::name($file);

        $updateData = array();

        $fh = fopen($file, 'r');

        $total = 0;
        $inserted = 0;

        while (!feof($fh)) {

            $cells = [];
            $row = fgets($fh, 1024);
            $row = trim($row, "\r\n\t ");

            if (strpos($row, ';') !== false) {
                $cells = explode(';', $row);
            } elseif (strpos($row, '|') !== false) {
                $cells = explode('|', $row);
            } else {
                $cells[] = trim($row, " \r\n\t\"");
            }

            foreach($cells as $cell) {

                if (filter_var($cell, FILTER_VALIDATE_EMAIL)) {
                    $total++;

                    $updateData[] = $cell;

                    if (count($updateData) >= 5000) {
                        $this->writeUpdates($updateData);
                        $updateData = array();
                        $bulk_count = 0;
                    }

                    $insertData = [$cell, 0, $name, $ts, $ts];
                    $stmt->execute($insertData);

                    $bulk_count++;

                    if ($bulk_count >= 250) {
                        $pdo->commit();
                        $pdo->beginTransaction();
                        $bulk_count = 0;
                    }

                    $inserted++;
                    continue;
                }
            }
        }

        $pdo->commit();

        if (count($updateData) > 0) {
            $this->writeUpdates($updateData);
            $updateData = array();
            $bulk_count = 0;
        }

        if (isset(\Auth::User()->id)) {
            $user_id = \Auth::User()->id;
        } else {
            $user_id = 1;
        }

        Notification::create([
            'user_id' => $user_id,
            'level' => 'info',
            'is_important' => 1,
            'message' => "Fichier blackliste $name importé."
        ]);

        \Log::info("End of Import blackliste a partir fichier -- $file");
        \App\Helpers\Profiler::report("import_blackliste");
    }

    private function writeUpdates($mails) {
        foreach($this->bases as $b) {
            \DB::table('destinataires')
                ->where('base_id',$b->id)
                ->whereIn('mail', $mails)
                ->update(['statut' => 666]); //BLACKLISTE
        }
    }
}
