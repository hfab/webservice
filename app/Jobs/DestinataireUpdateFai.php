<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;


class DestinataireUpdateFais extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new command instance.
     *
     * @return void
     */

    protected $chunk_size = 10000;
    protected $chunk_size_update = 10000;

    public function __construct()
    {

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $default_fai = 6;
        $fais = \App\Models\Fai::all();

        \DB::table('destinataires')
            ->select('id', 'mail')
            ->where('statut', 0)
            ->chunk($this->chunk_size, function ($destinataires) use ($default_fai, $fais){
                $fai_dest_ids = array();
                foreach($destinataires as $d) {
                    $fai_id = $default_fai;
                    foreach ($fais as $k_fai) {
                        foreach($k_fai->get_domains() as $dom) {
                            if (strpos($d->mail, $dom) !== false) {
                                $fai_id = $k_fai->id;
                                break 2;
                            }
                        }
                    }

                    $fai_dest_ids[$fai_id][]=$d->id;
                    if(count($fai_dest_ids[$fai_id]) > $this->chunk_size_update){
                        $this->writeUpdates($fai_dest_ids[$fai_id], $fai_id);
                    }
                }
                foreach($fai_dest_ids as $faiid => $tabids){
                    if(count($tabids) > 0){
                        $this->writeUpdates($fai_dest_ids[$faiid], $faiid);
                    }
                }
            });

    }

    private function writeUpdates($ids, $fai_id) {
        \DB::table('destinataires')->whereIn('id', $ids)->update(['fai_id' => $fai_id]);
    }
}