<?php

namespace App\Jobs;

use App\Jobs\Job;

use App\Models\Notification;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

class DownloadBaseEmailJ extends Job implements SelfHandling, ShouldQueue
{

    use InteractsWithQueue, SerializesModels;
    public $base_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($base_id)
    {
        $this->base_id = $base_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      $labaseid = $this->base_id;

      $baseobject = \DB::table('bases')
      ->where('id', $labaseid)
      ->first();

      \Log::info('Base > ' . $baseobject->nom . ' > debut export : ' . date('Ymd H:i:s'));

      $today = date('Ymd');

      $offset = 100000;

      $base_counter = \DB::table('destinataires')
      ->where('base_id',$labaseid)
      ->count();

      // supprimer ancien fichier si il y a
      $exportname = 'export_base_' .$baseobject->code .'_'. $today . '.csv';
      $pathname = storage_path() . '/listes/download/';
      $file = $pathname.$exportname;
      $fp = fopen($file,"w+");

      fclose($fp);

      $long = strlen($base_counter) - substr_count($offset,'0');
      $nbretape = substr($base_counter,0,$long);

      $skip = 0;

      for ($i = 1; $i <= $nbretape; $i++) {

        echo "Etape ". $i . ' / '. $nbretape . "\n";
        $arraymail=array();

        $lignedb = \DB::table('destinataires')
        ->select('mail')
        ->where('base_id',$labaseid)
        ->skip($skip)
        ->take($offset)
        ->get();

        $arraymail = array_pluck($lignedb,'mail');

        if(isset($arraymail)){
          $content = '';

          foreach ($arraymail as $val) {
          $content .= $val . "; \n";
          }

          $fp = fopen($file,"a+");
          fwrite($fp, $content);

        }

        $skip = $skip + $offset;

        unset($content);
        unset($arraymail);

      }

      $arraymail=array();

      $lignedb = \DB::table('destinataires')
      ->select('mail')
      ->where('base_id',$labaseid)
      ->skip($skip)
      ->take($offset)
      ->get();

      $arraymail = array_pluck($lignedb,'mail');

      if(isset($arraymail)){
        $content = '';

        foreach ($arraymail as $val) {
        $content .= $val . "; \n";
        }

        $fp = fopen($file,"a+");
        fwrite($fp, $content);

      }


      \Log::info('Base > ' . $baseobject->nom . ' > fin export : ' . date('Ymd H:i:s'));
      Notification::create([
          'user_id' => 1,
          'level' => 'info',
          'is_important' => true,
          'message' => "Bdd export " .$baseobject->nom. " success"
      ]);


    }
}
