<?php

namespace App\Jobs;

use App\Classes\Routeurs\RouteurMindbaz;
use App\Models\Base;
use App\Models\Notification;
use App\Models\Sender;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\SendersPopulate;

class BaseImportMindbaz extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $base_id;
    public $file;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($base_id, $file)
    {
        $this->base_id = $base_id;
        $this->file = storage_path().'/listes/'.$file;
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {

        $base_id = $this->base_id;
        $file = $this->file;

        if (!is_file($file)) {
            $this->error('File not found : '.$file);
        }

        \Log::info("Import Destinataires dans la base ($base_id) (QUEUE COMMAND) du fichier -- '.$file");

        $ts = date('Y-m-d H:i:s');

        \App\Helpers\Profiler::start('import');

        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $base = Base::find($base_id);

        $extension = \File::extension($file);
        $name = \File::name($file);

        $fh = fopen($file, 'r');

        $base_id = $base->id;
        $liste = str_replace(".$extension",'',$name);

        $mindbaz = new RouteurMindbaz();

        $routeur = \DB::table('routeurs')
            ->where('nom', 'Mindbaz')
            ->where('is_active', 1)
            ->first();

        if(empty($routeur)){
            \Log::error('BaseImportMindbaz : DISABLED MINDBAZ');
            exit;
        }

        $import_file = storage_path()."/import_mindbaz_".$name.".csv";

        $handle = fopen( $import_file , "w");

        \DB::table('destinataires')
            ->select('id', 'mail')
            ->where('statut', 0)
            ->where('base_id', $base_id)
            ->where('liste', $name)
            ->chunk(10000, function ($destinataires) use ($handle) {
                $content = "";
                foreach ($destinataires as $desti) {
                    $content .= "$desti->id;$desti->mail\r\n";
                }
                fwrite($handle, $content);
            });
        fclose($handle);

        $senders = Sender::where('routeur_id', $routeur->id)
            ->distinct('nom')
            ->get();

        $imported = false;

        $filename = $file;
        //On importe par défaut sur toutes les "bases"/senders Mindbaz
        foreach($senders as $sender)
        {
            $mindbaz->import_list($sender, $import_file);
            $imported = true;
        }

        if (isset(\Auth::User()->id)) {
            $user_id = \Auth::User()->id;
        } else {
            $user_id = 1;
        }

        if($imported) {
            Notification::create([
                'user_id' => $user_id,
                'level' => 'info',
                'is_important' => true,
                'message' => "Fichier $name importé sur Mindbaz."
            ]);
        }

        \Log::info("End of Import Mindbaz base ($base_id) (QUEUE COMMAND) du fichier -- '.$file");
        \App\Helpers\Profiler::report();
    }
}
