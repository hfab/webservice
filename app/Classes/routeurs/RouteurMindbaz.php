<?php namespace App\Classes\Routeurs;

use \App\Models\Sender;
use \App\Models\Routeur;
use Carbon\Carbon;

class RouteurMindbaz
{
    const MBZ_WS_NAMESPACE = 'http://www.mindbaz.com/webservices/';

    private $routeur;

    public function __construct()
    {
      if(\Schema::hasTable('routeurs')) {
           $this->routeur = Routeur::where('nom', 'Mindbaz')->first();
       }
        $this->send_frequency = 250; //default value
        if(!empty(getenv('MB_FREQ_SEND'))) {
            $this->send_frequency = getenv('MB_FREQ_SEND');
        }
    }

    private function createHeader($IdSite, $Login, $Password)
    {
        $struct = new \Illuminate\Database\Eloquent\Collection;
        $struct->IdSite = new \SoapVar($IdSite, XSD_INTEGER, null, null, null, self::MBZ_WS_NAMESPACE);
        $struct->Login = new \SoapVar($Login, XSD_STRING, null, null, null, self::MBZ_WS_NAMESPACE);
        $struct->Password = new \SoapVar($Password, XSD_STRING, null, null, null, self::MBZ_WS_NAMESPACE);
        $header_values = new \SoapVar($struct, SOAP_ENC_OBJECT, null, null,null, self::MBZ_WS_NAMESPACE);
        $header = new \SoapHeader(self::MBZ_WS_NAMESPACE, "MindbazAuthHeader", $header_values);
        return $header;
    }

    function create_campagne_light(\App\Models\Campagne $campagne, \App\Models\Sender $sender, \App\Models\Planning $planning = null, array $targetIds)
    {
        $routeur = Routeur::where('nom', 'Mindbaz')->first();

        //Webservice authentification
        $Login = $sender->nom;
        $Password = $sender->password;
        $IdSite = $sender->site_id;

        $idConfig = $sender->domaine;
        $idTestTarget = $sender->bat_liste;

        $Options = array();

        $expediteur = "";


        $html = $campagne->generateHtml($routeur);

        if(!empty($planning)){
            $planningid = $planning->id;
            if($planning->type == 'anonyme'){
                $expediteur = $campagne->expediteur_anonyme;
                $html = $campagne->generateHtmlAnonyme($routeur);
            }
        }

        if(empty($expediteur)){
            $expediteur = $campagne->expediteur;
        }


        $this->campaignSoapClient = new \SoapClient("https://webservice.mindbaz.com/campaign.asmx?WSDL");

        date_default_timezone_set('Europe/Paris');
        $date = (new \DateTime())->format('c');
        $CampaignParams = array(
            'campMode' => 'SIMPLE',
            'campType' => 'NEWSLETTER',
            'genSpeed' => $this->send_frequency,
            'hasTxtMsg' => false,
            'idConfig' => $idConfig,
            'idTestTarget' => $idTestTarget,
            'name' => $campagne->ref,
            'subject' => $campagne->objet,
            'nhdActive' => true,
            'useListUnsubscribe' => true,
            'responseAlias' => $expediteur,
            'senderAlias' => $expediteur,
            'creationDate' => $date,
            'lastUpdateDate' => $date);
        $Params = array('campParameters' => $CampaignParams, 'htmlMsg' => trim($html, " \t\n\r\0\x0B"), 'txtMsg' => null);

        $this->campaignSoapClient->__setSoapHeaders($this->createHeader($IdSite, $Login, $Password));

        $result = $this->campaignSoapClient -> CreateCampaign($Params);

        if(!$result or !isset($result->CreateCampaignResult) or !isset($result->CreateCampaignResult->idCampaign)){
            \Log::error('RouteurMindbaz : createCampagne CreateCampaign return error'.json_encode($result));
            return false;
        }

        $campagne_id = $result->CreateCampaignResult->idCampaign;

        $Params = array('idCampaign' => $campagne_id);
        $result = $this->campaignSoapClient -> TrackAll($Params);


        //on supprime la liste BAT par défaut
        $target = $this->remove_list($sender, 1, $campagne_id);

        if(!empty($targetIds)) {
            foreach($targetIds as $targetId)
            {
                $target = $this->get_list($sender, $targetId);
//            \Log::info(json_encode($target));
//            continue;
                $Params = array('idCampaign' => $campagne_id,
                    'newSegment' => array(
                        'name' => $target->GetTargetResult->parameters->name,
                        'idTarget' => $targetId,
                        'isExcluded' => false,
                        'isRandom' => false,
                        'percent' => 100,
                        'mailobject' => ''
                    ));
                $result = $this->campaignSoapClient->AddSegment($Params);
                //echo var_dump($result->AddSegmentResult);
                if (!isset($result) or !$result->AddSegmentResult) {
                    \Log::error('RouteurMindbaz : createCampagne AddSegment return error' . json_encode($result));
                    return false;
                }
            }

        }
        return $campagne_id;
    }

    function create_campagne(\App\Models\Campagne $campagne, \App\Models\Sender $sender, \App\Models\Planning $planning = null, array $targetIds)
    {
        //Webservice authentification
        $Login = $sender->nom;
        $Password = $sender->password;
        $IdSite = $sender->site_id;

        $idConfig = $sender->domaine;
        $idTestTarget = $sender->bat_liste;

        $Options = array();

        $expediteur = "";

        if(!empty($planning)){
            $planningid = $planning->id;
            if($planning->type == 'anonyme'){
                $expediteur = $campagne->expediteur_anonyme;
            }
        }

        if(empty($expediteur)){
            $expediteur = $campagne->expediteur;
        }

        $this->campaignSoapClient = new \SoapClient("https://webservice.mindbaz.com/campaign.asmx?WSDL");

        date_default_timezone_set('Europe/Paris');
        $date = (new \DateTime())->format('c');
        $CampaignParams = array(
            'campMode' => 'SIMPLE',
            'campType' => 'NEWSLETTER',
            'genSpeed' => $this->send_frequency,
            'hasTxtMsg' => false,
            'idConfig' => $idConfig,
            'idTestTarget' => $idTestTarget,
            'name' => $campagne->ref,
            'subject' => $campagne->objet,
            'nhdActive' => true,
            'useListUnsubscribe' => true,
            'responseAlias' => $expediteur,
            'senderAlias' => $expediteur,
            'creationDate' => $date,
            'lastUpdateDate' => $date);
        $Params = array('campParameters' => $CampaignParams, 'htmlMsg' => trim($campagne->html, " \t\n\r\0\x0B"), 'txtMsg' => null);

        $this->campaignSoapClient->__setSoapHeaders($this->createHeader($IdSite, $Login, $Password));

        $result = $this->campaignSoapClient -> CreateCampaign($Params);

        if(!$result or !isset($result->CreateCampaignResult) or !isset($result->CreateCampaignResult->idCampaign)){
            \Log::error('RouteurMindbaz : createCampagne CreateCampaign return error'.json_encode($result));
            return false;
        }

        $campagne_id = $result->CreateCampaignResult->idCampaign;

        $Params = array('idCampaign' => $campagne_id);
        $result = $this->campaignSoapClient -> TrackAll($Params);

        $target = $this->remove_list($sender, 1, $campagne_id);

        if(!empty($targetIds)) {
            //on supprime la liste BAT par défaut
//            $target = $this->remove_list($sender, 1, $campagne_id);

            $excluded_lists = \DB::table('excluded_lists')
                ->select('list_id')
                ->where('planning_id', $planning->id)
                ->get();

//            \Log::info('before if count excluded lists - '.count($excluded_lists)." ".json_encode($excluded_lists));
            if(count($excluded_lists)>0)
            {
//                \Log::info('before excluded lists');
                $excluded = $this->exclude_lists($sender, $campagne_id, $excluded_lists);
//                \Log::info('after excluded lists');
                if(!$excluded){
                    \Log::error('RouteurMindbaz : createCampagne exclude_lists return error');
                    return false;
                }
            }
//            \Log::info('after if count excluded lists - '.count($excluded_lists)." ".json_encode($excluded_lists));

            foreach($targetIds as $targetId)
            {
                $target = $this->get_list($sender, $targetId);
//            \Log::info(json_encode($target));
//            continue;
                $Params = array('idCampaign' => $campagne_id,
                    'newSegment' => array(
                        'name' => $target->GetTargetResult->parameters->name,
                        'idTarget' => $targetId,
                        'isExcluded' => false,
                        'isRandom' => false,
                        'percent' => 100,
                        'mailobject' => ''
                    ));
                $result = $this->campaignSoapClient->AddSegment($Params);
                //echo var_dump($result->AddSegmentResult);
                if (!isset($result) or !$result->AddSegmentResult) {
                    \Log::error('RouteurMindbaz : createCampagne AddSegment return error' . json_encode($result));
                    return false;
                }
            }

        }
//        echo "<br/> CAMPAGNE ID -- ".$result->CreateCampaignResult->idCampaign;
//        echo var_dump($resultat);
        return $campagne_id;
    }

    function send_bat_mindbaz($cid)
    {
        $senders = Sender::where('routeur_id', $this->routeur->id)->get();

        foreach($senders as $sender)
        {
            $Login = $sender->nom;
            $Password = $sender->password;
            $IdSite = $sender->site_id;

            $this->campaignSoapClient = new \SoapClient("https://webservice.mindbaz.com/campaign.asmx?WSDL");
            $this->campaignSoapClient->__setSoapHeaders($this->createHeader($IdSite, $Login, $Password));

            $Params = array('idCampaign' => $cid);
            $resultat = $this->campaignSoapClient->SendBAT($Params);
            if(!$resultat){
                \Log::error('RouteurMindbaz : send_bat return error'.json_encode($resultat));
                return false;
            }
        }
        return true;
    }

    function send_bat($campagne_id)
    {
        $bat = \App\Models\Campagne::find($campagne_id);

        $routeur = Routeur::where('nom', 'Mindbaz')->first();
        \Cache::forget('c' . $bat->id . '-r'.$routeur->id);

        $html = $bat->generateHtml($routeur);

        $sender = Sender::where('routeur_id', $this->routeur->id)
            ->where('quota', '>', 0)
            ->where('is_bat', 1)
            ->orderBy('id','desc')
            ->first();

        $Login = $sender->nom;
        $Password = $sender->password;
        $IdSite = $sender->site_id;
        $idConfig = $sender->domaine;
        $idTestTarget = $sender->bat_liste;

        $this->campaignSoapClient = new \SoapClient("https://webservice.mindbaz.com/campaign.asmx?WSDL");

        date_default_timezone_set('Europe/Paris');
        $date = (new \DateTime())->format('c');
        $CampaignParams = array(
            'campMode' => 'SIMPLE',
            'campType' => 'NEWSLETTER',
            'genSpeed' => $this->send_frequency,
            'hasTxtMsg' => false,
            'idConfig' => $idConfig,
            'idTestTarget' => $idTestTarget,
            'name' => $bat->ref,
            'subject' => "[MB - BAT]".$bat->objet,
            'nhdActive' => true,
            'useListUnsubscribe' => true,
            'responseAlias' => $bat->expediteur,
            'senderAlias' => $bat->expediteur,
            'creationDate' => $date,
            'lastUpdateDate' => $date);

        $Params = array('campParameters' => $CampaignParams, 'htmlMsg' => $html, 'txtMsg' => null);

        $this->campaignSoapClient->__setSoapHeaders($this->createHeader($IdSite, $Login, $Password));

        $result = $this->campaignSoapClient -> CreateCampaign($Params);

        if(!$result or !isset($result->CreateCampaignResult) or !isset($result->CreateCampaignResult->idCampaign)){
            \Log::error('RouteurMindbaz : createCampagne CreateCampaign return error'.json_encode($result));
            return 'false';
        }

        $cid = $result->CreateCampaignResult->idCampaign;

        $Params = array('idCampaign' => $cid);
        $result = $this->campaignSoapClient -> TrackAll($Params);


        $Params = array('idCampaign' => $cid);
        $resultat = $this->campaignSoapClient->SendBAT($Params);
        if(!$resultat){
            \Log::error('RouteurMindbaz : send_bat return error'.json_encode($resultat));
            return 'false';
        }

        return 'true';
    }

    function show_lists(\App\Models\Sender $sender)
    {
        if($sender->routeur->nom != 'Mindbaz'){
            \Log::error("RouteurMindbaz : show_lists $sender->nom is not a Mindbaz sender");
            exit;
        }

        $lists = array();

        //Webservice authentification
        $Login = $sender->nom;
        $Password = $sender->password;
        $IdSite = $sender->site_id;

        $Options = array();

        $compteur = 0;
        $start = 0;
        $limit = 10;
        $nbBoucles = 1;

        $this->soapClient = new \SoapClient("https://webservice.mindbaz.com/target.asmx?WSDL");

        $this->soapClient->__setSoapHeaders($this->createHeader($IdSite, $Login, $Password));

        while($compteur < $nbBoucles)
        {
            $params = array('location' => "allLocation",
                'start'=> $start,
                'limit'=>$limit,
                'sortField'=>'idTarget',
                'sortDir' => 'ASC',
                'nameFilter' => '');

//            echo "<br/> COMPTEUR $compteur -- NB BOUCLES $nbBoucles -- start $start";
            $result = $this->soapClient->List($params);
//            echo 'total = ' . $result->ListResult->total;

            $nbBoucles = ceil($result->ListResult->total / $limit);
            if (property_exists($result->ListResult, 'records')) {
//                dd($result);
                $conf = $result->ListResult->records;
                if (gettype($conf) == 'array') {
                   for ($i = 0; $i < count($conf); $i++) {
//                       echo var_dump($conf[$i]);
                   }
                    $lists = array_merge($lists, $conf);
                } else {
//                    echo var_dump($conf->TargetListRecords);
                    $lists = array_merge($lists, $conf->TargetListRecords);

                }
            }

            $start += $limit;
            $compteur++;
        }
        return $lists;
    }

    function import_list(\App\Models\Sender $sender, $filename)
    {
        if($sender->routeur->nom != 'Mindbaz'){
            \Log::error("RouteurMindbaz : import_list $sender->nom is not a Mindbaz sender");
            exit;
        }

        $extension = \File::extension($filename);
        $name = \File::name($filename);

        $ftp = \DB::table('ftp_mindbaz')
        ->where('sender_id',$sender->id)
        ->first();

        $host = $ftp->ftp_host;
        $login = $ftp->ftp_login;
        $pass = $ftp->ftp_pass;
        $folder = $ftp->ftp_folder;

        $connexion_id = ssh2_connect($host, 22);
        $logged = ssh2_auth_password($connexion_id, $login, $pass);

        $sftp = ssh2_sftp($connexion_id);

        $fh = fopen($filename, "r");

        $stream = fopen("ssh2.sftp://$sftp/$folder/$name.$extension", 'w');

        $content = "";

        while (!feof($fh))
        {
            $row = fgets($fh, 1024);
            $content .= $row;
        }

        fwrite($stream, $content);

        $admin = \DB::table('user_group')->whereIn('name', ["Admin", "Super Admin"])->get();
        $admin_users = \DB::table('users')->whereIn('user_group_id', array_pluck($admin,'id'))->get();

        //Webservice authentification
        $Login = $sender->nom;
        $Password = $sender->password;
        $IdSite = $sender->site_id;

        $Options = array();

        $this->soapClient = new \SoapClient("https://webservice.mindbaz.com/import.asmx?WSDL");

        $this->soapClient->__setSoapHeaders($this->createHeader($IdSite, $Login, $Password));

        $result = $this->soapClient -> ScheduleImportNow(array(
                'importParameters' => array(
                    'filename'=>"$name.$extension",
                    'codepage'=>1252 ,
                    'columnDelimiter'=>';',
                    'columnNamesFirstLine'=>false,
                    'deleteFile'=>false,
                    'emailNotification'=> implode(";",array_pluck($admin_users,"email")),
                    'insertEnabled'=>true,
                    'keepFld7'=>false,
                    'keyDbFieldName'=>'fld0',
                    'rowDelimiter'=>'\\r\\n',
                    'startLine'=>'1',
//                    'textQualifier'=>'"',
                    'safeMode'=> false,
                    'columnMapping' => array(
                        array('dbFieldName'=> 'fld43','fileColumnName'=>'col0','idMappingMode'=>1),
                        array('dbFieldName'=> 'fld1','fileColumnName'=>'col1','idMappingMode'=>1),
                        array('dbFieldName'=> 'fld0','fileColumnName'=>'col2','idMappingMode'=>1),
                        array('dbFieldName'=> 'fld100','fileColumnName'=>'col3','idMappingMode'=>1),
//                        array('dbFieldName'=> 'fld15','fileColumnName'=>'prenom','idMappingMode'=>1),
//                        array('dbFieldName'=> 'fld13','fileColumnName'=>'civ','idMappingMode'=>1,'format'=>'ID')
                    )
                )
            )
        );

        return $result;
    }

    function get_unsubscribers()
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $today = date('Ymd');

        $host = getenv('MB_FTP_HOST');
        $login = getenv('MB_FTP_LOGIN');
        $pass  = getenv('MB_FTP_PASS');
        $folder  = getenv('MB_FTP_FOLDER');
        if(!empty($folder) and strpos($folder, "/") === false) {
            $folder = $folder ."/" ;
        }

        $stream = fopen(storage_path('desinscrits')."/unsubscribe_mb_$today.csv", "w");

        $connexion_id = ssh2_connect($host, 22);
        $logged = ssh2_auth_password($connexion_id, $login, $pass);

        $sftp = ssh2_sftp($connexion_id);

        $fh = fopen("ssh2.sftp://$sftp/".$folder."EXPORT/unsubs_$today.csv", 'r');

        $compteur = 0;
        while (!feof($fh))
        {
            $row = fgets($fh, 1024);

            if (strpos($row, ';') !== false) {
                $cells = explode(';', $row);
            } elseif (strpos($row, '|') !== false) {
                $cells = explode('|', $row);
            } else {
                $cells[] = trim($row, " \r\n\t\"");
            }

            $email = trim($cells[0], " \r\n\t\"");

            $cr = \DB::table('campagnes_routeurs')
                ->select('campagne_id', 'planning_id')
                ->where('cid_routeur', trim($cells[3], " \r\n\t\""))
                ->first();

            $ligne = $email . ";" . trim($cells[1], " \r\n\t\"");

            if (!empty($cr)) {
                $ligne .= ";$cr->campagne_id;$cr->planning_id";
            }

            $ligne .= "\n";

            fwrite($stream, $ligne);
            $compteur++;
        }


        fclose($fh);
        fclose($stream);

        return $compteur;
    }

    function get_clickers()
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $today = date('Ymd');

        $host = getenv('MB_FTP_HOST');
        $login = getenv('MB_FTP_LOGIN');
        $pass  = getenv('MB_FTP_PASS');
        $folder  = getenv('MB_FTP_FOLDER');
        if(!empty($folder) and strpos($folder, "/") === false) {
            $folder = $folder ."/" ;
        }

        $stream = fopen(storage_path('cliqueurs')."/clics_mb_$today.csv", "w");

        $connexion_id = ssh2_connect($host, 22);
        $logged = ssh2_auth_password($connexion_id, $login, $pass);

        $sftp = ssh2_sftp($connexion_id);

        $fh = fopen("ssh2.sftp://$sftp/".$folder."EXPORT/clicks_$today.csv", 'r');

        $cliqueurs = array();

        while (!feof($fh))
        {
            $row = fgets($fh, 1024);

            if (strpos($row, ';') !== false) {
                $cells = explode(';', $row);
            } elseif (strpos($row, '|') !== false) {
                $cells = explode('|', $row);
            } else {
                $cells[] = trim($row, " \r\n\t\"");
            }

            if($cells[5] != 'Edito'){
                continue;
            }

            $email = trim($cells[0], " \r\n\t\"");
            $cr = \DB::table('campagnes_routeurs')
                ->select('campagne_id')
                ->where('cid_routeur', trim($cells[2], " \r\n\t\""))
                ->first();

            if(!isset($cliqueurs[$cr->campagne_id])){
                $cliqueurs[$cr->campagne_id] = array();
            }

            $cliqueurs[$cr->campagne_id][$email]="";
        }

        $compteur = 0;
        foreach($cliqueurs as $campagne_id => $emails)
        {
            $camp = \DB::table('campagnes')
                ->select('theme_id')
                ->where('id', $campagne_id)
                ->first();

            foreach($emails as $eml)
            {
                $ligne = "$campagne_id;$eml->mail;$camp->theme_id\n";
                fwrite($stream, $ligne);
                $compteur++;
            }
        }
        fclose($stream);
        fclose($fh);
        return $compteur;
    }

    function get_bounces()
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $today = date('Ymd');

        $host = getenv('MB_FTP_HOST');
        $login = getenv('MB_FTP_LOGIN');
        $pass  = getenv('MB_FTP_PASS');
        $folder  = getenv('MB_FTP_FOLDER');
        if(!empty($folder) and strpos($folder, "/") === false) {
            $folder = $folder ."/" ;
        }

        $stream = fopen(storage_path('bounces')."/bounces_mb_$today.csv", "w");

        $connexion_id = ssh2_connect($host, 22);
        $logged = ssh2_auth_password($connexion_id, $login, $pass);

        $sftp = ssh2_sftp($connexion_id);

        $fh = fopen("ssh2.sftp://$sftp/".$folder."EXPORT/npai_$today.csv", 'r');

        $compteur = 0;

        while (!feof($fh))
        {
            $row = fgets($fh, 1024);

            if (strpos($row, ';') !== false) {
                $cells = explode(';', $row);
            } elseif (strpos($row, '|') !== false) {
                $cells = explode('|', $row);
            } else {
                $cells[] = trim($row, " \r\n\t\"");
            }

            if($cells[2] != 'HardBounce'){
                continue;
            }

            $email = trim($cells[0], " \r\n\t\"");

            $cr = \DB::table('campagnes_routeurs')
                ->select('campagne_id')
                ->where('cid_routeur', trim($cells[3], " \r\n\t\""))
                ->first();

            $ligne = "$cr->campagne_id;$email;".$cells[5]."\n";
            fwrite($stream, $ligne);
            $compteur++;
        }
        fclose($stream);
        fclose($fh);
        return $compteur;
    }

    function get_openers()
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $today = date('Ymd');

        $host = getenv('MB_FTP_HOST');
        $login = getenv('MB_FTP_LOGIN');
        $pass  = getenv('MB_FTP_PASS');
        $folder  = getenv('MB_FTP_FOLDER');
        if(!empty($folder) and strpos($folder, "/") === false) {
            $folder = $folder ."/" ;
        }

        $stream = fopen(storage_path('bounces')."/openings_mb_$today.csv", "w");

        $connexion_id = ssh2_connect($host, 22);
        $logged = ssh2_auth_password($connexion_id, $login, $pass);

        $sftp = ssh2_sftp($connexion_id);

        $fh = fopen("ssh2.sftp://$sftp/".$folder."EXPORT/openings_$today.csv", 'r');

        $ouvreurs = array();

        while (!feof($fh))
        {
            $row = fgets($fh, 1024);
            fwrite($stream, $row);

            if (strpos($row, ';') !== false) {
                $cells = explode(';', $row);
            } elseif (strpos($row, '|') !== false) {
                $cells = explode('|', $row);
            } else {
                $cells[] = trim($row, " \r\n\t\"");
            }

            $campagne_id = trim($cells[2], " \r\n\t\"");;
            $email = trim($cells[0], " \r\n\t\"");

            $cr = \DB::table('campagnes_routeurs')
                ->select('campagne_id')
                ->where('cid_routeur', $campagne_id)
                ->first();

            if(!isset($ouvreurs[$cr->campagne_id])){
                $ouvreurs[$cr->campagne_id]=array();
            }

            $ouvreurs[$cr->campagne_id] = $ouvreurs[$cr->campagne_id] + [$email, $cells[4], $cells[5], $cells[6]];
        }
        return $ouvreurs;
    }

    function get_list(\App\Models\Sender $sender, $list_id)
    {
        if($sender->routeur->nom != 'Mindbaz'){
            \Log::error("RouteurMindbaz : show_lists $sender->nom is not a Mindbaz sender");
            exit;
        }

        $lists = array();

        //Webservice authentification
        $Login = $sender->nom;
        $Password = $sender->password;
        $IdSite = $sender->site_id;

        $Options = array();

        $this->soapClient = new \SoapClient("https://webservice.mindbaz.com/target.asmx?WSDL");

        $this->soapClient->__setSoapHeaders($this->createHeader($IdSite, $Login, $Password));

        $result = $this->soapClient -> GetTarget(array('idTarget' => $list_id));

        return $result;
    }

    function schedule_campaign(\App\Models\Sender $sender, $cid, $datetime)
    {
        $Login = $sender->nom;
        $Password = $sender->password;
        $IdSite = $sender->site_id;

        $latest = \Carbon\Carbon::now()->addMinutes(5);
        $wanted = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$datetime);

        $diff = $latest->diffInMinutes($wanted, false);

        \Log::info($diff);

        if($diff <= 0){
            $datetime = $latest;
            \Log::info(json_encode($datetime));
        }

        $date = (new \DateTime($datetime))->format('c');

        $Params = array('idCampaign' => $cid,
            'calendarDates' => array(
                $date
            )
        );

        $this->soapClient = new \SoapClient("https://webservice.mindbaz.com/campaign.asmx?WSDL");

        $this->soapClient->__setSoapHeaders($this->createHeader($IdSite, $Login, $Password));

        $result = $this-> soapClient -> Schedule($Params);
        return $result;
    }

    function calculate_spamscore(\App\Models\Sender $sender, $cid, $subscriber_id=null)
    {
        $scores_sa = array();
        $scores_vr = array();

        $Login = $sender->nom;
        $Password = $sender->password;
        $IdSite = $sender->site_id;

        $this->soapClient = new \SoapClient("https://webservice.mindbaz.com/campaign.asmx?WSDL");

        $this->soapClient->__setSoapHeaders($this->createHeader($IdSite, $Login, $Password));

        $Params = array('idCampaign' => $cid);

        $result = $this-> soapClient -> GetAllSegments($Params);

        if (!isset($result->GetAllSegmentsResult) or !isset($result->GetAllSegmentsResult->Segment)) {
            \Log::error('RouteurMindbaz@calculate_spamscore : no results');
            return false;
        }

        $tab = $result->GetAllSegmentsResult->Segment;
        //\Log::info(json_encode($result->GetAllSegmentsResult->Segment));
        if(!is_array($tab)){
            $obj = $tab;
            $tab = array();
            $tab[] = $obj;
        }

        foreach($tab as $segment)
        {
            $Params = array(
                'idCampaign' => $cid,
                'segmentPosition' => $segment->position,
//            'idSubscriber' => $idSubscriber
                'idSubscriber' => 1 // TODO : par défaut à 1 car ne change pas la valeur du score suivant les ID, à changer ?
            );

            $result = $this->soapClient->GetSpamScore($Params);

//            $segment_name = str_replace("segment", '',$segment->parameters->name);
            $segment_name = $segment->parameters->name;

            if (!isset($result->GetSpamScoreResult->score) or !isset($result->GetSpamScoreResult->vrspamScore)) {
                $scores_sa[$segment_name] = 0;
                $scores_vr[$segment_name] = 0;
                continue;
            }

            $scores_sa[$segment_name] = $result->GetSpamScoreResult->score;
            $scores_vr[$segment_name] = $result->GetSpamScoreResult->vrspamScore;
        }
        return [$scores_sa, $scores_vr] ;
    }

    function get_subscriberid($sender, $subscriber_email)
    {
        $Login = $sender->nom;
        $Password = $sender->password;
        $IdSite = $sender->site_id;

        $this->soapClient = new \SoapClient("https://webservice.mindbaz.com/subscriberv2.asmx?WSDL");

        $this->soapClient->__setSoapHeaders($this->createHeader($IdSite, $Login, $Password));

        $Params = array('email' => $subscriber_email, 'idFields' => array(0));

        $result = $this-> soapClient -> GetSubscriberByEmail($Params);

        if(empty($result)){
            return false;
        }

        return $result->GetSubscriberByEmailResult;
    }

    function get_subscribersids($sender, array $subscribers_email)
    {
        $Login = $sender->nom;
        $Password = $sender->password;
        $IdSite = $sender->site_id;

        $this->soapClient = new \SoapClient("https://webservice.mindbaz.com/subscriberv2.asmx?WSDL");

        $this->soapClient->__setSoapHeaders($this->createHeader($IdSite, $Login, $Password));

        $subscribers_ids = array();

        $Params = array();
        $Params['emails'] = $subscribers_email;
        $Params['idFields'] = array(0);

        $result = $this-> soapClient -> GetSubscribersByEmail($Params);

        if(empty($result)){
            return array();
        }
        return $result->GetSubscribersByEmailResult->Subscriber;
    }

    function set_unsubscribers($sender, array $subscribers_ids)
    {
        $Login = $sender->nom;
        $Password = $sender->password;
        $IdSite = $sender->site_id;

        $this->soapClient = new \SoapClient("https://webservice.mindbaz.com/subscriberv2.asmx?WSDL");

        $this->soapClient->__setSoapHeaders($this->createHeader($IdSite, $Login, $Password));

        $false_unsubscribe = 0;

        foreach ($subscribers_ids as $sid)
        {
            $Params = array('idSubscriber' => $sid);
            $result = $this->soapClient->Unsubscribe($Params);
            if(!$result){
                $false_unsubscribe++;
            }
        }

        if(!empty($false_unsubscribe)){
            \Log::warning("RouteurMindbaz@set_unsubscribers : $false_unsubscribe");
            return $false_unsubscribe;
        }
        return true;
    }

    function recup_stats($sender, $cid)
    {
        $Login = $sender->nom;
        $Password = $sender->password;
        $IdSite = $sender->site_id;

        $this->soapClient = new \SoapClient("https://webservice.mindbaz.com/statisticsV2.asmx?WSDL");

        $this->soapClient->__setSoapHeaders($this->createHeader($IdSite, $Login, $Password));

        $result = $this-> soapClient -> GetSentsStatistics3(
            array(
                'idCampaign' => $cid,  //id de la campagne
                'nbDays' => 14, //lunette de 14 jours
                'mode' => 0)
        );
//        var_dump($result);
        if(empty($result) or !isset($result->GetSentsStatistics3Result) or !isset($result->GetSentsStatistics3Result->SentStatistics)){
            \Log::warning("RouteurMindbaz@recup_stats : ".json_encode($result));
            return false;
        }

        $stats = $result->GetSentsStatistics3Result->SentStatistics;
        if(! is_array($stats)){
            $stats = array();
            $obj = $result->GetSentsStatistics3Result->SentStatistics;
            $stats[] = $obj;
        }
        //var_dump($stats);
        return $stats;
    }

    function delete_campaign($sender, $cid)
    {
        $Login = $sender->nom;
        $Password = $sender->password;
        $IdSite = $sender->site_id;

        $this->soapClient = new \SoapClient("https://webservice.mindbaz.com/campaign.asmx?WSDL");

        $this->soapClient->__setSoapHeaders($this->createHeader($IdSite, $Login, $Password));

        $Params = array('idCampaign' => $cid);
        $result = $this->soapClient-> DeleteCampaign( $Params );
        if(!$result){
            \Log::error('RouteurMindbaz@delete_campaign : return error'.json_encode($result));
            return false;
        }
        return true;
    }

    function remove_list($sender, $position_id, $cid)
    {
        $Login = $sender->nom;
        $Password = $sender->password;
        $IdSite = $sender->site_id;

        $this->soapClient = new \SoapClient("https://webservice.mindbaz.com/campaign.asmx?WSDL");

        $this->soapClient->__setSoapHeaders($this->createHeader($IdSite, $Login, $Password));

        $Params = array('idCampaign' => $cid, 'segmentPosition' => $position_id);
        $result = $this->soapClient-> DeleteSegment( $Params );
        if(!$result){
            \Log::error('RouteurMindbaz@remove_list : return error'.json_encode($result));
            return false;
        }
        return true;
    }

    function calculate_volume($sender, $cid)
    {
        $count = 0;
        $Login = $sender->nom;
        $Password = $sender->password;
        $IdSite = $sender->site_id;

        $this->soapClient = new \SoapClient("https://webservice.mindbaz.com/campaign.asmx?WSDL");

        $this->soapClient->__setSoapHeaders($this->createHeader($IdSite, $Login, $Password));

        $Params = array('idCampaign' => $cid);
        $result = $this->soapClient-> CountAllSegments( $Params );
        if(!$result){
            \Log::error('RouteurMindbaz@calculate_volume : return error'.json_encode($result));
            return 0;
        }
        return $result->CountAllSegmentsResult;
    }

    function exclude_lists($sender, $cid, $excluded_lists)
    {
        $Login = $sender->nom;
        $Password = $sender->password;
        $IdSite = $sender->site_id;

        $this->campaignSoapClient = new \SoapClient("https://webservice.mindbaz.com/campaign.asmx?WSDL");

        $this->campaignSoapClient->__setSoapHeaders($this->createHeader($IdSite, $Login, $Password));

        foreach($excluded_lists as $el)
        {
//            \Log::info(json_encode($el));
//            continue;
            $target = $this->get_list($sender, $el->list_id);
            $Params = array('idCampaign' => $cid,
                'newSegment' => array(
                    'name' => $target->GetTargetResult->parameters->name,
                    'idTarget' => $el->list_id,
                    'isExcluded' => true,
                    'isRandom' => false,
                    'percent' => 100,
                    'mailobject' => ''
                ));
            $result = $this->campaignSoapClient->AddSegment($Params);
            //echo var_dump($result->AddSegmentResult);
//            \Log::info('RouteurMindbaz@exclude_lists : return '.json_encode($result));
            if (!isset($result) or !$result->AddSegmentResult) {
                \Log::error('RouteurMindbaz@exclude_lists : return error'.json_encode($result));
                return false;
            }
        }
        return true;
    }

    function check_status($sender, $name_filter, $cid_routeur)
    {
        $Login = $sender->nom;
        $Password = $sender->password;
        $IdSite = $sender->site_id;

        $this->soapClient = new \SoapClient("https://webservice.mindbaz.com/campaign.asmx?WSDL");
        $this->soapClient->__setSoapHeaders($this->createHeader($IdSite, $Login, $Password));

        $params = array('location' => "",
            'start'=> 0,
            'limit'=> 1,
            'sortField'=>'creationDate',
            'sortDir' => 'DESC',
            'nameFilter' => $name_filter,
//            'campMode'=> 'SIMPLE',
            'campType' => 'NEWSLETTER',
        );

        $result = $this->soapClient->List( $params );
//        echo 'total = ' . $result->ListResult->total;
//        $camp = $result->ListResult->records;
//        echo var_dump($result);
        if( !property_exists($result->ListResult, 'records') or $result->ListResult->records->CampaignListRecords->idCampaign != $cid_routeur) {
            echo "HERE";

            \Log::error("RouteurMindbaz@check_status : La campagne n'existe pas (CID $cid_routeur)");
            return false;
        }

        $camp = $result->ListResult->records;
//        echo  var_dump($camp->CampaignListRecords->schedulingInfos);
        $status = $camp->CampaignListRecords->schedulingInfos->progState;
        //TODO: enlever sending status ?
        if($status == 'SENDING' or  $status == 'SENDING_FINISHED' or $status == 'SENDING_FINISHED' or $status == 'SENDING_FINISHED_WITH_ERRORS' ){
            return true;
        }
        return false;
    }

    function create_list($sender, $filepath)
    {
        $Login = $sender->nom;
        $Password = $sender->password;
        $IdSite = $sender->site_id;

        /* Fabien Modif
        // old
        $host = getenv('MB_FTP_HOST');
        $login = getenv('MB_FTP_LOGIN');
        $pass  = getenv('MB_FTP_PASS');
        $folder  = getenv('MB_FTP_FOLDER');
        */

        $ftp = \DB::table('ftp_mindbaz')
        ->where('sender_id',$sender->id)
        ->first();

        $host = $ftp->ftp_host;
        $login = $ftp->ftp_login;
        $pass = $ftp->ftp_pass;
        $folder = $ftp->ftp_folder;

        $expiry_date = date('Y-m-d', strtotime(date("Y-m-d"). ' + 2 days'));

        $extension = \File::extension($filepath);
        $name = \File::name($filepath);

        // $host = getenv('MB_FTP_HOST');
        // $login = getenv('MB_FTP_LOGIN');
        // $pass  = getenv('MB_FTP_PASS');
        // $folder  = getenv('MB_FTP_FOLDER');
        if(!empty($folder) and strpos($folder, "/") === false) {
            $folder = $folder ."/" ;
        }

        $fh = fopen($filepath, "r");

        $connexion_id = ssh2_connect($host, 22);
        $logged = ssh2_auth_password($connexion_id, $login, $pass);

        $sftp = ssh2_sftp($connexion_id);
        $stream = fopen("ssh2.sftp://$sftp/".$folder."$name.$extension", 'w');

        $content_to_copy = "";

        while (!feof($fh))
        {
            $row = fgets($fh, 1024);
            $content_to_copy .= $row;
        }

        fwrite($stream, $content_to_copy);
        fclose($fh);
        fclose($stream);

        $this->soapClient = new \SoapClient("https://webservice.mindbaz.com/target.asmx?WSDL");
        $this->soapClient->__setSoapHeaders($this->createHeader($IdSite, $Login, $Password));

        $result = $this->soapClient -> CreateTarget(array(
            'targetParameters' => array(
                'name'=> $name,
                'isTestTarget'=>false,
                'pctSelect'=>100,
                'maxSelect'=>-1,
                'isRandomMode'=>false,
                'excludedTargets'=>null
            )));

        $idTarget = $result->CreateTargetResult->idTarget;

        //on ajoute un filtre par champ sur "l'etat abonné (fld7) = abonné (0)" pour ne pas cibler des désabonnés
        $result = $this->soapClient -> SetTargetFieldFilter(array(
            'fieldFilters' => array(
                'idTarget' => $idTarget,
                'isFilterEnabled' => true,
                'filters' => array(
                    array('idField'=> 7,'fieldOperator'=>'EGAL_A','fieldValue'=>'0'),
                    array('idField'=> 100,'fieldOperator'=>'EGAL_A','fieldValue'=> getenv('CLIENT_TITLE')) // pour  cibler les abonnes du "BON TOR" car SC2 = 3 TORs
                )
            )
        ));
        //ajout du filtre par fichier
        $result = $this->soapClient -> SetTargetFileFilter(array(
            'fileFilter' => array(
                'idTarget' => $idTarget,
                'isFilterEnabled' => true,
//                'filename'=> "filterfile\$name.$extension",
                'filename'=> "$name.$extension",
                'dbFieldName'=>'fld43',
                'fileAutomaticDeletionDate'=> $expiry_date
            )

        ));

//        echo $this->soapClient -> GetTargetDesc(array('idTarget'=>$idTarget))->GetTargetDescResult;
        //désactivation du filtre par fichier dans notre cible
//        $result = $this->soapClient -> SetTargetFileFilter(array(
//            'fileFilter' => array(
//                'idTarget' => $idTarget,
//                'isFilterEnabled' => false,
//                'filename'=> '',
//                'dbFieldName'=>'',
//                'fileAutomaticDeletionDate'=> $expiry_date
//            )
//
//        ));
        return $idTarget;
    }

    function count_subscribers($sender)
    {
        $count = 0;
        $Login = $sender->nom;
        $Password = $sender->password;
        $IdSite = $sender->site_id;

        $this->soapClient = new \SoapClient("https://webservice.mindbaz.com/SubscriberV2.asmx?WSDL");

        $this->soapClient->__setSoapHeaders($this->createHeader($IdSite, $Login, $Password));

        $result = $this->soapClient -> CountSubscribers();
        if(!empty($result->CountSubscribersResult)){
            //echo $result->CountSubscribersResult;
            $count = $result->CountSubscribersResult;
            \Log::info("MINDBAZ $sender->name -- COUNT SUBSCRIBERS -- ".$count);
        }
        return $count;
    }


    function get_subscriberid_all($sender, $subscribers_email)
    {
        $Login = $sender->nom;
        $Password = $sender->password;
        $IdSite = $sender->site_id;
        $this->soapClient = new \SoapClient("https://webservice.mindbaz.com/subscriberv2.asmx?WSDL",['cache_wsdl' => WSDL_CACHE_NONE]);

        $this->soapClient->__setSoapHeaders($this->createHeader($IdSite, $Login, $Password));
        $result = $this-> soapClient -> GetSubscribersByEmail(array('emails' => $subscribers_email, 'idFields' => array(0,1,2,3,4)));
        if(empty($result)){
            return false;
        }

        return $result->GetSubscribersByEmailResult;
    }

    function get_subscriberid_all_npai($sender, $subscribers_email)
    {
        $Login = $sender->nom;
        $Password = $sender->password;
        $IdSite = $sender->site_id;
        $this->soapClient = new \SoapClient("https://webservice.mindbaz.com/subscriberv2.asmx?WSDL",['cache_wsdl' => WSDL_CACHE_NONE]);

        $this->soapClient->__setSoapHeaders($this->createHeader($IdSite, $Login, $Password));
        $result = $this-> soapClient -> GetSubscribersByEmail(array('emails' => $subscribers_email, 'idFields' => array(0,1,2,3,4,5,6,7)));
        if(empty($result)){
            return false;
        }

        return $result->GetSubscribersByEmailResult;
    }


    function get_subscriberbyidclient_all($sender, $subscribers_email)
    {
        $Login = $sender->nom;
        $Password = $sender->password;
        $IdSite = $sender->site_id;
        $this->soapClient = new \SoapClient("https://webservice.mindbaz.com/subscriberv2.asmx?WSDL",['cache_wsdl' => WSDL_CACHE_NONE]);

        $this->soapClient->__setSoapHeaders($this->createHeader($IdSite, $Login, $Password));
        // var_dump($subscribers_email);
        $result = $this-> soapClient -> GetSubscriberByIdClient(array('idClient' => $subscribers_email, 'idFields' => array(0,1,2,3,4,5,6,7)));
        if(empty($result)){
            return false;
        }

        return $result->GetSubscriberByIdClientResult;
    }



}
