<?php namespace App\Classes\Routeurs;

use \App\Models\Destinataire;
use \App\Models\Clic;
use \App\Models\Ouverture;
use \App\Models\Campagne;
use \App\Models\Sender;
use \App\Models\Bounce;
use \App\Models\Routeur;
use \App\Models\CampagneRouteur;

class RouteurPhoenix
{
    private $routeur;
    private $client;
    private $header;

    public function __construct()
    {
        $routeurinfo = \DB::table('routeurs')->where('nom','Phoenix')->first();
        $mesinfo = \DB::table('senders')->where('routeur_id', $routeurinfo->id)->first();

        $this->routeur = Routeur::where('nom','Phoenix')->first();
        // $this->client = curl_init();
        // $this->header = array();
        // a rajouter api-key
        // $this->header[] = $mesinfo->password; //TODO : mettre une VARIABLE
        // $this->header[] = 'Content-Type: application/json';
        // $this->header[] = 'Accept: text/plain';
        // $this->header[] = 'Expect:';
    }

    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    //          FONCTION DE BASE DU ROUTEURS
    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    ////////
    //  ACCOUNT
    ///////
    function account($key)
    {
        $this->client = curl_init();
        $this->header = array();


        // $this->header[] = $mesinfo->password; //TODO : mettre une VARIABLE
        $this->header[] = 'api-key: '. $key;
        $this->header[] = 'Content-Type: application/json';
        $this->header[] = 'Accept: text/plain';
        $this->header[] = 'Expect:';

        curl_setopt($this->client, CURLOPT_URL, 'https://api.phoenix.wf/account/');
        curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);

        $resultat = curl_exec ($this->client);
        $response = json_decode($resultat);

        return $response;
    }

    function account_https($key)
    {
        $this->client = curl_init();
        $this->header = array();


        // $this->header[] = $mesinfo->password; //TODO : mettre une VARIABLE
        $this->header[] = 'api-key: '. $key;
        $this->header[] = 'Content-Type: application/json';
        $this->header[] = 'Accept: text/plain';
        $this->header[] = 'Expect:';

        curl_setopt($this->client, CURLOPT_URL, 'https://api.phoenix.wf/account/');
        curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
        curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

        $resultat = curl_exec ($this->client);
        $response = json_decode($resultat);

        return $response;
    }

    //*****************************************************************************************************************
    //*****************************************************************************************************************
    ////////
    //  KIT
    ///////
    function kits_get($key)
    {

        $this->client = curl_init();
        $this->header = array();

        $this->header[] = 'api-key: '. $key;
        $this->header[] = 'Content-Type: application/json';
        $this->header[] = 'Accept: text/plain';
        $this->header[] = 'Expect:';

        curl_setopt($this->client, CURLOPT_URL, 'https://api.phoenix.wf/kit/get/');
        curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
        curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);
        $resultat = curl_exec ($this->client);
        $response = json_decode($resultat);

        if(isset($response->error) && !$response->error){
            return $response;
        }
        \Log::error("[RouteurPhoenix] kits_get : " . $resultat);
        return false;
    }

    function kit_get($key, $id)
    {

        $this->client = curl_init();
        $this->header = array();

        $this->header[] = 'api-key: '. $key;
        $this->header[] = 'Content-Type: application/json';
        $this->header[] = 'Accept: text/plain';
        $this->header[] = 'Expect:';

        curl_setopt($this->client, CURLOPT_URL, 'https://api.phoenix.wf/kit/get/'.$id);
        curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
        curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

        $resultat = curl_exec ($this->client);
        $response = json_decode($resultat);

        if(isset($response->error) && !$response->error){
            return $response->kit_name;
        }
        \Log::error("[RouteurPhoenix] kit_get : ".$resultat);
        return false;
    }

    function kit_create($key, $name, $subject, $sender_name, $html)
    {

        $this->client = curl_init();
        $this->header = array();

        $this->header[] = 'api-key: '. $key;
        $this->header[] = 'Content-Type: application/json';
        $this->header[] = 'Accept: text/plain';
        $this->header[] = 'Expect:';

        $datas = array(
            'name'=>$name,
            'subject'=> $subject,
            'sender_name'=> $sender_name,
            'html' => $html
        );

        $data_json = json_encode($datas);

        curl_setopt($this->client, CURLOPT_URL, 'https://api.phoenix.wf/kit/create');
        curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
        curl_setopt($this->client, CURLOPT_POST, 1);
        curl_setopt($this->client, CURLOPT_POSTFIELDS,$data_json);
        curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

        $resultat = curl_exec ($this->client);
        //\Log::info($resultat);
        $response = json_decode($resultat);

        if(isset($response->error) && !$response->error){
            return $response->kit_id;
        }
        \Log::error("[RouteurPhoenix] kit_create : ".$resultat);
        return false;
    }

    function kit_edit($key, $name, $subject, $sender_name, $html)
    {

        $this->client = curl_init();
        $this->header = array();

        $this->header[] = 'api-key: '. $key;
        $this->header[] = 'Content-Type: application/json';
        $this->header[] = 'Accept: text/plain';
        $this->header[] = 'Expect:';

        $datas = array(
            'name'=>$name,
            'subject'=> $subject,
            'sender_name'=> $sender_name,
            'html' => $html
        );

        $data_json = json_encode($datas);

        curl_setopt($this->client, CURLOPT_URL, 'https://api.phoenix.wf/kit/create');
        curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
        curl_setopt($this->client, CURLOPT_POST, 1);
        curl_setopt($this->client, CURLOPT_POSTFIELDS,$data_json);
        curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

        $resultat = curl_exec ($this->client);
        $response = json_decode($resultat);
        if(isset($response->error) && !$response->error){
            return true; // le kit a été modifié
        }
        \Log::error("[RouteurPhoenix] kit_edit : ".$resultat);
        return false;
    }
    //*****************************************************************************************************************
    //*****************************************************************************************************************
    ////////
    //  LISTE
    ///////
    function listes_get($key)
    {

        $this->client = curl_init();
        $this->header = array();

        $this->header[] = 'api-key: '. $key;
        $this->header[] = 'Content-Type: application/json';
        $this->header[] = 'Accept: text/plain';
        $this->header[] = 'Expect:';

        curl_setopt($this->client, CURLOPT_URL, 'https://api.phoenix.wf/list');
        curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
        curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);
        $resultat = curl_exec ($this->client);
        $response = json_decode($resultat);

        // fermeture de la session cURL
        curl_close($this->client);

        return $response;
    }

    function listes_get_https($key)
    {

        $this->client = curl_init();
        $this->header = array();

        $this->header[] = 'api-key: '. $key;
        $this->header[] = 'Content-Type: application/json';
        $this->header[] = 'Accept: text/plain';
        $this->header[] = 'Expect:';

        curl_setopt($this->client, CURLOPT_URL, 'https://api.phoenix.wf/list');
        curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
        curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);
        $resultat = curl_exec ($this->client);
        $response = json_decode($resultat);

        // fermeture de la session cURL
        curl_close($this->client);

        return $response;
    }

    function liste_create($key, $name, $tags=null)
    {

        $this->client = curl_init();
        $this->header = array();

        $this->header[] = 'api-key: '. $key;
        $this->header[] = 'Content-Type: application/json';
        $this->header[] = 'Accept: text/plain';
        $this->header[] = 'Expect:';

        $datas = array();
        $datas['list_name'] = $name;
        if(!empty($tags)) {
            $datas['tags'] = [$tags];
        }
        $data_json = json_encode($datas);

        curl_setopt($this->client, CURLOPT_URL, 'https://api.phoenix.wf/list/create');
        curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
        curl_setopt($this->client, CURLOPT_POST, 1);
        curl_setopt($this->client, CURLOPT_POSTFIELDS,$data_json);
        curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

        $resultat = curl_exec ($this->client);
        $response = json_decode($resultat);

        // if(!$response->error){
        //     return $response->data->list_id; // le kit a été modifié
        // }
        // return false;

        if(isset($response->error) && !$response->error){
            return $response->data->list_id;
        }
        \Log::error("[RouteurPhoenix] liste_create : ".$resultat);
        return false;
    }
    //*****************************************************************************************************************
    //*****************************************************************************************************************
    ////////
    //  CONTACT
    ///////
    function contact_edit($key, $email,$fields)
    {

        $this->client = curl_init();
        $this->header = array();

        $this->header[] = 'api-key: '. $key;
        $this->header[] = 'Content-Type: application/json';
        $this->header[] = 'Accept: text/plain';
        $this->header[] = 'Expect:';

        curl_setopt($this->client, CURLOPT_URL, 'https://api.phoenix.wf/contact/edit');
        curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
        curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

        $resultat = curl_exec ($this->client);
        $response = json_decode($resultat);

        return $response;
    }

    function contact_add($key, $type=null, $contact=null, $list_id, $url, $credit=null)
    {

        $this->client = curl_init();
        $this->header = array();

        $this->header[] = 'api-key: '. $key;
        $this->header[] = 'Content-Type: application/json';
        $this->header[] = 'Accept: text/plain';
        $this->header[] = 'Expect:';

        $datas = array();

        if(!empty($type)){
            $datas['type'] = $type; //facultatif
        }
        if(!empty($contact)){
            $datas['contact'] = $contact; //facultatif
        }
        if(!empty($list_id)){
            $datas['list_id'] = $list_id;
        }
        if(!empty($url)){
            $datas['url'] = $url;
        }
        if(!empty($credit)){
            $datas['credit'] = $credit;
        }

        $data_json = json_encode($datas);

        curl_setopt($this->client, CURLOPT_URL, 'https://api.phoenix.wf/contact/add');
        curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
        curl_setopt($this->client, CURLOPT_POST, 1);
        curl_setopt($this->client, CURLOPT_POSTFIELDS,$data_json);
        curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

        $resultat = curl_exec ($this->client);
        $response = json_decode($resultat);

        if(isset($response->error) && !$response->error){
            return $response; // le kit a été modifié
        }
        \Log::error("[RouteurPhoenix] contact_add : ".$resultat);
        return false;
    }



    function contact_add_url($key,$list_id,$url){

        $this->client = curl_init();
        $this->header = array();

        $this->header[] = 'api-key: '. $key;
        $this->header[] = 'Content-Type: application/json';
        $this->header[] = 'Accept: text/plain';
        $this->header[] = 'Expect:';

        $datas = array();
        $datas['type'] = 'file';
        $datas['list_id'] = $list_id;
        $datas['url'] = $url;
        $data_json = json_encode($datas);
        // var_dump($data_json);
        curl_setopt($this->client, CURLOPT_URL, 'https://api.phoenix.wf/contact/add');
        curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
        curl_setopt($this->client, CURLOPT_POST, 1);
        curl_setopt($this->client, CURLOPT_POSTFIELDS,$data_json);
        curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);
        $resultat = curl_exec ($this->client);
        $response = json_decode($resultat);
        var_dump($response);
        // return false;


        if(isset($response->error) && !$response->error){
        return $response; // le kit a été modifié
        }
        \Log::error("[RouteurPhoenix] contact_add_url : ".$resultat);
        return false;
    }

    //*****************************************************************************************************************
    //*****************************************************************************************************************
    ////////
    //  CAMPAIGN
    ///////
    function campaign_getstat($key, $campaign_id)
    {

        $this->client = curl_init();
        $this->header = array();

        $this->header[] = 'api-key: '. $key;
        $this->header[] = 'Content-Type: application/json';
        $this->header[] = 'Accept: text/plain';
        $this->header[] = 'Expect:';

        $datas = array();
        $datas['campaign_id'] = $campaign_id;
        $data_json = json_encode($datas);

        curl_setopt($this->client, CURLOPT_URL, 'https://api.phoenix.wf/campaign/getstat');
        curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
        curl_setopt($this->client, CURLOPT_POST, 1);
        curl_setopt($this->client, CURLOPT_POSTFIELDS,$data_json);
        curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

        $resultat = curl_exec ($this->client);
        $response = json_decode($resultat);

        // echo 'debug';
        // var_dump($response);
        // die();
        if(isset($response)){
            return $response; // on retourne les infos stats de la campagne en question
        }
        \Log::error("[RouteurPhoenix] campaign_getstat : ".$resultat);
        return false;
    }

    function campaign_getallstat($key, $lower=null, $top=null)
    {

        $this->client = curl_init();
        $this->header = array();

        $this->header[] = 'api-key: '. $key;
        $this->header[] = 'Content-Type: application/json';
        $this->header[] = 'Accept: text/plain';
        $this->header[] = 'Expect:';

        $datas = array();
        if(empty($lower)){
            $datas['lower'] = date('Y-m-d H:i:s', strtotime("-2 weeks"));
        }
        if(empty($top)){
            $datas['top'] = date('Y-m-d H:i:s');
        }

        $data_json = json_encode($datas);

        curl_setopt($this->client, CURLOPT_URL, 'https://api.phoenix.wf/campaign/getallstat');
        curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
        curl_setopt($this->client, CURLOPT_POST, 1);
        curl_setopt($this->client, CURLOPT_POSTFIELDS,$data_json);
        curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

        $resultat = curl_exec ($this->client);
        $response = json_decode($resultat);

        if(isset($response->error) && !$response->error){
            return $response->data; // on retourne les infos stats des campagnes sur une periode TODO: verifier reponse
        }
        \Log::error("[RouteurPhoenix] campaign_getallstat : ".$resultat);
        return false;
    }

    function campaign_getevent($key, $type, $id)
    {

        $this->client = curl_init();
        $this->header = array();

        $this->header[] = 'api-key: '. $key;
        $this->header[] = 'Content-Type: application/json';
        $this->header[] = 'Accept: text/plain';
        $this->header[] = 'Expect:';

        $datas = array();

        $datas['type'] = $type;
        $datas['id'] = $id;

        $data_json = json_encode($datas);

        curl_setopt($this->client, CURLOPT_URL, 'https://api.phoenix.wf/campaign/getevent');
        curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
        curl_setopt($this->client, CURLOPT_POST, 1);
        curl_setopt($this->client, CURLOPT_POSTFIELDS,$data_json);
        curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

        $resultat = curl_exec ($this->client);
        $response = json_decode($resultat);

        // \Log::info($response->data);

        // echo 'Test var dump';
        // var_dump($response->email);

        if(isset($response->error) && !$response->error){
            return $response->data;
        }
        \Log::error("[RouteurPhoenix] campaign_getevent : ".$resultat);
         return false;
    }

    function campaigns_get($key)
    {

        $this->client = curl_init();
        $this->header = array();

        $this->header[] = 'api-key: '. $key;
        $this->header[] = 'Content-Type: application/json';
        $this->header[] = 'Accept: text/plain';
        $this->header[] = 'Expect:';

        curl_setopt($this->client, CURLOPT_URL, 'https://api.phoenix.wf/campaign/get');
        curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
        curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

        $resultat = curl_exec ($this->client);
        $response = json_decode($resultat);

        var_dump($response);

        if(isset($response->error) && !$response->error){
            return $response;
        }
        \Log::error("[RouteurPhoenix] campaigns_get : ".$resultat);
        return false;
    }

    function campaign_create($key, $name, $kit_id, $list_id, $router_id, $start_time)
    {

        $this->client = curl_init();
        $this->header = array();

        $this->header[] = 'api-key: '. $key;
        $this->header[] = 'Content-Type: application/json';
        $this->header[] = 'Accept: text/plain';
        $this->header[] = 'Expect:';

        $datas = array();

        $datas['name'] = $name;
        $datas['kit_id'] = $kit_id;
        $datas['list_id'] = $list_id;
        // $datas['segment_id'] = null;
        $datas['router_id'] = $router_id;
        $datas['start_time'] = $start_time;

        $data_json = json_encode($datas);

        curl_setopt($this->client, CURLOPT_URL, 'https://api.phoenix.wf/campaign/create');
        curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
        curl_setopt($this->client, CURLOPT_POST, 1);
        curl_setopt($this->client, CURLOPT_POSTFIELDS,$data_json);
        curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

        $resultat = curl_exec ($this->client);
        $response = json_decode($resultat);

        // if(!$response->error){
        //     return $response->data;
        // }
        // return false;

        // var_dump($response);

        // prevoir gestion si error
        if(isset($response->error) && !$response->error){
        return $response;
        }
        \Log::error("[RouteurPhoenix] campaign_create : ".$resultat);
        return false;
    }

    function campaign_create_segment($key, $name, $kit_id, $list_id, $segment_id, $router_id, $start_time)
    {

        $this->client = curl_init();
        $this->header = array();

        $this->header[] = 'api-key: '. $key;
        $this->header[] = 'Content-Type: application/json';
        $this->header[] = 'Accept: text/plain';
        $this->header[] = 'Expect:';

        $datas = array();

        $datas['name'] = $name;
        $datas['kit_id'] = $kit_id;
        $datas['list_id'] = [$list_id];
        $datas['segment_id'] = [$segment_id];
        $datas['router_id'] = [$router_id];
        $datas['start_time'] = [$start_time];

        $data_json = json_encode($datas);

        curl_setopt($this->client, CURLOPT_URL, 'https://api.phoenix.wf/campaign/create');
        curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
        curl_setopt($this->client, CURLOPT_POST, 1);
        curl_setopt($this->client, CURLOPT_POSTFIELDS,$data_json);
        curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

        $resultat = curl_exec ($this->client);
        $response = json_decode($resultat);

        if(!$response->error){
            return $response->data;
        }
        return false;
    }

    function campaign_bat($key, $kit_id, $email, $router_id)
    {

        $this->client = curl_init();
        $this->header = array();

        $this->header[] = 'api-key: '. $key;
        $this->header[] = 'Content-Type: application/json';
        $this->header[] = 'Accept: text/plain';
        $this->header[] = 'Expect:';

        $datas = array();

        $datas['kit_id'] = $kit_id;
        $datas['email'] = $email;
        $datas['router_id'] = [$router_id];

        $data_json = json_encode($datas);

        curl_setopt($this->client, CURLOPT_URL, 'https://api.phoenix.wf/campaign/bat');
        curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
        curl_setopt($this->client, CURLOPT_POST, 1);
        curl_setopt($this->client, CURLOPT_POSTFIELDS,$data_json);
        curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

        $resultat = curl_exec ($this->client);
        $response = json_decode($resultat);

        // if(!$response->error){
        return $response;
        // }
        // return false;
    }
    //*****************************************************************************************************************
    //*****************************************************************************************************************
    ////////
    //  PROCESSUS
    ///////
    function processus_get($key)
    {

        $this->client = curl_init();
        $this->header = array();

        $this->header[] = 'api-key: '. $key;
        $this->header[] = 'Content-Type: application/json';
        $this->header[] = 'Accept: text/plain';
        $this->header[] = 'Expect:';

        curl_setopt($this->client, CURLOPT_URL, 'https://api.phoenix.wf/process/get');
        curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
        curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

        $resultat = curl_exec ($this->client);
        $response = json_decode($resultat);

        if(!$response->error){
            return $response->data;
        }
        return false;
    }

    function processus_getinfo($key,$process_id)
    {

        $this->client = curl_init();
        $this->header = array();

        $this->header[] = 'api-key: '. $key;
        $this->header[] = 'Content-Type: application/json';
        $this->header[] = 'Accept: text/plain';
        $this->header[] = 'Expect:';

        $datas = array();

        $datas['process_id'] = $process_id;

        $data_json = json_encode($datas);

        curl_setopt($this->client, CURLOPT_URL, 'https://api.phoenix.wf/process/getinfo');
        curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
        curl_setopt($this->client, CURLOPT_POST, 1);
        curl_setopt($this->client, CURLOPT_POSTFIELDS,$data_json);
        curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

        $resultat = curl_exec ($this->client);
        $response = json_decode($resultat);

        if(!$response->error) {
            return $response->data->status;
        }
        return false;
    }

    //*****************************************************************************************************************
    //*****************************************************************************************************************
    ////////
    //  ROUTER
    ///////
    function router_get($key)
    {

        $this->client = curl_init();
        $this->header = array();

        $this->header[] = 'api-key: '. $key;
        $this->header[] = 'Content-Type: application/json';
        $this->header[] = 'Accept: text/plain';
        $this->header[] = 'Expect:';

        curl_setopt($this->client, CURLOPT_URL, 'https://api.phoenix.wf/router/get');
        curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
        curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

        $resultat = curl_exec ($this->client);
        $response = json_decode($resultat);

        if(isset($response->error) && !$response->error){
            return $response;
        }
        \Log::error("[RouteurPhoenix] kit_create : ".$resultat);
        return false;
    }
    // Ajout pour l'exemple
    function router_get_fab($key)
    {

        $this->client = curl_init();
        $this->header = array();

        $this->header[] = 'api-key: '. $key;
        $this->header[] = 'Content-Type: application/json';
        $this->header[] = 'Accept: text/plain';
        $this->header[] = 'Expect:';

        curl_setopt($this->client, CURLOPT_URL, 'https://api.phoenix.wf/router/get');
        curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
        curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

        $resultat = curl_exec ($this->client);
        $response = json_decode($resultat);

        if(!$response->error){
            //return $response->routers[0]->name;
            return $response;
        }
        // return $response->routers[0];
        return false;
    }

    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    //      END OF FONCTIONS DE BASE DU ROUTEURS
    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    //      FONCTIONS PRINCIPALES DE TOR
    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function create_campagne($key, \App\Models\Sender $sender, \App\Models\Planning $planning, $anonyme = false)
    {
        $campagne = Campagne::find($planning->campagne_id);

        if(empty($kit_id)) {
            $kit_id = $this->kit_create($key, $planning->campagne->ref, $planning->campagne->objet, $planning->campagne->expediteur, $planning->campagne->generateHtml($sender->routeur));
            $campagne->kid_routeur = $kit_id;
            $campagne->save();
        }
        return $kit_id;
    }

    function create_campagne_debug($key, $campagneid, $liste_id, $anonyme = false)
    {

        $this->client = curl_init();
        $this->header = array();

        $this->header[] = 'api-key: '. $key;
        $this->header[] = 'Content-Type: application/json';
        $this->header[] = 'Accept: text/plain';
        $this->header[] = 'Expect:';

        $kit_id = null;
        $campagne = Campagne::find($campagneid);
        $lehtmldev = file_get_contents('http://pastebin.com/raw/fKL5bWjT');
        // if(empty($kit_id)) {
        $kit_id = $this->kit_create($key, 'MonKitDebuG', 'MonObjetDebug', 'MonExpediteurDebug', $lehtmldev);
        // $campagne->kid_routeur = $kit_id;
        // $campagne->save();
        var_dump('La liste : ' . $liste_id);
        // on a que un seul routeur pour l'instant
        $retourrouteur = $this->router_get($key);
        // on fait notre create campagne
        $this->campaign_create($key, 'LaCampagnedebug',$kit_id,$liste_id,$retourrouteur->routers[0]->router_id, '1462117604' );
        //   }
        return $kit_id;
    }

    function import_liste($key, $sender, $url)
    {

        $this->client = curl_init();
        $this->header = array();

        $this->header[] = 'api-key: '. $key;
        $this->header[] = 'Content-Type: application/json';
        $this->header[] = 'Accept: text/plain';
        $this->header[] = 'Expect:';

        $today = date('Y-m-d H:i:s');
        $datas = array();
        $list_id = $this->liste_create($key, 'TORdev-'.$sender->id.'-'.$today);

        var_dump($list_id);
        if(!$list_id){
            return false;
        }

        $datas['type'] = 'url'; //facultatif
        $datas['list_id'] = $list_id;
        $datas['url'] = $url;

        $data_json = json_encode($datas);

        curl_setopt($this->client, CURLOPT_URL, 'https://api.phoenix.wf/contact/add');
        curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
        curl_setopt($this->client, CURLOPT_POST, 1);
        curl_setopt($this->client, CURLOPT_POSTFIELDS,$data_json);
        curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

        $resultat = curl_exec ($this->client);
        $response = json_decode($resultat);

        var_dump($response);

        // pas utile pour le moment
        // if(!$response->error){
        //     return false;
        // }
        return $list_id;

    }

    function send_campagne($key, \App\Models\Planning $planning)
    {


        $campagnes_routeurs = CampagneRouteur::where('routeur_id',$this->routeur->id)
            ->where('planning_id', $planning->id)
            ->get();

        foreach($campagnes_routeurs as $cr)
        {
            if($this->processus_getinfo($key, $cr->taskid) != 'OK') {
                \Log::info("$planning->id --  Imports non terminés");
                exit;
            }
            $cr->listid;
            $cr->cid_routeur;
//            $this->campaign_send();
        }
    }

    function send_bat($id)
    {
      $campagne = Campagne::find($id);

      $routeur = Routeur::where('nom','Phoenix')->first();
      $htmldelacampagne = $campagne->generateHtml($routeur);

      $retourkit = $this->kit_create('e5ef90311fc93779122c5948c0fd0ccc8dc8b055',$campagne->ref . time(), '[-TEST-BAT-] ' . $campagne->objet , $campagne->expediteur, $htmldelacampagne);
      $retourrouteur = $this->router_get('e5ef90311fc93779122c5948c0fd0ccc8dc8b055');

        $utilisateurs = \App\Models\User::where('is_bat', 1)
            ->where('is_valid', 1)
            ->get();

        $tabat = array();
        foreach ($utilisateurs as $user) {
            $tabat[] = $user->email;
            \Log::info($user->email);
        }

      $timecampagne = time();
      $retourcampagne = $this->campaign_create('e5ef90311fc93779122c5948c0fd0ccc8dc8b055', $campagne->nom . '-api' . time(), $retourkit, 'AVTtyyd9j65yR4AmUgz8', $retourrouteur->routers[0]->router_id, $timecampagne);
    }

    function getBounces ($key, $campagne, $cid)
    {
        $softbounces = array();
        $hardbounces = array();
        $bulk = array();
        $compteur = 0;
        $softbounces = $this->campaign_getevent($key, 'softbounces', $cid);

        if ( !empty($softbounces) ) {
            foreach ($softbounces as $sb) {

                $destinataire = \DB::table('destinataires')
                    ->where('base_id', $campagne->base_id)
                    ->where('mail', $sb->email)
                    ->first();

                if (empty($destinataire)) {
                    continue;
                }

                $bulk[] = $destinataire->id;

                $bounce = Bounce::firstOrNew(['destinataire_id' => $destinataire->id, 'campagne_id' => $campagne->id]);
                $bounce->message = 'NPAI_SOFT';
                $bounce->save();

                if (count($bulk) > 500) {
                    \DB::table('destinataires')
                        ->whereIn('id', $bulk)
                        ->update(['statut' => DESTINATAIRE_BOUNCE]);
                    $bulk = [];
                }
                $compteur++;
            }

            if (count($bulk) > 0) {
                \DB::table('destinataires')
                    ->whereIn('id', $bulk)
                    ->update(['statut' => DESTINATAIRE_BOUNCE]);
                $bulk = [];
            }
        }
        echo " - Campagne " . $campagne->id . " - softbounces  -- cpt : $compteur\n";

        $compteur = 0;
        $bulk = array();
        $hardbounces = $this->campaign_getevent($key, 'hardbounces', $cid);

        if (! empty($hardbounces)) {
            foreach ($hardbounces as $sb) {

                $destinataire = \DB::table('destinataires')
                    ->where('base_id', $campagne->base_id)
                    ->where('mail', $sb->email)
                    ->first();

                if (empty($destinataire)) {
                    continue;
                }

                $bulk[] = $destinataire->id;

                $bounce = Bounce::firstOrNew(['destinataire_id' => $destinataire->id, 'campagne_id' => $campagne->id]);
                $bounce->message = 'NPAI_HARD';
                $bounce->save();

                if (count($bulk) > 500) {
                    \DB::table('destinataires')
                        ->whereIn('id', $bulk)
                        ->update(['statut' => DESTINATAIRE_BOUNCE]);
                    $bulk = [];
                }
                $compteur++;
            }

            if (count($bulk) > 0) {
                \DB::table('destinataires')
                    ->whereIn('id', $bulk)
                    ->update(['statut' => DESTINATAIRE_BOUNCE]);
                $bulk = [];
            }
            echo " - Campagne " . $campagne->id . " - hardbounces  -- cpt : $compteur\n";
        }
    }

    function getClicks ($key, /* $campagne, */ $cid)
    {
        $chainef = '';
        $clicks = array();
        $compteur = 0;
        $clicks = $this->campaign_getevent($key, 'click', $cid);

        if ($clicks !== false) {
            foreach ($clicks as $sb) {

                // $destinataire = \DB::table('destinataires')
                //     ->where('base_id', $campagne->base_id)
                //     ->where('mail', $sb->email)
                //     ->first();
                //
                // if (empty($destinataire)) {
                //     continue;
                // }
                //
                // $destinataire = \DB::table('destinataires')
                //     ->select('id')
                //     ->where('base_id',$campagne->base_id)
                //     ->where('mail', $sb->email)
                //     ->first();
                //
                // if(empty($destinataire)){
                //     continue;
                // }
                // $destinataire = \DB::table('destinataires')
                //     ->select('id')
                //     ->where('base_id',$campagne->base_id)
                //     ->where('mail', $sb->email)
                //     ->update([ 'clicked_at' => \DB::raw('now()')] );
                //
                // Clic::firstOrCreate(['destinataire_id' => $destinataire->id, 'campagne_id' => $campagne->id]);
                // $compteur++;

                // echo $sb->email;
                $chainef .= $sb->email . "\n";
            }
        }
        // echo " - Campagne " . $campagne->id . " - clicks  -- cpt : $compteur\n";
        // echo $chainef;
        $today = date('Y-m-d');
        $file = storage_path() . '/cliqueurs/clic_cid_' . $cid . '_date_' . $today . '.csv';
        $fp = fopen($file,"w+");
        fwrite($fp,$chainef);
        fclose($fp);
    }

    function checkStatutImport(\App\Models\Sender $sender, $taskid = null){

        $processus = $this->processus_get($sender->password);

        if($processus !== false and count($processus) == 0 ){
            return true;
        }
        return false;

//        $statut = $this->processus_getinfo($sender->password, $taskid);
//        if($statut != 'ended'){
//            return false;
//        }
//        return true;
    }

    function addDesinscrits(\App\Models\Sender $sender,\App\Models\Campagne $campagne, $listid)
    {
      \Log::info('Add Desinscrits Phoenix ' . $listid);

      $unsubscribe = $this->campaign_getevent($sender->password,'unsubscribe',$listid);

      $key = 0;
       foreach ($unsubscribe as $api) {
         if(!empty($api)){
           var_dump($api->email);
           \DB::statement("UPDATE destinataires SET statut ='90' WHERE mail ='". $api->email ."'");

       }

        // \DB::statement("UPDATE destinataires SET statut ='90' WHERE mail ='". $api->email ."'");
       }

    }

}
?>
