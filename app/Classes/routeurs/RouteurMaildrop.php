<?php namespace App\Classes\Routeurs;

use App\Models\Campagne;
use App\Models\CampagneSegments;
use App\Models\Sender;
use App\Models\CampagneRouteur;
use App\Models\Destinataire;

use App\Models\Bounce;
use App\Models\Clic;
use App\Models\Routeur;

class RouteurMaildrop
{
    private $client;

    public function __construct()
    {
        $this->client = new RouteurMaildropCurl();
    }

    function getClicks($accountKey, $cid)
    {

        $cliqueurs = array();
        $this->client->authenticate($accountKey, '');
        $this->client->SetParams(array(
                'cid' => $cid,
            )
        );

        $R = $this->client->post("http://api.dpmail.fr/json/Reports.Clicks.php");
        $ApiResponse = json_decode($R->body);

        if (!is_array($ApiResponse->data)) {
            // \Log::info("[RouteurMaildrop] getClicks : empty Campagne $campagne->id / CID $cid ");
            return;
        }

        foreach($ApiResponse->data as $obj)
        {
            $link_id = $obj->lid;

            $this->client->authenticate($accountKey, '');
            $this->client->SetParams(
                array
                (
                    'cid' => $cid,
                    'lid' => $link_id
                )
            );
            $R = $this->client->post("http://api.dpmail.fr/json/Reports.ClickDetails.php");
            $ApiResponse = json_decode($R->body);

//            dd($ApiResponse->data->data);

            foreach($ApiResponse->data->data as $obj2)
            {
                if (!isset($obj2->member->email)) {
                    continue;
                } else {
        //                    echo $obj2->member->email;
                    $cliqueurs[$obj2->member->email]="";
                }
            }
        }
        return $cliqueurs;
    }

    function getClicksDesti($accountKey, $campagne, $cid)
    {

        $desti_ids = array();
        $cliqueurs = array();

        $this->client->authenticate($accountKey, '');
        $this->client->SetParams(array(
                'cid' => $cid,
            )
        );

        $R = $this->client->post("http://api.dpmail.fr/json/Reports.Clicks.php");
        $ApiResponse = json_decode($R->body);

        if (!is_array($ApiResponse->data)) {
            // \Log::info("[RouteurMaildrop] getClicks : empty Campagne $campagne->id / CID $cid ");
            return;
        }

        foreach($ApiResponse->data as $obj)
        {
            $link_id = $obj->lid;

            $this->client->authenticate($accountKey, '');
            $this->client->SetParams(
                array
                (
                    'cid' => $cid,
                    'lid' => $link_id
                )
            );
            $R = $this->client->post("http://api.dpmail.fr/json/Reports.ClickDetails.php");
            $ApiResponse = json_decode($R->body);

//            dd($ApiResponse->data->data);

            foreach($ApiResponse->data->data as $obj2)
            {
                if (!isset($obj2->member->email)) {
                    continue;
                } else {
                    //                    echo $obj2->member->email;
                    $cliqueurs[$obj2->member->email]=$obj2->member->email;
                }
            }
            $destis = \DB::table('destinataires')
                ->select('id')
                ->where('base_id', $campagne->base_id)
                ->whereIn('mail', $cliqueurs)
                ->get();
            $desti_ids = $desti_ids + array_pluck($destis, 'id');
        }
        return $desti_ids;
    }

    function getBounces ($accountKey, $campagne, $cid)
    {
        $limit = 500;
        $cliqueurs = array();

        $compteur = 0;
        $start = 0;
        $nbBoucles = 1;
        $first = true;

        while($compteur < $nbBoucles) {
            $bulk = [];

            $this->client->authenticate($accountKey, '');
            $this->client->SetParams(array(
                    'cid' => $cid,
                    'start' => $start,
                    'limit' => $limit
                )
            );
            $R = $this->client->post("http://api.dpmail.fr/json/Reports.BounceMessages.php");

            $ApiResponse = json_decode($R->body);
            if (!isset($ApiResponse->data->total) || $ApiResponse->data->total == 0) {
                \Log::info("[RouteurMaildrop] getBounces : empty Campagne $campagne->id / CID $cid ");
                return;
            }

            if ($first) {
                $nbBoucles = ceil($ApiResponse->data->total / $limit);

                echo "$nbBoucles boucles";
                $first = false;
            }
            echo " - ".count($ApiResponse->data->data)." réponses";

            foreach($ApiResponse->data->data as $res) {
                $destinataire = \DB::table('destinataires')
                    ->where('base_id', $campagne->base_id)
                    ->where('mail', $res->email)
                    ->first();

                if(empty($destinataire)){
                    continue;
                }

                $bounce = Bounce::firstOrNew(['destinataire_id' => $destinataire->id, 'campagne_id'  => $campagne->id]);
                $bounce->message = $res->error_type;
                $bounce->save();

                if($res->error_type == "INVALID_DOMAIN"
                    || $res->error_type == "MAILBOX_INACTIVE"
                    || $res->error_type == "USER_UNKNOWN")
                {
                    $bulk[] = $destinataire->id;
                }
                if (count($bulk) > 500) {
                    \DB::table('destinataires')
                        ->whereIn('id', $bulk)
                        ->update(['statut' => DESTINATAIRE_BOUNCE]);
                    $bulk = [];
                }
            }

            if (count($bulk) > 0) {
                \DB::table('destinataires')
                    ->whereIn('id', $bulk)
                    ->update(['statut' => DESTINATAIRE_BOUNCE]);

                $bulk = [];
            }

            echo " - Campagne ".$campagne->id." - bounces : ".count($ApiResponse->data->data)." (cpt : $compteur)\n";
            $compteur++;
            $start += $limit;
        }
    }

    function getBouncesNew ($accountKey, $campagne, $cid)
    {
        $bounces = array();
        $start = 0;
        $limit = 500;

        $param = ['cid' => $cid, 'start' => $start, 'limit' => $limit];

        $this->client->authenticate($accountKey, '');
        $this->client->SetParams(
            $param
        );
        $R = $this->client->post("http://api.dpmail.fr/json/Reports.BounceMessages.php");

        $ApiResponse = json_decode($R->body);
        if(!$ApiResponse or !isset($ApiResponse->data->data)){
//            dd($ApiResponse);
            return false;
        }
//        echo "Campagne ".$campagne->id." - nombre : ".count($ApiResponse->data->data)."\n";

        foreach($ApiResponse->data->data as $obj)
        {
            $bounces[$obj->email] = $obj;
        }

        if($ApiResponse->data->total<$limit) {
//            echo "\n ". $ApiResponse->data->total ."< $limit";
            \Log::info('[RouteurMaildrop] getBouncesNew : COUNT BOUNCES -- '.count($bounces));
//            echo '\n [RouteurMaildrop] getBouncesNew : COUNT BOUNCES -- '.count($bounces);
//            print_r($bounces);
            return $bounces;
        }

        $nbBoucles = round(($ApiResponse->data->total / $limit), 0, PHP_ROUND_HALF_UP)+1;
        $compteur = 1;

        while($compteur < $nbBoucles) {

            $this->client->authenticate($accountKey, '');
            $this->client->SetParams(array(
                    'cid' => $cid,
                    'start' => $compteur,
                    'limit' => $limit
                )
            );
            $R = $this->client->post("http://api.dpmail.fr/json/Reports.BounceMessages.php");

            $ApiResponse = json_decode($R->body);
            if (!isset($ApiResponse->data->total) || $ApiResponse->data->total == 0) {
                \Log::info("[RouteurMaildrop] getBouncesNew : empty Campagne $campagne->id / CID $cid ");
                return $bounces;
            }

//            echo " - ".count($ApiResponse->data->data)." réponses";

            foreach($ApiResponse->data->data as $res) {

//                echo "\n $res->email";
//                $bounces[$res->email] = $res;

                // uniquement les hard bounces
                // erreurs liés aux FAIS à signaler par mail mais pas à mettre en bounces
                if($res->error_type == "INVALID_DOMAIN"
                    || $res->error_type == "MAILBOX_INACTIVE"
                    || $res->error_type == "USER_UNKNOWN"
                    || $res->error_type == "CONTENT_BLOCKED"
                    || $res->error_type == "SENDER_BLOCKED") {
                    $bounces[$res->email] = $res;
                }

            }

//            echo " - Campagne ".$campagne->id." - bounces : ".count($ApiResponse->data->data)." (cpt : $compteur)\n";
            $compteur++;
            $start += $limit;
        }
//        print_r($bounces);
        return $bounces;
    }

    function addDesinscrits ($accountKey, $campagne, $cid)
    {
        $start = 0;
        $limit = 100;

        $this->client->authenticate($accountKey, '');
        $this->client->SetParams(array(
                'cid' => $cid,
                'start' => $start,
                'limit' => $limit
            )
        );
        $R = $this->client->post("http://api.dpmail.fr/json/Reports.Unsubscribes.php");

        $ApiResponse = json_decode($R->body);
        if(!$ApiResponse or !isset($ApiResponse->data->data)){
//            dd($ApiResponse);
            return false;
        }
        echo "Campagne ".$campagne->id." - nombre : ".count($ApiResponse->data->data)."\n";

        foreach($ApiResponse->data->data as $obj)
        {
            $destinataire = \DB::table('destinataires')
                ->select('id')
                ->where('base_id', $campagne->base_id)
                ->where('mail', $obj->email)
                ->first();

            if(empty($destinataire)){
                continue;
            }

            \DB::table('destinataires')
                ->where('base_id', $campagne->base_id)
                ->where('mail', $obj->email)
                ->update(['statut' => DESTINATAIRE_OPTOUT,
                    'optout_at' => $obj->timestamp]);
        }

        if($ApiResponse->data->total<$limit) {
//            \Log::info("[RouteurMaildrop] addDesinscrits : finished Campagne $campagne->id / CID $cid ");
            return true;
        }

        $nbBoucles = round(($ApiResponse->data->total / $limit), 0, PHP_ROUND_HALF_UP)+1;

        $compteur=1;
        while($compteur <= $nbBoucles)
        {
            $this->client->authenticate($accountKey, '');
            $this->client->SetParams(array(
                    'cid' => $cid,
                    'start' => $compteur,
                    'limit' => $limit
                )
            );
            $R = $this->client->post("http://api.dpmail.fr/json/Reports.Unsubscribes.php");
            $ApiResponse = json_decode($R->body);

            if(!$ApiResponse){
                continue;
            }

            echo "Campagne ".$campagne->id." - nombre : ".count($ApiResponse->data->data)."\n";
            foreach($ApiResponse->data->data as $obj)
            {
                $destinataire = \DB::table('destinataires')
                    ->where('base_id', $campagne->base_id)
                    ->where('mail', $obj->email)
                    ->first();
                if(empty($destinataire)){
                    continue;
                }
                \DB::table('destinataires')
                    ->where('base_id', $campagne->base_id)
                    ->where('mail', $obj->email)
                    ->update(['statut' => DESTINATAIRE_OPTOUT, 'optout_at' => $obj->timestamp]);
            }
            $compteur++;
        }
        \Log::info("[RouteurMaildrop] addDesinscrits : DONE Campagne $campagne->id / CID $cid ");
        return true;
//        return false;
    }

    function updateCliqueurs($accountKey, $campagne)
    {

        $this->client->authenticate($accountKey, '');
        $this->client->SetParams(array(
                'cid' => $campagne->cid_routeur,
            )
        );
        $R = $this->client->post("http://api.dpmail.fr/json/Reports.Clicks.php");

        $ApiResponse = json_decode($R->body);

        if(!isset($ApiResponse->data)){
            \Log::error("[RouteurMaildrop] updateCliqueurs : NOT OK Campagne $campagne->id / AccountKey $accountKey ($R)");
        }
        foreach ($ApiResponse->data as $obj)
        {
            $link_id = $obj->lid;

            $this->client->authenticate($accountKey, '');
            $this->client->SetParams(
                array
                (
                    'cid' => $campagne->cid_routeur,
                    'lid' => $link_id
                )
            );
            $R = $this->client->post("http://api.dpmail.fr/json/Reports.ClickDetails.php");
            $ApiResponse = json_decode($R->body);

            foreach ($ApiResponse->data->data as $obj2)
            {
                $m = \DB::table('destinataires')
                    ->select('id')
                    ->whereBaseId($campagne->base_id)
                    ->whereMail($obj2->member->email)
                    ->first();

                if(!empty($m)) {
                    Clic::firstOrCreate(['mail_id' => $m->id, 'campagne_id' => $campagne->id]);
                }
            }
        }
        \Log::info("[RouteurMaildrop] updateCliqueurs : DONE Campagne $campagne->id / AccountKey $accountKey ");
        return true;
    }

    function getDesinscrits($sender, $campagne_id)
    {
        $desinscrits = array();
        $start = 0;

        $this->client->authenticate($sender->password, '');
        $this->client->SetParams(array(
                'cid' => $campagne_id,
                'start' => $start,
                'limit' => 500
            )
        );
        $R = $this->client->post("http://api.dpmail.fr/json/Reports.Unsubscribes.php");

        $ApiResponse = json_decode($R->body);
        if($ApiResponse->message == 'Success') {

            foreach($ApiResponse->data->data as $desin)
            {
                $desinscrits[$desin->email]="";
            }
            //rajouter
            return $desinscrits;
        }
        return false;
    }

    function getDesinscritsNew ($accountKey, $campagne, $cid, $from_id = null)
    {
        $start = 0;
        $limit = 500;

        $lastid = null;

        $desinscrits = array();

        $param = ['cid' => $cid, 'start' => $start, 'limit' => $limit];

        if(!empty($from_id)){
            $param['since_id'] = $from_id;
        }

        $this->client->authenticate($accountKey, '');
        $this->client->SetParams(
                $param
        );
        $R = $this->client->post("http://api.dpmail.fr/json/Reports.Unsubscribes.php");

        $ApiResponse = json_decode($R->body);
        if(!$ApiResponse or !isset($ApiResponse->data->data)){
//            dd($ApiResponse);
            return false;
        }
        echo "Campagne ".$campagne->id." - nombre : ".count($ApiResponse->data->data)."\n";

        foreach($ApiResponse->data->data as $obj)
        {
            $desinscrits[$obj->email] = $obj->timestamp;
            $lastid = $obj->id;
        }

        if($ApiResponse->data->total<$limit) {
//            \Log::info("[RouteurMaildrop] addDesinscrits : finished Campagne $campagne->id / CID $cid ");
            \Log::info('COUNT DESINSC -- '.count($desinscrits));
            return ['desinscrits' => $desinscrits, 'last_id' => $lastid];
        }

        $nbBoucles = round(($ApiResponse->data->total / $limit), 0, PHP_ROUND_HALF_UP)+1;

        $compteur=1;

        while($compteur <= $nbBoucles)
        {
            $param = ['cid' => $cid, 'start' => $compteur, 'limit' => $limit];

            if(!empty($from_id)){
                $param['since_id'] = $from_id;
            }

            $this->client->authenticate($accountKey, '');
            $this->client->SetParams(
                    $param
            );
            $R = $this->client->post("http://api.dpmail.fr/json/Reports.Unsubscribes.php");
            $ApiResponse = json_decode($R->body);

            if(empty($ApiResponse) or !isset($ApiResponse->data) or !isset($ApiResponse->data->data)){
                \Log::error("[RouteurMaildrop] : getDesinscritsNew (C$campagne->id/CID=$cid/AccountKEY=$accountKey) $R->body");
                return ['desinscrits' => $desinscrits, 'last_id' => $lastid]; // on n'a de bon retour donc on s'arrete la
            }

            echo "Campagne ".$campagne->id." - nombre : ".count($ApiResponse->data->data)."\n";
            foreach($ApiResponse->data->data as $obj)
            {
                $desinscrits[$obj->email] = $obj->timestamp;
                $lastid = $obj->id;
            }
            $compteur++;
        }
        \Log::info("[RouteurMaildrop] addDesinscrits : DONE Campagne $campagne->id / CID $cid ");
        \Log::info('COUNT DESINSC -- '.count($desinscrits));
        return ['desinscrits' => $desinscrits, 'last_id' => $lastid];
//        return false;
    }

    function create_campagne(\App\Models\CampagneSegments $campagne_segment)
    {
        $campagne = Campagne::find($campagne_segment->campagne_id);
        $sender = Sender::find($campagne_segment->sender_id);

        $objets = explode('|',$campagne->objet);
        $objet = $objets[rand(0,count($objets)-1)];

        $this->client->authenticate($sender->password, '');
        //base recup
        $labase = \DB::table('bases')->where('id',$campagne->base_id)->first();

        if($labase->email_reply == null || empty($labase->email_reply)){

          $params = [
              'subject' => $objet,
              'from_name' => $campagne->expediteur,
              'from_email' => $sender->domaine,
              'message_html' => $campagne->generateHtml($sender->routeur),
              'campaign_name' => $campagne->ref,
              'segment_id' => [ $campagne_segment->md_segment_id ],
          ];

        } else {
          // reply_email
          $params = [
              'subject' => $objet,
              'from_name' => $campagne->expediteur,
              'from_email' => $sender->domaine,
              'message_html' => $campagne->generateHtml($sender->routeur),
              'campaign_name' => $campagne->ref,
              'segment_id' => [ $campagne_segment->md_segment_id ],
              'reply_email' => $labase->email_reply
          ];
        }

        $this->client->SetParams($params);

        $R = $this->client->post("http://api.dpmail.fr/json/Campaign.Create.php");
        $ApiResponse = json_decode($R->body);

        if(isset($ApiResponse->message) && $ApiResponse->message == 'Success') {
            CampagneRouteur::create([
                'campagne_id' => $campagne->id,
                'planning_id' => $campagne_segment->planning_id,
                'sender_id' => $sender->id,
                'cid_routeur'=> $ApiResponse->data->cid
            ]);
            return $ApiResponse->data->cid;
        } else {
            \Log::error("[RouteurMaildrop] create_campagne : NOT OK Campagne $campagne->id / Sender $sender->id ($R)");
        }
        return false;
    }

    function createCampagne(\App\Models\Campagne $campagne, \App\Models\Sender $sender, Array $md_list_ids, \App\Models\Planning $planning = null)
    {
        $timestamp = date('Y-m-d H:i:s');
        $planningid = 0;

        if(!empty($planning))
        {
            $planningid = $planning->id;
            if($planning->type == 'anonyme'){
                $expediteur = $campagne->expediteur_anonyme;
            }
        }

        if(empty($expediteur)){
            $expediteur = $campagne->expediteur;
        }
        //Multi-objets aléatoire
        $objets = explode('|',$campagne->objet);
        $objet = $objets[rand(0,count($objets)-1)];

        $this->client->authenticate($sender->password, '');

        $params = [
            'subject' => $objet,
            'from_name' => $expediteur,
            'from_email' => $sender->domaine,
            'message_html' => $campagne->generateHtml($sender->routeur),
            'campaign_name' => $campagne->ref,
            'list_id' => $md_list_ids,
        ];
        $this->client->SetParams($params);

        \Log::info("[RouteurMaildrop] createCampagne : before appel $campagne->id / Sender $sender->id");
        $R = $this->client->post("http://api.dpmail.fr/json/Campaign.Create.php");
        \Log::info("[RouteurMaildrop] createCampagne : after appel $campagne->id / Sender $sender->id");
        $ApiResponse = json_decode($R->body);

        if(isset($ApiResponse->message) && $ApiResponse->message == 'Success') {
            \DB::table('campagnes_routeurs')->insert([
                'campagne_id' => $campagne->id,
                'planning_id' => $planningid,
                'sender_id' => $sender->id,
                'cid_routeur'=> $ApiResponse->data->cid,
                'created_at' => $timestamp,
                'updated_at' => $timestamp
            ]);
            return $ApiResponse->data->cid;
        } else {
//            \Log::error('Erreur création MD_campagne : '.print_r($ApiResponse, true));
            \Log::error("[RouteurMaildrop] createCampagne : NOT DONE Campagne $campagne->id / Sender $sender->id ($R)");
//            echo "campagne $campagne->id | sender $sender->id : ERROR\n";
//            dd($ApiResponse);
        }
        return false;
    }

    function createCampagneL($campagne, $html, $sender, Array $md_list_ids, $planning = null)
    {
        $timestamp = date('Y-m-d H:i:s');
        $planningid = 0;

        if(!empty($planning))
        {
            $planningid = $planning->id;
            if($planning->type == 'anonyme'){
                $expediteur = $campagne->expediteur_anonyme;
            }
        }

        if(empty($expediteur)){
            $expediteur = $campagne->expediteur;
        }

        //Multi-objets aléatoire
        $objets = explode('|',$campagne->objet);
        $objet = $objets[rand(0,count($objets)-1)];

        $labase = \DB::table('bases')->where('id',$campagne->base_id)->first();

        $this->client->authenticate($sender->password, '');

        if($labase == null || empty($labase->email_reply)){
            $params = [
            'subject' => $objet,
            'from_name' => $expediteur,
            'from_email' => $sender->domaine,
            'message_html' => $html,
            'campaign_name' => $campagne->ref,
            'list_id' => $md_list_ids,
            'metadata[headers][X-Tor-Base]' => 'TOR-'.md5(getenv('CLIENT_NAME').'-'.$campagne->base_id)
            ];

         } else {
            \Log::info('Email reply : ' . $labase->email_reply);

            $params = [
            'subject' => $objet,
            'from_name' => $expediteur,
            'from_email' => $sender->domaine,
            'message_html' => $campagne->generateHtml($sender->routeur),
            'campaign_name' => $campagne->ref,
            'list_id' => $md_list_ids,
            'reply_name' => $labase->nom_reply,
            'reply_email' => $labase->email_reply,
            'metadata[headers][X-Tor-Base]' => 'TOR-'.md5(getenv('CLIENT_NAME').'-'.$campagne->base_id)
            ];
        }

        if(!empty(getenv('VR_WHITELIST'))) {
            $params['metadata[headers][X-VR-Whitelist]'] = getenv('VR_WHITELIST');
        }
        $this->client->SetParams($params);

        \Log::info("[RouteurMaildrop] createCampagneL : before appel $campagne->id / Sender $sender->id");
        $R = $this->client->post("http://api.dpmail.fr/json/Campaign.Create.php");
        \Log::info("[RouteurMaildrop] createCampagneL : after appel $campagne->id / Sender $sender->id");
        $ApiResponse = json_decode($R->body);

        if(isset($ApiResponse->message) && $ApiResponse->message == 'Success') {
            \DB::table('campagnes_routeurs')->insert([
                'campagne_id' => $campagne->id,
                'planning_id' => $planningid,
                'sender_id' => $sender->id,
                'cid_routeur'=> $ApiResponse->data->cid,
                'created_at' => $timestamp,
                'updated_at' => $timestamp

            ]);
            return $ApiResponse->data->cid;
        } else {
//            \Log::error('Erreur création MD_campagne : '.print_r($ApiResponse, true));
            \Log::error("[RouteurMaildrop] createCampagnel : NOT DONE Campagne $campagne->id / Sender $sender->id ($R)");
//            echo "campagne $campagne->id | sender $sender->id : ERROR\n";
//            dd($ApiResponse);
        }
        return false;
    }

    function create_list(\App\Models\Sender $sender)
    {
        $this->client->authenticate($sender->password, '');
        $this->client->SetParams(array(
                'list_name' => 'TOR-s'.$sender->id.'-'.date('Ymd')
            )
        );
        \Log::info("[RouteurMaildrop] create_list : début appel $sender->id");
        $R = $this->client->post("http://api.dpmail.fr/json/List.Create.php");
        \Log::info("[RouteurMaildrop] create_list : fin appel $sender->id");
        $ApiResponse = json_decode($R->body);

        if(isset($ApiResponse->message) && $ApiResponse->message == 'Success') {
            return $ApiResponse->data->ListID;
        }
//        dd($R);
        \Log::error("[RouteurMaildrop] create_list : NOT OK Sender $sender->id ($R)");
        return false;
    }

    function add_custom_field($sender, $listid, $field_name = 'next_campaign')
    {
        $this->client->authenticate($sender->password, '');
        $this->client->SetParams(array(
                'ListID' => $listid,
                'name' => $field_name,
                'type' => 'NUMBER'
            )
        );
        \Log::info("[RouteurMaildrop] add_custom_field : début appel $sender->id / ListID $listid / URL $field_name");
        $R = $this->client->post("http://api.dpmail.fr/json/List.AddCustomField.php");
        \Log::info("[RouteurMaildrop] add_custom_field : fin appel $sender->id / ListID $listid / URL $field_name");
        $ApiResponse = json_decode($R->body);
        if ($ApiResponse->message == 'Success') {
            return true;
        }
        \Log::error("[RouteurMaildrop] add_custom_field : NOT OK Sender $sender->id ($R)");
        return false;
    }

    function delete_list(\App\Models\Sender $sender, $list_id)
    {
        $this->client->authenticate($sender->password, '');
        $this->client->SetParams(array(
                'ListID' => $list_id
            )
        );
        $R = $this->client->post("http://api.dpmail.fr/json/List.Delete.php");

        $ApiResponse = json_decode($R->body);
        if($ApiResponse == null || isset($ApiResponse->message) && $ApiResponse->message == 'Success') {
            \Log::error("[RouteurMaildrop] delete_list : DONE Liste $list_id / Sender $sender->id");
            return true;
        }
        \Log::error("[RouteurMaildrop] delete_list : NOT OK Liste $list_id / Sender $sender->id  ($R)");
        return false;
    }

    function add_destinataire(\App\Models\Destinataire $destinataire, \App\Models\Sender $sender, $md_list_id)
    {
        $this->client->authenticate($sender->password, '');
        $this->client->SetParams( [
            'ListID' => $md_list_id,
            'Email' => $destinataire->mail
        ]);

        $R = $this->client->post("http://api.dpmail.fr/json/Subscriber.Add.php");

        $ApiResponse = json_decode($R->body);
        if($ApiResponse->message == 'Success') {
            return true;
        }
        \Log::error("[RouteurMaildrop] add_destinataire : NOT OK Destinataire $destinataire->id / Sender $sender->id ($R)");
        return false;
    }

    function update_field(\App\Models\Sender $sender, $mail, $custom_field)
    {
        $this->client->authenticate($sender->password, '');
        $this->client->SetParams( [
            'ListID' => $sender->md_list_id,
            'Email' => $mail,
            'Customs_Fields' => $custom_field
        ]);

        $R = $this->client->post("http://api.dpmail.fr/json/Subscriber.Add.php");

        $ApiResponse = json_decode($R->body);
        if($ApiResponse->message == 'Success') {
            return true;
        }
//        \Log::error(print_r($ApiResponse, true));
        \Log::error("[RouteurMaildrop] update_field : NOT OK Sender $sender->id ($R)");
//        dd($ApiResponse);

        return false;
    }

    function add_destinataires_bulk($sender, $md_list_id, $destinataires)
    {
        $this->client->authenticate($sender->password, '');
        $this->client->SetParams( [
            'ListID' => $md_list_id,
            'batch' => $destinataires
        ]);

        $R = $this->client->post("http://api.dpmail.fr/json/Subscriber.ImportBatch.php");
        $ApiResponse = json_decode($R->body);

        if($ApiResponse->message == 'Success') {
            return true;
        }
        \Log::error("[RouteurMaildrop] add_destinataires_bulk : NOT OK Sender $sender->id ($R)");
        return false;
    }

    function create_segment($sender, $campagne_id, $custom_field)
    {

        $conditions = '[{ "field":"'.$custom_field.'", "op": "equal", "value":"'.$campagne_id.'" } ]';

        $this->client->authenticate($sender->password, '');

        $params = [
            'ListID' => $sender->md_list_id,
            'Name' => 'Segment-s'.$sender->id.'-c'.$campagne_id,
            'Conditions' => $conditions
        ];
        $this->client->SetParams( $params );

        echo "\tcreating segment in Sender ".$sender->id."\n";

        $R = $this->client->post("http://api.dpmail.fr/json/List.AddSegment.php");
        $ApiResponse = json_decode($R->body);

        if($ApiResponse->message == 'Success') {
            return $ApiResponse->data;
        }
//        var_dump($params);
        \Log::error("[RouteurMaildrop] create_segment : NOT OK Sender $sender->id ($R)");
        return $ApiResponse;
    }

    function populate_list($sender, $destinataires, $custom_fields = null)
    {
        $batch = [];
        $compt = 0;

        foreach($destinataires as $destinataire) {
            $row['email'] = (is_object($destinataire)) ? $destinataire->mail : $destinataire;

            if (is_array($custom_fields)) {
                $row['customs_fields'] = $custom_fields;
            }

            $batch[] = $row;
            $compt++;
        }

        $params = [
            'ListID' => $sender->md_list_id,
            'batch' => json_encode($batch)
        ];
        if (is_array($custom_fields)) {
            $params['customs_fields'] = $custom_fields;
        }
        echo "ListID : ".$sender->md_list_id." -- Compt -- $compt \n";

        $this->client->authenticate($sender->password, '');
        $this->client->SetParams($params);

        $R = $this->client->post("http://api.dpmail.fr/json/Subscriber.ImportBatch.php");
        $ApiResponse = json_decode($R->body);

        if(isset($ApiResponse->message) && $ApiResponse->message == 'Success') {
            return true;
        }
        \Log::error("[RouteurMaildrop] populate_list : NOT OK Sender $sender->id ($R)");
//        \Log::error(print_r($ApiResponse, true));
        return false;
    }

    function list_batch($sender, $BatchData)
    {
        $params = [
            'ListID' => $sender->md_list_id,
            'batch' => $BatchData
        ];

        $this->client->authenticate($sender->password, '');
        $this->client->SetParams($params);

        $R = $this->client->post("http://api.dpmail.fr/json/Subscriber.ImportBatch.php");
        $ApiResponse = json_decode($R->body);

        if(isset($ApiResponse->message) && $ApiResponse->message == 'Success') {
            return true;
        }
        \Log::error("[RouteurMaildrop] list_batch : NOT OK Sender $sender->id ($R)");
//        var_dump($R);

        return false;
    }

    function send_campagne($sender, $md_campagne_id)
    {
        $this->client->authenticate($sender->password, '');
        $this->client->SetParams(array(
                'cid' => $md_campagne_id,
            )
        );
        \Log::info("[RouteurMaildrop] send_campagne : before $md_campagne_id / Sender $sender->id");
        $R = $this->client->post("http://api.dpmail.fr/json/Campaign.Send.php");
        \Log::info("[RouteurMaildrop] send_campagne : after Campagne $md_campagne_id / Sender $sender->id");
        $ApiResponse = json_decode($R->body);
        if($ApiResponse->message == 'Success') {
            return true;
        }
//        dd($ApiResponse);
        \Log::error("[RouteurMaildrop] send_campagne : NOT OK Campagne $md_campagne_id / Sender $sender->id ($R)");
        return false;
    }

    function send_bat($id)
    {
        $bat = \App\Models\Campagne::find($id);

        $routeur = Routeur::where('nom', 'Maildrop')->first();
        \Cache::forget('c' . $bat->id . '-r'.$routeur->id);

        $html = $bat->generateHtml($routeur);

        $comptesbat = \App\Models\Sender::where('is_bat', true)
            ->where('routeur_id', $routeur->id)
            ->get(); // uniquement Maildrop

        foreach ($comptesbat as $compte) {

            if(empty($compte->bat_liste)) {
                continue;
            }

            $client = new \App\Classes\Routeurs\RouteurMaildropCurl();
            $client->authenticate($compte->password, '');

            //Multi-objets aléatoire
            $objets = explode('|', $bat->objet);
            $bat->objet = $objets[rand(0, count($objets) - 1)];


            $params = [
                'campaign_name' => $bat->ref,
                'reply_email' => $compte->domaine,
                'subject' => "[MD - BAT] " . $bat->objet,
                'from_email' => $compte->domaine,
                'from_name' => $bat->expediteur,
                'message_html' => $html,
                'list_id[]' => $compte->bat_liste,
                'metadata[headers][X-Tor-Base]' => 'TOR-'.md5(getenv('CLIENT_NAME').'-'.$bat->base_id)
            ];

            if(!empty(getenv('VR_WHITELIST'))) {
                $params['metadata[headers][X-VR-Whitelist]'] = getenv('VR_WHITELIST');
            }

            $client->SetParams($params);

            $R = $client->post("http://api.dpmail.fr/json/Campaign.Create.php");
            $ApiResponse = json_decode($R->body);

            $client->SetParams(array('cid' => $ApiResponse->data->cid));
            $R = $client->post("http://api.dpmail.fr/json/Campaign.Send.php");
            $ApiResponse = json_decode($R->body);

            if ("ok" != $ApiResponse->ResponseType) {
                \Log::error("[RouteurMaildrop] send_bat : NOT OK Campagne $id ($R)");
                return 'error';
            }
        }
        return 'ok';
    }

    // Partie pour statistiques
    function recup_click($accountKey, $cid)
    {

        $nombreclick = 0;
        $cliqueurs = array();
        $this->client->authenticate($accountKey, '');
        $this->client->SetParams(array(
                'cid' => $cid,
            )
        );

        $R = $this->client->post("http://api.dpmail.fr/json/Reports.Clicks.php");
        $ApiResponse = json_decode($R->body);

        if (!is_array($ApiResponse->data)) {
            // \Log::info("[RouteurMaildrop] getClicks : empty Campagne $campagne->id / CID $cid ");
            return 0;
        }

        foreach($ApiResponse->data as $obj)
        {
            $link_id = $obj->lid;

            $this->client->authenticate($accountKey, '');
            $this->client->SetParams(
                array
                (
                    'cid' => $cid,
                    'lid' => $link_id
                )
            );
            $R = $this->client->post("http://api.dpmail.fr/json/Reports.ClickDetails.php");
            $ApiResponse = json_decode($R->body);

            foreach($ApiResponse->data->data as $obj2)
            {
                if (!isset($obj2->member->email)) {
                    continue;
                } else {
                    $cliqueurs[$obj2->member->email]="";
                }
            }
        }
        $nombreclick = count($cliqueurs);
        unset($cliqueurs);
        return $nombreclick;
    }

    function recup_ouverture($accountKey, $cid)
    {
        $this->client->authenticate($accountKey, '');
        $this->client->SetParams( [
            'cid' => $cid,
        ]);

        $R = $this->client->post("http://api.dpmail.fr/json/Reports.Opens.php");
        $ApiResponse = json_decode($R->body);

        if($ApiResponse->message == 'Success') {
            // return $ApiResponse->data->total;
            return $ApiResponse->data;
        }
        \Log::info("[RouteurMaildrop] recup_ouverture : empty AccountKey $accountKey ($R)");
        return 0;
    }


    function recup_ouverture_page($accountKey, $cid, $page)
    {
        $this->client->authenticate($accountKey, '');
        $this->client->SetParams( [
            'cid' => $cid,
            'start' => $page,
        ]);

        $R = $this->client->post("http://api.dpmail.fr/json/Reports.Opens.php");
        $ApiResponse = json_decode($R->body);

        if($ApiResponse->message == 'Success') {
            // return $ApiResponse->data->total;
            return $ApiResponse->data;
        }
        \Log::info("[RouteurMaildrop] recup_ouverture : empty AccountKey $accountKey ($R)");
        return 0;
    }

    function recup_desabo($accountKey, $cid)
    {
        $this->client->authenticate($accountKey, '');
        $this->client->SetParams( [
            'cid' => $cid,
        ]);

        $R = $this->client->post("http://api.dpmail.fr/json/Reports.Unsubscribes.php");
        $ApiResponse = json_decode($R->body);

        if($ApiResponse->message == 'Success') {
            return $ApiResponse->data->total;
        }
        \Log::info("[RouteurMaildrop] recup_desabo : empty AccountKey $accountKey ($R)");
        return 0;
    }

    function recup_error($accountKey, $cid)
    {
        $this->client->authenticate($accountKey, '');
        $this->client->SetParams( [
            'cid' => $cid,
        ]);

        $R = $this->client->post("http://api.dpmail.fr/json/Reports.BounceMessages.php");
        $ApiResponse = json_decode($R->body);

        if($ApiResponse->message == 'Success') {
            return $ApiResponse->data->total;
        }
        \Log::info("[RouteurMaildrop] recup_error : empty AccountKey $accountKey ($R)");
        return 0;
    }

    function checkStatutImport($sender,$taskid)
    {
        $this->client->authenticate($sender->password, '');

        $this->client->SetParams( [
            'TaskID' => $taskid,
        ]);

        $R = $this->client->post("http://api.dpmail.fr/json/Subscriber.ImportByUrlGetStatus.php");
        $ApiResponse = json_decode($R->body);

        if(isset($ApiResponse->data->status) && $ApiResponse->data->status == 'COMPLETED') {
            return true;
        }
//        \Log::info("Reponse API au statut import by url : " . $R->body);
        \Log::info("[RouteurMaildrop] checkStatutImport : NOT OK Sender $sender->id / TaskID $taskid ($R)");
        return false;
    }

    function import_by_url($accountKey, $listid, $url)
    {
        $this->client->authenticate($accountKey, '');

        $this->client->SetParams( [
            'ListID' => $listid,
            'Url' => $url
        ]);

        \Log::info("[RouteurMaildrop] import_by_url : début appel $accountKey / ListID $listid / URL $url");
        $R = $this->client->post("http://api.dpmail.fr/json/Subscriber.ImportByUrl.php");
        $ApiResponse = json_decode($R->body);
        \Log::info("[RouteurMaildrop] import_by_url : fin appel $accountKey / ListID $listid / URL $url");
        if(isset($ApiResponse->message) && $ApiResponse->message == 'Success') {
            return $ApiResponse->data->TaskID;
        }

        \Log::error("[RouteurMaildrop] import_by_url : NOT OK AccountKey $accountKey / ListID $listid / URL $url ($R)");
        return false;
    }

    // modif ipcheck
    function IpPoolList($accountKey){
        $this->client->authenticate($accountKey, '');
        $R = $this->client->post("http://api.dpmail.fr/json/IpPool.List.php");
        $ApiResponse = json_decode($R->body);
        if (isset($ApiResponse->message) && $ApiResponse->message == 'Success') {
            $montableau = array();
            $montableau = $ApiResponse->data;
            return $montableau[0];
        }
        \Log::error("[RouteurMaildrop] IpPoolList : NOT OK AccountKey $accountKey ($R)");
        return false;
    }


    function IpPoolIps($accountKey, $poolid){
        $this->client->authenticate($accountKey, '');
        $params = [
            'poolid' => $poolid,
        ];
        $this->client->SetParams($params);
        $R = $this->client->post("http://api.dpmail.fr/json/IpPool.Ips.php");
        $ApiResponse = json_decode($R->body);
        if (isset($ApiResponse->message) && $ApiResponse->message == 'Success') {
            $montableau = array();
            $montableau = $ApiResponse->data;
            // return $montableau[0];
            return $montableau;
        }
        \Log::error("[RouteurMaildrop] IpPoolIps : NOT OK AccountKey $accountKey / PoolID $poolid ($R)");
        return false;
    }


  function CheckAccount($accountKey){
    $this->client->authenticate($accountKey, '');
    $params = [
    'string' => 'ok',
    ];
    $this->client->SetParams($params);
    $R = $this->client->post("http://api.dpmail.fr/json/Test.Echo.php");
    $ApiResponse = json_decode($R->body);
    if (isset($ApiResponse->message) && $ApiResponse->message == 'Success') {
      return true;
    } else {
      return false;
    }

  }


}
