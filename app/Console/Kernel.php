<?php namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

    /*
    * The Artisan commands provided by your application.
    *
    * @var array
    */
    protected $commands = [
        'App\Console\Commands\Abuse', //not used in code

        'App\Console\Commands\BasesDeactivate', //not used in code

        'App\Console\Commands\BaseAddDestinataires',

        'App\Console\Commands\BaseImportClics',
        'App\Console\Commands\BaseImportDesinscrits',
        'App\Console\Commands\BaseInsertDesinscrits',
        'App\Console\Commands\BaseImportBounces',

        // après le soucis de pdf4
        'App\Console\Commands\BaseImportDesinscritsOne',

        'App\Console\Commands\BaseImportMindbaz',

        'App\Console\Commands\TokensManage',
        'App\Console\Commands\TokensReinit', // not used in code
        'App\Console\Commands\TokensReset',
        'App\Console\Commands\TokensStats',

        // Campagnes : Phases (tokens, segment)
        'App\Console\Commands\CampagneAddRepoussoirs',
        'App\Console\Commands\CampagneLaunchSend',
        'App\Console\Commands\CampagneResetRepoussoirs',
        'App\Console\Commands\CampagneSegment',
        'App\Console\Commands\CampagneTokens',

        //Phase sélection de destinataires (sans prise en compte de pression marketing)
        'App\Console\Commands\CampagneDestinataires',

        'App\Console\Commands\CampagneTokensOld',
        'App\Console\Commands\CampagnePrepare',


        'App\Console\Commands\CampagneSegmentMaildrop',
        'App\Console\Commands\CampagneSegmentMaildropOld',
        'App\Console\Commands\CampagneSendMaildrop',

        // 'App\Console\Commands\CampagneSegmentSmessage',
        // 'App\Console\Commands\CampagneSendSmessage',

        // 'App\Console\Commands\CampagneSegmentPhoenix',
        // 'App\Console\Commands\CampagneSendPhoenix',

        // 'App\Console\Commands\CampagneSegmentMailForYou',
        // 'App\Console\Commands\CampagneSendMailForYou',

        'App\Console\Commands\CampagneSegmentMindbaz',
        'App\Console\Commands\CampagneSendMindbaz',

        // Stats
        'App\Console\Commands\CampagneClicks',
        'App\Console\Commands\CampagneBounces',
        'App\Console\Commands\CampagneLightBounces',
        'App\Console\Commands\CampagneStats',
        'App\Console\Commands\CampagneOptout',

        'App\Console\Commands\ComputeClicsMaildrop',
        // 'App\Console\Commands\ComputeClicsMailForYou',
        'App\Console\Commands\ComputeClicsMindbaz',

        'App\Console\Commands\ComputeOpeningsMindbaz',

        'App\Console\Commands\get_optout_and_bounces', // gets only bounces for smessage && phoenix

        'App\Console\Commands\CampagneUnsubscribeMaildrop',
        // 'App\Console\Commands\CampagneUnsubscribeSmessage',
        // 'App\Console\Commands\CampagneUnsubscribePhoenix',
        // 'App\Console\Commands\CampagneUnsubscribeMailforyou',
        'App\Console\Commands\CampagneUnsubscribeMindbaz',

        'App\Console\Commands\CampagneBouncesMaildrop',
        // 'App\Console\Commands\CampagneBouncesMailForYou',
        'App\Console\Commands\CampagneBouncesMindbaz',

        'App\Console\Commands\FaisStats',
        'App\Console\Commands\OuverturesStats',
        'App\Console\Commands\OptoutStats',

        // Cleaning or reset commands
        'App\Console\Commands\SenderResetQuota',
        'App\Console\Commands\ResetStatsTokens',
        'App\Console\Commands\ListeCleanMaildrop',

        // Geoloc commandes
        // 'App\Console\Commands\geolocmajdestinataire',
        // 'App\Console\Commands\geolocmajvolume',
        'App\Console\Commands\majstatsplanning',

        // 'App\Console\Commands\StatsRouteurSmessage',
        'App\Console\Commands\StatsRouteurMaildrop',
        // 'App\Console\Commands\StatsRouteurPhoenix',
        // 'App\Console\Commands\StatsRouteurMailforyou',
        'App\Console\Commands\StatsRouteurMindbaz',


        //IP commmands
        'App\Console\Commands\etatip',
        'App\Console\Commands\CheckEmailBl',

        'App\Console\Commands\CheckSenderMd',
        'App\Console\Commands\MailStats',

        //Wizweb ftp command
        'App\Console\Commands\WizwebApiFtp',

        //Traitement unsubscribe
        'App\Console\Commands\UnsubscribeTor',

        // pour recup les reponses
        'App\Console\Commands\EmailReply',

        //Back up only necessary files commands
        'App\Console\Commands\ServerDump',
        'App\Console\Commands\ServerImport',
        'App\Console\Commands\ServerCheck',

        //Plannings monitoring
        'App\Console\Commands\PlanningCheck',


        // TODO : import des cliqueurs version de Fab à comparer avec Ade
        'App\Console\Commands\ImportFichierCliqueurs',
        'App\Console\Commands\ParseCampagnesRouteursClic',
        'App\Console\Commands\DeleteFileCliqueurs',
        'App\Console\Commands\FichierInformationDestiCsv',

        'App\Console\Commands\ComputeClics', // not used in code
        'App\Console\Commands\ComputePixel', // not used in code
        'App\Console\Commands\DestinataireUpdateFai', // not used in code
        'App\Console\Commands\dispatch_destinataire_domaine', // not used in code  (asso domain <-> desti)

        // DEV
        'App\Console\Commands\TestCommand', // not used in code
        // NOSQL : Commands Arango
        // 'App\Console\Commands\DumpOuverturesToArango',
        // 'App\Console\Commands\DumpOuverturesToArangoCron',
        // 'App\Console\Commands\ArangoTokenSegment',
        // 'App\Console\Commands\ArangoMajCollection',
        // 'App\Console\Commands\DumpArangoGeneral',

        // pour alerter si bug m4y encore
        'App\Console\Commands\Alert_Routing',

        // test import fichier auto
        // 'App\Console\Commands\MailForYouImportBlacklist',

        'App\Console\Commands\MajSettings',
        'App\Console\Commands\CheckBL',

        // Arango dev
        // 'App\Console\Commands\ArangoDumpDesti',
        // 'App\Console\Commands\ArangoGenerateTokens',

        'App\Console\Commands\StatsBaseDetails',

        // edatis
        // 'App\Console\Commands\CampagneSendEdatis',
        // 'App\Console\Commands\CampagneUnsubscribeEdatis',
        // 'App\Console\Commands\StatsRouteurEdatis',

        'App\Console\Commands\ExtractCsv',
        'App\Console\Commands\FichierInfoDate',
        // 'App\Console\Commands\CampagneCaImport',
        // 'App\Console\Commands\CampagneCaCalculMois',
        // 'App\Console\Commands\CampagneCaRoi',
        // 'App\Console\Commands\CampagneCaConcat',
        // 'App\Console\Commands\CampagneCaRecupOldCampagne',

        'App\Console\Commands\ExtractEmailOuvreurs',
        'App\Console\Commands\ExtractBddJson',
        'App\Console\Commands\exportMindbazUnsubscribe',
        'App\Console\Commands\ExtractEmailOuvreursCampagneId',
        'App\Console\Commands\BaseImportMindbazChoixSender',
        'App\Console\Commands\BaseImportMindbazBrute',
        'App\Console\Commands\BaseImportHardBounceOne',
        'App\Console\Commands\ExtractOuvreursTheme',
        'App\Console\Commands\ExtractOuvreursBdd',

        'App\Console\Commands\exportMindbazUnsubscribescope',
        // 'App\Console\Commands\BouncesCompilation',

        'App\Console\Commands\OuvMDv2',
        'App\Console\Commands\DedupConsole',

        'App\Console\Commands\SandBox',

        'App\Console\Commands\TokenWeek',
        'App\Console\Commands\TokensManageOne',
        'App\Console\Commands\ExtractOuvreursThemeV2',
        'App\Console\Commands\ExtractOuvreursBddV2',



    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //\Log::info('Kernel->schedule');

        if (\Schema::hasTable('settings')) {
            $clean_relaunch = \DB::table('settings')
                ->where('parameter', 'clean_relaunch')
                ->first();
        }

        if (env('APP_ENV') == 'prod') {

            $heure = date('H');

            //On génère les tokens (important que ça se fasse après la phase de desinscription)
            if($heure > 4) {

                if (empty($clean_relaunch) or $clean_relaunch->value == 1) {
                    $schedule->command('tokens:manage')
                        ->hourly()
                        ->withoutOverlapping();
                }

                $schedule->command('planning:check')
                    ->everyThirtyMinutes()
                    ->withoutOverlapping();

                // Pour préparer les campagnes à envoyer (tokens+segments)
                $schedule->command('campagne:prepare')
                    ->cron('* * * * *')
                    ->withoutOverlapping();

                //Pour lancer les envois des campagnes segmentées
                $schedule->command('campagne:launch_send')
                    ->cron('* * * * *')
                    ->withoutOverlapping();
            }

            //Vérifie si il y a un problème de serveur (disque, mysql, nginx)
	        $schedule->command('server:check')
	        	->hourly()
				->withoutOverlapping();

            // On récupère les stats ouvertures (pixel)
            $schedule->command('ouvertures:stats')
                ->dailyAt('22:00')
                ->withoutOverlapping();

            //Etat des lieux des IP pour MailDrop
            $schedule->command('ip:etat')->daily();
            //On reset les stats des tokens
            $schedule->command('reset:statstokens')->dailyAt('00:00');

            $schedule->command('reply:traitement')->dailyAt('00:20');

            //On lance la récupération des désabonnés MailDrop + Phoenix + Smessage
            $schedule->command('campagne:unsubscribe_maildrop')->dailyAt('00:30');
            // $schedule->command('campagne:unsubscribe_edatis')->dailyAt('00:30');
            // $schedule->command('campagne:unsubscribe_phoenix')->dailyAt('00:30');
            // $schedule->command('campagne:unsubscribe_smessage')->dailyAt('00:30');
            // $schedule->command('campagne:unsubscribe_mailforyou')->dailyAt('00:30');
            $schedule->command('campagne:unsubscribe_mindbaz')->dailyAt('05:05');

            // En dev mais aucun soucis normalement
            // $schedule->command('campagne:bounces_mailforyou')->dailyAt('00:50');
            $schedule->command('campagne:bounces_maildrop')->dailyAt('00:50');
            $schedule->command('campagne:bounces_mindbaz')->dailyAt('00:50');
            //TODO : faire comme pour campagne:bounces_routeur -> campagne:bounces_routeur

            $schedule->command('report:out')->dailyAt('00:45'); //ne traite plus que les bounces

            //Les quotas des senders repartent de zéro
            $schedule->command('senders:reset_quota')->dailyAt('01:00');

            //On supprime les repoussoirs datant de +1 semaine
            $schedule->command('campagne:reset_repoussoirs')->dailyAt('01:00');

            //On supprime les listes MailDrop qui ne seront plus utiles
            $schedule->command('liste:clean_maildrop')->dailyAt('01:30');

            //On récupère cliqueurs (fichier global) depuis MailDrop et on l'importe dans clics
            $schedule->command('clics:compute_maildrop')->dailyAt('02:00');
            // $schedule->command('clics:compute_mailforyou')->dailyAt('02:00');
            $schedule->command('clics:compute_mindbaz')->dailyAt('06:00');

//            $schedule->command('openings:compute_mindbaz')->dailyAt('02:15');

            // Récupère les désinscrits de l'autre serveur Tor
            $schedule->command('unsubscribe:get')->dailyAt('02:30');

            //Tokens datant de +2 semaines supprimés
            $schedule->command('tokens:reset')->dailyAt('03:00');

            //Dump des données du serveur actuel (pour re-injecter vers un autre serveur)
            if(getenv('DUMP') == 'active') {
                $schedule->command('server:dump')
                    ->dailyAt('03:15')
                    ->withoutOverlapping();
            }
            //Import de données d'un autre serveur
            if(!empty(getenv('DUMP_SERVER'))){
                $schedule->command('server:import')
                    ->dailyAt('04:00')
                    ->withoutOverlapping();
            }

            //On récupère les stats désinscrits+bounces dans une seule table
            $schedule->command('optout:stats')->dailyAt('05:00');

            $schedule->command('ouv:md')->dailyAt('06:00');


            //Consolidation STATS : Récup toutes les 4 heures
            /*
            if($heure%4 == 0) {
                $active_routeurs = \DB::table('routeurs')
                    ->select('id', 'nom')
                    ->where('is_active', 1)
                    ->whereNotIn('nom', ['MailForYou','Edatis'])
                    ->get();
                foreach($active_routeurs as $ar)
                {
                    //Consolidation des stats via API
                    $name = strtolower($ar->nom);
                    $schedule->command("stats:maj$name")
                        ->hourly()
                        ->withoutOverlapping();
                }

                $schedule->command('stats:majmailforyou')
                ->hourly();

            }

            $schedule->command('stats:majedatis')
            ->daily();


            $schedule->command('destinataire:geoloc_maj')
                ->dailyAt('21:00')
                ->withoutOverlapping();


            */
            // a refaire commun différent routeur
            $schedule->command('alert:check')
                ->hourly()
                ->withoutOverlapping();


            // $schedule->command('ca:init_db_concat')->daily();
            // $schedule->command('ca:old')->daily();
            // $schedule->command('ca:calcul')->hourly();
            // $schedule->command('ca:roi')->hourly();

            if (env('EXPORT_TYPE') == 'agv'){
              $schedule->command('extract:bdd 11 http://tor-agv.lead-factory.net/webservice-import-lead?webservice_key=T4Qnzg17r2XptRYvYTD9qD36A5uo93gOnNUKb23J9p03h5SMm1J5K51kl6U3aIxn')->daily();
            }

            if (env('CLIENT_NAME') == 've'){
              $schedule->command('export:mb_unsubscribe 16 19')->daily();
              $schedule->command('export:mb_unsubscribe 15 21')->daily();
            }

            // rajouter les bases manquantes

            if (env('CLIENT_NAME') == 'ouistipix'){

              $schedule->command('tokens:week')->dailyAt('00:05');

              $schedule->command('export:mb_unsubscribe 8 4')->daily();
              $schedule->command('export:mb_unsubscribe 8 6')->daily();


              $schedule->command('export:mb_unsubscribe 9 5')->daily();
              $schedule->command('export:mb_unsubscribe 10 5')->daily();

              $schedule->command('export:mb_unsubscribe 12 7')->daily();
              // $schedule->command('export:mb_unsubscribe 11 4')->daily();

              $schedule->command('export:mb_unsubscribe 13 8')->daily();
              $schedule->command('export:mb_unsubscribe 14 8')->daily();

            }



        }
    }
}
