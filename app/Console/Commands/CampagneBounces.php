<?php namespace App\Console\Commands;

use App\Classes\Routeurs\RouteurMaildrop;
use App\Classes\Routeurs\RouteurPhoenix;
use App\Classes\Routeurs\RouteurSmessage;
use App\Models\Sender;
use App\Models\CampagneRouteur;
use App\Models\Campagne;
use App\Models\Planning;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;

class CampagneBounces extends Command {

    /**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'campagne:bounces';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Get bounces for one campaign';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

    public function handle()
    {

        $this->routeur_md = new RouteurMaildrop();
        $this->routeur_sm = new RouteurSmessage();
        $this->routeur_ph = new RouteurPhoenix();

        \App\Helpers\Profiler::start('campagne:bounces');

        $planning = Planning::find($this->argument('planning_id'));
        $campagne = Campagne::find($planning->campagne_id);

        $camp_rout = CampagneRouteur::where('planning_id', $planning->id)->get();

        foreach($camp_rout as $row)
        {
            $sender = Sender::find($row->sender_id);

            \Log::info('Campagne:bounces - campagne '.$campagne->id.', sender '.$sender->id);

//            //Si MailDrop
//            if ($sender->routeur->nom == 'Maildrop') {
//                echo "Sender $sender->id : ";
//                $this->routeur_md->getBounces($sender->password, $campagne, $row->cid_routeur);
//            }
            //Si SMessage
            if ($sender->routeur->nom == 'Smessage') {

                echo "Sender $sender->id : ";
                $this->routeur_sm->getBounces($sender, $campagne, $row->cid_routeur);
            }
            //Si Phoenix
            if ($sender->routeur->nom == 'Phoenix') {
                echo "Sender $sender->id : ";
                $this->routeur_ph->getBounces($sender, $campagne, $row->cid_routeur);
            }

            // on fait notre call mailforyou ici
        }

        \App\Helpers\Profiler::report();
    }

    protected function getArguments()
    {
        return [
            ['planning_id', InputArgument::REQUIRED, 'Planning id.'],
        ];
    }
}
