<?php namespace App\Console\Commands;

use Illuminate\Console\Command;

class ImportListes extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'import_listes';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Import listes from /EMAIL';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
        \App\Models\Destinataire::unguard();

        $base_code = $this->ask('Code base ?');
        $base = \App\Models\Base::where('code', $base_code)->first();

        $path = realpath(storage_path().'/emails/'.$base_code);
        $objects = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path), \RecursiveIteratorIterator::SELF_FIRST);
        $pattern = '#.*RAJOUTER/(.*)_.*/(.*).txt#U';

        foreach($objects as $name => $object){
            if (strpos(strtolower($name), '.zip') !== false) {
                continue;
            }
            if (strpos(strtolower($name), '.rar') !== false) {
                continue;
            }

            if (!preg_match($pattern, $name, $matches)) {
                continue;
            }

            echo "\n Fichier : $name \n";

            $origin = ucfirst(strtolower($matches[1]));
            $liste = str_replace('dedubliques/','',$matches[2]);

            $f = fopen($name, 'r');
            $i = 0;
            while(!feof($f) ) {
                $row = fgets($f, 1024);
                if (strlen($row) == 0) {
                    continue;
                }

                $adresse = trim($row, "\t\n\r ");

                \App\Models\Destinataire::firstOrCreate(['liste' => $liste, 'origine'=> $origin, 'base_id' => $base->id, 'mail' => \App\Models\Destinataire::crypt($adresse) ]);

                if ($i % 100 == 0) {
                    echo '.';
                }
                $i++;
            }
        }
	}
}
