<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;

class ServerDump extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'server:dump';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Lance le script shell permettant de faire le dump des fichiers necessaires..';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        \Log::info('ServerDump : started');

        if(getenv('DUMP') == 'active') {
            // start the db and folders compress & export
            exec('cd ~/'.getenv('CLIENT_URL'));
            exec('mysqldump -u' . getenv('DB_USERNAME') . ' -p' . getenv('DB_PASSWORD') . ' ' . getenv('DB_DATABASE') . ' --ignore-table=' . getenv('DB_DATABASE') . '.tokens_' . date('Y-m-d') . ' | gzip > ~/' . getenv('CLIENT_URL') . '/dump.sql.gz');
            exec('tar -zcvf storage.tar.gz storage/');
            exec('tar -zcvf public.tar.gz public/');

        }
        \Log::info('ServerDump : finished');

    }
}