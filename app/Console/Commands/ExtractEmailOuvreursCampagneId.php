<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ExtractEmailOuvreursCampagneId extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'extract:ouvreurs_c {nom_fichier} {campagne_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Extrait les ouvreurs pour une campagne';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

      $campagneid = $this->argument('campagne_id');
      //$scopedebut = $this->argument('scope_debut');
      //$scopefin = $this->argument('scope_fin');


      // checkdunombre d'ouvreurs

      $nb_ouvreurs = \DB::table('ouvertures')
      // ->whereBetween('created_at', [$scopedebut,$scopefin])
      ->where('campagne_id', $campagneid)
      ->count();
      echo ('Nombre ouvertures pour la campagne : ' . $nb_ouvreurs ."\n");
      // $nb_ouvreurs = 1000;
      $offset = 1000;
      $skip = 0;

      $long = strlen($nb_ouvreurs) - substr_count($offset,'0');
      $nbretape = substr($nb_ouvreurs,0,$long);
      // echo $nbretape . "\n";
      // $chaine
      for ($i = 1; $i <= $nbretape; $i++) {
        $chaine = '';
        $destinatairesid = \DB::table('ouvertures')
        ->select('destinataire_id')
        ->where('campagne_id', $campagneid)
        // ->whereBetween('created_at', [$scopedebut,$scopefin])
        ->distinct()
        ->skip($skip)
        ->take($offset)
        ->get();

        $did = array_pluck($destinatairesid, 'destinataire_id');
        var_dump($did);

        $destinatairesemails = \DB::table('destinataires')
        ->select('mail')
        ->where('statut','<','3')
        ->whereIn('id', $did)
        ->get();
        var_dump($destinatairesemails);

        foreach ($destinatairesemails as $key => $v) {
          $chaine .= $v->mail."; \n";
        }



        $file = storage_path() . '/listes/download/'.$this->argument('nom_fichier').'.csv';
        $fp = fopen($file,"a+");
        fwrite($fp, $chaine);
        fclose($fp);

        $skip = $skip + $offset;

      }



      die();
      // $long = strlen($nbrtotaldesti) - substr_count($offset,'0');

      //  si $nb_ouvreurs > 10000

      // esle

      // je coupe en block de 1k pour where in

      $destinatairesid = \DB::table('ouvertures')
      ->select('destinataire_id')
      ->whereBetween('created_at', [$scopedebut,$scopefin])
      ->distinct()
      ->get();

      $did = array_pluck($destinatairesid, 'destinataire_id');

      var_dump($did);

      $destinatairesemails = \DB::table('destinataires')
      ->select('mail')
      ->whereIn('id', $did)
      ->get();


      var_dump($destinatairesemails);




    }

    protected function getArguments()
    {
        return [
            ['nom_fichier', InputArgument::REQUIRED, 'Nom fichier'],
            ['campagne_id', InputArgument::REQUIRED, 'La campagne'],
            // ['scope_debut', InputArgument::REQUIRED, 'Date début'],
            // ['scope_fin', InputArgument::REQUIRED, 'Date de fin'],
        ];
    }
}
