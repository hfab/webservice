<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Notification;
use Symfony\Component\Console\Input\InputArgument;

class BaseImportBounces extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'base:import_bounces';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ajoute des désinscrits depuis un fichier situé dans le dossier desinscrits.';

    /**
     * Create a new command instance.ha voilà
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->bases = array();
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        $this->bases = \DB::table('bases')->select('id')->get();
        $file = storage_path().'/bounces/'.$this->argument('file');
        $filename = $this->argument('file');
        if (!is_file($file)) {
            $this->error('File not found : '.$file);
        }

        \Log::info("[BaseImportBounces] : Import bounces à partir du fichier -- $file");

        $ts = date('Y-m-d H:i:s');

        \App\Helpers\Profiler::start('base:import_bounces');

        $bulk_count = 0;

        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $extension = \File::extension($file);
        $name = \File::name($file);

        $updateData = array();

        $fh = fopen($file, 'r');

        $total = 0;
        $inserted = 0;

        while (!feof($fh))
        {
            $cells = [];
            $row = fgets($fh, 1024);
            $row = trim($row, "\r\n\t ");

            if (strpos($row, ';') !== false) {
                $cells = explode(';', $row);
            } elseif (strpos($row, '|') !== false) {
                $cells = explode('|', $row);
            } else {
                $cells[] = trim($row, " \r\n\t\"");
            }

            foreach($cells as $cell)
            {

                if (filter_var($cell, FILTER_VALIDATE_EMAIL)) {
                    $total++;

                    $updateData[] = $cell;

                    $bulk_count++;
                    if (count($updateData) >= 5000) {
                        $this->writeUpdates($updateData, $filename);
                        $updateData = array();
                        $bulk_count = 0;
                    }

                    $inserted++;
                    continue;
                }
            }
        }

        if (count($updateData) > 0) {
            $this->writeUpdates($updateData, $filename);
            $updateData = array();
            $bulk_count = 0;
        }

        if (isset(\Auth::User()->id)) {
            $user_id = \Auth::User()->id;
        } else {
            $user_id = 1;
        }

        Notification::create([
            'user_id' => $user_id,
            'level' => 'info',
            'is_important' => 1,
            'message' => "Fichier bounces $name importé."
        ]);

        \Log::info("[BaseImportBounces] : End of Import bounces a partir fichier -- $file");
        \App\Helpers\Profiler::report("base:import_bounces");
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['file', InputArgument::REQUIRED, 'Path to the file']
        ];
    }

    private function writeUpdates($mails, $filename) {
        $now = date('Y-m-d H:i:s');
        foreach($this->bases as $b) {
            \DB::table('destinataires')
                ->where('base_id',$b->id)
                ->whereNull('optout_file')
                ->whereIn('mail', $mails)
                ->update(['statut' => 44, 'optout_at'=> $now, 'optout_file' => $filename]);
        }
    }
}
