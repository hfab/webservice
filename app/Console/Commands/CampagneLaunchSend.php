<?php namespace App\Console\Commands;

use App\Classes\Routeurs\RouteurMaildrop;
use App\Classes\Routeurs\RouteurPhoenix;
use App\Models\Planning;
use App\Models\Campagne;
use App\Models\Sender;
use App\Classes\Routeurs\RouteurSmessage;
use Illuminate\Console\Command;
use Carbon\Carbon;

class CampagneLaunchSend extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'campagne:launch_send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Routine : preparation to send';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $auto_setting = \DB::table('settings')
                ->where('parameter', 'is_auto')
                ->first();

        if(!$auto_setting or $auto_setting->value != 1) {
            \Log::notice('CampagneLaunchSend : AUTOMATIC CAMPAIGN NOT ACTIVE');
            exit;
        }

        $heure = date('H');
        $time = date('H:i');

        if($heure <= 4) {
            exit;
        }

        $plannings = Planning::where('date_campagne', date('Y-m-d'))
            ->where('time_campagne','<=',$time)
            ->whereNotNull('tokens_at')
            ->whereNotNull('segments_at')
            ->whereNull('sent_at')
            ->get();

        \Log::info('CampagneLaunchSend : '.count($plannings).' plannifs à traiter');

        foreach($plannings as $planning)
        {
          // var_dump($planning->routeur->nom);
            $campagne = Campagne::find($planning->campagne_id);
            $date_segments = Carbon::parse($planning->segments_at);

            if (\App::environment('prod')) {
                \App\Models\Notification::create([
                    'user_id' => 1, // /!\ User System
                    'level' => 'info',
                    'message' => 'Envoi de la campagne ' . $campagne->ref,
                    'url' => '/campagne/' . $campagne->id . '/stats'
                ]);
                \Log::info('CampagnePrepare : Send pour ' .$campagne->id.' (Campagne) / '.$planning->id. ' (Planning)');

                if($planning->routeur->nom == 'Smessage'){
                    $routeur = new RouteurSmessage();
                    $senderenvoi = \DB::table('campagnes_routeurs')
                        ->where('campagne_id', $campagne->id)
                        ->where('planning_id', $planning->id)
                        ->where('created_at','>', date('Y-m-d 00:00:00'))
                        ->get();
                    // pour chaque senders sur lequel la campagne est présente,
                    // on vérifie si l'import s'est bien passé, sinon on ne lance pas le process d'envois
                    foreach ($senderenvoi as $lesender)
                    {
                        $infosender = Sender::find($lesender->sender_id);

                        if(!$routeur->checkStatutImport($infosender,$lesender->cid_routeur)) {
                            \Log::info("Campagne $campagne->id / Planning $planning->id : Import non terminé sur $infosender->id ($infosender->nom)");
                            continue 2;
                        }
                    }
                    $this->call('campagne:send_smessage', array('planning_id' => $planning->id));
                } else if ($planning->routeur->nom == 'Maildrop') {
                    $routeur = new RouteurMaildrop();
                    $senderenvoi = \DB::table('campagnes_routeurs')
                        ->where('campagne_id', $campagne->id)
                        ->where('planning_id', $planning->id)
                        ->where('created_at','>', date('Y-m-d 00:00:00'))
                        ->get();
                    // pour chaque senders sur lequel la campagne est présente,
                    // on vérifie si l'import s'est bien passé, sinon on ne lance pas le process d'envois
                    foreach ($senderenvoi as $lesender)
                    {
                        $infosender = Sender::find($lesender->sender_id);

                        if(!$routeur->checkStatutImport($infosender,$lesender->taskid)) {
                            \Log::info("Campagne $campagne->id / Planning $planning->id : Import non terminé sur $infosender->id ($infosender->nom)");
                            continue 2;
                        }
                    }
                    $this->call('campagne:send_maildrop', array('planning_id' => $planning->id));
                } else if ($planning->routeur->nom == 'Phoenix') {
                    $routeur = new RouteurPhoenix();
                    $senderenvoi = \DB::table('campagnes_routeurs')
                        ->where('campagne_id', $campagne->id)
                        ->where('planning_id', $planning->id)
                        ->where('created_at','>', date('Y-m-d 00:00:00'))
                        ->get();
                    // pour chaque senders sur lequel la campagne est présente,
                    // on vérifie si l'import s'est bien passé, sinon on ne lance pas le process d'envois
                    foreach ($senderenvoi as $lesender)
                    {
                        $infosender = Sender::find($lesender->sender_id);

                        if(!$routeur->checkStatutImport($infosender,$lesender->taskid)) {
                            \Log::info("Campagne $campagne->id / Planning $planning->id : Imports non terminés.");
                            continue 2;
                        }
                    }
                    $this->call('campagne:send_phoenix', array('planning_id' => $planning->id));
                } else if ($planning->routeur->nom == 'MailForYou') {
                  $this->call('campagne:send_m4u', array('planning_id' => $planning->id));
                }
                else if ($planning->routeur->nom == 'Mindbaz') {
                    $this->call('campagne:send_mindbaz', array('planning_id' => $planning->id));
                }
            }
        }
    }
}
