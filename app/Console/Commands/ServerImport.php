<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;

class ServerImport extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'server:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Récupère un dump d\'un autre serveur pour l\'importer ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        \Log::info('ServerImport : started');

        if(!empty(getenv('DUMP_SERVER'))) {

            exec('cd ~/'.getenv('CLIENT_URL'));

            //todo manually
//            exec('rm -rf public_old/ storage_old/');

            //we delete old files in case they exist
            exec('rm -rf storage.tar.gz public.tar.gz');
            exec('rm -rf dump.sql.gz');

            ////todo manually keep the old folders to avoid " 'ip' folder deletion" -> causing error on commands
//            exec('mv storage/ storage_old/');

            exec('scp forge@'.getenv('DUMP_SERVER').':~/'.getenv('DUMP_SERVER').'/storage.tar.gz .');
            exec('scp forge@'.getenv('DUMP_SERVER').':~/'.getenv('DUMP_SERVER').'/public.tar.gz .');
            exec('scp forge@'.getenv('DUMP_SERVER').':~/'.getenv('DUMP_SERVER').'/dump.sql.gz .');

            //extract folders backups : //todo manually
//            exec('tar -zxvf storage.tar.gz');
//            exec('tar -zxvf public.tar.gz');

            //import bdd
            exec('zcat dump.sql.gz | mysql -u'.getenv('DB_USERNAME').' -p'.getenv('DB_PASSWORD').' '.getenv('DB_DATABASE'));
        }

        \Log::info('ServerImport : finished');

    }
}
