<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class StatsBaseDetails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bases:stats';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $pattern = array(

      '@orange.fr',
      '@wanadoo.fr',
      '@voila.fr',


      '@free.fr',
      '@aliceadsl.fr',
      '@libertysurf.fr',
      '@freesbee.fr',
      '@online.fr',

      '@sfr.fr',
      '@neuf.fr',
      '@cegetel.net',
      '@numericable.fr',
      '@noos.fr',
      '@club-internet.fr',

      '@bbox.fr',

      '@laposte.net'

      );

      $bdd = \DB::table('bases')->get();

      foreach ($bdd as $bd) {

        echo "Base : " . $bd->nom . "\n";
      // var_dump($pattern);
      foreach ($pattern as $v) {
        // $count = \DB::statement("select count(id) from destinataires where base_id = 1 and statut = 0 and mail like '%".$v."';");

        $count = \DB::table('destinataires')->where('base_id', $bd->id)->where('statut','0')->where('mail','LIKE','%'.$v)->count();

        // var_dump($v);
        echo "FAI : " . $v . "\n";
        echo "Nombre : " . $count . "\n";

      }
      echo "\n";
      echo "\n";
      }

    }
}
