<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\Planning;
use App\Classes\Routeurs\RouteurMailForYou;
use App\Models\Campagne;
use App\Models\Routeur;
use App\Models\Fai;
use App\Models\Sender;

class CampagneSegmentMailForYou extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campagne:segment_mailforyou {planning_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Segment Mail For You';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $auto_setting = \DB::table('settings')
            ->where('parameter', 'is_auto')
            ->first();
        if(!$auto_setting or $auto_setting->value != 1){
            \Log::info('CampagneLaunchSend : AUTOMATIC CAMPAIGN NOT ACTIVE');
            exit;
        }
        $routeur_setting = \DB::table('settings')
            ->where('parameter', 'is_mailforyou')
            ->first();

        if(!$routeur_setting or $routeur_setting->value != 1){
            \Log::info('CampagneLaunchSend : MailForYou DISABLE');
            exit;
        }

        $planning = Planning::find($this->argument('planning_id'));
        $planning->nb_trials++;
        $planning->save();

        $campagneid = $planning->campagne_id;
        echo "\nCampagne Segment \n";
        $routeur = new RouteurMailForYou();
        $lacampagne = Campagne::where('id',$campagneid)->first();
        $m4u = Routeur::where('nom','MailForYou')->first();

        $anonyme = "";
        if($planning->type == 'anonyme') {
            $anonyme = "_a";
            $ano = true;
        }

        \Log::info("[CampagneSegmentMailForYou][P$planning->id] : Début Segmentation (Planning $planning->id/Campagne $lacampagne->id) {Trial n°$planning->nb_trials}");

        $fais = Fai::all();
        $lesadressestokens = array();
        $lesadressestokens = \DB::table('tokens')
            ->select('destinataire_id')
            //           ->where('fai_id',$f->id)
            ->where('campagne_id',$campagneid)
            ->where('planning_id',$planning->id)
            ->where('date_active',$planning->date_campagne)
            ->get();

        $info_planning_sender = \DB::table('planning_senders')
            ->where('planning_id',$this->argument('planning_id'))
            ->first();

        $lesenderm4y = \DB::table('senders')
            ->where('id',$info_planning_sender->sender_id)->first();

        $file = storage_path() . '/mailforyou/s' . $lesenderm4y->nom . '_c' . $lacampagne->id .'_p'.$planning->id. $anonyme.'.csv';
        $sender = Sender::find($lesenderm4y->id);

        $quota_left = $sender->quota_left - count($lesadressestokens);

        //l'historique
        \DB::table('senders_history')
            ->insert(
                [
                    'quota_before' => null,
                    'sender_id' => $sender->id,
                    'fai_id' => null,
                    'planning_id' => $planning->id,
                    'quota_left_before' => $sender->quota_left,
                    'quota_left' => $quota_left,
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s")
                ]
            );

        // ici que ca change
        $this->write_tokens($file, $lesadressestokens);
        $sender->quota_left = $quota_left;
        $sender->nb_campagnes_left -= 1;
        $sender->save();

        if(file_exists(storage_path() . '/mailforyou/zip/' . $lacampagne->ref)){
            \Log::info("Le fichier zip de la campagne existe deja");
        } else {
            mkdir(storage_path() . '/mailforyou/zip/' . $lacampagne->ref);
        }

        $zip = new \ZipArchive;
        $filename = storage_path() . '/mailforyou/zip/'. $lacampagne->ref .'/fichiercampagne.zip';
        if($zip->open($filename, \ZipArchive::CREATE) === true)
        {
            $zip->addFile('index.html');
            $zip->addFromString('index.html',$lacampagne->generateHtmlM4u($m4u));
            $zip->close();

        } else {
            echo 'Echec creation ZIP' . "\n";
        }

        $liste = array();

        $chemin = storage_path() . '/mailforyou/s' . $lesenderm4y->nom . '_c' . $lacampagne->id .'_p'.$planning->id. $anonyme.'.csv';

        $fp = fopen($chemin,"r");
        if ($fp)
        {
            while (!feof($fp))
            {
                $buffer = fgets($fp);
                $buffer = trim($buffer);

                if($buffer !== ''){
                    // on casse la chaine ici ?
                    $e = explode(";", $buffer);
                    // var_dump($buffer);
                    $liste[] = [$e[0],$e[1],$e[2]];
                }
            }
            fclose($fp);
        }

        echo 'Creation liste desti' . "\n";
        $retour = $routeur->CreationListeDestinataireFields_new($lesenderm4y->password,'Dev_Liste_Segment_production_'.time(),$lesenderm4y->api_login,$lesenderm4y->api_key);
        echo 'Import liste desti' . "\n";
        shuffle($liste);
        $decoupeliste = array_chunk($liste,5000);

        foreach ($decoupeliste as $t) {
            echo 'Import Chunk' . "\n";
            $retour2 = $routeur->AjoutListeDestinataireFields_new($retour['id'], $t,$lesenderm4y->api_login,$lesenderm4y->api_key);
            var_dump($retour2);
        }

        echo 'Import liste desti fini' . "\n";

        echo 'Creation Campagne' . "\n";
        $retour3 = $routeur->CreationCampagne_new($lacampagne->nom . '-' . time(), $lacampagne->objet, $lesenderm4y->password,$lesenderm4y->api_login,$lesenderm4y->api_key);
        var_dump('ID campagne');

        echo 'Ajout Zip ' . "\n";
        $retour4 = $routeur->SaveHtmlZip_new($retour3['id'], $filename,$lesenderm4y->api_login,$lesenderm4y->api_key);

        echo 'Zip OK' . "\n";
        // set planification routage
        echo 'Plannif' . "\n";
        $retour5 = $routeur->SetPlanificationRoutage_new($retour3['id'],$retour['id'], $lacampagne->expediteur, $lesenderm4y->domaine,$lesenderm4y->api_login,$lesenderm4y->api_key,$planning->freq_send);
        var_dump($retour5);
        // enregistrement campagne routeurs
        $array_campagne_routeurs_envoi[] = array(
            'campagne_id' => $lacampagne->id,
            'sender_id' => $lesenderm4y->id,
            'cid_routeur' => $retour5['id'],
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
            'taskid' => '0',
            'planning_id' => $planning->id
        );

        // je check si la ligne existe déjà
        $check = \DB::table('campagnes_routeurs')->where('planning_id',$planning->id)->where('cid_routeur',$retour5['id'])->first();
        // si non j'ajoute insert
        // else je fais rien
        // pas bon en fait :/
        // if($check == null){
        \DB::table('campagnes_routeurs')->insert($array_campagne_routeurs_envoi);
        // }

        \Log::info("Save du Segment_at M4Y");

        $planning->nb_trials = 0;
        $planning->segments_at = date('Y-m-d H:i:s');
        $planning->save();

        \Log::info("[CampagneSegmentMailForYou][P$planning->id] : Fin Segmentation (Planning $planning->id/Campagne $lacampagne->id)");

    }

    public function write_tokens($file, $tokens) {


        $fp = fopen($file,"a+");
        // $content = "";
        $content = null;
        foreach($tokens as $desti){
            $destinataire = \DB::table('destinataires')
                ->select('id','mail','hash')
                ->where('id', $desti->destinataire_id)
                ->first();
            $content .= $destinataire->mail.";". $desti->destinataire_id . ";" . $destinataire->hash . "\n";
            // $content .= $destinataire->mail . "\n";

            // $arraylistemfu[] = array($destinataire->mail);

        }
        fwrite($fp,$content);
        fclose($fp);


    }

    protected function getArguments()
    {
        return [
            ['planning_id', InputArgument::REQUIRED, 'Planning id.'],
        ];
    }
}
