<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class OptoutStats extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'optout:stats';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mise à jour des optout par base';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Log::info("DEBUT - Calcul des optout par base - " .  date('Y-m-d'));
        $basesinfo = \DB::table('bases')->get();
        \DB::table('optout_stats')->truncate();
        foreach ($basesinfo as $labase) {

          $nombreoptout = \App\Models\Destinataire::where('statut', DESTINATAIRE_OPTOUT)->where('base_id',$labase->id)->count();
          \DB::statement("INSERT INTO optout_stats (base_id,count,created_at,updated_at) VALUES ('". $labase->id . "','". $nombreoptout ."','". date("Y-m-d H:i:s") ."','". date("Y-m-d H:i:s")."')");

        }
        \Log::info("FIN - Calcul des optout par base OK - " .  date('Y-m-d'));
    }
}
