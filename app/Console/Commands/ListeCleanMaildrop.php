<?php namespace App\Console\Commands;

use App\Classes\Routeurs\RouteurMaildrop;
use Illuminate\Console\Command;
use App\Models\Sender;

class ListeCleanMaildrop extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'liste:clean_maildrop';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'On supprime toutes les listes qui ont plus de 2 semaines.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $today = date('Y-m-d');
        $before = date('Y-m-d', strtotime('-7 days', strtotime($today)));
        $between = date('Y-m-d', strtotime('-14 days', strtotime($today)));

        $routeur = \DB::table('routeurs')
            ->where('nom', 'Maildrop')
            ->first();

        $maildrop = new RouteurMaildrop();

        $senders = Sender::where('routeur_id', $routeur->id)
            ->get();

        foreach ($senders as $sender) {

            $campagnes_routeurs = \DB::table('campagnes_routeurs')
                ->select('listid')
                ->where('sender_id', $sender->id)
                ->where('listid', '!=', '')
                ->where('created_at', '<', $before)
                ->where('created_at', '>', $between)
                ->get();

            foreach ($campagnes_routeurs as $cr) {
                $maildrop->delete_list($sender, $cr->listid);
            }
        }
    }
}
