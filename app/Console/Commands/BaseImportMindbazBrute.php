<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Symfony\Component\Console\Input\InputArgument;
use App\Classes\Routeurs\RouteurMindbaz;
use App\Models\Base;
use App\Models\Notification;
use App\Models\Sender;

class BaseImportMindbazBrute extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'base:import_mindbaz_b {base_id} {sender_id} {filename}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Base import_sender no liste';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $base_id = $this->argument('base_id');
        $sender_id = $this->argument('sender_id');
        $file = $this->argument('filename');

        if (empty($base_id) or empty($file)) {
            $this->error('Empty base_id or file');
        }

        $this->file = storage_path().'/listes/'.$file;
        $this->base_id = $base_id;

        if (!is_file($this->file)) {
            $this->error("Error : $this->file not found");
        }

        \Log::info("Import Destinataires dans la base ($base_id) (QUEUE COMMAND) du fichier -- '.$file");

        $ts = date('Y-m-d H:i:s');

        // \App\Helpers\Profiler::start('import');

        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $base = Base::find($base_id);

        $extension = \File::extension($file);
        $name = \File::name($file);

//        $fh = fopen($file, 'r');

        $base_id = $base->id;
        $liste = str_replace(".$extension",'',$name);
        echo 'WHERE Liste = ' . $name . "\n";
        $mindbaz = new RouteurMindbaz();

        $routeur = \DB::table('routeurs')
            ->where('nom', 'Mindbaz')
            ->where('is_active', 1)
            ->first();

        if(empty($routeur)){
            \Log::error('BaseImportMindbaz : DISABLED MINDBAZ');
            exit;
        }

        $sender = Sender::where('routeur_id', $routeur->id)
            ->where('id',$sender_id)
            ->first();

        $imported = false;

        $filename = $file;

        // echo 'Avant de rentrer dans la boucle sender' . "\n";

        //On importe par d�faut sur toutes les "bases"/senders Mindbaz
        // foreach($senders as $sender)
        // {

            $import_file = storage_path()."/mindbaz/bs$sender->id"."_".$name."_import_mindbaz.csv";

            // $this->current_max_id = $mindbaz->count_subscribers($sender) + 60000000 ; // 60M car un import de 1M s'est lance +42 fois du coup => l'id interne chez mb est a +57M actuellement)

            $this->current_max_id = $mindbaz->count_subscribers($sender) + 10000000; // test pour le petit serveur agv
            $this->current_max_id++;

            $this->handle = fopen( $import_file , "w");

            \DB::table('destinataires')
                ->select('id', 'mail')
                ->where('base_id', $base_id)
                ->where('statut', 0)
                // ->where('liste', $name)
                ->chunk(1000, function ($destinataires) {
                    $content = "";
                    foreach ($destinataires as $desti) {
                        $content .= "$desti->id;$desti->mail;$this->current_max_id;".getenv('CLIENT_TITLE')."\r\n";
                        $this->current_max_id++;
                    }
                    fwrite($this->handle, $content);
                });
            fclose($this->handle);

            $mindbaz->import_list($sender, $import_file);
            $imported = true;
        // }

        if (isset(\Auth::User()->id)) {
            $user_id = \Auth::User()->id;
        } else {
            $user_id = 1;
        }

        if($imported) {
            Notification::create([
                'user_id' => $user_id,
                'level' => 'info',
                'is_important' => true,
                'message' => "Fichier $name importé sur Mindbaz."

            ]);
        }

        \Log::info("End of Import Mindbaz base ($base_id) (QUEUE COMMAND) du fichier -- '.$file");
        // \App\Helpers\Profiler::report();

    }

    protected function getArguments()
    {
        return [
            ['base_id', InputArgument::REQUIRED, 'Base'],
            ['sender_id', InputArgument::REQUIRED, 'Sender'],
            ['filename', InputArgument::REQUIRED, 'Fichier'],
        ];
    }
}
