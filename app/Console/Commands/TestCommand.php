<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Classes\Routeurs\RouteurMailForYou;

class TestCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

      $routeur = new RouteurMailForYou();
      $timestart=microtime(true);
      echo 'Create Liste' . "\n";
      $retour = $routeur->CreationListeDestinataire(100,'MesurePerf50kBlocChunkRapide' . time());
      var_dump($retour);
      echo 'Construction du tableau' . "\n";
      $chemin = storage_path() . '/mailforyou/listetestimport50k.csv';
      $fp = fopen($chemin,"r");
      // $counter = 0;
      // $bloc = 1000;
      if ($fp)
      {
        while (!feof($fp))
        {

          $buffer = fgets($fp);
          $buffer = trim($buffer);
          // echo $buffer;
          // $liste[] = array($buffer);

          if($buffer !== ''){
            $liste[] = array($buffer);
            // $bloc = $bloc - 1;
            // $counter = $counter + 1;
          }

          // if($bloc == 0){
          //   echo $counter. "\n";
          //   echo 'Debut Import 1K' . "\n";
          // $retour['id']
          // $retour2 = $routeur->AjoutListeDestinataire($retour['id'], $liste);
          // var_dump($retour2);
          //
          // echo 'Fin Import' . "\n";
          // $bloc = 1000;
          // $liste=array();
          // }

        }
        fclose($fp);
      }


      $decoupeliste = array_chunk($liste,5000);

      foreach ($decoupeliste as $t) {
        echo 'Import Chunk' . "\n";
        $retour2 = $routeur->AjoutListeDestinataire($retour['id'], $t);
        var_dump($retour2);
      }


    //  $retour2 = $routeur->AjoutListeDestinataire($retour['id'], $liste);


      $timeend=microtime(true);
      $time=$timeend-$timestart;

      //Afficher le temps d'éxecution
      $page_load_time = number_format($time, 3);
      echo "Debut du script: ".date("H:i:s", $timestart) . "\n";
      echo "Fin du script: ".date("H:i:s", $timeend). "\n";
      echo "Script execute en " . $page_load_time . " sec";

      \Log::info("Script execute en " . $page_load_time . " sec");


      die();
        // j'ouvre les tokens
        echo 'Lancement de la requete' . "\n";
        $lesadressestokens = \DB::table('tokens')
            ->select('destinataire_id')
            // ->where('fai_id',$f->id)
            ->where('campagne_id',3)
            // ->where('planning_id',$planning->id)
            // ->where('date_active',$planning->date_campagne)
            ->get();

        echo 'Creation du fichier' . "\n";
        // je fais le chemin
        $file = storage_path() . '/mailforyou/listetestimport100k.csv';
        // je call ma fonction
        $this->write_tokens($file, $lesadressestokens);
    }

    public function write_tokens($file, $tokens) {

        $fp = fopen($file,"a+");
        $content = null;
        foreach($tokens as $desti){
            $destinataire = \DB::table('destinataires')
                ->select('id','mail')
                ->where('id', $desti->destinataire_id)
                ->first();
            $content .= $destinataire->mail."\n";
        }
        fwrite($fp,$content);
        fclose($fp);

    }
}
