<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

use App\Helpers\Profiler;
use DB;
//
include(storage_path("ip/geoip.inc"));
include(storage_path("ip/geoipcity.inc"));
include(storage_path("ip/geoipregionvars.php"));

class geolocmajdestinataire extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'destinataire:geoloc_maj';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mise a jour de la table destinaires en fonction des ouvertures';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function fire()
    {



    }

    public function handle()
    {

        //   _____       _           _      _____           _       _
        //  |  __ \     | |         | |    / ____|         (_)     | |
        //  | |  | | ___| |__  _   _| |_  | (___   ___ _ __ _ _ __ | |_
        //  | |  | |/ _ \ '_ \| | | | __|  \___ \ / __| '__| | '_ \| __|
        //  | |__| |  __/ |_) | |_| | |_   ____) | (__| |  | | |_) | |_
        //  |_____/ \___|_.__/ \__,_|\__| |_____/ \___|_|  |_| .__/ \__|
        //                                                   | |
        //                                                   |_|

        $ville = '';
        $codepostal ='';

        Profiler::start("geoloc maj destinataires");
        // init var
        $nbrtotaldesti = null;
        $lastdestinaireid = null;
        $memoirefait = array();
        echo "GO geoloc ouvertures"."\n";

        $today = date('Y-m-d 00:00:00');
        $twodaysago = date('Y-m-d 00:00:00',strtotime ( '-2 days' , strtotime ( $today )));

        //   _____           _   _                                   _
        //  |  __ \         | | (_)                                 | |
        //  | |__) |_ _ _ __| |_ _  ___    ___  _   ___   _____ _ __| |_ _   _ _ __ ___  ___
        //  |  ___/ _` | '__| __| |/ _ \  / _ \| | | \ \ / / _ \ '__| __| | | | '__/ _ \/ __|
        //  | |  | (_| | |  | |_| |  __/ | (_) | |_| |\ V /  __/ |  | |_| |_| | | |  __/\__ \
        //  |_|   \__,_|_|   \__|_|\___|  \___/ \__,_| \_/ \___|_|   \__|\__,_|_|  \___||___/
        //
        //

        $chemin = storage_path('ip/GeoLiteCity.dat');
        $gi = geoip_open($chemin . realpath("GeoLiteCity.dat"),GEOIP_STANDARD);

        $nbrtotaldesti = \DB::table('ouvertures')
            ->whereNotNull('ip')
            ->where('created_at', '>', $twodaysago)
            ->distinct()
            ->count();

        $offset = 10000;

        $long = strlen($nbrtotaldesti) - substr_count($offset,'0');

        echo $nbrtotaldesti . "\n";
        $nbretape = substr($nbrtotaldesti,0,$long);
        $skip = 0;

        for ($i = 1; $i <= $nbretape; $i++) {

            $lignedb = \DB::table('ouvertures')
                ->whereNotNull('ip')
                ->where('created_at', '>', $twodaysago)
                ->skip($skip)
                ->take($offset)
                ->get();

            echo "Etape ". $i . '/ '. $nbretape . "\n";
            foreach ($lignedb as $db) {

                // @ pour sauter les erreurs car not found
                $record = @geoip_record_by_addr($gi,$db->ip);
                $datemaj = \Carbon\Carbon::now();

                if(isset($record)){

                  if(isset($record->city)){
                    $ville = $record->city;
                  }
                  if(isset($record->postal_code)){
                    $codepostal = $record->postal_code;
                  }

                  \DB::statement("UPDATE destinataires SET ville = " . utf8_encode(\DB::connection()->getPdo()->quote($ville)) . ", ip = '" . $db->ip . "', departement = '" . $codepostal . "' WHERE id='" . $db->destinataire_id . "'");

                  $lastdestinaireid = $db->id;

                //   \Log::info('ID de la dernière ouverture traitée' . $lastdestinaireid);

                }

                // if(!isset($record)){
                //     continue;
                //
                // }
                //
                // // partie geoloc
                // \DB::statement("UPDATE destinataires SET ville = "
                //     . utf8_encode(\DB::connection()->getPdo()->quote($record->city))
                //     . ", ip = '" . $db->ip . "', departement = '"
                //     . $record->postal_code
                //     . "' WHERE id='" . $db->destinataire_id . "'");
                //
                //
                //
                // // }
                // // echo '--------------' . "\n";
                //
                // $lastdestinaireid = $db->id;
            }

            $skip = $skip + $offset;
        }

        // fin on prend le reste

        $lignedb = \DB::table('ouvertures')
            ->whereNotNull('ip')
            ->where('created_at', '>', $twodaysago)
            ->skip($skip)
            ->take($offset)
            ->get();

        $ville = '';
        $codepostal ='';

        foreach ($lignedb as $db) {

            // @ pour sauter les erreurs car not found
            $record = @geoip_record_by_addr($gi,$db->ip);

            $datemaj = \Carbon\Carbon::now();
            // if isset // a ajouter après

            if(isset($record)){

              if(isset($record->city)){
                $ville = $record->city;
              }
              if(isset($record->postal_code)){
                $codepostal = $record->postal_code;
              }

              \DB::statement("UPDATE destinataires SET ville = " . utf8_encode(\DB::connection()->getPdo()->quote($ville)) . ", ip = '" . $db->ip . "', departement = '" . $codepostal . "' WHERE id='" . $db->destinataire_id . "'");

              $lastdestinaireid = $db->id;

              \Log::info('ID de la dernière ouverture traitée' . $lastdestinaireid);

            }


        }

        Profiler::report("geoloc maj destinataires");

    }

}
