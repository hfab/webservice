<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CampagneCaRoi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ca:roi';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calcul du roi des campagnes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
     public function handle()
     {

          // Ã  voir pour changer et amÃ©liorer si besoin
          $maildropcost = 0.06;
          // var_dump($maildropcost);

          $year = date("Y");
          $month = date("m");
          $betweenlong = $year . "-" . $month . "-01 00:00:00";
          $betweenshort = $year . "-" . $month . "-31 23:59:59";
          $today = date("Y-m-d H:i:s");
          $moisdelacompta = date_parse(date("Y-m-d H:i:s"));


          $campagnes_ca = \DB::table('campagne_ca_relation')->orderBy('id', 'asc')->get();
          //var_dump($campagnes_ca);die();
          foreach ($campagnes_ca as $v) {

               $get_plannings = \DB::table('plannings')
               ->where('campagne_id', '=', $v->id_campagne)
               ->where('volume_selected', '>=', 1)
               ->whereBetween('date_campagne', [$betweenlong, $betweenshort])
               ->get();
               //var_dump($get_campagne);die();

               $cumulvolume = 0;
               $routagecost = 0;
               $kilo = 0;

               foreach ($get_plannings as $vplan) {
                    //var_dump($vplan->volume_selected);die();
                    $cumulvolume = $cumulvolume + $vplan->volume_selected;
                    //var_dump($cumulvolume);die();
                    if($cumulvolume > 1000){
                         $kilo = $cumulvolume / 1000;
                         $routagecost = $kilo * $maildropcost;
                    }
               }

               /*
               $oldvolume = \DB::table('campagnes_ca_concat')
               ->where('campagnes_ca_concat.id', '=', $v->id_campagnes_ca)
               ->where('campagnes_ca_concat.base_id', $v->base_id_ca)
               ->where('mois_compta', $month )
               ->first();

               */

               // $cumulvolume = $cumulvolume + $oldvolume->ca_volume_total;

               \DB::table('campagnes_ca_concat')
               ->where('campagnes_ca_concat.id', '=', $v->id_campagnes_ca)
               ->where('campagnes_ca_concat.base_id', $v->base_id_ca)
               ->where('mois_compta', $month )
               ->update(['updated_at' => $today,
               'ca_volume_total'=> $cumulvolume,
               'cout_routage'=> $routagecost]);


          }



          //on s'occupe des bases maintenant

          $get_base_id = \DB::table('bases')
          ->select('id')
          ->orderBy('id', 'asc')
          ->get();
          //var_dump($get_base_id);die();
          foreach ($get_base_id as $v) {

               $get_campagne_concat = \DB::table('campagnes_ca_concat')
               //->join('campagnes','campagnes_ca.campagne_id','=','campagnes.id')
               ->where('base_id', $v->id)
               ->whereBetween('created_at', [$betweenlong, $betweenshort])
               ->get();
               //var_dump($get_campagne_concat);die();
               foreach ($get_campagne_concat as $vcampagne) {

                    $campagne_cumul_volume = 0;
                    $campagne_cumul_cout = 0;
                    $campagne_cumul_ca_brut = 0;
                    $campagne_cumul_ca_net = 0;

                    foreach ($get_campagne_concat as $ca_campa) {

                         // var_dump($ca_campa);
                         $campagne_cumul_ca_brut = $campagne_cumul_ca_brut + $ca_campa->ca_brut;
                         $campagne_cumul_ca_net = $campagne_cumul_ca_net +  $ca_campa->ca_net;
                         $campagne_cumul_volume = $campagne_cumul_volume + $ca_campa->ca_volume_total;
                         $campagne_cumul_cout = $campagne_cumul_cout + $ca_campa->cout_routage;
                         //var_dump($campagne_cumul_volume);die();
                    }
                    //var_dump($campagne_cumul_volume);die();

                    $get_base_ca = \DB::table('campagnes_ca_base')
                    ->where('periode_compta', $moisdelacompta['month'])
                    ->where('base_id', $v->id)
                    ->first();
                    //var_dump($get_base_ca);die();

                    // var_dump($get_base_ca);
                    // die();

                    if($get_base_ca == NULL){

                      \DB::table('campagnes_ca_base')
                      ->insert([
                      'updated_at' => $today,
                      'created_at'=>$today,
                      'base_id'=> $v->id,
                      'ca_brut' => $campagne_cumul_ca_brut,
                      'ca_net' => $campagne_cumul_ca_net,
                      'periode_compta'=> $month,
                      'base_volume_total' => $campagne_cumul_volume,
                      'base_cout_total'=> $campagne_cumul_cout]);

                    } else {

                      \DB::table('campagnes_ca_base')
                      ->where('base_id', $v->id)
                      ->where('periode_compta', $moisdelacompta['month'])
                      ->update([
                      'updated_at' => $today,
                      'created_at'=>$today,
                      'base_id'=> $v->id,
                      'ca_brut' => $campagne_cumul_ca_brut,
                      'ca_net' => $campagne_cumul_ca_net,
                      'periode_compta'=> $month,
                      'base_volume_total' => $campagne_cumul_volume,
                      'base_cout_total'=> $campagne_cumul_cout]);

                    }
               }
          }

          //      $lesBases= \DB::table('bases')->select('id')->orderBy('id', 'asc')->get();
          // //var_dump($lesBases);die();
          //      foreach ($lesBases as $v) {
          // //var_dump($v);die();
          //           $get_campagne = \DB::table('campagnes')
          //           ->where('base_id', '=', $v->id)
          //           ->whereBetween('created_at', [$betweenlong, $betweenshort])
          //           ->get();
          // //var_dump($get_campagne);die();
          //           foreach ($get_campagne as $vcampagne) {
          //
          //                $volume_campagne = \DB::table('plannings')->select('campagne_id', 'volume_selected')
          //                ->where('campagne_id', '=', $vcampagne->id)
          //                ->where('volume_selected', '>=', 1)
          //                ->get();
          // //var_dump($volume_campagne);die();
          //                // if ($volume_campagne == NULL){
          //                //      break;
          //                // }
          //
          //                $cumulvolume = 0;
          //                $routagecost = 0;
          //                $kilo = 0;
          //
          //                foreach ($volume_campagne as $vc) {
          //                     $cumulvolume = $cumulvolume + $vc->volume_selected;
          //                      //var_dump($volume_campagne);die();
          //                }
          // //var_dump($cumulvolume);die();
          //                if($cumulvolume > 1000){
          //                     $kilo = $cumulvolume / 1000;
          //                     $routagecost = $kilo * $maildropcost;
          //                }
          //
          //                \DB::table('campagnes_ca_concat')
          //                ->where('campagnes_ca_concat.nom', $vcampagne->nom)
          //                ->where('campagnes_ca_concat.base_id', $vcampagne->base_id)
          //                ->where('mois_compta', $month )
          //                ->update(['updated_at' => $today,
          //                'ca_volume_total'=> $cumulvolume,
          //                'cout_routage'=> $routagecost]);
          //           }
          //       }




     }
}
