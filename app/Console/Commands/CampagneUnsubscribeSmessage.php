<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Classes\Routeurs\RouteurSmessage;
use App\Models\Routeur;

class CampagneUnsubscribeSmessage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campagne:unsubscribe_smessage';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Récupère les désabonnés de Smessage et lance le job de désinscription sur toutes les bases.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Log::info("[CampagneUnsubscribeSmessage] : Début");
        $today = date('Ymd');
        $routeur = new RouteurSmessage();
        // je recupere dans campagne routeur les created ad du jour
        $two_weeks_ago = date('Y-m-d 00:00:00', strtotime('-2 weeks'));

        $smessage = Routeur::where('nom','Smessage')->first();

        // on récupère que les senders de SM
        $senders = \DB::table('senders')
            ->select('id')
            ->where('routeur_id', $smessage->id)
            ->get();

        $informations = \DB::table('campagnes_routeurs')
            ->where('created_at','>',$two_weeks_ago)
            ->whereIn('sender_id', array_pluck($senders, 'id'))
            ->get();

        $handle = fopen(storage_path()."/desinscrits/unsubscribe_sm_$today.csv", 'w');
        $compteur = 0;


        foreach ($informations as $lignecampagnerouteur)
        {
            \Log::info("[CampagneUnsubscribeSmessage] : Campagne $lignecampagnerouteur->campagne_id - Planning $lignecampagnerouteur->planning_id");
            if(empty($lignecampagnerouteur->cid_routeur)){
                \Log::info("[CampagneUnsubscribeSmessage] : empty cid_routeur (Campagne $lignecampagnerouteur->campagne_id - Planning $lignecampagnerouteur->planning_id)");
                continue;
            }

            $lesender = \DB::table('senders')->where('id', $lignecampagnerouteur->sender_id)->first();
            $lacampagne = \DB::table('senders')->where('id', $lignecampagnerouteur->sender_id)->first();

           $desinscrits = $routeur->recup_desinscrits($lesender->nom,$lesender->password,$lignecampagnerouteur->cid_routeur);
            if(empty($desinscrits)){
                continue;
            }

            $content = "";
            foreach($desinscrits as $unsub)
            {
                $content .= "$unsub\n";
                $compteur++;
            }
            fwrite($handle, $content);

        }
        fclose($handle);

        \Log::info("[CampagneUnsubscribeSmessage] : finished (Count : $compteur)");
        \Log::info("[CampagneUnsubscribeSmessage] : launch of BaseImportDesinscrits command");
        \Artisan::call('base:import_desinscrits', ['file' => "unsubscribe_sm_$today.csv"]);
        \Log::info("[CampagneUnsubscribeSmessage] : launch of BaseImportDesinscrits command OK");
        
    }
}
