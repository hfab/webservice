<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;

class etatip extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ip:etat';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Recupere une liste IP et compare avec les dnsbl';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $chaineresultat = null;
      //
      $email = '';

      $handle = fopen(storage_path('ip/listeipmd.txt'), 'r');

      if ($handle)
      {
      	while (!feof($handle))
      	{
      		$buffer = fgets($handle);
            $buffer = trim($buffer);

            if (!filter_var($buffer, FILTER_VALIDATE_IP) === false) {
                // echo("$buffer is a valid IP address \n");
                echo $this->checkipmaison($buffer);
                $email .= $this->checkipmaison($buffer);
            } else {
                // echo("$buffer is not a valid IP address \n");
                echo $buffer . "\n";
                $email .= $buffer . "\n";;
            }

            $buffer = null;
      	}

      	fclose($handle);
      }

      // var_dump($email);
      Mail::raw($email, function ($message) {

          $message->from('dev@lead-factory.net', 'DevTor');
          $message->to('fabien@lead-factory.net')->subject('Rapport Blacklist IP MD ' . date('d-m-Y'));
          $message->cc('hernoux.fabien@gmail.com');
          $message->cc('adeline.sc2@gmail.com');
          $message->cc('yveschaponic@gmail.com');


          return "true";
      });
    }

    public function checkipmaison($ip){
      $listed = null;
      $dnsbl_lookup = array(
        "zen.spamhaus.org",
        "dnsbl.sorbs.net",
        "all.spamrats.com",
        "b.barracudacentral.org"
      );

      $versionlongue = array(
        "spam.fusionzero.com",
        "access.redhawk.org",
        "all.rbl.jp",
        "all.s5h.net",
        "all.spamrats.com",
        "b.barracudacentral.org",
        "bl.blocklist.de",
        "bl.emailbasura.org",
        "bl.mailspike.org",
        "bl.score.senderscore.com",
        "bl.spamcannibal.org",
        "bl.spamcop.net",
        "bl.spameatingmonkey.net",
        "bogons.cymru.com",
        "cblplus.anti-spam.org.cn",
        "cidr.bl.mcafee.com",
        "combined.njabl.org",
        "db.wpbl.info",
        "dnsbl-1.uceprotect.net",
        "dnsbl-2.uceprotect.net",
        "dnsbl-3.uceprotect.net",
        "dnsbl.dronebl.org",
        "dnsbl.inps.de",
        "dnsbl.justspam.org",
        "dnsbl.kempt.net",
        "dnsbl.rv-soft.info",
        "dnsbl.sorbs.net",
        "dnsbl.tornevall.org",
        "dnsbl.webequipped.com",
        "dnsrbl.swinog.ch",
        "fnrbl.fast.net",
        "ip.v4bl.org",
        "ips.backscatterer.org",
        "ix.dnsbl.manitu.net",
        "korea.services.net",
        "l2.apews.org",
        "l2.bbfh.ext.sorbs.net",
        "list.blogspambl.com",
        "lookup.dnsbl.iip.lu",
        "mail-abuse.blacklist.jippg.org",
        "psbl.surriel.com",
        "rbl2.triumf.ca",
        "rbl.choon.net",
        "rbl.dns-servicios.com",
        "rbl.efnetrbl.org",
        "rbl.orbitrbl.com",
        "rbl.polarcomm.net",
        "singlebl.spamgrouper.com",
        "spam.abuse.ch",
        "spam.dnsbl.sorbs.net",
        "spam.pedantic.org",
        "spamguard.leadmon.net",
        "spamrbl.imp.ch",
        "spamsources.fabel.dk",
        "st.technovision.dk",
        "tor.dan.me.uk",
        "tor.dnsbl.sectoor.de",
        "truncate.gbudb.net",
        "ubl.unsubscore.com",
        "virbl.dnsbl.bit.nl",
        "zen.spamhaus.org"
      );

      if ($ip){
      $reverse_ip = implode(".", array_reverse(explode(".", $ip)));
      foreach($dnsbl_lookup as $host){
        if (checkdnsrr($reverse_ip . "." . $host . ".", "A")) {

          $listed.= $reverse_ip . ' - ' . $host . "\n";
          }
        }
      }

    if ($listed){
      return $listed;
      } else {
      // echo '"A" not found';
      }
    }
}
