<?php namespace App\Console\Commands;

use App\Classes\Routeurs\RouteurMaildrop;
use App\Classes\Routeurs\RouteurPhoenix;
use App\Models\Planning;
use App\Models\Campagne;
use App\Models\Sender;
use App\Classes\Routeurs\RouteurSmessage;
use Illuminate\Console\Command;
use Carbon\Carbon;

class CampagnePrepare extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'campagne:prepare';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Routine : preparation (tokens, senders) of ONE planning.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $auto_setting = \DB::table('settings')
                ->where('parameter', 'is_auto')
                ->first();

        if(!$auto_setting or $auto_setting->value != 1){
            \Log::notice('CampagneLaunchSend : AUTOMATIC CAMPAIGN NOT ACTIVE');
            exit;
        }

        $heure = date('H');
        $time = date('H:i');

        if($heure <= 4)
        {
            echo "exit";
            exit;
        }

        $plannings = Planning::where('date_campagne', date('Y-m-d'))
            ->where('time_campagne','<=',$time)
            ->whereNull('sent_at')
            ->orderBy('time_campagne')
            ->get();


        \Log::info('CampagnePrepare : '.count($plannings).' plannifs à traiter');

        $token_command = 'campagne:tokens';

        $clean_relaunch = \DB::table('settings')
            ->where('parameter', 'clean_relaunch')
            ->first();

        if(!empty($clean_relaunch) && $clean_relaunch->value !=1) {
            $token_command = 'campagne:destinataires';
        }

        foreach($plannings as $planning)
        {
        //   print_r($planning->routeur->nom);
            $campagne = Campagne::find($planning->campagne_id);

            if ($planning->tokens_at == null && $planning->nb_trials / 1 < 2 ) {
                \App\Models\Notification::create([
                    'user_id' => 1, // /!\ User System
                    'level' => 'info',
                    'message' => 'Sélection des tokens pour '.$campagne->ref,
                    'url' => '/campagne/'.$campagne->id.'/stats'
                ]);
                \Log::info('CampagnePrepare : Tokens pour '.$campagne->id.' (Campagne) / '.$planning->id. ' (Planning)');


                $this->call($token_command,array('planning_id' => $planning->id));
//                continue;

            }
            //Pour faire le bloc tokens + segments
            if ($planning->tokens_at != null && $planning->segments_at == null && $planning->nb_trials / 1 < 2 ) {
                \App\Models\Notification::create([
                    'user_id' => 1, // /!\ User System
                    'level' => 'info',
                    'message' => 'Création des Segments '.$campagne->ref,
                    'url' => '/campagne/'.$campagne->id.'/stats'
                ]);

                $mindbaz_setting = \DB::table('settings')
                                        ->where('parameter', 'mindbaz_mode')
                                        ->first();

                //Dans le cas où la segmentation se relance plusieurs fois
                \DB::statement("DELETE FROM campagnes_routeurs WHERE planning_id = $planning->id");
                \Log::info('CampagnePrepare : Segments pour '.$campagne->id.' Campagne) / '.$planning->id. ' (Planning)');

                if($planning->routeur->nom == 'Maildrop') {
                    \Log::info('Maildrop on fait les segments '.$campagne->id.' (Campagne) / '.$planning->id. ' (Planning)');
                    $this->call("campagne:segment_maildrop",array('planning_id' => $planning->id));
                    break;
                } else if ($planning->routeur->nom == 'Smessage') {
                    \Log::info('Smessage on fait les segments '.$campagne->id.' (Campagne) / '.$planning->id. ' (Planning)');
                    $this->call('campagne:segment_smessage',array('planning_id' => $planning->id));
                    break;
                } else if ($planning->routeur->nom == 'Phoenix') {
                    \Log::info('Phoenix on fait les segments '.$campagne->id.' (Campagne) / '.$planning->id. ' (Planning)');
                    $this->call('campagne:segment_phoenix',array('planning_id' => $planning->id));
                    break;
                } else if ($planning->routeur->nom == 'MailForYou') {
                    \Log::info('MailForYou on fait les segments '.$campagne->id.' (Campagne) / '.$planning->id. ' (Planning)');
                    $this->call('campagne:segment_mailforyou',array('planning_id' => $planning->id));
                    break;

                } else if ($planning->routeur->nom == 'Edatis') {
                    \Log::info('Edatis on fait les segments + envoi '.$campagne->id.' (Campagne) / '.$planning->id. ' (Planning)');
                    $this->call('campagne:send_edatis',array('planning_id' => $planning->id));

                } else if ( $planning->routeur->nom == 'Mindbaz' && !empty($mindbaz_setting) && $mindbaz_setting->value == 'segment' ) {
                    \Log::info('MailForYou on fait les segments '.$campagne->id.' (Campagne) / '.$planning->id. ' (Planning)');
                    $this->call('campagne:segment_mindbaz',array('planning_id' => $planning->id));

                    break;
                } else {
                    \Log::info('Pas de segmentation possible pour ce routeur '.$planning->routeur_id);
                    continue;
                }
            }
        }
    }
}
