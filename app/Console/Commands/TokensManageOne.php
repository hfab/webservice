<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use App\Models\Fai;
use App\Models\User;
use App\Models\Base;

use Carbon\Carbon;



class TokensManageOne extends Command
{

  protected $total_tokens = 0;
  protected $i = 0;
  protected $total_destinataires = 0;

  protected $bulk = 200;
  protected $chunk = 25000;
  protected $cache_tokens = [];
  protected $dest_ids = [];



    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'tokens:manageone';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fait les tokens pour une unique base';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        var_dump($this->argument('base_id'));
        $today = Carbon::today();

        $quotas = Fai::quotas();
        $timestamp = date('Y-m-d H:i:s');
        $date_active = $today->format('Y-m-d');

        $bases = Base::where('is_active', 1)
        ->where('id', $this->argument('base_id'))
        ->get();

        foreach($bases as $b)
        {

            $num = \DB::table('destinataires')
                ->where('base_id', $b->id)
                ->where('statut', 0)
                ->where('tokenized_at', '<', $date_active)
                ->count();

            echo "Count : $num \n";

            if ($num == 0) {
                continue;
            }

            $i = 0;
            \DB::table('destinataires')
                ->select('id', 'fai_id', 'base_id', 'sender_id', 'origine', 'departement', 'ville', 'civilite', 'datenaissance')
                ->where('base_id', $b->id)
                ->where('statut', '<', 3)
                ->where('tokenized_at', '<', $date_active)
                ->chunk($this->chunk, function ($destinataires) use ($quotas, $timestamp, $date_active) {
                    $this->i++;
                    foreach ($destinataires as $destinataire) {
                        if ($destinataire->fai_id == 0) {
                            if ($destinataire->origine != 'Administrateurs') {
                                //                    dd($destinataire);
                                echo "\nFAI_ID\n";
                            }
                            continue;
                        }

                        if($destinataire->civilite === null){
                          $sexe = 3;
                        } else {
                          // echo("\n" . "Civilite [" . $destinataire->civilite . "]" . "\n");
                          $sexe = $destinataire->civilite;
                        }

                        $pression = $quotas[$destinataire->fai_id];
                        if ($pression < 0) {
                            $pression = ((date('z') % $pression) == 0) ? 1 : 0;
                        }

                        for ($i = 1; $i <= $pression; $i++) {

                            $fields = [
                                'destinataire_id' => $destinataire->id,
                                'priority' => $i,
                                'fai_id' => $destinataire->fai_id,
                                'base_id' => $destinataire->base_id,
                                'date_active' => $date_active,
                                'sender_id' => $destinataire->sender_id,
                                'created_at' => $timestamp,
                                'updated_at' => $timestamp,
                                'ville' => $destinataire->ville,
                                'departement' => $destinataire->departement,
                                'civilite' => $sexe,
                                'datenaissance' => substr($destinataire->datenaissance,0,4),

                            ];

                            $this->cache_tokens[] = $fields;
                        }
                        $this->dest_ids[$destinataire->id] = $destinataire->id;

                        if (count($this->cache_tokens) > $this->bulk) {
                            \DB::table('tokens')->insert($this->cache_tokens);
                            $count = count($this->cache_tokens);

                            $this->total_tokens += $count;
                            $this->cache_tokens = [];
                        }
                        echo "Destinataires : " . str_pad($this->total_destinataires, 12, ' ', STR_PAD_LEFT) . "\t\t";
                        echo "Tokens : " . str_pad($this->total_tokens, 12, ' ', STR_PAD_LEFT) . "\t";
                        echo "i : $this->i\r";

                        $i++;
                        echo "\r $i";
                    }
                });

            if (count($this->cache_tokens) > 0) {
                $write = \DB::table('tokens')->insert($this->cache_tokens);
                if (!$write) {
                    \Log::error(json_encode($this->cache_tokens));
                }

                $this->total_tokens += count($this->cache_tokens);
                $this->cache_tokens = [];
            }

            //Bulk Updates
            $cache_dids = [];
            foreach ($this->dest_ids as $did) {
                $cache_dids[] = $did;
                if (count($cache_dids) > 500) {
                    \DB::table('destinataires')
                        ->where('base_id', $b->id)
                        ->whereIn('id', $cache_dids)
                        ->update(['tokenized_at' => $timestamp]);
                    $this->total_destinataires += count($cache_dids);
                    $cache_dids = [];
                }
            }

            if (count($cache_dids) > 0) {
                \DB::table('destinataires')
                    ->where('base_id', $b->id)
                    ->whereIn('id', $cache_dids)
                    ->update(['tokenized_at' => $timestamp]);
                $this->total_destinataires += count($cache_dids);
                $cache_dids = [];
            }
            $this->dest_ids = array();
            echo "\n I : $this->i\n";
        }



        // \Artisan::call('reset:statstokens');

        /* if(getenv('APP_ENV') == 'prod'){
         if($this->total_destinataires > 0) {
             $email = "Nombre de tokens générés : $this->total_tokens pour $this->total_destinataires destinataires. \n\n $report";
             Mail::raw($email, function ($message) {
                 $destinataires = User::where('is_valid', 1)
                     ->where('user_group_id', 1)
                     ->where('email', '!=', "")
                     ->get();
                 $message->from('dev@lead-factory.net', 'Tor')->subject(getenv('CLIENT_URL').' - Rapport génération tokens ('.date('d-m-Y').')');
                 foreach ($destinataires as $d) {
                     $message->to($d->email);
                 }
                 return true;
             });
          }
         }
         */

        echo "\n\nTOTAL\n";
        echo "Destinataires : ".str_pad($this->total_destinataires, 12, ' ', STR_PAD_LEFT)." - ";
        echo "Tokens : ".str_pad($this->total_tokens, 12, ' ', STR_PAD_LEFT)."\n";

    }

    protected function getArguments()
    {
        return [
            ['base_id', InputArgument::REQUIRED, 'base_id'],
        ];
    }
}
