<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Base;

class WizwebApiFtp extends Command
{
    protected $md;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wizwebapi:ftp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Put a file with all @ in md5 with their status (ok, optout) on Wizweb FTP.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if(empty(getenv('WIZWEB_FTP_HOST'))){
            exit;
        }

        try {
            $pdo = \DB::connection()->getPdo();

        } catch (\PDOException $e) {

            \Log::error("Wizweb API FTP : NOT OK (DB CONNEXION PB) $e->getMessage");

            $error_response = array(
                'status' => 'SYSTEM_ERROR' ,
                'message' => $e->getMessage(),
                'error_code' => 45
            );

            echo json_encode($error_response);
            exit;
        }

        $ftp_server = getenv('WIZWEB_FTP_HOST');
        $ftp_user = getenv('WIZWEB_FTP_USER');
        $ftp_pass = getenv('WIZWEB_FTP_PASS');
        $ftp_conn = ftp_connect($ftp_server) or \Log::error("Could not connect to $ftp_server");

        if(!$ftp_conn){
            $error_response = array(
                'status' => 'SYSTEM_ERROR' ,
                'message' => 'Can not connect to FTP',
                'error_code' => 45
            );

            echo json_encode($error_response);
            exit;
        }

        $login = ftp_login($ftp_conn, $ftp_user, $ftp_pass);

        \DB::disableQueryLog();

        $bases = Base::all();
        $today = date('Ymd');
        $timestamp = date('Ymd');

//        $pdo = \DB::connection()->getpdo();

        $resultFile = \DB::select('SELECT @@global.secure_file_priv');
        $where = $resultFile[0];
        $var = '@@global.secure_file_priv';
        $path = ($where->$var);
        if(empty($path)){
            $path = '/tmp/';
        }

        foreach($bases as $base) {

            $filename = "dump_".getenv('CLIENT_NAME')."_".$base->id."_$timestamp";
            $ext = ".csv";
//        $filepath = storage_path() . "/listes/";

            $filepath = $path.$filename.$ext;
            exec("rm $filepath");

            echo $filepath."\n";

            $query = sprintf("SELECT hash as md5, CASE statut WHEN 0 THEN 'OK' ELSE 'OUTPUT' END AS status FROM destinataires WHERE base_id = %d INTO OUTFILE '%s' FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n'",$base->id, $filepath);
//            echo "before select";
            $pdo->exec($query);
//            echo "after select";

//            exec("mv $filepath ".storage_path()."/$filename.csv");

//            exit;

            exec("cp $filepath ".storage_path()."/wizweb/$filename.csv");

            $zip = new \ZipArchive();
            $filename_zip = storage_path()."/wizweb/".$filename.".zip";
            if ($zip->open($filename_zip, \ZipArchive::CREATE)!==TRUE) {
                \Log::error("WizwebApiFtp : Can not open $filename_zip");
                continue;
            }
            $new_filename = substr($filepath,strrpos($filepath,'/') + 1);
            $zip->addFile($filepath,$new_filename);
            $zip->close();
            $remote_path = $filename.'.zip';
            ftp_put($ftp_conn, $remote_path, $filename_zip, FTP_ASCII);
        }
    }
}
