<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Classes\Routeurs\RouteurSmessage;

class StatsRouteurSmessage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stats:majsmessage';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mise à jour des statistiques Smessage';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

      \Log::info('Debut du calcul des statistiques Smessage');

      $routeur = new RouteurSmessage();
      $lerouteurid = \DB::table('routeurs')->where('nom','Smessage')->first();
      $smessage = \DB::table('senders')->where('routeur_id', $lerouteurid->id)->get();

      $tableaucampagnecompte = array();
      $bladetab = array();
      $tabaccount = array();

      foreach ($smessage as $compte) {

        $tabaccount['compte'] = $compte->nom;
        $tabaccount['mdp'] = $compte->password;

      $tableaucampagnecompte[] = $routeur->lister_campagne_compte($compte->nom,$compte->password);
      $elemnbr = count($tableaucampagnecompte[0]);

      for ($i=0; $i < $elemnbr; $i++) {

        $tableaucampagnecompte[0][$i] = array_merge($tableaucampagnecompte[0][$i],$tabaccount);
        $bladetab[] = $tableaucampagnecompte[0][$i];
      }

      unset($tableaucampagnecompte);
      }

      // on va chercher le bloc de maj pour pouvoir select le dernier
      $blocid = \DB::table('stats_smessage')->max('bloc_maj');
      $blocid = $blocid + 1;

      foreach ($bladetab as $valuetab) {
        if(stristr($valuetab['date_envoi'], 'Non')){
        $valuetab['date_envoi'] = '0';

        echo 'Pas de Calcul pour la campagne : ' . $valuetab['reference'] . ' CID :  ' . $valuetab['id'] . ' car non send' . "\n";
      } else {
        // verifier si le bloc existe déjà dans la campagne
        // pour update
        // -----
        //ou pas besoin en fait car on stock tout pour avoir evolution au choix
        $npai = count($routeur->recup_npai($valuetab['compte'], $valuetab['mdp'], $valuetab['id']));
        $npai_soft = count($routeur->recup_npai_soft($valuetab['compte'], $valuetab['mdp'], $valuetab['id']));
        $ouvreurs = count($routeur->recup_ouvreurs($valuetab['compte'], $valuetab['mdp'], $valuetab['id']));
        $cliqueurs = count($routeur->recup_cliqueurs($valuetab['compte'], $valuetab['mdp'], $valuetab['id']));
        $inactifs = count($routeur->recup_inactifs($valuetab['compte'], $valuetab['mdp'], $valuetab['id']));
        $desinscrits = count($routeur->recup_desinscrits($valuetab['compte'], $valuetab['mdp'], $valuetab['id']));

        echo 'Calcul pour la campagne : ' . $valuetab['reference'] . ' CID :  ' . $valuetab['id'] . "\n";

         // pour exploitation
         // SELECT distinct(id_smessage), id FROM `stats_smessage` order by date_maj desc
        \DB::statement("INSERT INTO stats_smessage (id_smessage, date_creation, date_envoi, total_mail, npai, npai_soft, ouvreurs, cliqueurs, inactifs, date_maj, bloc_maj, reference, desinscrits) VALUES (" . $valuetab['id'] . "," . $valuetab['date_creation'] . "," . $valuetab['date_envoi'] . "," . $valuetab['total_mail'] ."," . $npai .", " . $npai_soft . ",
        " .$ouvreurs ."," . $cliqueurs ."," . $inactifs .",". time() . "," . $blocid . ",'" . $valuetab['reference'] . "','" . $desinscrits . "')");

        }
      }

      // Partie 2 ou l'on rempli la table total
      $lastmaj = \DB::table('stats_smessage')->max('bloc_maj');
      $consoliderstats = \DB::table('stats_smessage')->where('bloc_maj',$lastmaj)->get();

      foreach ($consoliderstats as $item) {
      $checkref = \DB::table('stats_smessage_total')->where('reference',$item->reference)->where('bloc_maj',$lastmaj)->first(); // bloc maj

        if (is_null($checkref)) {
          \DB::statement("INSERT INTO stats_smessage_total (id_smessage, total_mail, npai, npai_soft, ouvreurs, cliqueurs, inactifs, date_maj, bloc_maj, reference, date_creation) VALUES
          ('". $item->id_smessage ."','" . $item->total_mail . "','" . $item->npai . "','" . $item->npai_soft . "','" . $item->ouvreurs . "','" . $item->cliqueurs . "','". $item->inactifs ."',
          '". time() ."','". $blocid ."','". $item->reference ."','". $item->date_creation ."')");

        } else {

          $totalmail = $checkref->total_mail + $item->total_mail;
          $totalnpai = $checkref->npai + $item->npai;
          $totalnpaisoft = $checkref->npai_soft + $item->npai_soft;
          $totalouvreurs = $checkref->ouvreurs + $item->ouvreurs;
          $totalcliqueurs = $checkref->cliqueurs + $item->cliqueurs;
          $totalinactifs = $checkref->inactifs + $item->inactifs;
          $totaldesinscrits = $checkref->desinscrits + $item->desinscrits;


          \DB::statement("UPDATE stats_smessage_total SET npai ='" . $totalnpai . "' WHERE reference ='" . $item->reference . "' AND bloc_maj = '" . $lastmaj . "'");
          \DB::statement("UPDATE stats_smessage_total SET npai_soft ='" . $totalnpaisoft . "' WHERE reference ='" . $item->reference . "' AND bloc_maj = '" . $lastmaj . "'");
          \DB::statement("UPDATE stats_smessage_total SET cliqueurs ='" . $totalcliqueurs . "' WHERE reference ='" . $item->reference . "' AND bloc_maj = '" . $lastmaj . "'");
          \DB::statement("UPDATE stats_smessage_total SET inactifs ='" . $totalinactifs . "' WHERE reference ='" . $item->reference . "' AND bloc_maj = '" . $lastmaj . "'");
          \DB::statement("UPDATE stats_smessage_total SET ouvreurs ='" . $totalouvreurs . "' WHERE reference ='" . $item->reference . "' AND bloc_maj = '" . $lastmaj . "'");
          \DB::statement("UPDATE stats_smessage_total SET total_mail ='" . $totalmail . "' WHERE reference ='" . $item->reference . "' AND bloc_maj = '" . $lastmaj . "'");
          \DB::statement("UPDATE stats_smessage_total SET desinscrits ='" . $totaldesinscrits . "' WHERE reference ='" . $item->reference . "' AND bloc_maj = '" . $lastmaj . "'");


        }
      }

      \Log::info('Récupération des statistiques Smessage OK');

      $valueblocmax = \DB::table('stats_smessage_total')->max('bloc_maj');
      $statscampagneold = \DB::table('stats_smessage_total')->where('bloc_maj',$valueblocmax - 1)->get();
      $statscampagne = \DB::table('stats_smessage_total')->where('bloc_maj',$valueblocmax)->get();
      \Mail::send('stats.mailstats', ['statscampagne' => $statscampagne, 'statscampagneold' => $statscampagneold, 'type' => 'smessage'], function ($m) use ($statscampagne) {
      $m->from('dev@lead-factory.net', 'Tor');
      $m->to('fabien@lead-factory.net', 'Fabien')->subject(getenv('CLIENT_URL') .' - Stats des campagnes Smessage (' . date('d-m-Y').')');
      $m->cc('adeline.sc2@gmail.com');
      });

      \Log::info('Envoi du mail avec les stats Smessage');
    }
  }
