<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class EmailReply extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reply:traitement';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Traite les messages de reponse';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \App\Helpers\Profiler::start('Email Reply');
        $counterdelete = 0;
        $counterdesabo = 0;
        $wherein = array();
        $tabwherein = array();
        $tabdelete = array();

        echo "Ouverture du flux mail \n";
        // $server = '{ssl0.ovh.net:143}INBOX';
        // $username = 'info@lead-factory.net';
        // $password = 'aqwzsx17++';

        // $server = '{195.154.17.199:143}INBOX'; // AUTO
        // $username = 'reply@email.lanewsdedelire.fr';
        // $password = 'Yx5wmGuFcCDC';

        // je rqt sur la table
        // je fais les différents mails

        $rqt = \DB::table('email_reply')->orderBy('id','asc')->get();
        // var_dump($rqt);
        foreach($rqt as $infomail)
        {
            $c = 0;
            // var_dump($infomail);
            $server = $infomail->host; // AUTO
            $username = $infomail->username;
            $password = $infomail->password;
            echo $username . "\r";


            $mbox = imap_open($server , $username, $password);
            $mails = FALSE;
            if (FALSE === $mbox) {
                $err = 'La connexion a échoué. Vérifiez vos paramètres!';
            } else {
                $info = imap_check($mbox);
                if (FALSE !== $info) {
                    echo "Lecture du flux mail" . "\n";
                    // $nbMessages = min(99999999, $info->Nmsgs);
                    $nbMessages = min(9999, $info->Nmsgs);
                    $mails = imap_fetch_overview($mbox, '1:'.$nbMessages, 0);
                    echo "Fin lecture du flux mail" . "\n";
                } else {
                    $err = 'Impossible de lire le contenu de la boite mail';
                }
            }

            // die();
            if (FALSE === $mails) {
                echo $err;
            } else {
                echo $informationboite = 'La boite aux lettres contient '.$info->Nmsgs.' message(s) dont ' . $info->Recent.' recent(s)';
                foreach ($mails as $mail)
                {

                    $corps = imap_body($mbox, $mail->uid, FT_UID);
                    $tabdelete[] = $mail->msgno;
                    var_dump($corps);

                    if($this->word_search($corps)){
                        // si c'est trouvé
                    }  else {

                        // on verifie mail_from
                        $c = $c + 1;
                        echo $c . '/' . $info->Nmsgs . "\n";

                        if(isset($mail->from) || isset($mail->from) === true || empty($mail->from) === false){
                            // var_dump($mail->from);
                            preg_match('#<(.+)>#isU', $mail->from, $matches);
                            if(count($matches) > 0){
                                // var_dump($matches);
                                $counterdesabo = $counterdesabo + 1;
                                $wherein[] = $matches[1];


                            }

                        }
                    }
                }

            }

            $counter = 0;
            foreach ($wherein as $v)
            {
                $counter = $counter + 1;
                echo 'Counter : ' . $counter . ' /500' . "\n";
                $tabwherein[] = $v;

                if($counter == 500){
                    $this->update_desti($tabwherein);
                    $tabwherein = array();
                    $counter = 0;

                }
            }
            // on lance le reste
            $this->update_desti($tabwherein);

            echo 'Fin du script'. "\n";

            //  imap_close($mbox);
            //  var_dump($tabdelete);

            $counterdelete=count($tabdelete);
            // die();

            foreach ($tabdelete as $y)
            {

                $counterdelete = $counterdelete - 1;
                echo $counterdelete . "\n";
                var_dump($y);
                imap_delete($mbox, $y);

            }

            echo 'Expunge' . "\n";
            imap_expunge($mbox);
            imap_close($mbox);
            // die();

        }

        \Log::info("Counter desabo mail non unique via info@ : " . $counterdesabo);
        \App\Helpers\Profiler::report();
    }

    public function update_desti($email)
    {
        // on fait un fichier après ?
        $statut = 42;
        $optout_file = 'email_reply';

        \DB::table('destinataires')
            ->whereIn('mail', $email)
            ->update(['statut' => $statut, 'optout_file' => $optout_file, 'optout_at' => date('Y-m-d H:i:s')]);
    }

    public function word_search($string)
    {
        $expression = array(
            'absence',
            'absent',
            'absente',
            'vacances',
            'vacance',
            'congés',
            'congé',
            'conges',
            'conge',
            'maternité',
            'maternite',

            // autre termes à discuter pour garder
            'avons bien reçu',
            'avons bien recu',
            'en cours de traitement',

        );
        // changement d'approche
        // si j'ai absence, vacances, congés, je garde

        $counter = 0;
        foreach ($expression as $var)
        {
            if(preg_match("/". $var ."/i", $string)) {
                $counter = 1;
            }
            if($counter > 0){
                return true;
            } else {
                return false;
            }
            // faire le retour ici si une occurence trouvé
        }
    }
}
