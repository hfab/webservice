<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Planning;
use App\Models\Campagne;
use App\Models\Routeur;
use App\Models\Fai;
use App\Models\Sender;

use App\Classes\Routeurs\RouteurEdatis;

class CampagneSendEdatis extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campagne:send_edatis {planning_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Lance envoi Edatis';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

      $auto_setting = \DB::table('settings')
          ->where('parameter', 'is_auto')
          ->first();
      if(!$auto_setting or $auto_setting->value != 1){
          \Log::info('CampagneLaunchSend : AUTOMATIC CAMPAIGN NOT ACTIVE');
          return 0;
      }
      $routeur_setting = \DB::table('settings')
          ->where('parameter', 'is_edatis')
          ->first();

      if(!$routeur_setting or $routeur_setting->value != 1){
          \Log::info('CampagneLaunchSend : Edatis DISABLE');
          return 0;
      }

      $bloc = \DB::table('senders_history')->max('bloc');

      if(empty($bloc)){
          $bloc = 0;
      }
      $this->max_bloc = $bloc + 1;

      $routeur = new RouteurEdatis();

      $expediteur_roulement = array("newsletter");
      $rand_keys = array_rand($expediteur_roulement);
      // var_dump($expediteur_roulement[$rand_keys]);

      $planning = Planning::find($this->argument('planning_id'));
      $fais = Fai::all();
      $edatis = \DB::table('routeurs')->where('nom', 'Edatis')->first();
      $redatis = Routeur::where('nom','Edatis')->first();

      $campagneid = $planning->campagne_id;
      $lacampagne = Campagne::where('id',$campagneid)->first();

      $planning->nb_trials++;
      $planning->save();

      $nombreaccount = $planning->nbr_sender;
      $multi_mono =  \DB::table('planning_senders')
          ->where('planning_id', $planning->id)
          ->first();

      $anonyme = "";
      if($planning->type == 'anonyme') {
          $anonyme = "_a";
          $ano = true;
      }

      if($multi_mono == null) {
        // multi compte
        echo 'multi compte';
        if(empty($nombreaccount)){
            $nombreaccount = 2;
        }
        \Log::info("Multi Compte Edatis");

        $sender_edatis = \DB::table('senders')
            ->select('id','nom','password','domaine')
            ->where('routeur_id', $edatis->id)
            ->where('nb_campagnes_left', '>', 0)
            ->where('quota_left', '>', 0)
            ->get();
        $tabsender = array();
        foreach ($sender_edatis as $s_e)
        {
            $tabsender[] = array('id' => $s_e->id,'nom' => $s_e->nom,'password' => $s_e->password,'domaine' => $s_e->domaine);
        }

        $senderenvoi = array_slice($tabsender,0,$nombreaccount);

        $countFai = \DB::table('tokens')
            ->select('fai_id', \DB::raw('count(*) as total'))
            ->where('planning_id',$planning->id)
            ->groupBy('fai_id')
            ->get();

        foreach($countFai as $cf)
        {
          $volumeFaiTotal = $cf->total;
          // On récupère les tokens PAR FAI
          $lesadressestokens = \DB::table('tokens')
          ->select('destinataire_id')
          ->where('campagne_id',$lacampagne->id)
          ->where('fai_id',$cf->fai_id)
          ->where('date_active',$planning->date_campagne)
          ->where('planning_id',$planning->id)
          ->get();

            $nombreparfai = count($lesadressestokens);
            $sizechunck = $nombreparfai / $nombreaccount;
            $downset = 0;
            $offset = $sizechunck;

                // foreach mes comptes pour remplir les listes avec chaque fai
            foreach ($senderenvoi as $s)
            {

              $file = storage_path() . '/edatis/s' . $s['nom'] . '_c' . $lacampagne->id .'_p'.$planning->id. $anonyme.'.csv';
              $aliste = array_slice($lesadressestokens,$downset,$offset);

              $offset = $offset + $sizechunck;
              $downset = $downset + $sizechunck;

              $this->write_tokens($file, $aliste);

              $sender = Sender::find($s['id']);
              $quota_left = $sender->quota_left - $sizechunck;
              $sender->quota_left = $quota_left;

              $sender->save();
            }
          }

          foreach ($senderenvoi as $s)
          {

            $limiteApi = 0;

            $html = $lacampagne->generateHtml($redatis);
            $campagneId = $routeur->create_campagne(
              $s['password'],
              $lacampagne->ref.'_v1_'.time(),
              $lacampagne->objet,
              $lacampagne->expediteur,
              $expediteur_roulement[$rand_keys].'@'.$s['domaine'],
              'reply',
              'reply@'.$sender->domaine,
              '',
              $html);

            $chemin = storage_path() . '/edatis/s' . $s['nom'] . '_c' . $lacampagne->id .'_p'.$planning->id. $anonyme.'.csv';
            // send des contacts

            $fp = fopen($chemin,"r");
            if ($fp) {
                while (!feof($fp))
                {
                    $buffer = fgets($fp);
                    $buffer = trim($buffer);

                    if($buffer !== ''){
                        // tor id
                        $e = explode(";", $buffer);
                        $liste[] = array('email' => $e[0],'TORID'=>$e[1]);
                    }
                }
                fclose($fp);
            }

            $chunckliste = array_chunk($liste,100);
            $cumulimport = 0;

            $nb_call = 1;
            foreach ($chunckliste as $liste_c) {

              \Log::info('Import sur : ' . $s['nom']);
              $cumulimport = $cumulimport + count($liste_c);
              \Log::info('Cumul import : ' . $cumulimport);
              $limiteApi = $limiteApi + count($liste_c);

              $nb_call = $nb_call + 1;
              $var_chucnk = $routeur->send_campagne($s['password'],$campagneId,$liste_c);
              var_dump($var_chucnk);

              if($nb_call == 25){
                \Log::info($nb_call .  'Appels Pause');
                sleep(11);
                $nb_call = 0;
              }

            }

            $array_campagne_routeurs_envoi = array(
                'campagne_id' => $lacampagne->id,
                'sender_id' => $s['id'],
                'cid_routeur' => $campagneId,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
                'taskid' => '1',
                'planning_id' => $planning->id
            );

            \DB::table('campagnes_routeurs')->insert($array_campagne_routeurs_envoi);

            unset($liste);
            unset($chunckliste);

            $sender = Sender::find($s['id']);
            $nb_campagnes_left = $sender->nb_campagnes_left - 1;
            $sender->nb_campagnes_left = $nb_campagnes_left;
            $sender->save();

          }

          $planning->nb_trials = 0;
          $planning->segments_at = date('Y-m-d H:i:s');
          $planning->sent_at = date('Y-m-d H:i:s');
          $planning->save();

      } else {
        $limiteApi = 0;
        // simple compte
        echo 'simple compte';
        \Log::info("Simple Compte Edatis");
        $lesadressestokens = array();
        $lesadressestokens = \DB::table('tokens')
        ->select('destinataire_id')
        ->where('campagne_id',$campagneid)
        ->where('planning_id',$planning->id)
        ->where('date_active',$planning->date_campagne)
        ->get();

        $info_planning_sender = \DB::table('planning_senders')
            ->where('planning_id',$this->argument('planning_id'))
            ->first();

        $lesender = \DB::table('senders')
            ->where('id',$info_planning_sender->sender_id)->first();
        $sender = Sender::find($lesender->id);
        $file = storage_path() . '/edatis/s' . $lesender->nom . '_c' . $lacampagne->id .'_p'.$planning->id. $anonyme.'.csv';

        $quota_left = $sender->quota_left - count($lesadressestokens);
        //l'historique
        \DB::table('senders_history')
            ->insert(
                [
                    'quota_before' => null,
                    'sender_id' => $sender->id,
                    'fai_id' => null,
                    'planning_id' => $planning->id,
                    'quota_left_before' => $sender->quota_left,
                    'quota_left' => $quota_left,
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s"),
                    'bloc' => $this->max_bloc
                ]
            );

        // vide le fichier
        fopen($file, 'w');
        $this->write_tokens($file, $lesadressestokens);

        $html = $lacampagne->generateHtml($redatis);
        $campagneId = $routeur->create_campagne($lesender->password,
            $lacampagne->ref.'_v1_'.time(),
            $lacampagne->objet,
            $lacampagne->expediteur,
            $expediteur_roulement[$rand_keys].'@'.$sender->domaine,
            'reply',
            $expediteur_roulement[$rand_keys].'@'.$sender->domaine,
            '',
            $html);

        var_dump($campagneId);
        $liste = array();
        $chemin = storage_path() . '/edatis/s' . $lesender->nom . '_c' . $lacampagne->id .'_p'.$planning->id. $anonyme.'.csv';

        $fp = fopen($chemin,"r");
        if ($fp) {
            while (!feof($fp))
            {
                $buffer = fgets($fp);
                $buffer = trim($buffer);

                if($buffer !== ''){
                    // tor id
                    $e = explode(";", $buffer);
                    $liste[] = array('email' => $e[0],'TORID'=>$e[1]);
                }
            }
            fclose($fp);
        }

        // imports limités a 100
        $chunckliste = array_chunk($liste,100);
        $nb_call = 1;
        foreach ($chunckliste as $liste_c) {

          $limiteApi = $limiteApi + count($liste_c);

          $nb_call = $nb_call + 1;
          $s = $routeur->send_campagne($lesender->password,$campagneId,$liste_c);
          var_dump($s);

          if($nb_call == 25){
            \Log::info($nb_call .  'Appels Pause');
            sleep(11);
            $nb_call = 0;
          }

        }

        $array_campagne_routeurs_envoi = array(
            'campagne_id' => $lacampagne->id,
            'sender_id' => $lesender->id,
            'cid_routeur' => $campagneId,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
            'taskid' => '1',
            'planning_id' => $planning->id
        );

        \DB::table('campagnes_routeurs')->insert($array_campagne_routeurs_envoi);

        $nb_campagnes_left = $sender->nb_campagnes_left - 1;
        $sender->nb_campagnes_left = $nb_campagnes_left;
        $sender->quota_left = $quota_left;
        $sender->save();

        $planning->nb_trials = 0;
        $planning->segments_at = date('Y-m-d H:i:s');
        $planning->sent_at = date('Y-m-d H:i:s');
        $planning->save();
      }


    }

    public function write_tokens($file, $tokens) {
        $fp = fopen($file,"a+");
        // $content = "";
        $content = null;
        foreach($tokens as $desti){
            $destinataire = \DB::table('destinataires')
                ->select('id','mail','hash')
                ->where('id', $desti->destinataire_id)
                ->first();
            // $content .= $destinataire->mail.";". $desti->destinataire_id . ";" . $destinataire->hash . ";" . $destinataire->hash . "\n";
            // $content .= $destinataire->mail . "\n";
            $content .= $destinataire->mail.";". $desti->destinataire_id . "\n";


        }
        fwrite($fp,$content);
        fclose($fp);
    }

    protected function getArguments()
    {
        return [
            ['planning_id', InputArgument::REQUIRED, 'Planning id.'],
        ];
    }
}
