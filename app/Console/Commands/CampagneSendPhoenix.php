<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Classes\Routeurs\RouteurPhoenix;
use App\Models\Sender;
use App\Models\Campagne;
use App\Models\Planning;
use App\Models\Routeur;



class CampagneSendPhoenix extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campagne:send_phoenix {planning_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Lance une campagne Phoenix';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      //TODO : uncomment when it is OK
      $auto_setting = \DB::table('settings')
        ->where('parameter', 'is_auto')
        ->first();
      if(!$auto_setting or $auto_setting->value != 1){
        \Log::info('CampagneLaunchSend : AUTOMATIC CAMPAIGN NOT ACTIVE');
        return 0;
      }
      $routeur_setting = \DB::table('settings')
        ->where('parameter', 'is_phoenix')
        ->first();

      if(!$routeur_setting or $routeur_setting->value != 1){
          \Log::info('CampagneLaunchSend : PHOENIX DISABLE');
          return 0;
        }

        $routeur = new RouteurPhoenix();
        $plannif = Planning::find($this->argument('planning_id'));
        $campagneid = $plannif->campagne_id;

        $today = \Carbon\Carbon::today();
        $ajd = date('Y-m-d');

        $senderenvoi = \DB::table('campagnes_routeurs')
            ->where('campagne_id', $campagneid)
            ->where('planning_id', $plannif->id)
            ->where('created_at','>', $today)
            ->get();

        $campagne = Campagne::find($campagneid);

        $anonyme = "";

        foreach ($senderenvoi as $lesender)
        {
            $infosender = Sender::find($lesender->sender_id);

            // var_dump($infosender);

            // j'ai mon senders
            echo $infosender->password . "\n";
            // j'ai besoin de recup la bonne ligne pour avoir la liste ID
            echo $lesender->listid . "\n";
            // j'ai besoin de la date du planning
            echo $plannif->date_campagne . "\n";
            echo $plannif->time_campagne . "\n";
            echo $plannif->campagne_id . "\n";

            // var_dump($this->showHtmlPhoenix($campagneid));

            $timecampagne = $plannif->date_campagne .' '. $plannif->time_campagne;
            $timecampagne = strtotime($timecampagne);

            // for dev
            $timecampagne = $timecampagne + 3600;

            // if ici et on creé la var
            if($plannif->type == 'anonyme'){
                $anonyme = "_a";

                // on fait le meme traitement qu'en dessous mais avec un show html anonyme et expediteur ano
                $htmldelacampagne = $this->showHtmlPhoenixAno($campagneid);
            } else {
                $htmldelacampagne = $this->showHtmlPhoenix($campagneid);
            }

            // $htmldelacampagne = $this->showHtmlPhoenix($campagneid);
            $retourrouteur = $routeur->router_get($infosender->password);
            $retourkit = $routeur->kit_create($infosender->password, $campagne->ref . time(), $campagne->objet , $campagne->expediteur, $htmldelacampagne);
            $retourcampagne = $routeur->campaign_create($infosender->password, $campagne->nom . '-api' . time(), $retourkit, $lesender->listid, $retourrouteur->routers[0]->router_id, $timecampagne);
            // partie de test
            \Log::info("Var retour campagne : " . $retourcampagne->campaign_id);

            \DB::statement("UPDATE campagnes_routeurs SET cid_routeur ='" . $retourcampagne->campaign_id . "' WHERE id ='". $lesender->id ."'");

        }

        $plannif->sent_at = \Carbon\Carbon::now();
        $plannif->save();

        exec("rm ~/".getenv('CLIENT_URL')."/storage/phoenix/*p$plannif->id$anonyme*");

        // on envoi le mail
        $email_volume_demande = 'Volume demandé : ' . $plannif->volume;
        $email_volume_orange = 'Volume orange demandé : ' . $plannif->volume_orange;
        $email_volume_selected = 'Volume réel utilisé : ' . $plannif->volume_selected;
        $email_date = 'Campagne programmée le ' . $plannif->date_campagne . ' ' . $plannif->date_campagne . ' / Envoi de la campagne terminé à ' . $plannif->sent_at;

        $senders = \DB::select("select nom from senders where id in (select sender_id from campagnes_routeurs where planning_id = :planning_id)", ['planning_id' => $plannif->id]);

        $used_senders = 'Compte(s) utilisé(s) : <br/>';

        foreach($senders as $s) {
            $used_senders .= "$s->nom <br/>";
        }

        \Mail::send('mail.mailinfocampagne',
            [
                'email_volume_demande' => $email_volume_demande,
                'email_volume_orange' => $email_volume_orange,
                'email_volume_selected' => $email_volume_selected,
                'email_date' => $email_date,
                'url' => getenv('CLIENT_TITLE'),
                'used_senders' => $used_senders,
            ], function ($message) use ($campagne) {
                $destinataires = \App\Models\User::where('is_valid', 1)
                    ->where('user_group_id', 1)
                    ->where('email', '!=', "")
                    ->get();
                $message->from('rapport@lead-factory.net', 'Tor')->subject("Rapport PH : envoi de $campagne->ref [$campagne->id] terminé au " . date('d-m-Y H:i'));
                foreach($destinataires as $d){
                    $message->to($d->email);
                }
                return "true";
                // $message->to('fabien@lead-factory.net');
                // $message->to('adeline.sc2@gmail.com');
            });
    }

    public function showHtmlPhoenix($id)
    {
        $campagne = Campagne::find($id);
        $routeur = Routeur::where('nom','Phoenix')->first();

        return $campagne->generateHtml($routeur);
    }


    public function showHtmlPhoenixAno($id)
    {
        $campagne = Campagne::find($id);
        $routeur = Routeur::where('nom','Phoenix')->first();

        return $campagne->generateHtmlAnonyme($routeur);
    }

    protected function getArguments()
    {
        return [
            ['planning_id', InputArgument::REQUIRED, 'Planning id.'],
        ];
    }
}
