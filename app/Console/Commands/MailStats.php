<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MailStats extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:stats';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command qui permet de recevoir les statistiques des campagnes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // le nom de la commande peut changer
        // futur gestion des param ? (MD, Smessage, Phoenix)
        // envoi de plusieurs info en 1 seul mail

        // if maildrop
        $lerouteurid = \DB::table('routeurs')->where('nom','Maildrop')->first();
        $comptemaildrop = \DB::table('senders')->where('routeur_id', $lerouteurid->id)->get();
        $lastmaj = \DB::table('stats_maildrop')->max('bloc_maj');
        $statsrouteurv2 = \DB::table('stats_maildrop_total')->where('bloc_maj',$lastmaj)->groupBy('reference')->get();

        // getenv('CLIENT_URL');
        $varmail = null;
        \Mail::send('mail.mailstatscampagne',
        [
        'statsrouteurv2' => $statsrouteurv2,
        'url' => getenv('CLIENT_URL')
      ], function ($message) use ($varmail) {
          $destinataires = \App\Models\User::where('is_valid', 1)
              ->where('user_group_id', 1)
              ->where('email', '!=', "")
              ->get();
          $message->from('rapport@lead-factory.net', 'Tor')->subject("Statistiques des campagnes MD au " . date('d-m-Y H:i'));
          foreach($destinataires as $d){
              $message->to($d->email);
          }
          return "true";
        });

        // if smessage


    }
}
