<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Classes\Routeurs\RouteurEdatis;
use App\Models\Routeur;

class CampagneUnsubscribeEdatis extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campagne:unsubscribe_edatis';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Unsubscribe Edatis';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Log::info("[CampagneUnsubscribeMailforyou] : Début");

        $routeur = new RouteurEdatis();
        $today = date('Ymd');
        $two_weeks_ago = date('Y-m-d 00:00:00', strtotime('-2 weeks'));
        $redatis = Routeur::where('nom','Edatis')->first();
        $senders = \DB::table('senders')->select('id')->where('routeur_id', $redatis->id)->get();

        $informations = \DB::table('campagnes_routeurs')
          ->where('created_at','>',$two_weeks_ago)
          ->whereIn('sender_id', array_pluck($senders, 'id'))
          ->get();

        $file = storage_path() . '/desinscrits/unsubscribe_edatis_' . $today . '.csv';
        $fp = fopen($file,"w+");
        fclose($fp);
        $nbpageresult = 0;

        $chaine = '';

        foreach ($informations as $lignecampagnerouteur){

          echo 'lecture data' . "\n";

          \Log::info("CampagneUnsubscribeEdatis : Campagne $lignecampagnerouteur->campagne_id - Planning $lignecampagnerouteur->planning_id");
          if(empty($lignecampagnerouteur->cid_routeur)){
              \Log::info("CampagneUnsubscribeEdatis : empty cid_routeur (Campagne $lignecampagnerouteur->campagne_id - Planning $lignecampagnerouteur->planning_id)");
              continue;
          }

          $nbpageresult = $routeur->get_unsubscribers_infos($lignecampagnerouteur->sender_id,$lignecampagnerouteur->cid_routeur);

          $file = storage_path() . '/desinscrits/unsubscribe_edatis_' . $today . '.csv';
          $fp = fopen($file,"a+");
          fwrite($fp, '');
          fclose($fp);


          if($nbpageresult->numberOfPages < 2 ){
            // une seule page de resultat
            $unsubscribe = $routeur->get_unsubscribers($lignecampagnerouteur->sender_id,$lignecampagnerouteur->cid_routeur,1);
            foreach ($unsubscribe->results as $info_unsubscribe) {
              $chaine .= $info_unsubscribe->mail . "\n";
            }
            \Log::info("CampagneUnsubscribeEdatis : (Campagne $lignecampagnerouteur->campagne_id - Planning $lignecampagnerouteur->planning_id) - Lecture Page 1");

          } else {
            // multi page de result
            for ($i=1; $i < $nbpageresult->numberOfPages; $i++) {
              var_dump($i);
              $unsubscribe = $routeur->get_unsubscribers($lignecampagnerouteur->sender_id,$lignecampagnerouteur->cid_routeur,$i);

              if(isset($unsubscribe->results) && !empty($unsubscribe->results)){
                \Log::info("CampagneUnsubscribeEdatis : (Campagne $lignecampagnerouteur->campagne_id - Planning $lignecampagnerouteur->planning_id) - Lecture Page $i");
                foreach ($unsubscribe->results as $info_unsubscribe) {
                  $chaine .= $info_unsubscribe->mail . "\n";
                }
              }
            }

          }

        }

        // var_dump($chaine);

        $file = storage_path() . '/desinscrits/unsubscribe_edatis_' . $today . '.csv';
        $fp = fopen($file,"w");
        fwrite($fp, $chaine);
        fclose($fp);


        echo "Lancement fichier \n";
        \Log::info("Debut [import_desinscrits] unsubscribe EDATIS" . "[unsubscribe_edatis_" . $today . ".csv]");
        \Artisan::call('base:import_desinscrits', ['file' => "unsubscribe_edatis_" . $today . ".csv"]);
        \Log::info("Fin update [import_desinscrits] du fichier unsubscribe EDATIS" . "[unsubscribe_edatis_" . $today . ".csv]");


    }
}
