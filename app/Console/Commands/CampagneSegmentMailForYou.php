<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\Planning;
use App\Classes\Routeurs\RouteurMailForYou;
use App\Models\Campagne;
use App\Models\Routeur;
use App\Models\Fai;
use App\Models\Sender;

class CampagneSegmentMailForYou extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campagne:segment_mailforyou {planning_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Segment Mail For You';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // table sender_id / base_id
        // nombre de comptes dispatch
        // check nb campagnes left

        $auto_setting = \DB::table('settings')
            ->where('parameter', 'is_auto')
            ->first();
        if(!$auto_setting or $auto_setting->value != 1){
            \Log::info('CampagneLaunchSend : AUTOMATIC CAMPAIGN NOT ACTIVE');
            return 0;
        }
        $routeur_setting = \DB::table('settings')
            ->where('parameter', 'is_mailforyou')
            ->first();

        if(!$routeur_setting or $routeur_setting->value != 1){
            \Log::info('CampagneLaunchSend : MailForYou DISABLE');
            return 0;
        }

        $planning = Planning::find($this->argument('planning_id'));

        $nombreaccount = $planning->nbr_sender;

        $bloc = \DB::table('senders_history')->max('bloc');
        if(empty($bloc)){
            $bloc = 0;
        }
        $this->max_bloc = $bloc + 1;

        \Log::info("Nombre de comptes M4Y qui seront utilisés : " . $nombreaccount);

        $planning->nb_trials++;
        $planning->save();

        $campagneid = $planning->campagne_id;
        echo "\nCampagne Segment \n";
        $routeur = new RouteurMailForYou();
        $lacampagne = Campagne::where('id',$campagneid)->first();
        $m4u = Routeur::where('nom','MailForYou')->first();

        $anonyme = "";
        if($planning->type == 'anonyme') {
            $anonyme = "_a";
            $ano = true;
        }

        \Log::info("[CampagneSegmentMailForYou][P$planning->id] : Début Segmentation (Planning $planning->id/Campagne $lacampagne->id) {Trial n°$planning->nb_trials}");

        $fais = Fai::all();

        $multi_mono =  \DB::table('planning_senders')
            ->where('planning_id', $planning->id)
            ->first();

        if($multi_mono == null) {

            if(empty($nombreaccount)){
                $nombreaccount = 2; // to fix error "division by zero", we put a default value
            }

            // multi compte
            \Log::info("Multi Compte M4y");
            // a clean mais ok prod 
            /*
            // plus besoin de ca pour le moment

            // tableau des senders de la base
            $sender_m4y = \DB::table('senders_base')
                ->select('sender_id')
                ->where('base_id', $lacampagne->base_id)
                ->get();
            $tabsender = array();
            foreach ($sender_m4y as $s)
            {
                $tabsender[] = $s->sender_id;
            }

            */

            // where in id sender + routeur id
            $sender_envoi_m4y = \DB::table('senders')
                ->where('routeur_id', $m4u->id)
                // ->whereIn('id', $tabsender)
                ->where('nb_campagnes_left', '>', 0)
                ->where('quota_left', '>', 0)
                ->where('branch_id', $planning->branch_id)
                ->get();

            echo 'larray des senders';
            var_dump($sender_envoi_m4y);

            // pour que les comptes changent
            // shuffle($sender_envoi_m4y);

            // je prend mes comptes dispatch
            $senderenvoi = array_slice($sender_envoi_m4y,0,$nombreaccount);
            var_dump($senderenvoi);


            $countFai = \DB::table('tokens')
                ->select('fai_id', \DB::raw('count(*) as total'))
                ->where('planning_id',$planning->id)
                ->groupBy('fai_id')
                ->get();

            foreach($countFai as $cf)
            {
                echo 'taile du count : ' . $cf->total . "\n";

                $sizechunck = $cf->total / $nombreaccount;

                // $volumeFaiTotal = $cf->total;
                // On récupère les tokens PAR FAI
                /* $lesadressestokens = \DB::table('tokens')
                    ->select('destinataire_id')
                    ->where('campagne_id',$lacampagne->id)
                    ->where('fai_id',$cf->fai_id)
                    ->where('date_active',$planning->date_campagne)
                    ->where('planning_id',$planning->id)
                    ->get();

                    */

                // $nombreparfai = count($cf->total);
                var_dump('mon fai id  : ' . $cf->fai_id);
                var_dump('nombre par fai : ' . $cf->total);
                // $sizechunck = $nombreparfai / $nombreaccount;
                var_dump('taille array :' .  $sizechunck);
                $downset = 0;
                $offset = $sizechunck;


                $skip = 0;
                $take = $sizechunck;
                // foreach mes comptes pour remplir les listes avec chaque fai
                foreach ($senderenvoi as $s)
                {

                  // On récupère les tokens PAR FAI
                  $lesadressestokens = \DB::table('tokens')
                      ->select('destinataire_id')
                      ->where('campagne_id',$lacampagne->id)
                      ->where('fai_id',$cf->fai_id)
                      ->where('date_active',$planning->date_campagne)
                      ->where('planning_id',$planning->id)
                      ->skip($skip)->take($sizechunck)
                      ->get();




                    echo 'Remplissage liste Sender : ' . $s->nom . "\n";

                    $file = storage_path() . '/mailforyou/s' . $s->nom . '_c' . $lacampagne->id .'_p'.$planning->id. $anonyme.'.csv';

                    // je prend la partie qui m'interesse : array_slice($lesadressestokens,0,$nbcoup);
                    /* $aliste = array_slice($lesadressestokens,$downset,$offset);
                    var_dump('count off : ' . $offset);
                    var_dump('count down : ' . $downset);

                    $offset = $offset + $sizechunck;
                    $downset = $downset + $sizechunck;

                    */

                    var_dump('count aliste new : ' . count($lesadressestokens));

                    $this->write_tokens($file, $lesadressestokens);

                    $sender = Sender::find($s->id);
                    $quota_left = $sender->quota_left - $sizechunck;
                    $sender->quota_left = $quota_left;
                    // a mettre plus bas
                    // $sender->nb_campagnes_left -= 1;
                    $sender->save();
                    $skip = $skip + $sizechunck;
                }

                $skip = 0;
            }

            // die();

            foreach ($senderenvoi as $s)
            {
                echo 'Import serveur ' . $s->nom;

                if(file_exists(storage_path() . '/mailforyou/zip/' . $lacampagne->ref)){
                    \Log::info("Le fichier zip de la campagne existe deja");
                } else {
                    mkdir(storage_path() . '/mailforyou/zip/' . $lacampagne->ref);
                }

                $zip = new \ZipArchive;
                $filename = storage_path() . '/mailforyou/zip/'. $lacampagne->ref .'/fichiercampagne.zip';
                if($zip->open($filename, \ZipArchive::CREATE) === true)
                {
                    $zip->addFile('index.html');
                    $zip->addFromString('index.html',$lacampagne->generateHtmlM4u($m4u));
                    $zip->close();

                } else {
                    echo 'Echec creation ZIP' . "\n";
                }

                $liste = array();

                $chemin = storage_path() . '/mailforyou/s' . $s->nom . '_c' . $lacampagne->id .'_p'.$planning->id. $anonyme.'.csv';

                $fp = fopen($chemin,"r");
                if ($fp) {
                    while (!feof($fp))
                    {
                        $buffer = fgets($fp);
                        $buffer = trim($buffer);

                        if($buffer !== ''){
                            // on casse la chaine ici ?
                            $e = explode(";", $buffer);
                            // var_dump($buffer);
                            $liste[] = [$e[0],$e[1],$e[2]];
                        }
                    }
                    fclose($fp);
                }

                echo 'Creation liste desti' . "\n";
                $retour = $routeur->CreationListeDestinataireFields_new($s->password,'Segment_'.time(),$s->api_login,$s->api_key);
                echo 'Import liste desti' . "\n";
                shuffle($liste);
                $decoupeliste = array_chunk($liste,5000);

                foreach ($decoupeliste as $t)
                {
                    echo 'Import Chunk' . "\n";
                    $retour2 = $routeur->AjoutListeDestinataireFields_new($retour['id'], $t,$s->api_login,$s->api_key);
                    var_dump($retour2);
                }

                echo 'Import liste desti fini' . "\n";

                echo 'Creation Campagne' . "\n";
                $retour3 = $routeur->CreationCampagne_new($lacampagne->nom . '-' . time(), $lacampagne->objet, $s->password,$s->api_login,$s->api_key);
                var_dump('ID campagne');

                echo 'Ajout Zip ' . "\n";
                $retour4 = $routeur->SaveHtmlZip_new($retour3['id'], $filename,$s->api_login,$s->api_key);

                echo 'Zip OK' . "\n";
                // set planification routage
                echo 'Plannif' . "\n";
                $retour5 = $routeur->SetPlanificationRoutage_new($retour3['id'],$retour['id'], $lacampagne->expediteur, $s->domaine,$s->api_login,$s->api_key,$planning->freq_send);
                var_dump($retour5);
                // enregistrement campagne routeurs
                $array_campagne_routeurs_envoi = array(
                    'campagne_id' => $lacampagne->id,
                    'sender_id' => $s->id,
                    'cid_routeur' => $retour5['id'],
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s"),
                    'taskid' => '0',
                    'planning_id' => $planning->id
                );


                \DB::table('campagnes_routeurs')->insert($array_campagne_routeurs_envoi);

                \Log::info("Save du Segment_at M4Y");

                // enlever sender nb_campagnes_left

                // pour lenvoi je vais parcourir campagne routeur sur le planning id

                $sender = Sender::find($s->id);
                $nb_campagnes_left = $sender->nb_campagnes_left - 1;
                $sender->nb_campagnes_left = $nb_campagnes_left;
                $sender->save();

            }

            $planning->nb_trials = 0;
            $planning->segments_at = date('Y-m-d H:i:s');
            $planning->save();

            \Log::info("[CampagneSegmentMailForYou][P$planning->id] : Fin Segmentation (Planning $planning->id/Campagne $lacampagne->id)");
        } else {
            // mono compte
            \Log::info("Mono Compte M4y");
            $fais = Fai::all();
            $lesadressestokens = array();
            $lesadressestokens = \DB::table('tokens')
                ->select('destinataire_id')
                //           ->where('fai_id',$f->id)
                ->where('campagne_id',$campagneid)
                ->where('planning_id',$planning->id)
                ->where('date_active',$planning->date_campagne)
                ->get();

            $info_planning_sender = \DB::table('planning_senders')
                ->where('planning_id',$this->argument('planning_id'))
                ->first();

            $lesenderm4y = \DB::table('senders')
                ->where('id',$info_planning_sender->sender_id)->first();

            $file = storage_path() . '/mailforyou/s' . $lesenderm4y->nom . '_c' . $lacampagne->id .'_p'.$planning->id. $anonyme.'.csv';
            $sender = Sender::find($lesenderm4y->id);

            $quota_left = $sender->quota_left - count($lesadressestokens);

            //l'historique
            \DB::table('senders_history')
                ->insert(
                    [
                        'quota_before' => null,
                        'sender_id' => $sender->id,
                        'fai_id' => null,
                        'planning_id' => $planning->id,
                        'quota_left_before' => $sender->quota_left,
                        'quota_left' => $quota_left,
                        'created_at' => date("Y-m-d H:i:s"),
                        'updated_at' => date("Y-m-d H:i:s"),
                        'bloc' => $this->max_bloc
                    ]
                );

            // ici que ca change
            $this->write_tokens($file, $lesadressestokens);
            $sender->quota_left = $quota_left;
            $sender->nb_campagnes_left -= 1;
            $sender->save();

            if(file_exists(storage_path() . '/mailforyou/zip/' . $lacampagne->ref)){
                \Log::info("Le fichier zip de la campagne existe deja");
            } else {
                mkdir(storage_path() . '/mailforyou/zip/' . $lacampagne->ref);
            }

            $zip = new \ZipArchive;
            $filename = storage_path() . '/mailforyou/zip/'. $lacampagne->ref .'/fichiercampagne.zip';

            if($zip->open($filename, \ZipArchive::CREATE) === true) {
                $zip->addFile('index.html');
                $zip->addFromString('index.html',$lacampagne->generateHtmlM4u($m4u));
                $zip->close();
            } else {
                echo 'Echec creation ZIP' . "\n";
            }

            $liste = array();

            $chemin = storage_path() . '/mailforyou/s' . $lesenderm4y->nom . '_c' . $lacampagne->id .'_p'.$planning->id. $anonyme.'.csv';

            $fp = fopen($chemin,"r");
            if ($fp) {
                while (!feof($fp))
                {
                    $buffer = fgets($fp);
                    $buffer = trim($buffer);

                    if($buffer !== ''){
                        // on casse la chaine ici ?
                        $e = explode(";", $buffer);
                        // var_dump($buffer);
                        $liste[] = [$e[0],$e[1],$e[2]];
                    }
                }
                fclose($fp);
            }

            echo 'Creation liste desti' . "\n";
            $retour = $routeur->CreationListeDestinataireFields_new($lesenderm4y->password,'Dev_Liste_Segment_production_'.time(),$lesenderm4y->api_login,$lesenderm4y->api_key);
            echo 'Import liste desti' . "\n";
            shuffle($liste);
            $decoupeliste = array_chunk($liste,5000);

            foreach ($decoupeliste as $t)
            {
                echo 'Import Chunk' . "\n";
                $retour2 = $routeur->AjoutListeDestinataireFields_new($retour['id'], $t,$lesenderm4y->api_login,$lesenderm4y->api_key);
                var_dump($retour2);
            }

            echo 'Import liste desti fini' . "\n";

            echo 'Creation Campagne' . "\n";
            $retour3 = $routeur->CreationCampagne_new($lacampagne->nom . '-' . time(), $lacampagne->objet, $lesenderm4y->password,$lesenderm4y->api_login,$lesenderm4y->api_key);
            var_dump('ID campagne');

            echo 'Ajout Zip ' . "\n";
            $retour4 = $routeur->SaveHtmlZip_new($retour3['id'], $filename,$lesenderm4y->api_login,$lesenderm4y->api_key);

            echo 'Zip OK' . "\n";
            // set planification routage
            echo 'Plannif' . "\n";
            $retour5 = $routeur->SetPlanificationRoutage_new($retour3['id'],$retour['id'], $lacampagne->expediteur, $lesenderm4y->domaine,$lesenderm4y->api_login,$lesenderm4y->api_key,$planning->freq_send);
            var_dump($retour5);
            // enregistrement campagne routeurs
            $array_campagne_routeurs_envoi[] = array(
                'campagne_id' => $lacampagne->id,
                'sender_id' => $lesenderm4y->id,
                'cid_routeur' => $retour5['id'],
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
                'taskid' => '0',
                'planning_id' => $planning->id
            );

            // je check si la ligne existe déjà
            $check = \DB::table('campagnes_routeurs')->where('planning_id',$planning->id)->where('cid_routeur',$retour5['id'])->first();
            // si non j'ajoute insert
            // else je fais rien
            // pas bon en fait :/
            // if($check == null){
            \DB::table('campagnes_routeurs')->insert($array_campagne_routeurs_envoi);
            // }

            \Log::info("Save du Segment_at M4Y");

            $planning->nb_trials = 0;
            $planning->segments_at = date('Y-m-d H:i:s');
            $planning->save();

            \Log::info("[CampagneSegmentMailForYou][P$planning->id] : Fin Segmentation (Planning $planning->id/Campagne $lacampagne->id)");
        }
    }

    public function write_tokens($file, $tokens) {
        $fp = fopen($file,"a+");
        // $content = "";
        $content = null;
        foreach($tokens as $desti){
            $destinataire = \DB::table('destinataires')
                ->select('id','mail','hash')
                ->where('id', $desti->destinataire_id)
                ->first();
            $content .= $destinataire->mail.";". $desti->destinataire_id . ";" . $destinataire->hash . "\n";
            // $content .= $destinataire->mail . "\n";
            // $arraylistemfu[] = array($destinataire->mail);
        }
        fwrite($fp,$content);
        fclose($fp);
    }

    protected function getArguments()
    {
        return [
            ['planning_id', InputArgument::REQUIRED, 'Planning id.'],
        ];
    }
}
