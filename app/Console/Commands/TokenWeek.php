<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;


use App\Models\Fai;
use App\Models\User;
use App\Models\Base;

class TokenWeek extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tokens:week';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Commande pour les tokens sur une semaine';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $today = Carbon::today();
        $timestamp = date('Y-m-d H:i:s');
        $date_active = $today->format('Y-m-d');
        $weeknow = date('W');

        // je parcours les tables à is_classic 0
        $bases = Base::where('is_active', 1)
        ->where('is_classic', 0)
        ->get();

        var_dump($weeknow);

        foreach ($bases as $labase) {

          // je check la semaine en cours
          $newWeekOrNot = \DB::table('tokens_week')
              ->where('base_id', $labase->id)
              ->where('nb_semaine', $weeknow)
              ->count();

          var_dump($newWeekOrNot);

          if($newWeekOrNot < 1){
            // pas de tokens pour cette semaine, il faut les faire
            // si le num de semaine est diff je calcul tout car c'est une nouvelle semaine
            // donc on on lance normal et on record la semaine faite

            // kill des anciens tokens à voir

            \Artisan::call('tokens:manageone', ['base_id' => $labase->id]);

            $dataset[] = [
            'base_id' => $labase->id,
            'nb_semaine' => $weeknow,
            'created_at' => $timestamp,
            'updated_at' => $timestamp
            ];

            \DB::table('tokens_week')->insert($dataset);

          } else {

            var_dump('Lancement des tokens à ne pas refaire');
            \DB::table('tokens')
                ->select('destinataire_id')
                ->where('base_id', 8)
                ->whereNotNull('campagne_id')
                ->chunk(1000, function ($lestokens) {
                    var_dump('Traité 1000');
                    //$c = $c + 1000;
                    // var_dump(array_pluck($lestokens,'destinataire_id'));
                    $array_tokens = array_pluck($lestokens,'destinataire_id');

                    \DB::table('destinataires')
                    ->where('base_id',8)
                    ->whereIn('id', $array_tokens)
                    ->update(['tokenized_at' => date('Y-m-d H:i:s')]);
                });

              var_dump('Calcul du reste des tokens');
              \Artisan::call('tokens:manageone', ['base_id' => $labase->id]);
              /*

              // si c'est la même semaine id je maj juste les null avec la date du jour
              // echo 'déjà des tokens';
              // update des destinataires de la base
              \DB::table('destinataires')
              ->where('base_id', $labase->id)
              ->update(['tokenized_at' => $timestamp]);

              // update where campagne id is null
              // planning_id = 0


              $debug = \DB::table('tokens')
              ->where('base_id', $labase->id)
              ->whereNull('campagne_id')
              ->where('planning_id',"=",0)
              ->count();

              \Log::info('[TokensWeek]'.json_encode($debug));

              \DB::table('tokens')
              ->where('base_id', $labase->id)
              ->whereNull('campagne_id')
              ->where('planning_id',"=",0)
              ->update(['date_active' => $date_active]);

                */







          }












        }


    }
}
