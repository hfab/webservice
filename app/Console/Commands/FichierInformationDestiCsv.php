<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;

class FichierInformationDestiCsv extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'info:destinataires';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
      // $v[0] = email
      // $v[1] = civilite
      // $v[2] = nom
      // $v[3] = prenom
      // $v[4] = date naissance
      // $v[5] = complement adresse (appart)
      // $v[6] = complement adresse (batiment)
      // $v[7] = adresse
      // $v[8] = rien
      // $v[9] = code postal
      // $v[10] = commune
      // $v[11] = tel fixe
      // $v[12] = tel portable
      // $v[13] = ip

      \DB::disableQueryLog();
      \App\Helpers\Profiler::start('Debut importation big fichier csv');
      $labaseid = $this->argument('base_id');
      $lefichier = $this->argument('lefichier');

      // pour la suite, on a besoin de ca qu'une fois
      $counterpdo = 0;
      $pdo = \DB::connection()->getPdo();
      // rajouter civilite
      $sql = "UPDATE destinataires SET nom = :v_nom, prenom = :v_prenom, ville = :v_ville, departement = :v_departement, civilite = :v_civilite, datenaissance = :v_datenaissance WHERE base_id = " . $labaseid . " AND mail = :v_mail";
      $stmt = $pdo->prepare($sql);
      $pdo->beginTransaction();

      // $tabtest = array();
      // $tabfinalforupdate = array();
      $tabupdatematch = array();
      $bulkemailinfo = array();
      $bulkemailselect = array();
      $sql = 0;
      $updateStatement = '';
      $c = 0;
      $fichiercsv = fopen(storage_path("listes/")  . $lefichier , "r");
      while(!feof($fichiercsv)) {

        $prenom = "";
        $nom = "";
        $ville = "";
        $datenaissance = "";
        $dept = 0;
        $civilite = 3;

          echo $c . "\r";
          $c = $c + 1;
          // if($c == 100000){
          //   break;
          // }
        //   continue;
      $v = explode(";", trim(fgets($fichiercsv)));

      if(isset($v[0])){

      if (filter_var($v[0], FILTER_VALIDATE_EMAIL)) {
        $bulkemailselect[] = $v[0];
        // taille du chuck search
        if(count($bulkemailselect) == 1000){
          // echo 'Traitement CHUNCK 1000' . "\n";
          // on va faire un select where in
          $x = \DB::table('destinataires')->select('id','mail')->whereIn('mail', $bulkemailselect)->get();
          $bulkemailselect = array();

          if(count($x) > 0){
            //  foreach ($x as $z) {
            //   $tabupdatematch[] = $z->mail;
            //  }

            $tabupdatematch=array_pluck($x,'mail');
            var_dump($tabupdatematch);
          }
        }

        if(isset($v[1])){
          $checkcivilmme = strripos($v[1],'mme');
          if($checkcivilmme !== false){
            $civilite = 0;
          }

          $checkcivilmr = strripos($v[1],'mr');
          if($checkcivilmr !== false){
            $civilite = 1;
          }
        }

        if(isset($v[2])){
          $nom = $v[2];
        }
        if(isset($v[3])){
          $prenom = $v[3];
        }
        if(isset($v[9])){
          $dept = $v[9];
        }
        if(isset($v[10])){
          $ville = $v[10];
        }

        if(isset($v[4])){
          if($v[4] == '\N'){
            $datenaissance = '';
          } else {
            $datenaissance = $v[4];
          }

        }

        $bulkemailinfo[strtolower($v[0])] = array('nom' => $nom, 'prenom' => $prenom, 'ville' => $ville, 'departement' => $dept, 'civilite' => $civilite, 'datenaissance' => $datenaissance);

      }
      }
      // var_dump($tabupdatematch);

      $tabcount = count($tabupdatematch);

      if(count($tabupdatematch) > 0){

        foreach($tabupdatematch as $item){

           // var_dump($item);
           // bug sur l'addresse Al1dav1@free.fr ??
           $var = $bulkemailinfo[strtolower($item)];

            // var_dump($item);
            // var_dump($var);

           // peut etre probleme

           // var_dump($var['nom']);
           // var_dump($var['prenom']);
           $insertData = [$var['nom'],$var['prenom'],$var['ville'],$var['departement'], $var['civilite'], $var['datenaissance'], $item];
           // $insertData = [$var['nom'], $item];
           $stmt->execute($insertData);

           $counterpdo = $counterpdo + 1;
          //  if($counterpdo == 500){
           //
          //    echo 'COMMIT du bloc 500' . "\n";
          //    $pdo->commit();
          //    $pdo->beginTransaction();
          //    $counterpdo = 0;
           //
          //   }

           $tabcount = $tabcount - 1;
           // echo '[counter = ' . $counterpdo . '/500]' . "\n";
           // echo 'Reste traitement PDO : ' . $tabcount . "\n";
           // var_dump(count($bulkemailinfo));
           if($tabcount == 0){

             // echo 'COMMIT' . "\n";
             $pdo->commit();
             $pdo->beginTransaction();
             $tabupdatematch = array();
             $bulkemailinfo = array();

           }
       }
    }
    // $bulkemailinfo = array();
  }

      echo 'UPDATE Chunck Final OK' . "\n";
      $pdo->commit();

      fclose($fichiercsv);
      \Log::info("Fin du script MAJ contact csv");
      \App\Helpers\Profiler::report();

}

protected function getArguments()
{
    return [
        ['base_id', InputArgument::REQUIRED, 'Base ID'],
        ['lefichier', InputArgument::REQUIRED, 'Le fichier a lire'],
    ];
}


}
