<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Classes\Routeurs\RouteurMindbaz;
use App\Models\Routeur;
use App\Models\Campagne;

class ComputeOpeningsMindbaz extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'openings:compute_mindbaz';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Récupère les ouvreurs de Mindbaz et lance job : insertion ouvreurs dans la DB > table > ouvertures.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Log::info("[ComputeOpeningsMindbaz] : Début");
        $today = date('Ymd');
        $mindbaz = new RouteurMindbaz();

        $all_openers = $mindbaz->get_openers();

        $compteur = 0;

        $today = date('Ymd');
        $file = storage_path('ouvreurs')."/import_mb_openers_".$today.".csv";
        $inserts = array();

        foreach($all_openers as $campagne_id => $openers)
        {
            $campagne = Campagne::select('base_id')
                ->where('id', $campagne_id)
                ->first();

            $base_id = $campagne->base_id;

            foreach($openers as $opener)
                {
                $destinataire = \DB::table('destinataires')
                    ->select('id')
                    ->where('base_id', $base_id)
                    ->where('mail', $opener[0])
                    ->first();
                if(empty($destinataire)){
                    continue;
                }
                $inserts[] = [$destinataire->id, ];
            }
        }
        \Log::info("[ComputeOpeningsMindbaz] : finished (Count : $compteur)");
//        \Log::info("[ComputeOpeningsMindbaz] : launch of BaseImportDesinscrits command");
//        \Artisan::call('base:import_clics', ['file' => $file]);
//        \Log::info("[ComputeOpeningsMindbaz] : launch of BaseImportDesinscrits command OK");

    }
}
