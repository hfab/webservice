<?php namespace App\Console\Commands;

use App\Classes\Routeurs\RouteurMaildrop;
use App\Models\Base;
use App\Models\Campagne;
use App\Models\Destinataire;
use App\Models\Sender;
use App\Models\CampagneRouteur;
use Illuminate\Console\Command;

class Abuse extends Command
{

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'abuse';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Import unsubscribers from abuse@';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

    public function handle()
    {
        \Eloquent::unguard();

        // Traitement des désinscrits via abuse@dgcnit.fr
        $dateMails = date ( "Y-m-d",strtotime("-1 day"));
        $this->recupMails($dateMails);
    }

    private function recupMails($dateMails)
    {

        $infos = array();
        $bases = Base::all();
        $already = array();

        //eviter les doublons sur les adresses "abuse"
        foreach($bases as $base)
        {
            if($base->abuse_username==''){
                continue;
            }
            $infos[$base->abuse_username] = array($base->abuse_hostname, $base->abuse_password, $base->id);
        }

        //Recuperation des desinscrits pour chaque base
        foreach($infos as $username => $baseInfo)
        {
            // hostname, username, password
            echo "host - ".$baseInfo[0]." user - ".$username." pass - ".$baseInfo[1]." - base - ".$baseInfo[2];
            $inbox = imap_open($baseInfo[0], $username, $baseInfo[1]) or die('Cannot connect to Mail: ' . imap_last_error());
            $emails = imap_search($inbox, "SINCE $dateMails");

            foreach ($emails as $emailNumber) {
                $info = imap_headerinfo($inbox, $emailNumber);

                if(!isset($info->sender[0]) or !isset($info->sender[0]->mailbox)){
                    continue;
                }

                $email = $info->sender[0]->mailbox.'@'.$info->sender[0]->host;

                if(filter_var($email,FILTER_VALIDATE_EMAIL) === false or isset($already[$email])) {
                    continue;
                }
                
                //print_r("\n".$email);
                $already[$email] = 1;

                $desti = Destinataire::firstOrCreate(['mail' => $email, 'base_id' => $baseInfo[2]]);
                $desti->statut = DESTINATAIRE_ABUSED;
                $desti->save();
                
                $date = date('Ymd');

                file_put_contents(public_path().'/repoussoirs/abuse-'.$date.'.txt', $email."\n", FILE_APPEND);
                imap_errors();
            }
            imap_errors();
            imap_close($inbox);
        }
    }
}
