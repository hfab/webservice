<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
$path = storage_path('arangodb-php/autoload.php');
require $path;
use ArangoDBClient\Collection as ArangoCollection;
use ArangoDBClient\CollectionHandler as ArangoCollectionHandler;
use ArangoDBClient\Connection as ArangoConnection;
use ArangoDBClient\ConnectionOptions as ArangoConnectionOptions;
use ArangoDBClient\DocumentHandler as ArangoDocumentHandler;
use ArangoDBClient\Document as ArangoDocument;
use ArangoDBClient\Exception as ArangoException;
use ArangoDBClient\Export as ArangoExport;
use ArangoDBClient\ConnectException as ArangoConnectException;
use ArangoDBClient\ClientException as ArangoClientException;
use ArangoDBClient\ServerException as ArangoServerException;
use ArangoDBClient\Statement as ArangoStatement;
use ArangoDBClient\UpdatePolicy as ArangoUpdatePolicy;

use App\Helpers\Profiler;

class ArangoDumpDesti extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'arango:dumpdesti {base_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Dump les destinataires dans une collection';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

      Profiler::start("DumpDestiArango");


      $connectionOptions = array(
        // database name
        ArangoConnectionOptions::OPTION_DATABASE      => 'tor',

        // dev prod
        // server endpoint to connect to
        ArangoConnectionOptions::OPTION_ENDPOINT      => 'tcp://tordev.sc2consulting.fr:8529',
        // dev local
        //- ArangoConnectionOptions::OPTION_ENDPOINT      => 'tcp://127.0.0.1:8529',
        // authorization type to use (currently supported: 'Basic')
        ArangoConnectionOptions::OPTION_AUTH_TYPE     => 'Basic',
        // user for basic authorization
        ArangoConnectionOptions::OPTION_AUTH_USER     => 'fabien',
        // password for basic authorization
        ArangoConnectionOptions::OPTION_AUTH_PASSWD   => 'fabien',
        // connection persistence on server. can use either 'Close' (one-time connections) or 'Keep-Alive' (re-used connections)
        ArangoConnectionOptions::OPTION_CONNECTION    => 'Keep-Alive',
        // connect timeout in seconds
        ArangoConnectionOptions::OPTION_TIMEOUT       => 3,
        // whether or not to reconnect when a keep-alive connection has timed out on server
        ArangoConnectionOptions::OPTION_RECONNECT     => true,
        // optionally create new collections when inserting documents
        ArangoConnectionOptions::OPTION_CREATE        => true,
        // optionally create new collections when inserting documents
        ArangoConnectionOptions::OPTION_UPDATE_POLICY => ArangoUpdatePolicy::LAST,
        );

        var_dump($this->argument('base_id'));

        $connection = new ArangoConnection($connectionOptions);
        $handler = new ArangoDocumentHandler($connection);

        // faire les différentes étapes pour découper l'import
        // param base id ?
        $nbrtotaldesti = \DB::table('destinataires')
            ->where('base_id', $this->argument('base_id'))
            ->where('statut', '<', 3)
            ->count();

        echo $nbrtotaldesti . "\n";
        $offset = 10000;
        $skip = 0;

        $long = strlen($nbrtotaldesti) - substr_count($offset,'0');
        $nbretape = substr($nbrtotaldesti,0,$long);
        var_dump($nbretape);
        if(!isset($nbretape) || $nbretape == ''){
          $nbretape = 1;
        }
        echo 'Nombre etape : ' . $nbretape . "\n";

        for ($i = 1; $i <= $nbretape; $i++) {
          echo "Etape ". $i . '/ '. $nbretape . "\n";

          $importdesti = \DB::table('destinataires')
            ->where('base_id', $this->argument('base_id'))
            ->where('statut', '<', 3)
            ->skip($skip)
            ->take($offset)
            ->get();

          foreach ($importdesti as $desti) {
            echo 'Import : ' . $desti->mail . "\n";
            $destinataire = new ArangoDocument();
            $destinataire->set('email', $desti->mail);
            $destinataire->set('ville', $desti->ville);
            $destinataire->set('departement', $desti->departement);
            $handler->save('destinataires', $destinataire);
            $destinataire = NULL;
          }

          $skip = $skip + $offset;
        }

        /*
        $importdesti = \DB::table('destinataires')->get();
        // var_dump($importdesti);
        foreach ($importdesti as $desti) {
          echo 'Import : ' . $desti->mail . "\n";
          $destinataire = new ArangoDocument();
          $destinataire->set('email', $desti->mail);
          $destinataire->set('ville', $desti->ville);
          $destinataire->set('departement', $desti->departement);
          $handler->save('destinataires', $destinataire);
          $destinataire = NULL;
        }
        */


        Profiler::report("DumpDestiArango");
    }

    protected function getArguments()
    {
        return [
            ['base_id', InputArgument::REQUIRED, 'base id'],
        ];
    }
}
