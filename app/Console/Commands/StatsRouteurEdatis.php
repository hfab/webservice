<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Classes\Routeurs\RouteurEdatis;
use App\Models\Routeur;

class StatsRouteurEdatis extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stats:majedatis';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command qui met a jour les stats Edatis';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      \Log::info("[Maj Stats Edatis] : Début");

      $routeur = new RouteurEdatis();
      $today = date('Ymd');
      $two_weeks_ago = date('Y-m-d 00:00:00', strtotime('-2 weeks'));
      $redatis = Routeur::where('nom','Edatis')->first();
      $senders = \DB::table('senders')->select('id')->where('routeur_id', $redatis->id)->get();

      $informations = \DB::table('campagnes_routeurs')
        ->where('created_at','>',$two_weeks_ago)
        ->whereIn('sender_id', array_pluck($senders, 'id'))
        ->get();

      $lastmaj = \DB::table('stats_edatis')->max('bloc_maj');

      if(!isset($lastmaj)){
          $lastmaj = 1;
      } else {
          $lastmaj = $lastmaj + 1;
      }

      foreach ($informations as $i) {

        $total = array();
        $ouvertures = array();
        $receveid = array();

        $nbrouvfree = 0;
        $nbrouvsfr = 0;
        $nbrouvorange = 0;
        $nbrouvlaposte = 0;
        $nbrouvclub = 0;
        $nbrouvautre = 0;

        $nbrtotalfree = 0;
        $nbrtotalsfr = 0;
        $nbrtotalorange = 0;
        $nbrtotallaposte = 0;
        $nbrtotalclub = 0;
        $nbrtotalautre = 0;

        $nbrreceveidfree = 0;
        $nbrreceveidsfr = 0;
        $nbrreceveidorange = 0;
        $nbrreceveidlaposte = 0;
        $nbrreceveidclub = 0;
        $nbrreceveidautre = 0;


        $infosender = \DB::table('senders')->select('id','password','nom')->where('id', $i->sender_id)->first();
        $summary = $routeur->get_summary_campagne($infosender->password, $i->cid_routeur);

        var_dump($summary);
        \Log::info("Ligne Campagne routeur : " . $i->id);

        if(!is_object($summary)){
          \Log::info("Passage dans le continue : " . $i->id);
          continue;
        }


        $get_openers_page = $routeur->get_openers_infos($infosender->password,$i->cid_routeur);

        if($get_openers_page->numberOfPages == 0){

          echo 'No Data ' . "\n";
          continue;

        } else {

          for ($it=1; $it <= $get_openers_page->numberOfPages; $it++) {
            $get_openers = $routeur->get_openers($infosender->password,$i->cid_routeur,$it);
            foreach ($get_openers->results as $info_open) {
              // var_dump($info_open->mail);

              if(strstr($info_open->mail,'@free.fr')
                || strstr($info_open->mail,'@freesbee.fr')
                || strstr($info_open->mail,'@libertysurf.fr')
                || strstr($info_open->mail,'@worldonline.fr')
                || strstr($info_open->mail,'@online.fr')
                || strstr($info_open->mail,'@alicepro.fr')
                || strstr($info_open->mail,'@aliceadsl.fr')){
                $ouvertures['free'][] = $info_open->mail;
                  continue;
                  //
              }

              if(strstr($info_open->mail,'@sfr.fr')
                || strstr($info_open->mail,'@neuf.fr')
                || strstr($info_open->mail,'@cegetel.fr')
                || strstr($info_open->mail,'@cegetel.net')
                || strstr($info_open->mail,'@numericable.fr')){
                $ouvertures['sfr'][] = $info_open->mail;
                  continue;
                  //
              }

              if(strstr($info_open->mail,'@laposte.net')){
                $ouvertures['laposte'][] = $info_open->mail;
                  continue;
                  //
              }

              if(strstr($info_open->mail,'@orange.fr')
                || strstr($info_open->mail,'@wanadoo.fr')
                || strstr($info_open->mail,'@voila.fr')){
                $ouvertures['orange'][] = $info_open->mail;
                  continue;
                  //
              }

              if(strstr($info_open->mail,'@club-internet.fr')){
                $ouvertures['club'][] = $info_open->mail;
                  continue;
                  //
              }
              $ouvertures['autre'][] = $info_open->mail;

            }
          }
        }

        if(isset($ouvertures['free'])){
          $ouvertures['free'] = array_unique($ouvertures['free']);
          $nbrouvfree = count($ouvertures['free']);
          var_dump($ouvertures['free']);
        }

        if(isset($ouvertures['sfr'])){
          $ouvertures['sfr'] = array_unique($ouvertures['sfr']);
          $nbrouvsfr = count($ouvertures['sfr']);
        }

        if(isset($ouvertures['laposte'])){
          $ouvertures['laposte'] = array_unique($ouvertures['laposte']);
          $nbrouvlaposte = count($ouvertures['laposte']);
        }

        if(isset($ouvertures['orange'])){
          $ouvertures['orange'] = array_unique($ouvertures['orange']);
          $nbrouvorange = count($ouvertures['orange']);
        }

        if(isset($ouvertures['club'])){
          $ouvertures['club'] = array_unique($ouvertures['club']);
          $nbrouvclub = count($ouvertures['club']);
        }

        if(isset($ouvertures['autre'])){
          $ouvertures['autre'] = array_unique($ouvertures['autre']);
          $nbrouvautre = count($ouvertures['autre']);
          // var_dump(count($ouvertures['autre']));
        }

        $get_sent = $routeur->get_sent_infos($infosender->password,$i->cid_routeur);
        if($get_sent->numberOfPages == 0){

          echo 'No Data ' . "\n";
          continue;

        } else {
          for ($is=1; $is <= $get_sent->numberOfPages; $is++) {
            var_dump('Page number : ' . $is);
            $sent = $routeur->get_sent($infosender->password,$i->cid_routeur,$is);
            foreach ($sent->results as $info_sent) {

              if(strstr($info_sent->mail,'@free.fr')
                || strstr($info_sent->mail,'@freesbee.fr')
                || strstr($info_sent->mail,'@libertysurf.fr')
                || strstr($info_sent->mail,'@worldonline.fr')
                || strstr($info_sent->mail,'@online.fr')
                || strstr($info_sent->mail,'@alicepro.fr')
                || strstr($info_sent->mail,'@aliceadsl.fr')){
                $total['free'][] = $info_sent->mail;
                  continue;
                  //
              }

              if(strstr($info_sent->mail,'@sfr.fr')
                || strstr($info_sent->mail,'@neuf.fr')
                || strstr($info_sent->mail,'@cegetel.fr')
                || strstr($info_sent->mail,'@cegetel.net')
                || strstr($info_sent->mail,'@numericable.fr')){
                $total['sfr'][] = $info_sent->mail;
                  continue;
                  //
              }

              if(strstr($info_sent->mail,'@laposte.net')){
                $total['laposte'][] = $info_sent->mail;
                  continue;
                  //
              }

              if(strstr($info_sent->mail,'@orange.fr')
                || strstr($info_sent->mail,'@wanadoo.fr')
                || strstr($info_sent->mail,'@voila.fr')){
                $total['orange'][] = $info_sent->mail;
                  continue;
                  //
              }

              if(strstr($info_sent->mail,'@club-internet.fr')){
                $total['club'][] = $info_sent->mail;
                  continue;
                  //
              }

              $total['autre'][] = $info_sent->mail;
            }
          }
        }

        if(isset($total['free'])){
          $total['free'] = array_unique($total['free']);
          $nbrtotalfree = count($total['free']);
          // var_dump($ouvertures['free']);
        }

        if(isset($total['sfr'])){
          $total['sfr'] = array_unique($total['sfr']);
          $nbrtotalsfr = count($total['sfr']);
        }

        if(isset($total['laposte'])){
          $total['laposte'] = array_unique($total['laposte']);
          $nbrtotallaposte = count($total['laposte']);
        }

        if(isset($total['orange'])){
          $total['orange'] = array_unique($total['orange']);
          $nbrtotalorange = count($total['orange']);
        }

        if(isset($total['club'])){
          $total['club'] = array_unique($total['club']);
          $nbrtotalclub = count($total['club']);
        }

        if(isset($total['autre'])){
          $total['autre'] = array_unique($total['autre']);
          $nbrtotalautre = count($total['autre']);
          // var_dump(count($ouvertures['autre']));
        }


        $get_delivrability = $routeur->get_delivrability_infos($infosender->password,$i->cid_routeur);

        if($get_delivrability->numberOfPages == 0){

          echo 'No Data ' . "\n";
          continue;

        } else {
          for ($id=1; $id <= $get_delivrability->numberOfPages; $id++) {
            var_dump('Page number : ' . $id);
            $delivrabilty = $routeur->get_delivrability($infosender->password,$i->cid_routeur,$id);
            foreach ($delivrabilty->results as $info_delivrability) {

              if(strstr($info_delivrability->mail,'@free.fr')
                || strstr($info_delivrability->mail,'@freesbee.fr')
                || strstr($info_delivrability->mail,'@libertysurf.fr')
                || strstr($info_delivrability->mail,'@worldonline.fr')
                || strstr($info_delivrability->mail,'@online.fr')
                || strstr($info_delivrability->mail,'@alicepro.fr')
                || strstr($info_delivrability->mail,'@aliceadsl.fr')){
                $receveid['free'][] = $info_delivrability->mail;
                  continue;
                  //
              }

              if(strstr($info_delivrability->mail,'@sfr.fr')
                || strstr($info_delivrability->mail,'@neuf.fr')
                || strstr($info_delivrability->mail,'@cegetel.fr')
                || strstr($info_delivrability->mail,'@cegetel.net')
                || strstr($info_delivrability->mail,'@numericable.fr')){
                $receveid['sfr'][] = $info_delivrability->mail;
                  continue;
                  //
              }

              if(strstr($info_delivrability->mail,'@laposte.net')){
                $receveid['laposte'][] = $info_delivrability->mail;
                  continue;
                  //
              }

              if(strstr($info_delivrability->mail,'@orange.fr')
                || strstr($info_delivrability->mail,'@wanadoo.fr')
                || strstr($info_delivrability->mail,'@voila.fr')){
                $receveid['orange'][] = $info_delivrability->mail;
                  continue;
                  //
              }

              if(strstr($info_delivrability->mail,'@club-internet.fr')){
                $receveid['club'][] = $info_delivrability->mail;
                  continue;
                  //
              }

              $receveid['autre'][] = $info_delivrability->mail;


            }
          }
        }

        if(isset($receveid['free'])){
          $receveid['free'] = array_unique($receveid['free']);
          $nbrreceveidfree = count($receveid['free']);
          // var_dump($ouvertures['free']);
        }

        if(isset($receveid['sfr'])){
          $receveid['sfr'] = array_unique($receveid['sfr']);
          $nbrreceveidsfr = count($receveid['sfr']);
        }

        if(isset($receveid['laposte'])){
          $receveid['laposte'] = array_unique($receveid['laposte']);
          $nbrreceveidlaposte = count($receveid['laposte']);
        }

        if(isset($receveid['orange'])){
          $receveid['orange'] = array_unique($receveid['orange']);
          $nbrreceveidorange = count($receveid['orange']);
        }

        if(isset($receveid['club'])){
          $receveid['club'] = array_unique($receveid['club']);
          $nbrreceveidclub = count($receveid['club']);
        }

        if(isset($receveid['autre'])){
          $receveid['autre'] = array_unique($receveid['autre']);
          $nbrreceveidautre = count($receveid['autre']);
          // var_dump(count($ouvertures['autre']));
        }



        echo ('-----------' . "\n");

        $lacampagne = \DB::table('campagnes')->where('id', $i->campagne_id)->first();

        // $per_cent_free = $nbbfree / $totalfree * 100;

        // var_dump($nbrreceveidfree); != 0
        // var_dump($nbrtotalfree);

        if($nbrreceveidfree != 0 && $nbrtotalfree != 0){
          $p_delivrability_free = round($nbrreceveidfree / $nbrtotalfree * 100, 1);
          if($nbrouvfree != 0){
            $p_ouv_free = round($nbrouvfree / $nbrtotalfree * 100, 2);
          } else {
            $p_ouv_free = 0;
          }
        } else {
          $p_delivrability_free = 0;
        }

        if($nbrreceveidsfr != 0 && $nbrtotalsfr != 0){
          $p_delivrability_sfr = round($nbrreceveidsfr / $nbrtotalsfr * 100, 1);
          if($nbrouvsfr != 0){
            $p_ouv_sfr = round($nbrouvsfr / $nbrtotalsfr * 100, 2);
          } else {
            $p_ouv_sfr = 0;
          }
        } else {
          $p_delivrability_sfr = 0;
        }

        if($nbrreceveidlaposte != 0 && $nbrtotallaposte != 0){
          $p_delivrability_laposte = round($nbrreceveidlaposte / $nbrtotallaposte * 100, 1);
          if($nbrouvlaposte != 0){
            $p_ouv_laposte= round($nbrouvlaposte / $nbrtotallaposte * 100, 2);
          } else {
            $p_ouv_laposte = 0;
          }
        } else {
          $p_delivrability_laposte = 0;
        }

        if($nbrreceveidorange != 0 && $nbrtotalorange != 0){
          $p_delivrability_orange = round($nbrreceveidorange / $nbrtotalorange * 100, 1);
          if($nbrouvorange != 0){
            $p_ouv_orange = round($nbrouvorange / $nbrtotalorange * 100, 2);
          } else {
            $p_ouv_orange = 0;
          }
        } else {
          $p_delivrability_orange = 0;
        }

        if($nbrreceveidclub != 0 && $nbrtotalclub != 0){
          $p_delivrability_club = round($nbrreceveidclub / $nbrtotalclub * 100, 1);
          if($nbrouvclub != 0){
            $p_ouv_club = round($nbrouvclub / $nbrtotalclub * 100, 2);
          } else {
            $p_ouv_club = 0;
          }
        } else {
          $p_delivrability_club = 0;
        }

        if($nbrreceveidautre != 0 && $nbrtotalautre != 0){
          $p_delivrability_autre = round($nbrreceveidautre / $nbrtotalautre * 100, 1);
          if($nbrouvautre != 0){
            $p_ouv_autre = round($nbrouvautre / $nbrtotalautre * 100, 2);
          } else {
            $p_ouv_autre = 0;
          }
        } else {
          $p_delivrability_autre = 0;
        }

        \DB::table('stats_edatis')->insert([

            'bloc_maj' => $lastmaj,
            'reference' => $lacampagne->ref,
            'sender_id' => $infosender->id,
            'nb_sent' => $summary->sent,
            'nb_received' => $summary->received,
            'nb_open' => $summary->opens,
            'nb_clicks' => $summary->clicks,
            'nb_unsubscribe' => $summary->unsubscribed,

            'nb_soft_bounce' => $summary->softBounce,
            'nb_hard_bounce' => $summary->hardBounce,

            'total_free' => $nbrtotalfree,
            'total_sfr' => $nbrtotalsfr,
            'total_laposte' => $nbrtotallaposte,
            'total_orange' => $nbrtotalorange,
            'total_club_internet' => $nbrtotalclub,
            'total_autre' => $nbrtotalautre,

            'receveid_free' => $nbrreceveidfree,
            'receveid_sfr' => $nbrreceveidsfr,
            'receveid_laposte' => $nbrreceveidlaposte,
            'receveid_orange' => $nbrreceveidorange,
            'receveid_club_internet' => $nbrreceveidclub,
            'receveid_autre' => $nbrreceveidautre,

            'ouv_free' => $nbrouvfree,
            'ouv_sfr' => $nbrouvsfr,
            'ouv_laposte' => $nbrouvlaposte,
            'ouv_orange' => $nbrouvorange,
            'ouv_club_internet' => $nbrouvclub,
            'ouv_autre' => $nbrouvautre,

            'p_receveid_free' => $p_delivrability_free,
            'p_receveid_sfr' => $p_delivrability_sfr,
            'p_receveid_laposte' => $p_delivrability_laposte,
            'p_receveid_orange' => $p_delivrability_orange,
            'p_receveid_club_internet' => $p_delivrability_club,
            'p_receveid_autre' => $p_delivrability_autre,

            'p_ouv_free' => $p_ouv_free,
            'p_ouv_sfr' => $p_ouv_sfr,
            'p_ouv_laposte' => $p_ouv_laposte,
            'p_ouv_orange' => $p_ouv_orange,
            'p_ouv_club_internet' => $p_ouv_club,
            'p_ouv_autre' => $p_ouv_autre,

            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),

            'planning_id' => $i->planning_id

          ]);

          // calcul des pourcentages
          // $lastmaj

          // test pour ne pas surcharger
          sleep(5);


        }

        \Log::info("[Maj Stats Edatis] : Fin");
    }
}
