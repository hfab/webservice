<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Classes\Routeurs\RouteurMindbaz;
use App\Models\Routeur;

class ComputeClicsMindbaz extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clics:compute_mindbaz';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Récupère les cliqueurs de Mindbaz et lance job : insertion cliqueurs dans la DB > table > clics.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Log::info("[ComputeClicsMindbaz] : Début");

        $today = date('Ymd');
        $file = "clics_mb_$today.csv";

        $mindbaz = new RouteurMindbaz();

        $compteur = $mindbaz->get_clickers();

        \Log::info("[ComputeClicsMindbaz] : finished (Count : $compteur)");
        \Log::info("[ComputeClicsMindbaz] : launch of BaseImportClics command");
        \Artisan::call('base:import_clics', ['file' => $file]);
        \Log::info("[ComputeClicsMindbaz] : end of BaseImportClics command OK");

    }
}
