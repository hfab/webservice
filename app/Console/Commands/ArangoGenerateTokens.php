<?php

namespace App\Console\Commands;


use Illuminate\Console\Command;
// Cannot redeclare class ArangoDBClient\Autoloader
// $path = storage_path('arangodb-php/autoload.php');
// require $path;
use ArangoDBClient\Collection as ArangoCollection;
use ArangoDBClient\CollectionHandler as ArangoCollectionHandler;
use ArangoDBClient\Connection as ArangoConnection;
use ArangoDBClient\ConnectionOptions as ArangoConnectionOptions;
use ArangoDBClient\DocumentHandler as ArangoDocumentHandler;
use ArangoDBClient\Document as ArangoDocument;
use ArangoDBClient\Exception as ArangoException;
use ArangoDBClient\Export as ArangoExport;
use ArangoDBClient\ConnectException as ArangoConnectException;
use ArangoDBClient\ClientException as ArangoClientException;
use ArangoDBClient\ServerException as ArangoServerException;
use ArangoDBClient\Statement as ArangoStatement;
use ArangoDBClient\UpdatePolicy as ArangoUpdatePolicy;

use App\Helpers\Profiler;

class ArangoGenerateTokens extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'arango:generatetokens';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Genere les tokens pour une collection de desti';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

      // je selectionne collection et je la duplique dans tokens du jour
      $connectionOptions = array(
        // database name
        ArangoConnectionOptions::OPTION_DATABASE      => 'tor',

        // dev prod
        // server endpoint to connect to
        ArangoConnectionOptions::OPTION_ENDPOINT      => 'tcp://tordev.sc2consulting.fr:8529',
        // dev local
        // ArangoConnectionOptions::OPTION_ENDPOINT      => 'tcp://127.0.0.1:8529',
        // authorization type to use (currently supported: 'Basic')
        ArangoConnectionOptions::OPTION_AUTH_TYPE     => 'Basic',
        // user for basic authorization
        ArangoConnectionOptions::OPTION_AUTH_USER     => 'fabien',
        // password for basic authorization
        ArangoConnectionOptions::OPTION_AUTH_PASSWD   => 'fabien',
        // connection persistence on server. can use either 'Close' (one-time connections) or 'Keep-Alive' (re-used connections)
        ArangoConnectionOptions::OPTION_CONNECTION    => 'Keep-Alive',
        // connect timeout in seconds
        ArangoConnectionOptions::OPTION_TIMEOUT       => 3,
        // whether or not to reconnect when a keep-alive connection has timed out on server
        ArangoConnectionOptions::OPTION_RECONNECT     => true,
        // optionally create new collections when inserting documents
        ArangoConnectionOptions::OPTION_CREATE        => true,
        // optionally create new collections when inserting documents
        ArangoConnectionOptions::OPTION_UPDATE_POLICY => ArangoUpdatePolicy::LAST,
        );

        $connection = new ArangoConnection($connectionOptions);
        // $collection = new ArangoCollection($connection);
        $collectionHandler = new ArangoCollectionHandler($connection);
        $handler = new ArangoDocumentHandler($connection);
        // var_dump($collectionHandler);


        // récupération d'un document
        // $cursor = $collectionHandler->byExample('destinataires', ['ville' => 'San Lorenzo']);
        // $r = $cursor->getAll();
        // var_dump($r);

        // $userFromServer = $handler->get('destinataires', '105770');
        // $userFromServer->ville = 'Paris';
        // $userFromServer->tokenjour = 2;

        // $result = $handler->update($userFromServer);
        // var_dump($result);

        // $collectionHandler->get('destinataires');

        // var_dump($collectionHandler->getCollectionId('destinataires'));



        $documents = $collectionHandler->all('destinataires');
        // var_dump($documents['data']);
        // die();
        foreach ($documents as $value) {
          var_dump($value->email);
        }
    }
}
