<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\Campagne;
use App\Models\CampagneSegments;
use App\Models\Planning;
use App\Models\Sender;
use App\Models\Token;
use App\Classes\Routeurs\RouteurMaildrop;

class CampagneSegment extends Command
{
    protected $signature = 'campagne:segment {planning_id}';
    protected $description = 'Command description.';

    protected $num_tokens = 0;

    public function __construct()
    {
        parent::__construct();
        $this->routeur = new RouteurMaildrop();
    }

    public function handle()
    {
        $planning = Planning::find($this->argument('planning_id'));
        $campagne_id = $planning->campagne_id;

        $plannings = Planning::where('next_campaign', '!=', 0)
            ->where("date_campagne", date("Y-m-d"))
            ->where("sent_at", null)
            ->orderBy('id','desc')
            ->get();

        $planning = Planning::where('campagne_id', $campagne_id)
            ->orderBy('id','desc')
            ->first();

        $senders = Sender::where('md_list_id','!=','')
            ->get();

        $next_campaigns = [
            1 => "",
            2 => "2",
            3 => "3",
            4 => "4",
        ];


        $planning -> next_campaign = 1; // par défaut à 1

        if(count($plannings) == 4)
        {
            \Log::info("4 plannifications de campagnes en parallèles");
            exit;
        }
        $compare_nextc = array();
        if($plannings != null) {
            foreach($plannings as $p)
            {
                $compare_nextc[$p->next_campaign] = $p->next_campaign;
            }

            $diff_nextc  = array_diff_key($next_campaigns,$compare_nextc);
            if(count($diff_nextc) > 0) {
                $planning->next_campaign = min(array_keys($diff_nextc));
            }
        }

        $planning->save(); // on sauvegarde la valeur next_campaign pr savoir quel nxt_campaign utiliser pr la prochaine plannif

        \App\Helpers\Profiler::start();

        $custom_fields = ['next_campaign'.$next_campaigns[$planning->next_campaign] => $campagne_id];

        //TODO ajouter planning_id ?

        CampagneSegments::where('campagne_id', $campagne_id)->delete();

        foreach($senders as $sender) {
            $this->num_tokens = 0;

            echo "Sender : $sender->nom ($sender->id) \n";

            $tokens = Token::
                where('campagne_id', $campagne_id)
                ->where('planning_id', $planning->id)
                ->where('sender_id', $sender->id)
                ->chunk(5000, function($tokens) use ($sender, $custom_fields) {
                    // update custom_field
                    $bulk = [];
                    foreach($tokens as $token) {
                        $bulk[] = $token->destinataire->mail;
                        $this->num_tokens++;
                    }

                    echo "\t".$this->num_tokens."\n";
                     $ret = $this->routeur->populate_list($sender, $bulk, $custom_fields);
                });

            if ($this->num_tokens == 0) {
                continue;
            }

            $md_segment_id = $this->routeur->create_segment($sender, $campagne_id, 'next_campaign'.$next_campaigns[$planning->next_campaign]);
            if (is_array($md_segment_id) || is_object($md_segment_id)) {
                dd($md_segment_id);
            } else {
                $campagne_segment = CampagneSegments::create([
                    'campagne_id' => $campagne_id,
                    'planning_id' => $planning->id,
                    'md_segment_id' => $md_segment_id,
                    'sender_id' => $sender->id,
                    'md_list_id' => $sender->md_list_id
                ]);
                echo "\n";
            }
        }

        $planning->segments_at = date('Y-m-d H:i:s');
        $planning->save();

        \App\Helpers\Profiler::report('fin Segments');
    }
}
