<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ExtractOuvreursBddV2 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'extract:ouvreurs_bdd2 {nom_fichier} {base_id} {time_limit}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {


      $campagnes = \DB::table('campagnes')
          ->select('id')
          ->where('base_id', $this->argument('base_id'))
          ->where('created_at','>', $this->argument('time_limit').'00:00:00')
          ->get();


      foreach ($campagnes as $lacampagne) {
      echo 'la campagne : ' . $lacampagne->id . "\n";
      \DB::table('ouv_api')
          ->select('mail')
          ->where('created_at','>', $this->argument('time_limit').'00:00:00')
          ->where('campagne_id','=',$lacampagne->id)
          ->chunk(10000, function ($lesadresses) {
              var_dump('Bulk 10000');
              $array_a = array_pluck($lesadresses,'mail');
              /*
              $lc = \DB::table('campagnes')
              ->where('');

              $array_a = array_pluck($lesadresses,'destinataire_id');

              $result_actif = \DB::table('destinataires')
              ->select('mail')
              ->whereIn('mail', $array_a)
              ->where('statut','=',0)
              ->get();
              $chaine = '';

              */

              $chaine = '';
              foreach ($array_a as $key => $v) {
                $chaine .= $v."; \n";
              }

              $file = storage_path() . '/listes/download/'.$this->argument('nom_fichier').'.csv';
              $fp = fopen($file,"a+");
              fwrite($fp, $chaine);
              fclose($fp);
              unset($chaine);


          });
          }
    }
}
