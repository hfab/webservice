<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class TokensReinit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tokens:reinit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Réinitialise les tokens.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $today = date('Y-m-d');

        \DB::statement("update tokens set campagne_id = null, planning_id = 0 uploaded_at = '0000-00-00 00:00:00' where date_active = '$today' ");
        \DB::statement("update plannings set is_sent = NULL where date_campagne = '$today' ");

        \Artisan::call('tokens:stats');
    }
}
