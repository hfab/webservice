<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
// ajout du routeur
use App\Classes\Routeurs\RouteurSmessage;
use App\Models\Sender;
use App\Models\Campagne;
use App\Models\Planning;

class CampagneSendSmessage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campagne:send_smessage {planning_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envoi de campagne Smessage';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //TODO : uncomment when it is OK
        $auto_setting = \DB::table('settings')
          ->where('parameter', 'is_auto')
          ->first();
        if(!$auto_setting or $auto_setting->value != 1){
          \Log::info('CampagneLaunchSend : AUTOMATIC CAMPAIGN NOT ACTIVE');
          return 0;
        }
        $routeur_setting = \DB::table('settings')
          ->where('parameter', 'is_smessage')
          ->first();

        if(!$routeur_setting or $routeur_setting->value != 1){
            \Log::info('CampagneLaunchSend : SMESSAGE DISABLE');
            return 0;
          }

        $routeur = new RouteurSmessage();
        $plannif = Planning::find($this->argument('planning_id'));
        $campagneid = $plannif->campagne_id;

        $today = \Carbon\Carbon::today();
        $ajd = date('Y-m-d');
        // avec l'id de la campagne je regarde les senders qui l'on
        $senderenvoi = \DB::table('campagnes_routeurs')
            ->where('campagne_id', $campagneid)
            ->where('planning_id', $plannif->id)
            ->where('created_at','>', $today)
            ->get();

        $campagne = Campagne::find($campagneid);

        // pour chaque senders on vérifie si l'import s'est bien passé, sinon on ne lance pas le process d'envois
        foreach ($senderenvoi as $lesender)
        {
            $infosender = Sender::find($lesender->sender_id);

            if(!$routeur->checkStatutImport($infosender,$lesender->cid_routeur)) {
                \Log::info("Import non terminé sur $infosender->id ($infosender->nom)");
                die();
            }
        }

        $plannif->sent_at = \Carbon\Carbon::now();
        $plannif->save();

        //on fait partir les envois
        foreach ($senderenvoi as $lesender)
        {
            $infosender = Sender::find($lesender->sender_id);
            $routeur->send_campagne($infosender, $lesender->cid_routeur);
            \Log::info("Statut Import OK --> Send $infosender->id ($infosender->nom) OK");

            // recuperer sender nom via id
            $senderfichier = \DB::table('senders')->where('id', $lesender->sender_id)->first();

            // on supprime le fichier si existe
            if(file_exists(storage_path() . '/smessage/' . $senderfichier->nom . '_c' . $lesender->campagne_id . '.csv')){
                unlink(storage_path() . '/smessage/' . $senderfichier->nom . '_c' . $lesender->campagne_id . '.csv');
            }

            // test pour les logs
            if(file_exists(storage_path() . '/smessage/' . $senderfichier->nom . '_c' . $lesender->id . '.csv')){
                \Log::info('Le fichier existe toujours : ' . storage_path() . '/smessage/' . $senderfichier->nom . '_c' . $lesender->campagne_id . '.csv');
            } else {
                \Log::info('Le fichier est bien supprimé : ' . storage_path() . '/smessage/' . $senderfichier->nom . '_c' . $lesender->campagne_id . '.csv');
            }
        }

        $anonyme = "";
        // if ici et on creé la var
        if($plannif->type == 'anonyme'){
            $anonyme = "_a";
        }

        exec("rm ~/".getenv('CLIENT_URL')."/storage/smessage/*p$plannif->id$anonyme*");

        $senders = \DB::select("select nom from senders where id in (select sender_id from campagnes_routeurs where planning_id = :planning_id)", ['planning_id' => $plannif->id]);

        $used_senders = 'Compte(s) utilisé(s) : <br/>';

        foreach($senders as $s) {
            $used_senders .= "$s->nom <br/>";
        }

        // on envoi le mail
        $email_volume_demande = 'Volume demandé : ' . $plannif->volume;
        $email_volume_orange = 'Volume orange demandé : ' . $plannif->volume_orange;
        $email_volume_selected = 'Volume réel utilisé : ' . $plannif->volume_selected;
        $email_date = 'Campagne programmée le ' . $plannif->date_campagne . ' ' . $plannif->date_campagne . ' / Envoi de la campagne terminé à ' . $plannif->sent_at;

        \Mail::send('mail.mailinfocampagne',
            [
                'email_volume_demande' => $email_volume_demande,
                'email_volume_orange' => $email_volume_orange,
                'email_volume_selected' => $email_volume_selected,
                'email_date' => $email_date,
                'url' => getenv('CLIENT_TITLE'),
                'used_senders' => $used_senders,
            ], function ($message) use ($campagne) {
                $destinataires = \App\Models\User::where('is_valid', 1)
                    ->where('user_group_id', 1)
                    ->where('email', '!=', "")
                    ->get();
                $message->from('rapport@lead-factory.net', 'Tor')->subject("Rapport SM : envoi de $campagne->ref [$campagne->id] terminé au " . date('d-m-Y H:i'));
                foreach($destinataires as $d){
                    $message->to($d->email);
                }
                return "true";
                // $message->to('fabien@lead-factory.net');
                // $message->to('adeline.sc2@gmail.com');
            });

    }

    protected function getArguments()
    {
        return [
            ['planning_id', InputArgument::REQUIRED, 'Planning id.'],
        ];
    }
}
