<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Classes\Routeurs\RouteurMailForYou;
use App\Models\Routeur;

class CampagneBouncesMailForYou extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campagne:bounces_mailforyou';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ajoute les bounces m4y dans la bdd all bases';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $routeur = new RouteurMailForYou();

      $today = date('Ymd');
      $two_weeks_ago = date('Y-m-d 00:00:00', strtotime('-2 weeks'));
      $mailforyou = Routeur::where('nom','MailForYou')->first();
      $senders = \DB::table('senders')->select('id')->where('routeur_id', $mailforyou->id)->get();
      // taskid à 1 si des soucis 
      $informations = \DB::table('campagnes_routeurs')
          ->where('created_at','>',$two_weeks_ago)
          ->whereIn('sender_id', array_pluck($senders, 'id'))
          ->get();

      $e = "";
      foreach ($informations as $lignecampagnerouteur){
        $data = $routeur->LectureStatsRoutage($lignecampagnerouteur->cid_routeur);

        if(isset($data)){
          foreach ($data->records as $v) {

            if(isset($v['2'])){
              if($v['2'] != 0){
              // var_dump($v);
              // var_dump($v['2']);

                if($v['2'] == 2){
                  $e .= $v['1'] . "\n";
                }

                if($v['2'] == 3){
                  $e .= $v['1'] . "\n";
                }

                if($v['2'] == 4){
                  $e .= $v['1'] . "\n";
                }

                if($v['2'] == 10){
                  $e .= $v['1'] . "\n";
                }

                if($v['2'] == 12){
                  $e .= $v['1'] . "\n";
                }

              //
              }
            }
          }
        }

      }

      echo "create file \n";
      $file = storage_path() . '/bounces/bounces_m4y_' . $today . '.csv';
      // var_dump($file);
      $fp = fopen($file,"w+");
      fwrite($fp,$e);
      fclose($fp);
        echo "Lancement fichier \n";


        \Log::info("Creation du fichier bounces M4Y" . "[bounces_m4y_" . $today . ".csv]");
        \Artisan::call('base:import_bounces', ['file' => "bounces_m4y_" . $today . ".csv"]);
        \Log::info("[import_bounces] du fichier bounces M4Y" . "[bounces_m4y_" . $today . ".csv]");
      // var_dump($e);

    }
}
