<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Planning;

class get_optout_and_bounces extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:out';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();


    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $one_week_ago = date('Y-m-d H:i:s', strtotime('-1 week'));

        $plannings = Planning::where('sent_at','>', $one_week_ago)
            ->orderBy('sent_at', 'desc')
            ->select('id','campagne_id')
            ->groupBy('campagne_id')
            ->get();

        foreach($plannings as $planning) {
            echo "PlanningID ".$planning->id."\n";
            echo "CampagneID ".$planning->campagne_id."\n";
            \Log::info('GetOptoutAndBounces : Planning '.$planning->id .'-- Campagne '.$planning->campagne_id);
            
            \Artisan::call('campagne:bounces', ['planning_id' => $planning->id]);
            
            echo "\n-----------------------------------------------\n";
        }
    }
}
