<?php namespace App\Console\Commands;

use App\Classes\Routeurs\RouteurMaildrop;
use App\Models\Repoussoir;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Carbon\Carbon;
use App\Models\Campagne;
use App\Models\CampagneListe;
use App\Models\Destinataire;
//use App\Models\Fai;
use App\Models\Base;
use App\Models\Sender;
use App\Models\Token;

class CampagneAddRepoussoirs extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'campagne:add_repoussoirs';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Ajoute des @ en repoussoirs pour une campagne donnée.';

	/**
	 * Create a new command instance.ha voilà
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $campagne_id = $this->argument('campagne_id');
        $file = storage_path().'/repoussoirs/'.$this->argument('file');
        $ts = date('Y-m-d H:i:s');
        $bulk_count = 0;
        if (!is_file($file)) {
            \Log::error('Repoussoirs : File not found : '.$file);
            $this->error('File not found : '.$file);
            exit;
        }

        \Log::info('Insert in repoussoir pour la campagne '.$campagne_id.' -- Fichier '.$file);

        \App\Helpers\Profiler::start('import');

        \DB::disableQueryLog();

        $pdo = \DB::connection()->getPdo();
        $sql = "INSERT IGNORE INTO repoussoirs (destinataire_id, campagne_id, created_at, updated_at) VALUES (:destinataire, :campagne, :ts1, :ts2)";
        $stmt = $pdo->prepare($sql);
        $pdo->beginTransaction();

        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $today = Carbon::today();

        $nochecks = $this->option('no-checks');

        $campagne = Campagne::find($campagne_id);

        $old = Repoussoir::where('campagne_id', $campagne_id)->delete();

        $extension = \File::extension($file);
        $name = \File::name($file);

        $fh = fopen($file, 'r');

        $total = 0;
        $inserted = 0;
        $rejected = 0;
        $not_in_base = 0;

        $baseid = $campagne->base_id;
        $bulk = [];
        while ($row = fgets($fh, 1024)) {
            $cells = [];
            $row = trim($row, "\r\n\t ");

            if (strpos($row, ';') !== false) {
                $cells = explode(';', $row);
            } elseif (strpos($row, '|') !== false) {
                $cells = explode('|', $row);
            } else {
                $cells[] = trim($row, " \r\n\t\"");
            }

            foreach($cells as $cell) {
                // if cell is an md5, insert it
                if (strlen($cell)!=32) {
                    $rejected++;
                    continue;
                }
                $total++;

                $dest_check = \DB::table('destinataires')
                ->select('id')
                ->where('hash', $cell)
                ->where('base_id', $baseid)
                ->first();
//                        var_dump($dest_check);
                if ($dest_check == null) {
                    $not_in_base++;
                    continue;
                }

                $insertData = [$dest_check->id,$campagne->id, $ts, $ts];
                $stmt->execute($insertData);

                $bulk_count++;
                if ($bulk_count >= 250) {
                    $pdo->commit();
                    $pdo->beginTransaction();
                    $bulk_count = 0;
                }

                $inserted++;
                continue;
            }
        }

        $pdo->commit();

        if (isset(\Auth::User()->id)) {
            $user_id = \Auth::User()->id;
        } else {
            $user_id = 1;
        }

        \App\Models\Notification::create([
            'user_id' => $user_id,
            'level' => 'info',
            'is_important' => true,
            'message' => "Fichier repoussoir $name associé à la campagne$campagne->id - $campagne->nom.
            $inserted insérés, $not_in_base non présents, $rejected rejetés."
        ]);

        \Log::info("Fichier repoussoir $name associé à la campagne$campagne->id - $campagne->nom.
    $inserted insérés, $not_in_base non présents, $rejected rejetés.");
//        \Event::fire(new \App\Events\DestinatairesImported($infos));

        \App\Helpers\Profiler::report();

	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			['campagne_id', InputArgument::REQUIRED, 'Base ID'],
			['file', InputArgument::REQUIRED, 'Path to the file'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['no-checks', null, InputOption::VALUE_OPTIONAL, 'Inserts without checking if already present.', null],
		];
	}

}
