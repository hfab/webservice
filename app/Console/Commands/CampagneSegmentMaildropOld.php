<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use App\Classes\Routeurs\RouteurMaildrop;

use App\Models\Planning;
use App\Models\Campagne;
use App\Models\Routeur;
use App\Models\Fai;
use App\Models\Sender;
use App\Models\CampagneRouteur;

class CampagneSegmentMaildropOld extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campagne:segment_maildrop_old {planning_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Segment et import pour maildrop facon Smessage';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->routeur = new RouteurMaildrop();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $planning = Planning::find($this->argument('planning_id'));
        $lacampagne = Campagne::where('id',$planning->campagne_id)->first();
        $maildrop = Routeur::where('nom','Maildrop')->first();

        \Log::info("[RouteurSegmentMaildrop][P$planning->id] : Début Segmentation (Planning $planning->id/Campagne $lacampagne->id)");

        $fais = Fai::all();
        $mailsrestants = array();
        $selectedsender = array();

        $anonyme = "";

        if($planning->type == 'anonyme'){
            $anonyme = "_a";
        }

        //supprimer les fichiers de destinataires pour eviter d'avoir des fichiers lourds dans le cas où le script se répète
        exec("rm ".storage_path()."/maildrop/*p$planning->id$anonyme*");

        foreach($fais as $f)
        {
            $lesadressestokens = array();
            $lesadressestokens = \DB::table('tokens')
                ->select('destinataire_id')
                ->where('campagne_id',$lacampagne->id)
                ->where('fai_id',$f->id)
                ->where('date_active',$planning->date_campagne)
                ->where('planning_id',$planning->id)
                ->get();

            $maildropsender = Sender::where('quota_left','>',0)->where('routeur_id',$maildrop->id)->get();

            $fai_senders = \DB::table('fai_sender')
                ->where('fai_id',$f->id)
                ->whereIn('sender_id',$maildropsender->pluck('id')->all())
                ->orderByRaw('rand()')
                ->get();

            if(!$fai_senders){
                continue;
            }

            $calcul = count($lesadressestokens) / count($fai_senders);
            $count = round($calcul)+1;

            if($f->quota_sender > 0) {
                $count = min($f->quota_sender,$count);
            }

            $chunks=array();
            $chunks = array_chunk($lesadressestokens,$count);
            $indSender = 0;
            \Log::info("[RouteurSegmentMaildrop][P$planning->id] : FAI-$f->nom-@- ".count($lesadressestokens)." -Sender-".count($fai_senders)." -DIV-".$count." -COUNTCHUNK-".count($chunks));

            foreach($fai_senders as $fai_sender)
            {
                $sender = Sender::find($fai_sender->sender_id);
                if(!isset($chunks[$indSender])) {
                    break;
                }
                //si le sender n'a pas assez de places / jour pour envoyer on prends un autre sender
                //ou sinon on peut
                if($sender->quota_left < count($chunks[$indSender])) {
                    //                    array_merge($mailsrestants,$chunks[$indSender]);
                    continue;
                }

                if($fai_sender->quota_left < 0) {
                    continue;
                }

                if($fai_sender->quota_left > 0 and $fai_sender->quota_left < count($chunks[$indSender])){
                    continue;
                }

                // on check si le fichier existe
                // if(file_exists())
                $file = storage_path() . '/maildrop/s' . $sender->id . '_c' . $lacampagne->id .'_p'.$planning->id. $anonyme.'.csv';

                $this->write_tokens($file, $chunks[$indSender]);
                $sender->quota_left -= count($chunks[$indSender]);

                \DB::table('fai_sender')
                    ->where('fai_id',$f->id)
                    ->where('sender_id',$sender->id)
                    ->update(['quota_left' => $fai_sender->quota_left - count($chunks[$indSender])]);

                $sender->save();
                $selectedsender[$sender->id] = $sender;
                $indSender++;
            }
        }

        \Log::info("[RouteurSegmentMaildrop][P$planning->id] : Début Partie Import via API");
        foreach($selectedsender as $sid => $sender)
        {
            $listid = $this->routeur->create_list($sender);
            $created = $this->routeur->add_custom_field($sender,$listid,'tor_id');// on créer le champ tor_id dans la liste pour le pixel
            if(!$created){
                \Log::error("Segments stopped for Campagne $planning->campagne_id / Planning $planning->id : tor_id not created ");
                exit;
            }

            // il faut remplir la liste
            $laliste = array($listid);

            $url = 'http://' . getenv('CLIENT_URL') . '/mailexport/maildrop/s'. $sender->id . '_c' . $lacampagne->id .'_p'.$planning->id. $anonyme . '.csv';
            $taskid = $this->routeur->import_by_url($sender->password, $listid, $url);
//            echo "Creation de la campagne sur " . $sender->nom . ' avec comme from : ' . $sender->domaine . "\n";
            $cid = $this->routeur->createCampagne($lacampagne, $sender, $laliste, $planning );
            \Log::info("[RouteurSegmentMaildrop][P$planning->id] before UPDATE -- cid $cid");
            \DB::statement("UPDATE campagnes_routeurs SET listid ='$listid', taskid ='$taskid' WHERE cid_routeur = '$cid'");
            \Log::info("[RouteurSegmentMaildrop][P$planning->id] after UPDATE -- cid $cid");
        }
        \Log::info("[RouteurSegmentMaildrop][P$planning->id] : Fin Partie Import via API");
        $planning->segments_at = date('Y-m-d H:i:s');
        $planning->save();

        \Log::info("[RouteurSegmentMaildrop][P$planning->id] : Fin Segmentation (Planning $planning->id/Campagne $lacampagne->id)");
    }

    public function write_tokens($file, $tokens)
    {
        if(!file_exists($file)){
            $fp = fopen($file,"w");
            $content = "email;first_name;last_name;tor_id" . "\n";
            fwrite($fp,$content);
            fclose($fp);
        }

        $content = "";
        $fp = fopen($file,"a+");
        foreach($tokens as $desti)
        {
            $destinataire = \DB::table('destinataires')
                ->select('id','mail')
                ->where('id', $desti->destinataire_id)
                ->first();

            $content = $content . $destinataire->mail . ";;;" .$destinataire->id. "\n";
        }
        fwrite($fp,$content);
        fclose($fp);

    }

    protected function getArguments()
    {
        return [
            ['planning_id', InputArgument::REQUIRED, 'Planning id.'],
        ];
    }
}