<?php namespace App\Console\Commands;

use App\Models\Destinataire;
use Illuminate\Console\Command;
use App\Models\Sender;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class SenderResetQuota extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'senders:reset_quota';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Remet les quota senders à zéro';

	/**
	 * Create a new command instance.ha voilà
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        \DB::statement("update senders set quota_left = quota, nb_campagnes_left = nb_campagnes" );
		\DB::statement("UPDATE fai_sender SET quota_left = quota");
	}
}
