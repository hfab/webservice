<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use App\Classes\Routeurs\RouteurMaildrop;

use App\Models\Routeur;

class OuvMDv2 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ouv:md';

    /**
     * The console command description.
     *
     * @var string
     */

    protected $description = 'Nouvelle manière de récupérer les ouv MD / V2';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $routeur = new RouteurMaildrop();
        $ts = date('Y-m-d H:i:s');

        $campagne_ouverture = \DB::table('campagnes_routeurs')
        // ->where('created_at','>', $this->argument('time_limit').'00:00:00')
        ->where('created_at','>', date('Y-m-d H:i:s',strtotime('-30 days')))
        ->get();

        $maildrop = Routeur::where('nom','Maildrop')->first();

        foreach ($campagne_ouverture as $fcam_ouv) {

          $campagne_info = \DB::table('campagnes')
          ->where('id', $fcam_ouv->campagne_id)
          ->first();

          var_dump($fcam_ouv->campagne_id);
          $sender_info = \DB::table('senders')
          ->where('id', $fcam_ouv->sender_id)
          ->where('routeur_id', $maildrop->id)
          ->first();

          if($sender_info == NULL){
            continue;
          }

          // demande md cid / sender correspondant
          $ouv = $routeur->recup_ouverture($sender_info->password,$fcam_ouv->cid_routeur);
          var_dump('La campagne : ' . $campagne_info->ref);
          var_dump($ouv->total);
          $nb_ouvreurs = $ouv->total;
          $offset = 100;
          $long = strlen($nb_ouvreurs) - substr_count($offset,'0');
          $nbretape = substr($nb_ouvreurs,0,$long);

          for ($i = 1; $i <= $nbretape; $i++) {

            echo 'PAGE ' . $i . ' / ' . $nbretape . "\n";

            $ouvpage = $routeur->recup_ouverture_page($sender_info->password,$fcam_ouv->cid_routeur, $i);

            $pdo = \DB::connection()->getPdo();
            $sql = "INSERT IGNORE INTO ouv_api (mail, sender_id, campagne_id, theme_id, created_at, updated_at) VALUES (:mail, :sender_id, :campagne_id, :theme_id, :ts1, :ts2)";
            $stmt = $pdo->prepare($sql);
            $pdo->beginTransaction();

            var_dump($ouvpage->total);

            foreach ($ouvpage->data as $value) {
              // var_dump($value->email);

              $insertData = [$value->email, $fcam_ouv->sender_id, $fcam_ouv->campagne_id, $campagne_info->theme_id, $ts, $ts];
              $stmt->execute($insertData);

            }

            $pdo->commit();


          }


          $last = $nbretape + 1;
          $ouv3 = $routeur->recup_ouverture_page($sender_info->password,$fcam_ouv->cid_routeur, $last);

          $pdo = \DB::connection()->getPdo();
          $sql = "INSERT IGNORE INTO ouv_api (mail, sender_id, campagne_id, theme_id, created_at, updated_at) VALUES (:mail, :sender_id, :campagne_id, :theme_id, :ts1, :ts2)";
          $stmt = $pdo->prepare($sql);
          $pdo->beginTransaction();

          // var_dump($ouv->total);

          foreach ($ouv3->data as $value) {
            // var_dump($value);

            $insertData = [$value->email, $fcam_ouv->sender_id, $fcam_ouv->campagne_id, $campagne_info->theme_id, $ts, $ts];
            $stmt->execute($insertData);

          }

          $pdo->commit();
          var_dump('FIN pour : ' . $campagne_info->ref);

          // faire le page nbr etape +1



          // faire une fct data qui get les diff pages selon le nb de ouv





          // var_dump($ouv);


        }

        // table ouv2
        // bien mettre le theme id dans la new table
    }

    /*
    protected function getArguments()
    {
        return [
            ['time_limit', InputArgument::REQUIRED, 'Limite de temps'],
        ];
    }
    */
}
