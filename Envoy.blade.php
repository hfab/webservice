@servers(['web' => 'forge@tor.sc2consulting.fr'])

@task('deploy')
    cd /path/to/site
    git pull origin master
@endtask

@task('get-dump')
    mysqldump -a [database] -u[user] -p | gzip > [file].sql.gz

@endtask