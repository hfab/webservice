@extends('template')

@section('content')


<!-- jQuery UI -->
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

<!-- Input -->
    <script>
        $(document).ready(function(){
            {!! FormAutocomplete::selector('#recherche')->db_ca() !!}
        });
    </script>

    <script>
        $(document).ready(function(){
            {!! FormAutocomplete::selector('#ajouter')->db_ajout_ca() !!}
        });
    </script>
    <?php
    // init des total

    $totalcabrut = 0;
    $totalcanet = 0;
    $totalcout = 0;
    $totalvolume = 0;
    $totalmarge = 0;
    ?>

      <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">

                  <div class="x_title">
                    <h2>Chiffre d'affaire
                      <small>
                        <a href="/ca" class="btn btn-danger">Acceuil CA</a> <a href="/ca/mois/{{$moisdelacampagne['month']}}" class="btn btn-success">CA par Base</a>
                      </small>

                      <hr>

                      @foreach($lesbases as $base)

                      <a href="/ca/tri/base/{{$base->id}}" class="btn btn-primary btn-xs">{{$base->nom}}</a>

                      @endforeach

                      <hr>

                      @foreach($lesplatesformes as $plateformes)

                      <a href="/ca/tri/plateforme/{{$plateformes->id}}" class="btn btn-primary btn-xs">{{$plateformes->nom_plateforme}}</a>

                      @endforeach

                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <p>Dernière mise à jour calcul : {{$maj_info}}</p>
                    </ul>
                    <div class="clearfix"></div>
                  </div>


                <div class="x_content">
                  <br>
                  <div class="row">
                  <div class="col-md-3">

                    Recherche :
                    {!! Form::open(array('url' => 'ca/tri/search','files' => true, 'method' => 'post')) !!}
                    {!! Form::label('recherche', ' ') !!}
                    {!! Form::text('recherche', '') !!}
                    <input type="submit" value="Rechercher" class="btn btn-xs btn-success">
                    {!! Form::close() !!}
                    </div>
                    <div class="col-xs-3">
                    Forcer une campagne :
                    {!! Form::open(array('url' => 'ca/force','files' => true, 'method' => 'post')) !!}
                    {!! Form::label('ajouter', ' ') !!}
                    {!! Form::text('ajouter', '') !!}
                    <input type="submit" value="Ajouter" class="btn btn-xs btn-warning">
                    {!! Form::close() !!}

                    <hr>
                  </div>
                  </div>
                  <div class="row">
                    <div style="overflow-x:auto;">
                      <table class="table table-striped table-hover table-bordered">

                          <tr>
                            <th>Campagne</th>
                            <th>Base</th>
                            <th>Plateforme</th>
                            <th>CA Brut</th>
                            <th>CA Net</th>
                            <th>Volume Total</th>
                            <th>Cout routage</th>
                            <th>Marge</th>
                            <th>eCPM</th>
                            <th>Commentaire</th>
                            <th>Appel facture</th>

                          </tr>
                          @foreach($ca_concat as $c_r)
                          <tr>
                              <td class="col-xs-1">{{$c_r->nom}}</td>
                               <td class="col-xs-1">
                                        <?php

                                              // ne pas afficher si il trouve pas car la base n'existe plus
                                             $labaseid = \DB::table('bases')
                                             ->select('nom')
                                             ->where('id',$c_r->base_id)
                                             ->first();

                                             echo ($labaseid->nom);
                                         ?>
                               </td>

                               <td class="col-xs-1">
                                        <?php
                                             $laplateforme = \DB::table('plateformes_affi')
                                             ->select('nom_plateforme')
                                             ->where('id',$c_r->plateforme_id)
                                             ->first();

                                             echo ($laplateforme->nom_plateforme);
                                         ?>
                               </td>

                              <td class="col-xs-2"><input id="{{$c_r->id}}" type="number" step="0.01" name="ca_brut" value="{{$c_r->ca_brut}}" mois="{{$moisdelacampagne['month']}}"> €</td>
                              <td class="col-xs-2"><input id="{{$c_r->id}}" type="number" step="0.01" name="ca_net" value="{{$c_r->ca_net}}" mois="{{$moisdelacampagne['month']}}"> €</td>
                              <td class="col-xs-1">{{$c_r->ca_volume_total}}</td>
                              <td class="col-xs-1">{{$c_r->cout_routage}} €</td>
                              <td class="col-xs-1">
                                <?php
                                  $gains = $c_r->ca_net - $c_r->cout_routage;

                                  if($gains < 0){
                                    echo '<span class="text-danger">'.$gains.' €</span>';
                                  } else {
                                    echo '<span class="text-success">'.$gains.' €</span>';
                                  }
                                ?>
                              </td>

                              <td class="col-xs-1">
                                <?php

                                if($c_r->ca_volume_total > 1000){
                                  $ecpm = ($c_r->ca_net / $c_r->ca_volume_total) * 1000;
                                  echo round($ecpm,3) . " €";
                                } else {
                                  echo 'N/A';
                                }


                                ?>
                              </td>

                              <td class="col-xs-1"><input id="{{$c_r->id}}" type="text" name="commentaire" value="{{$c_r->commentaire}}" mois="{{$moisdelacampagne['month']}}"></td>

                              <td class="col-xs-1">

                                @if($c_r->aaf == 0)

                                <button id="{{$c_r->id}}" class="btn btn-danger" state="0" mois="{{$moisdelacampagne['month']}}" disabled><i class="fa fa-times" aria-hidden="true"></i></button>
                                <button id="{{$c_r->id}}" class="btn btn-success" state="1" mois="{{$moisdelacampagne['month']}}"><i class="fa fa-check-circle" aria-hidden="true"></i></button>

                                @else


                                <button id="{{$c_r->id}}" class="btn btn-danger" state="0" mois="{{$moisdelacampagne['month']}}"><i class="fa fa-times" aria-hidden="true"></i></button>
                                <button id="{{$c_r->id}}" class="btn btn-success" state="1" mois="{{$moisdelacampagne['month']}}" disabled><i class="fa fa-check-circle" aria-hidden="true"></i></button>

                                @endif

                              </td>
                          </tr>
                          <?php
                          // Partie total

                          $totalcabrut = $totalcabrut + $c_r->ca_brut;
                          $totalcanet = $totalcanet + $c_r->ca_net;
                          $totalcout = $totalcout + $c_r->cout_routage;
                          $totalvolume = $totalvolume + $c_r->ca_volume_total;
                          $totalmarge = $totalmarge + $gains;
                           ?>
                          @endforeach

                          <tr>
                            <td><b>Totaux</b></td>
                            <td></td>
                            <td></td>
                            <td><b>{{big_number($totalcabrut)}} €</b></td>
                            <td><b>{{big_number($totalcanet)}} €</b></td>
                            <td><b>{{big_number($totalvolume)}}</b></td>
                            <td><b>{{big_number($totalcout)}} €</b></td>

                            <td><b>{{big_number($totalmarge)}} €</b></td>

                          </tr>
                      </table>

                </div>
              </div>
          </div>
      </div>
      </div>



    <script>
    $().ready(function() {


    $("input").keyup(function(e){

      var value = $(this).val();
      var db_id = $(this).attr("id");
      var input_type = $(this).attr("name");
      var lemois = $(this).attr("mois");


      console.log('ID campagne : ' + db_id + ' / Valeur :' + value + ' / colonne : ' + input_type);
      var cc = $('input[id="'+db_id+'"][name="ca_brut"]').val();
      $('input[id="'+db_id+'"][name="ca_brut"]').change(function() {
        $('input[id="'+db_id+'"][name="ca_net"]').val(cc);
      });

      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': '<?php echo csrf_token(); ?>'
          }
      });

      $.ajax({
          url: '/ajax/ca',
          type: 'POST',
          data: {
              valeur: value,
              type: input_type,
              id: db_id,
              mois: lemois
          },
          dataType: 'JSON',
          success: function (data) {

              // on pourrai mettre du vert dans le label
              console.log('ok');
          },
          error: function () {
              console.log('erreur');
          },
         complete : function(){
            console.log('complete');
         }
      });




    });


    // event click button

    $("button").click(function() {


      var valeuraaf = $(this).attr("state");
      var db_id = $(this).attr("id");
      var lemois = $(this).attr("mois");

      console.log(valeuraaf);



        // je change le state de la ligne

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '<?php echo csrf_token(); ?>'
            }
        });

        $.ajax({
            url: '/ajax/ca/setaaf',
            type: 'POST',
            data: {
                valeur: valeuraaf,
                type: 'aaf',
                id: db_id,
                mois: lemois
            },
            dataType: 'JSON',
            success: function (data) {

                // on pourrai mettre du vert dans le label
                console.log('ok');
            },
            error: function () {
                console.log('erreur');
            },
           complete : function(){
              console.log('complete');
           }
          });

        });
        // je modifie le disbled

        });

    </script>

@endsection
