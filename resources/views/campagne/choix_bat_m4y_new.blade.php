@extends('template')

@section('content')
     <div class="row">
          {!! Form::open(array('url' => 'campagne/send_bat_mailforyou_choix', 'method' => 'POST'))  !!}
          <div class="col-xs-12">
               <i class="fa fa-2x fa-cogs text-success"></i>
               <span class="lead text-success">BAT - Mailforyou</span>
          </div>
          <br/>
          <br/>
          <div class="col-xs-12">
               <div class="col-xs-12">
                    <label for="inputPassword3" class="col-sm-2 control-label">Choisir le compte pour envoyer le BAT</label>
               </div>
               <div class="col-xs-10 col-sm-10">
                    <select class="form-control"  id="sender_id" name="sender_id" >
                         @foreach($sender_m4y as $lesender)
                              <option class="routeurChoice" value="{{$lesender->id}}">{{$lesender->nom}}</option>
                         @endforeach
                    </select>
               </div>
                    <br/>
                    <br/>
               <div class="col-xs-12">
                    {!! Form::hidden('campagne_id', $id) !!}
                    {!! Form::submit('Envoyer le BAT', array('class' => 'btn btn-success')) !!}
               </div>
          </div>
          {!! Form::close() !!}
     </div>
@endsection
