@extends('common.layout')

@section('content')

    {{--<div class="row">--}}
        {{--<div class="col-md-6">--}}
            {{--<div class="portlet light">--}}
                {{--<div class="portlet-title">--}}
                    {{--<div class="caption">--}}
                        {{--<i class="fa fa-cogs font-green-sharp"></i>--}}
                        {{--<span class="caption-subject font-green-sharp bold uppercase">Graph4</span>--}}
                    {{--</div>--}}
                    {{--<div class="tools">--}}
                        {{--<a href="javascript:;" class="collapse" data-original-title="" title="">--}}
                        {{--</a>--}}
                        {{--<a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title="">--}}
                        {{--</a>--}}
                        {{--<a href="javascript:;" class="reload" data-original-title="" title="">--}}
                        {{--</a>--}}
                        {{--<a href="javascript:;" class="remove" data-original-title="" title="">--}}
                        {{--</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="portlet-body">--}}
                    {{--<h4>Semi-transparent, black-colored label background.</h4>--}}
                    {{--<div id="chart_ouvreurs" style="width:100%; height:300px">--}}
{{--:)--}}
                    {{--</div>--}}

                    {{--<div id="pie_chart_4" class="chart" style="padding: 0px; position: relative;">--}}
                        {{--<canvas class="flot-base" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 515px; height: 300px;" width="643" height="375"></canvas>--}}
                        {{--<canvas class="flot-overlay" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 515px; height: 300px;" width="643" height="375"></canvas>--}}
                        {{--<div class="pieLabelBackground" style="position: absolute; width: 38px; height: 33px; top: 50.5px; left: 314.5px; opacity: 0.5; background-color: rgb(0, 0, 0);"></div>--}}
                        {{--<span class="pieLabel" id="pieLabel0" style="position: absolute; top: 50.5px; left: 314.5px;"><div style="font-size:8pt;text-align:center;padding:2px;color:white;">Series1<br>24%</div></span><div class="pieLabelBackground" style="position: absolute; width: 38px; height: 33px; top: 179.5px; left: 341.5px; opacity: 0.5; background-color: rgb(0, 0, 0);"></div><span class="pieLabel" id="pieLabel1" style="position: absolute; top: 179.5px; left: 341.5px;"><div style="font-size:8pt;text-align:center;padding:2px;color:white;">Series2<br>16%</div></span><div class="pieLabelBackground" style="position: absolute; width: 38px; height: 33px; top: 242.5px; left: 209.5px; opacity: 0.5; background-color: rgb(0, 0, 0);"></div><span class="pieLabel" id="pieLabel2" style="position: absolute; top: 242.5px; left: 209.5px;"><div style="font-size:8pt;text-align:center;padding:2px;color:white;">Series3<br>29%</div></span><div class="pieLabelBackground" style="position: absolute; width: 38px; height: 33px; top: 108.5px; left: 128.5px; opacity: 0.5; background-color: rgb(0, 0, 0);"></div><span class="pieLabel" id="pieLabel3" style="position: absolute; top: 108.5px; left: 128.5px;"><div style="font-size:8pt;text-align:center;padding:2px;color:white;">Series4<br>20%</div></span><div class="pieLabelBackground" style="position: absolute; width: 38px; height: 33px; top: 27.5px; left: 199.5px; opacity: 0.5; background-color: rgb(0, 0, 0);"></div><span class="pieLabel" id="pieLabel4" style="position: absolute; top: 27.5px; left: 199.5px;"><div style="font-size:8pt;text-align:center;padding:2px;color:white;">Series5<br>11%</div></span></div>--}}
                {{--</div>--}}
            {{--</div>--}}

        {{--</div>--}}

    {{--</div>--}}


    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">
                    Planning
                </span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                    <button id="btn_ouv" class="btn btn-primary">Collecter Ouvertures</button>
                    <button id="btn_bounces" class="btn btn-primary">Collecter Bounces</button>
                    <button id="btn_clicks" class="btn btn-primary">Collecter Clicks</button>
                    {{--<a href="/campagne/create" cl/ass="btn btn-success">Créer une campagne</a>--}}
                    {{--<a href="/theme" class="btn btn-primary">Gérer les Thèmes</a>--}}
                </div>
            </div>
        </div>
        <div class="portlet-body">

            <table class="table table-striped table-hover">
                <tr>
                    <th>Date</th>
                    <th>Volume</th>
                    <th>Tokens</th>
                    <th>Segments</th>
                    <th>Envoi</th>
                </tr>
                @foreach($campagne->plannings as $planning)
                    <tr>
                        <td>{{ $planning->date_campagne }}</td>
                        <td>{{ $planning->volume }}</td>
                        <td> @if ($planning->tokens_at == null)  <span style="color:#900" class="green icon-check"></span> @else<span style="color:#090" class="green icon-check"> {{ \Carbon\Carbon::parse($planning->tokens_at)->format('H:i') }}</span>@endif </td>
                        <td> @if ($planning->segments_at == null)  <span style="color:#900" class="green icon-check"></span> @else<span style="color:#090" class="green icon-check"> {{ \Carbon\Carbon::parse($planning->segments_at)->format('H:i') }}</span>@endif </td>
                        <td> @if ($planning->sent_at == null)  <span style="color:#900" class="green icon-check"></span> @else<span style="color:#090" class="green icon-check"> {{ \Carbon\Carbon::parse($planning->sent_at)->format('H:i') }}</span>@endif </td>
                    </tr>
                @endforeach

            </table>

        </div>
    </div>

    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">
                    Répartition
                </span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                    {{--<a href="/campagne/create" class="btn btn-success">Créer une campagne</a>--}}
                    {{--<a href="/theme" class="btn btn-primary">Gérer les Thèmes</a>--}}
                </div>
            </div>
        </div>
        <div class="portlet-body">

            <table class="table table-striped table-hover">
                @foreach(\App\Models\Fai::all() as $fai)
                <tr>
                   <th>{{ $fai->nom }}</th>
                    <td>{{ \App\Models\Token::where('campagne_id', $campagne->id)->where('fai_id', $fai->id)->count() }}</td>
                </tr>
                @endforeach
            </table>

        </div>
    </div>


    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">
Rapport
                </span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                    {{--<a href="/campagne/create" class="btn btn-success">Créer une campagne</a>--}}
                    {{--<a href="/theme" class="btn btn-primary">Gérer les Thèmes</a>--}}
                </div>
            </div>
        </div>
        <div class="portlet-body">

            <table class="table table-striped table-hover">
                <tr>
                    <th>Date</th>
                    <th>Envoyés</th>
                    <th>Ouvreurs</th>
                    <th>Cliqueurs</th>
                    <th>Bounces</th>
                </tr>

                @foreach($campagne_stats as $stat)
                    <tr>
                        <td>{{ $stat->date }}</td>
                        <td>{{ $stat->envoyes }}</td>
                        <td>{{ $stat->ouvertures }}</td>
                        <td>{{ $stat->clicks }}</td>
                        <td>{{ $stat->bounces }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>

    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">
                    Segments (chez les Routeurs)
                </span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                    {{--<a href="/campagne/create" class="btn btn-success">Créer une campagne</a>--}}
                    {{--<a href="/theme" class="btn btn-primary">Gérer les Thèmes</a>--}}
                </div>
            </div>
        </div>
        <div class="portlet-body">

            <table class="table table-striped table-bordered">
                <tr>
                    <th>Sender ID</th>
                    <th>Sender Nom</th>
                    <th>Maildrop Segment_ID</th>
                    <th>Maildrop List_ID</th>
                    {{--<th>Destinataires</th>--}}
                </tr>

                @foreach($segments as $segment)
                    <tr>
                        <td>{{ $segment->sender_id }}</td>
                        <td>{{ $segment->sender->nom }}</td>
                        <td>{{ $segment->md_segment_id }}</td>
                        <td>{{ $segment->md_list_id }}</td>
                    </tr>
                @endforeach
            </table>

        </div>
    </div>

    <input type="hidden" id="campagne_id" value="{{ $campagne->id }}" />
    <input type="hidden" id="token" value="{{ csrf_token() }}" />


@endsection

@section('footer-scripts')
    {{--<script src="/js/charts.js" />--}}
    <script>

        $(function() {

            var campagne_id = $('#campagne_id').val();
            var token = $('#token').val();

            var formData = {
                'campagne_id': campagne_id,
                '_token': token
            };

            $('#btn_ouv').click(function() {
                $.post( "/ajax/c_stats_ouv", formData );
                toastr["success"]("Collecte des ouvreurs en cours");
            });

            $('#btn_bounces').click(function() {
                $.post( "/ajax/c_stats_bounces", formData );
                toastr["success"]("Collecte des NPAI en cours");
            });

            $('#btn_clicks').click(function() {
                $.post( "/ajax/c_stats_clicks", formData );
                toastr["success"]("Collecte des Clicks en cours");
            });

        });
    </script>
@endsection