@extends('common.layout')

@section('content')
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs font-green-sharp"></i>
            <span class="caption-subject font-green-sharp bold uppercase">
                Choisir un fichier repoussoir<b>*</b> pour <span style="color:#ffcc33">{{$campagne->ref}} - {{$campagne->nom}}</span> ({{$campagne->base->nom}})
            </span>
        </div>
        <div class="actions">
            <div class="btn-group btn-group-devided">
                <a href="/campagne" class="btn red">Retour</a>
            </div>
        </div>
    </div>
    <div class="portlet-body">
        <form action="" id="repoussoirForm" method="post">
            <table class="table table-striped table-hover">
                <tr>
                    <th>Fichier à utiliser</th>
                </tr>
                @foreach ($files as $file)
                <tr>
                    <td> <label> {!! Form::radio('file',$file) !!} &nbsp;&nbsp; {{\File::name($file)}} </label></td>
                </tr>
                @endforeach
            </table>
            {!! Form::hidden('campagneid', $campagne->id, ['id' => 'campagneid']) !!}
            {!! Form::submit('Valider',['class' => 'btn btn-large btn-primary']) !!}
            <input type="hidden" id="_token" value="{{ csrf_token() }}" />
        </form>
        <span><b>*</b>permet d'exclure sur 7 jours des destinataires lors de l'envoi de la campagne.</span>
    </div>
</div>
@endsection

@section('footer')
<script>
    $( "#repoussoirForm" ).submit(function( event ) {
        event.preventDefault();

        $.post( "/campagne/md5/action", {
            file: $('input[name=file]:checked').val(),
            campagneid: $('#campagneid').val(),
            _token: $('#_token').val()
        })
                .done(function( data ) {
                });
    });
</script>
@endsection