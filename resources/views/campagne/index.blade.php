@extends('common.layout')

@section('content')
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs font-green-sharp"></i>
            <span class="caption-subject font-green-sharp bold uppercase">Campagnes</span>
        </div>
        <div class="actions">
            <div class="btn-group btn-group-devided">
                <a title="Ajouter Campagne" href="/campagne/create" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i></a>

                <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    Actions
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a title="Importer un fichier MD5 d'@ repoussoir" href="/upload">Importer repoussoir</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a title="Importer des images à héberger" href="/dezip">Importer images</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a title="Extraire les désinscrits par campagne" href="campagne/unsubscribers/">Extraire désinscrits / campagne</a></li>
                </ul>
                @if( (\Auth::User()->user_group->name == 'Admin' or \Auth::User()->user_group->name == 'Super Admin' )and getenv('CLIENT_PLAN') == 'plus')
                    <a href="/tag" class="btn purple-soft">Tags</a>
                @endif
            </div>
        </div>
    </div>
    <div class="portlet-body">
      <div class="row">
          <div class="span8">
          {!! Form::open(array('action' => 'CampagneController@index', 'method' => 'get', 'id' => 'baseTri')) !!}
          &nbsp;&nbsp;&nbsp;
          {!! Form::label('recherche', ' ') !!}
          {!! Form::text('recherche', $recherche) !!}
          <button type="submit" class="btn btn-default">Rechercher</button>
              <p>
                  <select id="base_id" name="base_id" class="form-control input-medium" onchange='filterByBase();'>
                      <option>Filtrer par base </option>
                      @foreach ($basesinfo as $labase)
                          <option value="{{$labase->id}}" @if($labase->id == $base_id) selected="selected" @endif>{{$labase->nom}}</option>
                      @endforeach
                      <option value="0" @if( is_numeric($base_id) && $base_id == 0) selected="selected" @endif> Multi-bases </option>
                  </select>
              </p>
          {!! Form::close() !!}
      </div>
      </div>
      <div class="row">
        <br />
      </div>
        <table class="table table-striped table-hover">
            <tr>
                <th>Base</th>
                <th>Nom</th>
                <th>Référence</th>
                <th>Planifications</th>
                <th>Date de création</th>
                <th>Actions</th>
                <th></th>
            </tr>
            @foreach($campagnes as $campagne)
            <tr>
                <td class="col-xs-1">@if($campagne->base_id == 0) Multi-bases @else {{$campagne->base->nom}} @endif</td>
                <td class="col-xs-1">{{$campagne->nom}}</td>
                <td class="col-xs-1">{{$campagne->ref}}</td>
                <td class="col-xs-1">{{$campagne->plannings->count()}}</td>

                <td class="col-xs-1">{{$campagne->created_at}}</td>
                <td class="col-xs-2">
                    <a title="Editer" class="btn btn-primary btn-sm" href="/campagne/{{$campagne->id}}/edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                    <!-- <a title="Statistiques" class="btn btn-primary btn-sm" href="/campagne/{{$campagne->id}}/stats"><i class="fa fa-area-chart" aria-hidden="true"></i></a> -->
                    <a title="Plannifier" class="btn btn-primary btn-sm" href="/campagne/{{$campagne->id}}/planning"><i class="fa fa-clock-o" aria-hidden="true"></i></a>
                    @if($is_mindbaz_list)
                        <a title="Plannifier Mindbaz" class="btn btn-primary btn-sm" href="/campagne/{{$campagne->id}}/mindbaz_planning"><i>MB</i></a>
                    @endif
                    <a title="Dupliquer" id= 'c{{$campagne->id}}' class="btn btn-success btn-sm" href="/campagne/{{$campagne->id}}/dupliquercampagne"><i class="fa fa-files-o" aria-hidden="true"></i></a>
                    <a class="btn btn-warning btn-sm" href="/campagne/{{$campagne->id}}/md5">Repoussoir</a>
                </td>
                <td class="col-xs-1">
                    @if(!empty($campagne->info))
                        <i title="{{$campagne->info}}" class="fa fa-info-circle" aria-hidden="true"></i>
                    @endif
                </td>
            </tr>
            @endforeach
        </table>
        {!! $campagnes->appends(['base_id' => $base_id, 'recherche' => $recherche])->render() !!}
    </div>
</div>
@endsection

@section('footer')
<!-- jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>

<!-- jQuery UI -->
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

<!-- Input -->
    <script>
        $(document).ready(function(){
            {!! FormAutocomplete::selector('#recherche')->db2() !!}
        });
    </script>

@foreach($campagnes as $campagne)
<script>
$('#c{{$campagne->id}}').click(function(e) {
    e.preventDefault(); // Prevent the href from redirecting directly
    var linkURL = $(this).attr("href");
    warnBeforeRedirect(linkURL);
  });
  function warnBeforeRedirect(linkURL) {
    swal({
      title: "Voulez vous dupliquer cette campagne ?",
      text: "Pensez à éditer les informations sur la copie de la campagne",
      type: "warning",
      showCancelButton: true
    }, function() {
      // Redirect the user
      window.location.href = linkURL;
    });
  }
</script>
@endforeach

@endsection

@section('footer-scripts')
    <script type="text/javascript">
        function filterByBase() {
            document.getElementById("baseTri").submit();
        }
    </script>
@endsection
