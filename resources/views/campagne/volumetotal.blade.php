@extends('common.layout')

@section('content')

@inject('volume', 'App\Http\Controllers\CampagneController')

    {!! Form::model(new \App\Models\Planning, array('method' => 'post', 'route' => array('storePlanning'), 'class'=>'form-horizontal') ) !!}

    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">
Volume totaux des campagnes
                </span>
            </div>
            <!-- <div class="actions">
                <div class="btn-group btn-group-devided">
                    <button type="submit" id="store" class="btn btn-success" onkeyup="verif();"> Enregistrer </button>
                    <button type="button" class="btn btn-primary" id="add_planning"> Ajouter une plannif </button>

                    <a href="/upload" class="btn red">Importer MD5</a>
                </div>
            </div> -->
        </div>
        <div class="portlet-body">

          <table class="table table-hover table-bordered table-striped">
              <tr>
                  <th>Nom</th>
                  <th>Reference</th>
                  <th>Volume total souhaité</th>
                  <th>Volume total déjà envoyé</th>
                  <th>Volume total restant</th>
                  <th>Dernier envoi</th>
                  <th>Action</th>
              </tr>

          @foreach ($lescampagnes as $lacampagne)

          <tr>
              <td>{{ $lacampagne->nom }}</td>
              <td>{{ $lacampagne->ref }}</td>
              <td>{{ $lacampagne->volume_total }}</td>
              <td>{{ $volume->calculvolumetotal($lacampagne->id) }}</td>
              <td>
              <?php
              if ($lacampagne->volume_total > 0) {
                echo $lacampagne->volume_total - $volume->calculvolumetotal($lacampagne->id);
              }
              ?>
              </td>
              <td>{{ $volume->dernier_envoi($lacampagne->id) }}</td>
              <td><a class="btn btn-primary btn-sm" href="/campagne/{{$lacampagne->id}}/edit">Editer</a> / <a class="btn btn-success btn-sm" href="/campagne/{{$lacampagne->id}}/planning">Plannifer</a></td>
          </tr>

          @endforeach
        </table>
        </div>
    </div>


@endsection

@section('footer-scripts')

@endsection
