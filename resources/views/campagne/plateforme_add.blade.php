@extends('template')

@section('content')

      <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">

                  <div class="x_title">
                    <h2>Chiffre d'affaire -
                      <small> <a href="/ca" class="btn btn-warning">Retour</a> <a href="/plateforme/add" class="btn btn-success">Ajouter</a>
                      </small>
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br>
                    <div class="row">
                      {!! Form::open(array('url' => 'plateforme/add','files' => true, 'method' => 'post')) !!}

                      <div class="form-group">
                          <label for="mentions_header" class="col-sm-2 control-label">Nom</label>
                          <div class="col-sm-10">
                              {!! Form::text('nom_plateforme', '', ['class' => 'form-control']) !!}
                          </div>
                      </div>
                      <br>
                      <br>
                      <div class="form-group">
                          <label for="mentions_header" class="col-sm-2 control-label">Lien</label>
                          <div class="col-sm-10">
                              {!! Form::text('lien', '', ['class' => 'form-control']) !!}
                          </div>
                      </div>

                      <button type="submit" class="btn btn-success">Valider</button>
                      {!! Form::close() !!}

                  </div>


                </div>


      </div>
      </div>


@endsection
