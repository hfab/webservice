@extends('template')

@section('content')

      <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">

                  <div class="x_title">
                    <h2>Upload Repoussoir (en MD5) <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br>
                    <div class="row">

                      @if(Session::has('success'))
                        <div class="alert-box success">
                        <h2>{!! Session::get('success') !!}</h2>
                        </div>
                      @endif
                      <div class="secure">Charger un fichier md5</div>
                      {!! Form::open(array('url'=>'apply/upload','method'=>'POST', 'files'=>true)) !!}
                       <div class="control-group">
                        <div class="controls">
                        {!! Form::file('fichier') !!}
                    <p class="errors">{!!$errors->first('fichier')!!}</p>
                  @if(Session::has('error'))
                  <p class="errors">{!! Session::get('error') !!}</p>
                  @endif
                      </div>
                      </div>
                      <div id="success"> </div>
                    {!! Form::submit('Envoyer', array('class'=>'send-btn')) !!}
                    {!! Form::close() !!}

                  </div>


      </div>
      </div>
      </div>

          @endsection
