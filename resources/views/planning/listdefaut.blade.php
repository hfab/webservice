@extends('common.layout')

@section('content')

    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">
                    Ajouter configuration planning par defaut
                </span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                    <a title="Retour" href="/planning" class="btn btn-success">Retour</a>
                </div>
            </div>

        </div>
        <div class="portlet-body">

          <div class="table-responsive">
              <table class="table table-hover table-bordered table-striped">
                  <tr>
                      <th style="text-align:center;">ID</th>
                      <th style="text-align:center;">Action</th>
                  </tr>
                  @foreach($config_defaut as $laconfig)
                      <tr>
                          <td style="text-align:center;">{{ $laconfig->config_id }}</td>
                          <td style="text-align:center;">
                            <a title="Retour" href="/planning/delete_defaut_planning/{{ $laconfig->config_id }}" class="btn btn-danger">Supprimer</a>
                          </td>
                      </tr>
                  @endforeach
              </table>
          </div>

          </div>


@endsection
