<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Alerte serveur {{ $url }}</title>
<body style="margin: 0px 0px 0px 0px; padding: 0px 0px 0px 0px; font-family: Trebuchet MS, Arial, Verdana, sans-serif;">

<!-- Start Main Table -->
<table width="100%" height="100%" cellpadding="0" style="padding: 20px 0px 20px 0px; background-color: #34495e;">
    <tr align="center">
        <td>

            <table>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
            <!-- Start Header -->
            <table style="width:580px; height:108px;" background="#2980b9" border="0">
            <tr>
                <td valign="top" style="width:456px;">
                    <h1 style="font-size:22px; color:#22baba;margin-left:20px; margin-top:26px;">Alerte serveur {{ $url }}</h1>
                </td>
                <td valign="top">
                    <p style="color:#aeaeae; font-size:11px; margin-top:34px;"><?php echo date('d-m-Y H:i'); ?></p>
                </td>
            </tr>
            </table>

            <table cellpadding="0" cellspacing="0" width="650" style="padding:30px 25px 30px 25px; background-color:white; text-align:center;">
                 <tr>

                  <th style="color:red; padding:15px 5px 15px 5px;">Disk</th>
                  <th style="color:red; padding:15px 5px 15px 5px;">MySQL</th>
                  <th style="color:red; padding:15px 5px 15px 5px;">Nginx</th>

                </tr>
                
                <tr>
	                <td>{{$disk}}</td>
	                <td>{{$mysql}}</td>
	                <td>{{$nginx}}</td>
                </tr>
                
                    </table>
                </td>
            </tr>

     </table>
     
     {{$profiler}}

</body>
</html>
