<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Rapport Envoi</title>
<body style="margin: 0px 0px 0px 0px; padding: 0px 0px 0px 0px; font-family: Trebuchet MS, Arial, Verdana, sans-serif;">

<!-- Start Main Table -->
<table width="100%" height="100%" cellpadding="0" style="padding: 20px 0px 20px 0px; background-color: #34495e;">
    <tr align="center">
        <td>

            <table>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
            <!-- Start Header -->
            <table style="width:580px; height:108px;" background="#2980b9" border="0">
                <tr>
                    <td valign="top" style="width:456px;">
                        <h1 style="font-size:22px; color:#22baba;margin-left:20px; margin-top:26px;">Rapport TOR {{$url}}</h1>
                    </td>
                    <td valign="top">
                        <p style="color:#aeaeae; font-size:11px; margin-top:34px;"><?php echo date('d-m-Y H:i'); ?></p>
                    </td>
                </tr>
            </table>

            <table cellpadding="0" cellspacing="0" width="580" style="padding:30px 0px 30px 0px; background-color:white;">
                <tr>
                    <td valign="top" style="color:#808080; font-size:11px; padding:0px 39px 0px 47px; text-align:justify; line-height:25px;">
                        <p>{{$email_volume_demande}}</p>
                        <p>{{$email_volume_selected}}</p>
                        <p>{{$email_date}}</p>
                        <p>{!!$used_senders!!}</p>
                    </td>
                    <td>

                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
