@extends('common.layout')

@section('content')
    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">Planning général</span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                    <a href="/m4y/envoi" class="btn btn-success">Statut Envois M4Y</a><a href="/volumetotal" class="btn btn-warning">Volumes totaux/campagne</a>
                </div>
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
                <div class="span8">

                    <form id="baseTri" action="/planning" method="get">
                        <p>
                            <select id="base_id" name="base_id" class="form-control input-medium" onchange='filterByBase();'>
                                <option> Filtrer par base </option>
                                @foreach ($basesinfo as $labase)
                                    <option value="{{$labase->id}}" @if($labase->id == $base_id) selected="selected" @endif>{{$labase->nom}}</option>
                                @endforeach
                                <option value="0" @if( is_numeric($base_id) && $base_id == 0) selected="selected" @endif> Multi-bases </option>
                            </select>
                        </p>
                    </form>
                </div>
            </div>
            <div class="row">
                <br />
            </div>
            <div class="table-responsive">
                <table class="table table-hover table-bordered table-striped">
                    <tr>
                        <th style="text-align:center;">ID</th>
                        <th style="text-align:center;">Date</th>
                        <th style="text-align:center;">Base</th>
                        <th style="text-align:center;">Campagne</th>
                        <th style="text-align:center;">Volume <br /> demandé</th>
                        <th style="text-align:center;">Tokens</th>
                        <th style="text-align:center;">Volume <br /> sélectioné</th>
                        <th style="text-align:center;">Segments</th>
                        <th style="text-align:center;">Envoyée</th>
                        <th style="text-align:center;">Routeur</th>
                        <th style="text-align:center;">Action</th>
                    </tr>
                    @foreach($plannings as $leplanning)
                    <tr>
                        <td style="text-align:center;">{{ $leplanning->id }}</td>
                        <td @if ($leplanning->date_campagne == date('Y-m-d')) style="background:#EFE" @elseif($leplanning->date_campagne < date('Y-m-d')) style="background:#EEF"  @else style="background:#fff2e6" @endif>{{date('d-m-Y', strtotime($leplanning->date_campagne))}} {{date('H:i',strtotime($leplanning->time_campagne))}}</td>
                        <td style="text-align:center;">{{ $leplanning->code }}@if(is_null($leplanning->code)) multi @endif</td>
                        <td style="text-align:center;">{{ $leplanning->ref }}</td>
                        <td style="text-align:center;">{{ big_number($leplanning->volume) }}</td>
                        <td style="text-align:center;"> @if ($leplanning->tokens_at == null)  <span style="color:#900" class="green icon-check"></span> @else<span style="color:#090" class="green icon-check"> {{ \Carbon\Carbon::parse($leplanning->tokens_at)->format('H:i') }}</span>@endif </td>
                        <td style="text-align:center;"> @if ($leplanning->volume_selected == null)  - @else<span style="color:purple;"> {{ big_number($leplanning->volume_selected) }}</span>@endif </td>
                        <td style="text-align:center;"> <a target="_blank" href="/planning/{{$leplanning->id}}/senders">@if ($leplanning->segments_at == null)  <span style="color:#900" class="green icon-check"></span> @else<span style="color:#090" class="green icon-check"> {{ \Carbon\Carbon::parse($leplanning->segments_at)->format('H:i') }}</a></span>@endif </td>
                        <td style="text-align:center;"> @if ($leplanning->sent_at == null)  <span style="color:#900" class="green icon-check"></span> @else<span style="color:#090" class="green icon-check"> {{ \Carbon\Carbon::parse($leplanning->sent_at)->format('H:i') }}</span>@endif </td>
                        <td style="text-align:center;">
                        <?php
                            $plan = \App\Models\Planning::find($leplanning->id);
                            echo $plan->routeur->nom . '<hr>';
                            if(!empty($plan->selectedsender)) {
                                echo $plan->selectedsender->nom;
                            }
                        ?>
                        </td>
                        <td style="text-align:center;">
                            <a class="btn btn-primary btn-sm" href="/campagne/{{$leplanning->campagne_id}}/@if($is_mindbaz_list && $plan->routeur->nom=='Mindbaz'){{'mindbaz_'}}@endif{{'planning'}}">Planifier</a>
                            <a class="btn btn-primary btn-sm" href="/campagne/{{$leplanning->campagne_id}}/edit">Editer HTML</a>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
        {!! $plannings->appends(['base_id' => $base_id])->render() !!}
    </div>
@endsection

@section('footer-scripts')
    <script type="text/javascript">
        function filterByBase() {
            document.getElementById("baseTri").submit();
        }
    </script>
@endsection
