@extends('common.layout')

@section('content')

    <div class="portlet light">
        <div class="portlet-body">

        <a title="Campagnes" href="/campagne"><button type="button" class="btn btn-primary"><i class="fa fa-envelope-o fa-lg" aria-hidden="true"></i><br><span class="visibleSmart"> Campagnes</span></button></a>
        <a title="Plannings" href="/planning"><button type="button" class="btn btn-primary"><i class="fa fa-calendar fa-lg" aria-hidden="true"></i><br><span class="visibleSmart"> Planning</span></button></a>
        </div>
    </div>

@endsection

