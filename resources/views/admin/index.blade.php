@extends('common.layout')

@section('content')
    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">
                Réglages avancés
                </span>
            </div>
            <div class="actions">
            </div>
        </div>
        <div class="portlet-body">
            <table class="table table-striped table-hover table-bordered">
                <tr>
                    <th>Type</th>
                    <th>Actions</th>
                </tr>
                <tr>
                    <td> Routeurs </td>
                    <td> <a href="/admin/routeur/"> <button type="button" class="btn btn-primary">Modifier</button> </a> </td>
                </tr>
                <tr>
                    <td> Envois automatiques </td>
                    <td>
                    @if(isset($autostatus->value))
                        @if($autostatus->value == 1)
                            <a title="Désactiver" class="btn btn-success" href="/admin/auto/disable"><i class="fa fa-toggle-off" aria-hidden="true"></i></a>
                        @else
                            <a title="Activer" class="btn btn-danger" href="/admin/auto/enable"><i class="fa fa-toggle-on" aria-hidden="true"></i></a>
                        @endif
                    @endif
                    </td>
                </tr>

                <tr>
                    <td> Pression marketing </td>
                    <td>
                        @if(isset($cleanrelaunchstatus->value))
                            @if($cleanrelaunchstatus->value == 1)
                                <a title="Désactiver" class="btn btn-success" href="/admin/cleanrelaunch/disable"><i class="fa fa-toggle-off" aria-hidden="true"></i></a>
                            @else
                                <a title="Activer" class="btn btn-danger" href="/admin/cleanrelaunch/enable"><i class="fa fa-toggle-on" aria-hidden="true"></i></a>
                            @endif
                        @endif
                    </td>
                </tr>

                <tr>
                    <td> Branches senders </td>
                    <td> <a href="/admin/branch/"> <button type="button" class="btn btn-primary">Modifier</button> </a> </td>
                </tr>
                <tr>
                    <td> Pixels de tracking </td>
                    <td> <a href="/admin/pixel/"> <button type="button" class="btn btn-primary">Modifier</button> </a> </td>
                </tr>
            </table>
        </div>
    </div>
@endsection
