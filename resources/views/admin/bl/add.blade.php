@extends('common.layout')

@section('content')
    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">
                Ajouter un nom de domaine manuellement
                </span>
            </div>
            <div class="actions">
              <a href="/outils/bl/"> <button type="button" class="btn btn-success">Retour</button> </a>
            </div>
        </div>
        <div class="portlet-body">
            {!! Form::open(array('url' => 'outils/bl/add','files' => true, 'method' => 'post')) !!}

            <div class="form-group">
              <label for="listename">Nom de domaine :</label>
              <input type="text" class="form-control" id="ndd_mano" name="ndd_mano">
            </div>

            <button type="submit" class="btn btn-success">Modifier la liste</button>
            {!! Form::close() !!}



        </div>
    </div>
@endsection
