@extends('common.layout')

@section('content')
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs font-green-sharp"></i>
            <span class="caption-subject font-green-sharp bold uppercase">
            Mettre à jour une branche
            </span>
        </div>
        <div class="actions">
            <div class="btn-group btn-group-devided">
            </div>
        </div>
    </div>
    <div class="portlet-body">
        {!! Form::model($branch, array('route' => array('admin.branch.update', $branch->id), 'method'=> 'put')) !!}
        @include('admin.branch.form')
        {!! Form::close() !!}
    </div>
</div>
@endsection