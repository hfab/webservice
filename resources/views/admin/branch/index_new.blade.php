@extends('template')

@section('content')

      <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">

                  <div class="x_title">
                    <h2>Admin</h2>
                    <ul class="nav navbar-right panel_toolbox">

                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br>
                    <div class="row">
                      <a title="Ajouter Branche" class="btn btn-success" href="/admin/branch/create"><i class="fa fa-plus" aria-hidden="true"></i> </a>


                    </div>

                    <div class="row">

                      <table class="table table-striped table-hover table-bordered">
                          <tr>
                              <th>Identifiant</th>
                              <th>Nom</th>
                              <th>Actions</th>
                          </tr>
                          @foreach($branches as $b)
                              <tr>
                                  <td> {{$b->id}} </td>
                                  <td> {{$b->name}} </td>

                                  @if($b->is_active == '1')
                                  <td> <a class="btn btn-primary" href="/admin/branch/{{$b->id}}/edit"><i class="fa fa-pencil" aria-hidden="true"></i></a> <a title="Désactiver" class="btn btn-success" href="/admin/branch/{{$b->id}}/disable"><i class="fa fa-toggle-off" aria-hidden="true"></i>
                                      </a> </td>
                                  @else
                                  <td> <a class="btn btn-primary" href="/admin/branch/{{$b->id}}/edit"><i class="fa fa-pencil" aria-hidden="true"></i></a> <a title="Activer" class="btn btn-danger" href="/admin/branch/{{$b->id}}/enable"><i class="fa fa-toggle-on" aria-hidden="true"></i>
                                      </a></td>
                                  @endif

                              </tr>
                          @endforeach
                      </table>


                    </div>


      </div>
      </div>
      </div>

          @endsection
