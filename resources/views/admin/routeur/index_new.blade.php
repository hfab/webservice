@extends('template')

@section('content')

      <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">

                  <div class="x_title">
                    <h2>Routeurs <small></small></h2>
                    <!--
					<ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
					-->
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br>
                    <div class="row">

                      <table class="table table-striped table-hover table-bordered">
                          <tr>
                              <th>Identifiant</th>
                              <th>Nom</th>
                              <th>Actions</th>
                          </tr>

                          @foreach($routeurs as $r)
                              <tr>
                                  <td> {{$r->id}} </td>
                                  <td> {{$r->nom}} </td>
                                  <td>
                                      <?php
                                      // join en controller sinon
                                      $status = \DB::table('settings')
                                      ->where('context', 'routeur')
                                      ->where('info', $r->id)
                                      ->first();
                                      // var_dump($status);
                                      ?>

                                      @if(isset($status->value))
                                          @if($status->value == 1)
                                          <a title="Désactiver" class="btn btn-success" href="/admin/routeur/{{$r->id}}/disable"><i class="fa fa-toggle-off" aria-hidden="true"></i></a>
                                          @else
                                          <a title="Activer" class="btn btn-danger" href="/admin/routeur/{{$r->id}}/enable"><i class="fa fa-toggle-on" aria-hidden="true"></i></a>
                                          @endif

                                          @if($r->nom == 'Mindbaz')
                                              <?php
                                              $status_mindbaz = \DB::table('settings')
                                              ->where('parameter', 'mindbaz_mode')
                                              ->first();
                                              ?>
                                              @if($status_mindbaz->value == 'list')
                                              <a title="Activer mode liste" class="btn btn-success" href="/admin/routeur/{{$r->id}}/disable_segment"><i class="fa fa-toggle-on" aria-hidden="true"></i></a>
                                              @else
                                              <a title="Désactiver mode liste" class="btn btn-danger" href="/admin/routeur/{{$r->id}}/enable_segment"><i class="fa fa-toggle-off" aria-hidden="true"></i></a>
                                              @endif
                                          @endif
                                      @endif
                                  </td>
                              </tr>
                          @endforeach
                      </table>

                    </div>


      </div>
      </div>
      </div>

          @endsection
