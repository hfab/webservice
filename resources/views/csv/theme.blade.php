@extends('common.layout')

@section('content')

<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs font-green-sharp"></i>
            <span class="caption-subject font-green-sharp bold uppercase">Exportation CSV</span>
        </div>
        <div class="actions">
            <div class="btn-group btn-group-devided">
                <!-- 
				<a href="/campagne/create" class="btn btn-success">Ajouter une campagne</a>
                <a href="/upload" class="btn red">Importer repoussoir</a>
				-->
            </div>
        </div>
    </div>
    <div class="portlet-body">
        
            <!-- 
			<tr>
                <th>Base</th>
                <th>Nom</th>
                <th>Référence</th>
                <th>Planifications</th>
                <th>Date de création</th>
                <th>Actions</th>
            </tr>
            @foreach($campagnes as $campagne)
            <tr>
                <td>{{$campagne->base->nom}}</td>
                <td>{{$campagne->nom}}</td>
                <td>{{$campagne->ref}}</td>
                <td>{{$campagne->plannings->count()}}</td>

                <td>{{$campagne->created_at}}</td>
                <td>
                    <a class="btn btn-primary" href="/campagne/{{$campagne->id}}/edit">Editer</a>
                    <a class="btn btn-primary" href="/campagne/{{$campagne->id}}/stats">Statistiques</a>
                    <a class="btn btn-primary" href="/campagne/{{$campagne->id}}/planning">Planifier</a>
                    <a class="btn btn-warning" href="/campagne/{{$campagne->id}}/md5">Repoussoir</a>
                </td>
            </tr>
            @endforeach
			
			-->


  <a href='/csv/base'><button type="button" class="btn btn-primary">Exporter par base</button></a>
  <a href='/csv/campagne'><button type="button" class="btn btn-danger">Exporter par campagne</button></a>
  <a href='/csv/theme'><button type="button" class="btn btn-success">Exporter par thème</button></a>
  <a href='/csv/periode'><button type="button" class="btn btn-warning">Exporter par période</button></a>

   
    </div>
</div>

@endsection
