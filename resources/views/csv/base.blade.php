@extends('common.layout')

@section('content')

<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs font-green-sharp"></i>
            <span class="caption-subject font-green-sharp bold uppercase">Exportation CSV - Choix BASE</span>
        </div>
        <div class="actions">
            <div class="btn-group btn-group-devided">
                
				<a href="/csv/" class="btn btn-success">Retour menu CSV</a>
                
				
            </div>
        </div>
    </div>
    <div class="portlet-body">
	
		{!! Form::open(array('url' => 'csv/base/')) !!}
        <table class="table table-striped table-hover">
            <tr>
                <th>Base à extraire vers csv</th>
            </tr>

            @foreach ($bases as $base)
            <tr>
                <td> <label> {!! Form::radio('baseid',$base->id) !!} &nbsp;&nbsp; {{ $base->nom }} </label></td>
            </tr>
            @endforeach
        </table>
		
		{!! Form::submit('Valider',['class' => 'btn btn-large btn-primary']) !!}
        <input type="hidden" id="_token" value="{{ csrf_token() }}" />
        {!! Form::close() !!}
		
   
    </div>
</div>

@endsection