@extends('template')

@section('content')

      <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">

                  <div class="x_title">
                    <h2>Gestion Segments <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <a title="Créer un segment" href="/segment/create" class="btn btn-success">Créer un segment</a>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div class="row">
                      <div>
                          <select class="form-control" id="base">
                              @foreach($bases as $b)
                                  <option value="{{$b->id}}">{{$b->nom}}</option>
                              @endforeach
                                  <option value="0"> Toutes bases confondues </option>
                          </select>
                          <select class="form-control" id="segment">
                              @foreach($segments as $segment)
                                  <option value="{{$segment->id}}">{{$segment->nom}}</option>
                              @endforeach
                          </select>
                          <a id="showVolume" title="Simuler volume" class="btn btn-primary btn-sm" >Simuler le volume</a>
                          <span class="loading-gif" id="loading-gif"><img src='/images/icon/loading_spinner.gif' alt='loading gif'></span>
                          <br/>
                          <center><span id="volumePotentiel"></span></center>
                      </div>
                    </div>



                    <br>
                    <div class="row">

                      <table class="table table-striped table-hover">
                          <tr>
                              <th>ID</th>
                              <th>Nom</th>
                              <th>Date de création</th>
                              <th>Actions</th>
                          </tr>
                          @foreach($segments as $segment)
                          <tr>
                              <td class="col-xs-1">{{$segment->id}}</td>
                              <td class="col-xs-1">{{$segment->nom}}</td>
                              <td class="col-xs-1">{{$segment->created_at}}</td>
                              <td class="col-xs-5">
                                  <a title="Editer" class="btn btn-primary" href="/segment/{{$segment->id}}/edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>

                                  {!! Form::open(array('route' => array('segment.destroy', $segment->id), 'method' => 'delete')) !!}
                                  <button class="btn btn-danger" type="submit"><i class="fa fa-times" aria-hidden="true"></i></button>
                                  {!! Form::close() !!}
                              </td>
                          </tr>
                          @endforeach
                      </table>

                  </div>


      </div>
      </div>
      </div>


      <!-- jQuery -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>

      <!-- jQuery UI -->
      <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

      <script type="text/javascript">

          $().ready(function() {

              var jqxhr = {abort: function () {}};

              $('#showVolume').on("click", function () {

                  jqxhr.abort();

                  var segment_id = $('#segment').val();
                  var base_id = $('#base').val();

                  console.log(segment_id);
                  console.log(base_id);

                  if (segment_id != 0) {

                      //Affiche le logo de chargement, vide les autres resultats
                      $('#volumePotentiel').empty();

                      $('#loading-gif').css('display', 'block');

                      $.ajaxSetup({
                          headers: {
                              'X-CSRF-TOKEN': '<?php echo csrf_token(); ?>'
                          }
                      });

                      jqxhr = $.ajax({
                          url: '/segment/ajax',
                          type: 'POST',
                          data: {
                              segment_id: segment_id,
                              base_id: base_id
                          },
                          dataType: 'JSON',
                          success: function (data) {

                              var count = data;
                                  <?php $fais = \App\Models\Fai::get();
                                  $lesfais = array();
                                  foreach($fais as $f){
                                      $lesfais[$f->id] = $f->nom;
                                  }
                                  ?>

                              var fais = <?php echo json_encode($lesfais); ?>;

                              var text_resultats = "<span> ";

                              $.each(data, function (index, item) {
                                  //console.log(' INDEX -- '+ index + '  -- FAI ' +fais[index]);
                                  //      console.log(item);
                                  text_resultats = text_resultats + fais[item.fai_id] + " : " + item.max_tok  +" <br/>";
                              });

                              text_resultats = text_resultats + "</span> ";

                              var potentiel = " destinataire(s) potentiel(s)";
  //                            if (count < 2) {
  //                                var potentiel = " destinataire potentiel";
  //                            }


                              console.log('COMPTE -- '+ text_resultats + potentiel);

                              //Retire Loading Gif
                              $('#loading-gif').css('display', 'none');
                              $('#volumePotentiel').append(text_resultats + potentiel);

                          },
                          error: function (e) {
                              console.log(e.responseText);
                          }
                      });
                  }
              });
          });
      </script>

@endsection
