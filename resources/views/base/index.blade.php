@extends('common.layout')

@section('content')

    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">
                    Liste des bases
                </span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                    <a title="Ajouter base" class="btn btn-success" href="/base/create"><i class="fa fa-plus" aria-hidden="true"></i></a>

                    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        Actions
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        <li><a title="Import de bounces/désinscrits/blackliste" href="/base/add" class="btn"><i class="fa fa-upload" aria-hidden="true"></i></a></li>
                        <li role="separator" class="divider"></li>
                        <li><a title="Désinscrire manuellement" href="/unsubscribe" class="btn">Désinscription</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a title="Gestion des segments" href="/segment" class="btn">Segments</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a title="Gestion des fichiers" href="/downl" class="btn">Gestion des fichiers</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="portlet-title">

              {!! Form::open(array('url' => 'base/searchmailall')) !!}
              {!! Form::label('searchall', 'Recherche all') !!}
              {!! Form::text('searchall') !!}
              {!! Form::submit('Rechercher',['class' => 'btn btn-sm btn-primary']) !!}
              {!! Form::close() !!}

        </div>

        <div class="portlet-body">


            <table class="table table-bordered table-striped table-hover">
                <tr>
                    <th>Identifiant</th>
                    <th>Code</th>
                    <th>Nom</th>
                    <th>Destinataires</th>
                    <th colspan="5">Actions</th>
                </tr>

                @foreach($bases as $b)
                    <tr>
                        <td>{{ $b->id }}</td>
                        <td>{{ $b->code }}</td>
                        <td>{{ $b->nom }}</td>
                        <td>{{ big_number($b->ViewCountDestinataires->num) }}</td>
                        <td>
                            <a title="Modifier" class="btn btn-primary btn-xs" href="/base/{{$b->id}}/edit" !!}><i class="fa fa-pencil" aria-hidden="true"></i></a>

                            <span style="position: relative;">
                                <button title="Ajouter destinataires" class="btn green btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <i class="fa fa-user-plus" aria-hidden="true"></i>
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a title="Ajouter contact" href="/base/{{$b->id}}/add_destinataires" !!} >Ajouter liste @</a></li>
                                    <li><a title="Ajouter contact perso" href="/base/{{$b->id}}/add_destinataires_perso" >Ajouter liste @ + champs</a></li>

                                </ul>
                            </span>

                            <a title="Statistiques" class="btn yellow-casablanca btn-xs" href="/base/{{$b->id}}/stats" !!}><i class="fa fa-pie-chart" aria-hidden="true"></i></a>
                            <span style="position: relative;">
                                <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    Plus
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a title="Extraire contact" href="/base/{{$b->id}}/extractcontacts" !!} >Extraire @</a></li>
                                    <li><a title="Extraire MD5" href="/base/extract/{{$b->id}}/extractmd5" >Extraire MD5</a></li>
                                    <li><a title="Comparer" href="/base/{{$b->id}}/compare_upload/" >Comparer @</a></li>
                                    <li><a title="Comparer" href="/base/{{$b->id}}/destinataire/" >Consulter @</a></li>
                                </ul>
                            </span>
                        </td>
                        <td>
                            {!! Form::open(array('route' => array('base.destroy', $b->id), 'method' => 'delete')) !!}
                            <button class="btn default red baseDestroy btn-xs" type="submit"><i class="fa fa-times" aria-hidden="true"></i></button>
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>

@endsection

@section('footer')
<script>
    $('.baseDestroy').click(function(e) {
        e.preventDefault();
        warnBeforeRedirect($(this));
    });
    function warnBeforeRedirect($element) {
        swal({
            title: "Voulez vous supprimer cette base ?",
            text: "Les campagnes, destinataires, plannings associés à cette base seront supprimés.",
            type: "warning",
            showCancelButton: true
        },
        function(isConfirm) {
            if (isConfirm) {
                console.log($element.closest("form"));
                $element.closest("form").submit();
                //swal("Deleted!", "Your imaginary file has been deleted.", "success");
            }
        });
    }
</script>
@endsection
