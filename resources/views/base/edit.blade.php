@extends('common.layout')

@section('content')

    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">
                    Mettre à jour une base
                </span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                    {{--<a href="/campagne/create" class="btn btn-success">Créer une campagne</a>--}}
                    {{--<a href="/theme" class="btn btn-primary">Gérer les Thèmes</a>--}}
                </div>
            </div>
        </div>
        <div class="portlet-body">
            @if(isset($erreur))
                <div class="alert alert-dismissable alert-danger">
                    <strong> {{$erreur}} </strong>
                </div>
            @endif
            {!! Form::model($base, array('method'=> 'PATCH', 'route' => array('base.update', $base->id), 'class'=>'form-horizontal')) !!}
            @include('base.form')
            {!! Form::close() !!}

        </div>
    </div>

@endsection