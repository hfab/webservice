@extends('common.layout')

@section('content')
    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">
                    Choix champs perso pour import
                </span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                </div>
            </div>
        </div>
        <div class="portlet-body">
          {!! Form::open(array('url' => 'base/' . $base->id . '/add_destinataires_perso','files' => true, 'method' => 'post')) !!}

          <div class="row" style="margin-left:0.33em;">
              {!! Form::file('fichier') !!}
          </div>

          <hr>

          <div class="row" style="margin-left:0.33em;">
              Choisir les champs correspondant au fichier :
          </div>
          <div id='zclic'>
          <select name="leselect[]" class="chosen" multiple style="width: 500px;" id="chzn">
          <option value="mail" selected>Email</option>
          <option value="nom">Nom</option>
          <option value="prenom">Prenom</option>
          <option value="datenaissance" >Date de naissance</option>
          <option value="ville">Ville</option>
          <option value="departement">Département</option>
          <option value="tel">Téléphone Fixe</option>
          <!-- <option value="telportable">Téléphone Portable</option> -->
          <option value="adresse">Adresse postale</option>
          </select>
          </div>
        <hr>

        <p>
            <ol id="order-list"></ol>
            <div id="fabien"></div>
        </p>

        <button type="submit" class="btn btn-success">Valider</button>

        {!! Form::close() !!}

        <br /> <br />
        </div>
    </div>
@endsection

@section('footer-scripts')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.6.2/chosen.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.6.2/chosen.jquery.js"></script>
<script src="https://cdn.jsdelivr.net/chosen/1.1.0/chosen.proto.js"></script>
<script src="http://labo.tristan-jahier.fr/chosen_order/chosen.order.jquery.min.js"></script>

<script type="text/javascript">
    var config = {
        '.chosen-select'           : {},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
        '.chosen-select-width'     : {width:"95%"}
    }

    $(document).ready(function() {
        // Chosenify every multiple select DOM elements with class 'chosen'
        $('#chzn').chosen();

        // Get a reference to the DOM element
        var MY_SELECT = $('select[multiple].chosen').get(0);

        $('#zclic , body').click(function() {
            var selection = ChosenOrder.getSelectionOrder(MY_SELECT);

            $('#order-list').empty();
            $('#fabien').empty();
            $(selection).each(function(i) {
                $('#order-list').append("<li>" + selection[i] + "</li>");
                $('#fabien').append('<input type="hidden" name="result[]" value="'  + selection[i] + '">');

            });
        });

        // $('#set-order').click(function() {
        //     ChosenOrder.setSelectionOrder(MY_SELECT, $('#input-order').val().split(','), true);
        // });
    });
</script>
@endsection
