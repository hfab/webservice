@extends('common.layout')

@section('content')


    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">
Ajouter une base
                </span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                    {{--<a href="/campagne/create" class="btn btn-success">Créer une campagne</a>--}}
                    {{--<a href="/theme" class="btn btn-primary">Gérer les Thèmes</a>--}}

                </div>
            </div>
        </div>
        <div class="portlet-body">
            @if(isset($error))
                <div class="alert alert-dismissable alert-danger">
                    <strong> {{$error}} </strong>
                </div>
            @endif
            {!! Form::model(new \App\Models\Base, array('route' => array('base.store'), 'method'=> 'post', 'class'=>'form-horizontal')) !!}
            @include('base.form')
            {!! Form::close() !!}

        </div>
    </div>

@endsection
