@extends('common.layout')

@section('content')
<form id="formUpload" class="form-horizontal">
    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">
                    Import
                </span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                    <select id="type" name="type" class="btn">
                        <option value="bounces" >Import Bounces</option>
                        <option value="blacklistes" >Import Blacklistes</option>
                        <option value="desinscrits" >Import Désinscrits</option>
                    </select>
                    <button id="import" class="btn btn-danger" href="">Parcourir</button>
                </div>
            </div>
        </div>
        <div class="portlet-body">
            <div class="form-group" id="upload-progress">
                <label class="col-sm-3 control-label">Upload en cours...</label>
                <div class="col-sm-9">
                    <div class="fileupload-process">
                        <div id="total-progress" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                            <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="_token" value="{{ csrf_token() }}" />
</form>
@endsection

@section('footer')
    <script src="/js/dropzone.js"></script>
    <script>
        var myDropzone = new Dropzone(document.body, {
            url: "/base/add_type",
            acceptedFiles:".csv,.txt",
            createImageThumbnails: false,
            parallelUploads: 1,
            previewTemplate: '<div style="display:none"></div>',
            autoQueue: true,
            clickable: "#import",
            init: function() {
                this.on("success", function(file, responseText) {

                });
            }
        });

        myDropzone.on("totaluploadprogress", function(progress) {
            document.querySelector("#total-progress .progress-bar").style.width = progress + "%";
            if (progress == 100) {
                toastr['success']('Fichier importé, traitement en cours.');
            }
        });

        myDropzone.on("sending", function(file, xhr, formData) {
            formData.append("_token", $('#_token').val());
            formData.append("type", $('#type').val());
            document.querySelector("#upload-progress").style.opacity = "1";
        });

        myDropzone.on("queuecomplete", function(progress) {
            document.querySelector("#upload-progress").style.opacity = "0";
        });

        $('#import').click(function(e) {
            e.preventDefault();
        })

        $('#formUpload').submit(function() {
            return false;
        })
    </script>
@endsection
