@extends('common.layout')

@section('content')

    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">Statistiques optout par base</span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                  <a href="/stats" class="btn btn-primary">Retour statistiques générales</a>
                </div>
            </div>
        </div>
        <div class="portlet-body">
            <table class="table table-hover table-bordered table-striped">

              <tr>
                  <th>Optout</th>
                  <th>Base</th>
              </tr>

                    @foreach($optout as $row)
                    <tr>
                      <td>
                        <?php
                           $basenom = \DB::table('bases')->where('id',$row->base_id)->first();
                           // var_dump();
                           echo $basenom->nom;
                        ?>

                      </td>
                      <td>
                        {{big_number($row->count)}}
                      </td>

                    </tr>
                    @endforeach


            </table>
        </div>
    </div>

@endsection
