@extends('common.layout')

@section('content')

    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">Statistiques MailForYou API</span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                    <a href="/stats" class="btn btn-success">Retour</a>
                    <!-- <a href="/button2" class="btn btn-primary">Bouton2</a> -->
                </div>
            </div>
        </div>
        <div class="portlet-body">

          <div class="caption">
              <i class="fa fa-cogs font-blue"></i>
              <span class="caption-subject font-blue bold uppercase">Dernière mise à jour > {{$timemaj}}</span>
          </div>

          <table class="table table-hover table-bordered table-striped">
          <tr>

              <th>Informations</th>
              <th>Routeur</th>
              <th>Volume envoyé</th>
              <th>Volume d'emails aboutis</th>
              <th>Délivrabilité (FAI)</th>
              <th>Ouvreurs</th>
              <th>Ouvreurs (FAI)</th>
              <th>Cliqueurs</th>
              <th>Désinscription</th>
          </tr>

          @foreach($ds as $r)

          <tr>
              <td>{{$r->reference}} / {{$r->planning_id}}
                <?php
                 $datesend = \DB::table('plannings')->where('id',$r->planning_id)->first();
                 if(!is_null($datesend)){
                   if(isset($datesend->campagne_id)){
                     $lacampagne = \DB::table('campagnes')->where('id',$datesend->campagne_id)->first();
                     if(isset($lacampagne->base_id)){
                       $labase = \DB::table('bases')->where('id',$lacampagne->base_id)->first();
                     }
                   }
                   if(isset($labase->nom)){
                     echo '<p><hr>'.$labase->nom.'</p>';
                   }
                   if(isset($datesend->sent_at)){
                     echo '<p><hr>Envoyée le : ' . $datesend->sent_at;
                   }
                 }

                 ?>

                </p>

              </td>
              <td>
                <?php
                  $infosender = \DB::table('senders')
                      ->select('nom')
                      ->where('id', $r->sender_id)
                      ->first();

                  echo $infosender->nom;
                ?>
              </td>
              <th>{{$r->nb_sent}}</th>
              <th>
                <?php

                $percent = round($r->nb_received / $r->nb_sent * 100, 2);
                // var_dump($percent);
                //
                if ($percent > 50) {
                    $class = "success";
                } elseif ($percent <= 50 && $percent > 25) {
                    $class = 'warning';
                } else {
                    $class = 'danger';
                }
                ?>
                <div class="progress">
                    <div class="progress-bar progress-bar-{{$class}}" role="progressbar" aria-valuenow="{{ $percent }}"
                         aria-valuemin="0" aria-valuemax="100" style="width:{{ $percent }}%">
                    </div>
                </div>
              </th>
              <td>



                Free {{$r->p_receveid_free }} %
                <?php
                $percent = $r->p_receveid_free;
                if ($percent > 50) {
                    $class = "success";
                } elseif ($percent <= 50 && $percent > 25) {
                    $class = 'warning';
                } else {
                    $class = 'danger';
                }
                ?>
                <div class="progress">
                    <div class="progress-bar progress-bar-{{$class}}" role="progressbar" aria-valuenow="{{ $percent }}"
                         aria-valuemin="0" aria-valuemax="100" style="width:{{ $percent }}%">
                    </div>
                </div>



                SFR {{$r->p_receveid_sfr }} %
                <?php
                $percent = $r->p_receveid_sfr;
                if ($percent > 50) {
                    $class = "success";
                } elseif ($percent <= 50 && $percent > 25) {
                    $class = 'warning';
                } else {
                    $class = 'danger';
                }
                ?>
                <div class="progress">
                    <div class="progress-bar progress-bar-{{$class}}" role="progressbar" aria-valuenow="{{ $percent }}"
                         aria-valuemin="0" aria-valuemax="100" style="width:{{ $percent }}%">
                    </div>
                </div>



                Laposte {{$r->p_receveid_laposte }} %
                <?php
                $percent = $r->p_receveid_laposte;
                if ($percent > 50) {
                    $class = "success";
                } elseif ($percent <= 50 && $percent > 25) {
                    $class = 'warning';
                } else {
                    $class = 'danger';
                }
                ?>
                <div class="progress">
                    <div class="progress-bar progress-bar-{{$class}}" role="progressbar" aria-valuenow="{{ $percent }}"
                         aria-valuemin="0" aria-valuemax="100" style="width:{{ $percent }}%">
                    </div>
                </div>

                Orange {{$r->p_receveid_orange }} %
                <?php
                $percent = $r->p_receveid_orange;
                if ($percent > 50) {
                    $class = "success";
                } elseif ($percent <= 50 && $percent > 25) {
                    $class = 'warning';
                } else {
                    $class = 'danger';
                }
                ?>
                <div class="progress">
                    <div class="progress-bar progress-bar-{{$class}}" role="progressbar" aria-valuenow="{{ $percent }}"
                         aria-valuemin="0" aria-valuemax="100" style="width:{{ $percent }}%">
                    </div>
                </div>


                 Autre {{$r->p_receveid_autre }} %
                 <?php
                 $percent = $r->p_receveid_autre;
                 if ($percent > 50) {
                     $class = "success";
                 } elseif ($percent <= 50 && $percent > 25) {
                     $class = 'warning';
                 } else {
                     $class = 'danger';
                 }
                 ?>
                 <div class="progress">
                     <div class="progress-bar progress-bar-{{$class}}" role="progressbar" aria-valuenow="{{ $percent }}"
                          aria-valuemin="0" aria-valuemax="100" style="width:{{ $percent }}%">
                     </div>
                 </div>


               </td>
              <td> <b>{{$r->nb_open}} |
                <?php
                $per_cent_ouv = $r->nb_open / $r->nb_sent * 100;
                echo round($per_cent_ouv,2) . " %";
                ?>
                </b>
              </td>
                <td>

                Free {{$r->p_ouv_free}} %
                <div class="progress">
                    <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="{{ $r->p_ouv_free }}"
                         aria-valuemin="0" aria-valuemax="100" style="width:{{ $r->p_ouv_free }}%">
                    </div>
                </div>

                SFR {{$r->p_ouv_sfr}} %
                <div class="progress">
                    <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="{{ $r->p_ouv_sfr }}"
                         aria-valuemin="0" aria-valuemax="100" style="width:{{ $r->p_ouv_sfr }}%">
                    </div>
                </div>

                Laposte {{$r->p_ouv_laposte}} %
                <div class="progress">
                    <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="{{ $r->p_ouv_laposte }}"
                         aria-valuemin="0" aria-valuemax="100" style="width:{{ $r->p_ouv_laposte }}%">
                    </div>
                </div>

                Orange {{$r->p_ouv_orange}} %
                <div class="progress">
                    <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="{{ $r->p_ouv_orange }}"
                         aria-valuemin="0" aria-valuemax="100" style="width:{{ $r->p_ouv_orange }}%">
                    </div>
                </div>

                Autre {{$r->p_ouv_autre}} %
                <div class="progress">
                    <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="{{ $r->p_ouv_autre }}"
                         aria-valuemin="0" aria-valuemax="100" style="width:{{ $r->p_ouv_autre }}%">
                    </div>
                </div>

              </td>
              <td>{{$r->nb_clicks}} |
                <?php
                $per_cent_clic = $r->nb_clicks / $r->nb_sent * 100;
                echo round($per_cent_clic,2). " %";
                ?>
              </td>
              <td>{{$r->nb_unsubscribe}} |
                <?php
                $per_cent_desabo = $r->nb_unsubscribe / $r->nb_sent * 100;
                echo round($per_cent_desabo,2) . " %";
                ?>
              </td>

          </tr>
          @endforeach

        </table>
        </div>
    </div>

@endsection
