@extends('common.layout')

@section('content')

    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">Statistiques MailForYou API</span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                    <a href="/stats" class="btn btn-success">Retour</a>
                    <!-- <a href="/button2" class="btn btn-primary">Bouton2</a> -->
                </div>
            </div>
        </div>
        <div class="portlet-body">

          <div class="caption">
              <i class="fa fa-cogs font-blue"></i>
              <span class="caption-subject font-blue bold uppercase">Dernière mise à jour > {{$timemaj}}</span>
          </div>

          <table class="table table-hover table-bordered table-striped">
          <tr>

              <th>Informations</th>
              <th>Routeur</th>
              <th>Volume envoyé</th>
              <th>Volume d'emails aboutis</th>
              <th>Délivrabilité (FAI)</th>
              <th>Ouvreurs</th>
              <th>Ouvreurs (FAI)</th>
              <th>Cliqueurs</th>
              <th>Désinscription</th>
          </tr>

          @foreach($ds as $r)

          <tr>
              <td>{{$r->reference}} / {{$r->planning_id}}

               <?php
                $datesend = \DB::table('plannings')->where('id',$r->planning_id)->first();
                $lacampagne = \DB::table('campagnes')->where('id',$datesend->campagne_id)->first();
                $labase = \DB::table('bases')->where('id',$lacampagne->base_id)->first();

                echo '<p><hr>'.$labase->nom.'</p>';
                if(isset($datesend->sent_at)){
                  echo '<p><hr>Envoyée le : ' . $datesend->sent_at;
                }
                ?>
                </p>

              </td>
              <td>
                <?php
                  $infosender = \DB::table('campagnes_routeurs')
                      ->select('sender_id')
                      ->where('planning_id', $r->planning_id)
                      ->where('cid_routeur', $r->id_m4y)
                      ->first();
                  // var_dump($infosender->sender_id);
                  $infosender = \DB::table('senders')->where('id', $infosender->sender_id)->first();
                  echo $infosender->nom;
                ?>
              </td>
              <th>{{$r->nb_send}}</th>
              <th>{{$r->aboutis_percent}} %
                <?php
                $percent = $r->aboutis_percent;
                if ($percent > 50) {
                    $class = "success";
                } elseif ($percent <= 50 && $percent > 25) {
                    $class = 'warning';
                } else {
                    $class = 'danger';
                }
                ?>
                <div class="progress">
                    <div class="progress-bar progress-bar-{{$class}}" role="progressbar" aria-valuenow="{{ $percent }}"
                         aria-valuemin="0" aria-valuemax="100" style="width:{{ $percent }}%">
                    </div>
                </div>
              </th>
              <td>



                Free {{$r->aboutis_free }} %
                <?php
                $percent = $r->aboutis_free;
                if ($percent > 50) {
                    $class = "success";
                } elseif ($percent <= 50 && $percent > 25) {
                    $class = 'warning';
                } else {
                    $class = 'danger';
                }
                ?>
                <div class="progress">
                    <div class="progress-bar progress-bar-{{$class}}" role="progressbar" aria-valuenow="{{ $percent }}"
                         aria-valuemin="0" aria-valuemax="100" style="width:{{ $percent }}%">
                    </div>
                </div>



                SFR {{$r->aboutis_sfr }} %
                <?php
                $percent = $r->aboutis_sfr;
                if ($percent > 50) {
                    $class = "success";
                } elseif ($percent <= 50 && $percent > 25) {
                    $class = 'warning';
                } else {
                    $class = 'danger';
                }
                ?>
                <div class="progress">
                    <div class="progress-bar progress-bar-{{$class}}" role="progressbar" aria-valuenow="{{ $percent }}"
                         aria-valuemin="0" aria-valuemax="100" style="width:{{ $percent }}%">
                    </div>
                </div>



                Laposte {{$r->aboutis_laposte }} %
                <?php
                $percent = $r->aboutis_laposte;
                if ($percent > 50) {
                    $class = "success";
                } elseif ($percent <= 50 && $percent > 25) {
                    $class = 'warning';
                } else {
                    $class = 'danger';
                }
                ?>
                <div class="progress">
                    <div class="progress-bar progress-bar-{{$class}}" role="progressbar" aria-valuenow="{{ $percent }}"
                         aria-valuemin="0" aria-valuemax="100" style="width:{{ $percent }}%">
                    </div>
                </div>

                Orange {{$r->aboutis_orange }} %
                <?php
                $percent = $r->aboutis_orange;
                if ($percent > 50) {
                    $class = "success";
                } elseif ($percent <= 50 && $percent > 25) {
                    $class = 'warning';
                } else {
                    $class = 'danger';
                }
                ?>
                <div class="progress">
                    <div class="progress-bar progress-bar-{{$class}}" role="progressbar" aria-valuenow="{{ $percent }}"
                         aria-valuemin="0" aria-valuemax="100" style="width:{{ $percent }}%">
                    </div>
                </div>


                 Autre {{$r->aboutis_autre }} %
                 <?php
                 $percent = $r->aboutis_autre;
                 if ($percent > 50) {
                     $class = "success";
                 } elseif ($percent <= 50 && $percent > 25) {
                     $class = 'warning';
                 } else {
                     $class = 'danger';
                 }
                 ?>
                 <div class="progress">
                     <div class="progress-bar progress-bar-{{$class}}" role="progressbar" aria-valuenow="{{ $percent }}"
                          aria-valuemin="0" aria-valuemax="100" style="width:{{ $percent }}%">
                     </div>
                 </div>


               </td>
              <td> <b>{{$r->ouvreurs}} |
                <?php
                $per_cent_ouv = $r->ouvreurs / $r->nb_send * 100;
                echo round($per_cent_ouv,2) . " %";
                ?>
                </b>
              </td>
                <td>

                Free {{$r->per_cent_free_ouv}} %
                <div class="progress">
                    <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="{{ $r->per_cent_free_ouv }}"
                         aria-valuemin="0" aria-valuemax="100" style="width:{{ $r->per_cent_free_ouv }}%">
                    </div>
                </div>

                SFR {{$r->per_cent_sfr_ouv}} %
                <div class="progress">
                    <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="{{ $r->per_cent_sfr_ouv }}"
                         aria-valuemin="0" aria-valuemax="100" style="width:{{ $r->per_cent_sfr_ouv }}%">
                    </div>
                </div>

                Laposte {{$r->per_cent_laposte_ouv}} %
                <div class="progress">
                    <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="{{ $r->per_cent_laposte_ouv }}"
                         aria-valuemin="0" aria-valuemax="100" style="width:{{ $r->per_cent_laposte_ouv }}%">
                    </div>
                </div>

                Orange {{$r->per_cent_orange_ouv}} %
                <div class="progress">
                    <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="{{ $r->per_cent_orange_ouv }}"
                         aria-valuemin="0" aria-valuemax="100" style="width:{{ $r->per_cent_orange_ouv }}%">
                    </div>
                </div>

                Autre {{$r->per_cent_autre_ouv}} %
                <div class="progress">
                    <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="{{ $r->per_cent_autre_ouv }}"
                         aria-valuemin="0" aria-valuemax="100" style="width:{{ $r->per_cent_autre_ouv }}%">
                    </div>
                </div>

              </td>
              <td>{{$r->cliqueurs}} |
                <?php
                $per_cent_clic = $r->cliqueurs / $r->nb_send * 100;
                echo round($per_cent_clic,2). " %";
                ?>
              </td>
              <td>{{$r->desinscriptions}} |
                <?php
                $per_cent_desabo = $r->desinscriptions / $r->nb_send * 100;
                echo round($per_cent_desabo,2) . " %";
                ?>
              </td>

          </tr>
          @endforeach

        </table>
        </div>
    </div>

@endsection
