@extends('common.layout')

@section('content')

    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">Statistiques Maildrop API</span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                    <a href="/stats" class="btn btn-success">Retour</a>
                    <!-- <a href="/button2" class="btn btn-primary">Bouton2</a> -->
                </div>
            </div>
        </div>
        <div class="portlet-body">

          <div class="caption">
              <i class="fa fa-cogs font-blue"></i>
              <span class="caption-subject font-blue bold uppercase">Liste des campagnes maildrop :</span>
          </div>

          <table class="table table-hover table-bordered table-striped">
          <tr>

              <th>Référence</th>
              <th>Ouvreurs</th>
              <th>Cliqueurs</th>
              <th>Désinscription</th>
              <th>Action</th>
          </tr>

          @foreach($statsrouteurv2 as $maildrop)

          <tr>
              <td>{{$maildrop->reference}} |  <span>Créé le {{$maildrop->created_at}}</span></td>
              <td>{{$maildrop->ouvreurs}}</td>
              <td>{{$maildrop->cliqueurs}}</td>
              <td>{{$maildrop->desinscriptions}}</td>

              <td><a href="/stats/routeur/maildrop/{{ $maildrop->reference }}"/> <button type="button" class="btn btn-warning">+ de détails</button></a></td>
          </tr>
          @endforeach

        </table>
        </div>
    </div>

@endsection
