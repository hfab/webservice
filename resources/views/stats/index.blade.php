@extends('common.layout')

@section('content')
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs font-green-sharp"></i>
            <span class="caption-subject font-green-sharp bold uppercase">Statistiques des campagnes par routeur</span>
        </div>
        <div class="actions">
            <div class="btn-group btn-group-devided">
                <a href="/stats" class="btn btn-primary">Retour - Accueil Statistiques</a>
            </div>
        </div>
    </div>
    <div class="portlet-body">
        <div class="list-group">
        @foreach($active_routeurs as $ar)
              <a href="/stats/routeur/{{strtolower($ar->nom)}}" class="list-group-item">Statistiques {{$ar->nom}}</a>
        @endforeach
        </div>
    </div>
</div>
@endsection
