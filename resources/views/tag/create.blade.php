@extends('common.layout')

@section('content')

<div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">Créer un Tag</span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">

                </div>
            </div>
        </div>
        <div class="portlet-body">

          @if(isset($message))
          <div class="alert alert-dismissable alert-warning">
            Problème ! Vous n'avez pas complétez tous les champs.
          </div>

          {!! Form::model(new \App\Models\Tag, array('route' => array('tag.store'), 'class'=>'form-horizontal') ) !!}

          <div class="form-group">
              <label for="input_name" class="col-sm-2 control-label">Nom</label>
              <div class="col-sm-10">
                  {!! Form::text('nom', $nomdansinput, ['class' => 'form-control']) !!}
              </div>
          </div>
          <div class="form-group">
              <label for="input_url" class="col-sm-2 control-label">URL</label>
              <div class="col-sm-10">
                  {!! Form::textarea('url', $urldansinput, ['class' => 'form-control']) !!}
              </div>
          </div>
          @else
            {!! Form::model(new \App\Models\Tag, array('route' => array('tag.store'), 'class'=>'form-horizontal') ) !!}
            @include('tag.partial-fields')
            @endif
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-default">Enregistrer</button>
                </div>
            </div>
            </form>
        </div>
    </div>


@endsection
