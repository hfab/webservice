@extends('common.layout')

@section('content')

<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs font-green-sharp"></i>
            <span class="caption-subject font-green-sharp bold uppercase">Tags (pixel)</span>
        </div>
        <div class="actions">
            <div class="btn-group btn-group-devided">
                <a href="/tag/create" class="btn btn-success">Ajouter un tag</a>
            </div>
        </div>
    </div>
    <div class="portlet-body">
        <div class="alert alert-warning">
            Notice : ces tags sont ajoutés par défaut aux campagnes.
        </div>
        <div class="alert alert-info"> Option : ces variables sont à ajouter dans le tag si nécessaire.
            <ul class="small"> <li>[email] </li> <li> [emailsha1] </li> <li> [emailmd5] </li> <li> [subcode] </li> <li> [base] </li> </ul>
        </div>


        <table class="table table-striped table-hover">
            <tr>
                <th>Id</th>
                <th>Tag</th>
                <th>Date de création</th>
                <th colspan="2">Actions</th>
            </tr>
            @foreach($tags as $tag)
            <tr>
                <td>{{$tag->id}}</td>
                <td>{{$tag->nom}}</td>
                <td>{{$tag->created_at}}</td>
                <td>
                    <a class="btn btn-primary" href="/tag/{{$tag->id}}/edit">Editer</a>
                </td>
                <td>
                    {!! Form::open(array('route' => array('tag.destroy', $tag->id), 'method' => 'delete')) !!}
                    <button class="btn default red" type="submit">Supprimer</button>
                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </table>
    </div>
</div>

@endsection