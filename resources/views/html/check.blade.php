@extends('common.layout')

@section('content')

<div class="portlet light">
        <div class="portlet-title">
            <div class="caption">

            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                    <a href="/campagne" class="btn red">Retour</a>
                </div>
            </div>
        </div>
        <div class="portlet-body">
          <table id="tab_planning" class="table table-striped table-bordered">
            <tr>
                <th>Lien</th>
                <th>Etat</th>
            </tr>
          @foreach($tabimgblade as $img)
          <tr>
          <td>
          {{$img[0]}}
          </td>
          <td>
          {{$img[1]}}
          </td>
          <tr>
          @endforeach
          @foreach($tablinksblade as $links)
          <tr>
          <td>
          {{$links[0]}}
          </td>
          <td>
          {{$links[1]}}
          </td>
          <tr>
          @endforeach
          </table>
        </div>
    </div>

@endsection
