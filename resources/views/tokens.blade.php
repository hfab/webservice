@extends('common.layout')

@section('content')

    <h1>Tokens</h1>

    <table class="table table-hover table-bordered table-striped">
        @foreach($tokens as $token)
        <tr>
            <td>{{ $token->destinataire->mail }}</td>
            <td>{{ $token->priority }}</td>

        </tr>
        @endforeach
    </table>

@endsection
