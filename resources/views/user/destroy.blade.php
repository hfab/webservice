<h2> Supprimer un utilisateur </h2>


{!! Form::model(new \App\Models\User, array('route' => array('user.destroy'), 'method'=> 'delete')) !!}

@include('user.form')

{{ Form::submit('Delete') }}
{{ Form::close() }}



