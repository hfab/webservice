@extends('template')

@section('content')

      <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">

                  <div class="x_title">
                    <h2>Créer un utilisateur</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div class="row">

                      <div class="actions">
                      @if( \Auth::User()->user_group->name == 'Super Admin')
                          <div class="btn-group btn-group-devided">
                            <a title="Ajouter un utilisateur" class="btn btn-success" href="/user/create"><i class="fa fa-plus" aria-hidden="true"></i></a>
                            <a title="Retour" class="btn btn-danger" href="/user">Retour</a>
                          </div>
                      @endif
                      </div>

                    </div>

                    <br>
                    <div class="row">

                                  {!! Form::model(new \App\Models\User, array('route' => array('user.store'), 'method'=> 'post', 'class'=>'form-horizontal')) !!}
                                  @include('user.form')
                                  {!! Form::close() !!}

                  </div>


      </div>
      </div>
      </div>

          @endsection
