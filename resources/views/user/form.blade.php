<input type="hidden" name="_token" value="{{ csrf_token() }}" />

<div id="result"></div>

<div class="form-group">
    <label for="input_html" class="col-sm-2 control-label">Nom</label>
    <div class="col-sm-10">
        {!! Form::text('name', Input::old('name'), ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    <label for="input_html" class="col-sm-2 control-label">Email</label>
    <div class="col-sm-10">
        {!! Form::text('email', Input::old('email'), ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    <label for="input_html" class="col-sm-2 control-label">Mot de passe</label>
    <div class="col-sm-10">
        <input class="form-control" name="password" type="password" value="" id="mdp1">
    </div>
</div>

<div class="form-group">
    <label for="input_html" class="col-sm-2 control-label">Confirmer mot de passe</label>
    <div class="col-sm-10">
          <input class="form-control" name="passwordconfirm" type="password" value="" id="mdp2" onkeyup="verif();">
    </div>
</div>

@if( \Auth::User()->user_group->name == 'Super Admin' )
    <div class="form-group">
        <label for="input_html" class="col-sm-2 control-label">Groupe</label>
        <div class="col-sm-10">
            {!! Form::select('user_group_id', $user_group, Input::old('user_group_id')) !!}
        </div>
    </div>
@endif

<div class="form-group">
    <label for="input_html" class="col-sm-2 control-label">BAT</label>
    <div class="col-sm-10">
        {!! Form::checkbox('is_bat', Input::old('is_bat'), true, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" id="envoi" class="btn blue-steel">Enregistrer</button>
    </div>
</div>

<script>

function verif()
{
var val1   = document.getElementById("mdp1").value,
    val2   = document.getElementById("mdp2").value,
    result = document.getElementById("result");


if(val1!=val2){

  $("#envoi").attr("disabled", true);
  result.innerHTML='<div class="alert alert-dismissable alert-danger">Attention ! Les deux mots de passe ne sont pas identiques</div>';

  } else {

    $("#envoi").attr("disabled", false);
    result.innerHTML='<div class="alert alert-dismissable alert-success">Les deux mots de passe sont identiques</div>';
  }

}





</script>
