<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title> TOR - GÃ©nÃ©ration & Envoi </title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width" />
    <link rel="icon" href="Images/favicon.ico" />
    <link rel="stylesheet" type="text/css" href="css/styles_GenEmails.css" />
    <link href="js/multiple-select-master/multiple-select.css" rel="stylesheet"/>
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script src="js/multiple-select-master/jquery.multiple.select.js"></script>

    <script type="text/javascript">

        $.fn.setNow = function (onlyBlank) {
            var now = new Date($.now())
                , year
                , month
                , date
                , hours
                , minutes
                , seconds
                , formattedDateTime
                ;

            year = now.getFullYear();
            month = now.getMonth().toString().length === 1 ? '0' + (now.getMonth() + 1).toString() : now.getMonth() + 1;
            date = now.getDate().toString().length === 1 ? '0' + (now.getDate()).toString() : now.getDate();
            hours = now.getHours().toString().length === 1 ? '0' + now.getHours().toString() : now.getHours();
            minutes = now.getMinutes().toString().length === 1 ? '0' + now.getMinutes().toString() : now.getMinutes()+5;
            seconds = now.getSeconds().toString().length === 1 ? '0' + now.getSeconds().toString() : now.getSeconds();

            //formattedDateTime = year + '-' + month + '-' + date + 'T' + hours + ':' + minutes + ':' + seconds;
            formattedDateTime = year + '-' + month + '-' + date + 'T' + hours + ':' + minutes + ':' + '00';

            if ( onlyBlank === true && $(this).val() ) {
                return this;
            }

            $(this).val(formattedDateTime);

            return this;
        }

        $.fn.setAfter = function (onlyBlank) {
            var now = new Date($.now())
                , year
                , month
                , date
                , hours
                , minutes
                , seconds
                , formattedDateTime
                ;

            year = now.getFullYear();
            month = now.getMonth().toString().length === 1 ? '0' + (now.getMonth() + 1).toString() : now.getMonth() + 1;
            date = now.getDate().toString().length === 1 ? '0' + (now.getDate()).toString() : now.getDate()+1;
            hours = now.getHours().toString().length === 1 ? '0' + now.getHours().toString() : now.getHours();
            minutes = now.getMinutes().toString().length === 1 ? '0' + now.getMinutes().toString() : now.getMinutes()+5;
            seconds = now.getSeconds().toString().length === 1 ? '0' + now.getSeconds().toString() : now.getSeconds();

            //formattedDateTime = year + '-' + month + '-' + date + 'T' + hours + ':' + minutes + ':' + seconds;
            formattedDateTime = year + '-' + month + '-' + date + 'T' + '23' + ':' + '59' + ':' + '00';

            if ( onlyBlank === true && $(this).val() ) {
                return this;
            }

            $(this).val(formattedDateTime);

            return this;
        }

        function multiSelect()
        {
            var numero = document.getElementById('nbMore').value;
            var cal = parseInt(numero,10);
            var n = cal.toString();
            var idsend = "#sender"+n;
            //alert(idsend);

            $(idsend).multipleSelect({
                filter: true,
                placeholder : 'Choisir un compte',
                width: '250px'
            });

        }

        function displayBlock(param)
        {

            multiSelect();

            $("#envoiBlock").show();
            if(param == 'instant')
            {
                $("#end_datelab").hide();
                $("#start_datelab").hide();
            }

            if(param == 'today')
            {
                $("#start_datelab").show();

                $(function () {
                    // Handler for .ready() called.
                    $('#start_datelab input[type="datetime-local"]').setNow();

                });
                $("#end_datelab").hide();
            }

            if(param == 'days')
            {
                $("#start_datelab").show();
                $("#end_datelab").show();
                $(function () {
                    // Handler for .ready() called.
                    $('#start_datelab input[type="datetime-local"]').setNow();
                    $('#end_datelab input[type="datetime-local"]').setAfter();

                });
            }
        }
        function chargeSelects(num)
        {
            //Chargement du select (plateformes et comptes/senders)
            var elt = document.getElementById('plateforme'+num);
            var eleme = document.getElementById('sender'+num);
            var compt = 1;

            $.ajax({
                type: 'POST',
                dataType: "json",
                url: 'autocomplet2.php',
                data: 'select=plateforme',

                success: function(data){
                    $.each(data, function(index, element) {
                        var option = document.createElement("option");
                        var plateforme = index;
                        option.text = plateforme;
                        option.value = plateforme;
                        elt.appendChild(option);
                        if(compt==1)
                        {
                            $.each(element, function(ind, ele) {

                                var hiddenOption = document.createElement("option");
                                hiddenOption.text = ind;
                                hiddenOption.value = ind;
                                eleme.appendChild(hiddenOption);

                            });
                        }
                        compt++;
                    });
                    multiSelect();
                }
            });

        }

        function sleep(milliseconds)
        {
            var start = new Date().getTime();
            for (var i = 0; i < 1e7; i++) {
                if ((new Date().getTime() - start) > milliseconds){
                    break;
                }
            }
        }

        function more()
        {
            var numero = document.getElementById('nbMore').value;
            var cal = parseInt(numero,10)+1;
            var appendHTML = "<p id='envoiBlock"+cal+"'> <label> Plateforme <select id='plateforme"+cal+"' name='plateforme["+cal+"]' class='plateforme' onchange='changeSenders("+cal+");'> </select> </label> <label class='labelSend'> Sender <select multiple='multiple' id='sender"+cal+"' name='sender["+cal+"][]' class='sender'> </select> </label> <span onclick='more();' class='glyphicon glyphicon-plus-sign' aria-hidden='true'></span></p> " ;

            $( "#envoiBlock" ).append( appendHTML );
            document.getElementById("nbMore").value = cal;
            chargeSelects(cal);
        }

        function changeSenders(numero)
        {
            var plateforme = document.getElementById('plateforme'+numero).value;
            var eltsend = document.getElementById('sender'+numero);

            $.ajax({
                type: 'POST',
                dataType: "json",
                url: 'autocomplet2.php',
                data: 'sender='+plateforme,

                success: function(data){
                    $('#sender'+numero).empty();
                    //alert('#sender'+numero+' is empty now');
                    $.each(data, function(index, element) {
                        var option = document.createElement("option");
                        var sender = index;
                        option.name = "sender";
                        option.text = sender;
                        option.value = sender;
                        eltsend.appendChild(option);
                    });
                    multiSelect();
                }
            });
        }

        $(document).ready(function()
        {
            //Auto-completion pour les campagnes existantes
            $("#campagne").on("keyup", function(e) {

                $('#resultat').show('fast');
                campagne_val = document.getElementById('campagne').value;
                base_val = $('input[name=base]:checked').val();
                //alert(base_val);
                $.post("autocomplet2.php", {
                        campagne : campagne_val,
                        base : base_val },
                    function(data) {
                        $('#resultat').html(data);
                    });
            });

            //Chargement des selects (plateformes et comptes/senders)
            chargeSelects(1);
            //return false;

        });

        function modifChamp (camp)
        {
            document.getElementById("campagne").value = camp;
        }

        function afficherJours()
        {
            if(document.getElementById('jours').style.display != 'inline'){
                document.getElementById('jours').style.display = 'inline';
            }
            else
            {
                document.getElementById('jours').style.display = 'none';
            }
        }

        function checkAll()
        {
            var inputs = document.getElementsByClassName('fais');
            for(i = 0; i < inputs.length; i++)
            {
                if(inputs[i].type == 'checkbox' && inputs[i].checked == false )
                {
                    inputs[i].checked = true;
                }
                else if(inputs[i].type == 'checkbox' && inputs[i].checked == true )
                {
                    inputs[i].checked = false;
                }
            }
        }

    </script>

</head>
<body>
<div class='container'>
    <h2> GÃ©nÃ©ration & Envoi </h2>
    <form action="GenEnvoi.php" method="post" >

        <label class="title"> Base <span style='color:red'>*</span> </label> <br/>
        <label>CDD <input name="base" type="radio" value="cdd" <?php if( isset($_POST["base"]) && $_POST["base"] == "cdd" ) echo "checked"; else echo "checked"; ?> /> &nbsp; </label>
        <label>PDF <input name="base" type="radio" value="pdf" <?php if( isset($_POST["base"]) && $_POST["base"] == "pdf" ) echo "checked"; ?> /> &nbsp; </label>

        <hr>

        <label class="title"> Campagne <span style='color:red'>*</span> </label> <br/>
        <label> <input id='campagne' name="campagne" type="text" value="<?php if( isset($_POST["campagne"]) ) echo $_POST["campagne"]; ?>" style='width:200px;' /> &nbsp; </label>
        <div id='resultat'> </div>
        <hr>


        <label class="title"> Nombre de mails <span style='color:red'>*</span> </label>
        <br/>
        <label>&nbsp;&nbsp;<input type="number" name="mailNb" min="10" max="1000000" value="<?php if( isset($_POST["mailNb"]) ) echo $_POST["mailNb"]; else echo "50000"; ?>" /> </label>
        <br/><br/>

        <hr>

        <label class="title"> Envoi <span style='color:red'>*</span> </label>
        <div id='envoiChoix'>

            <label> &nbsp;&nbsp;Maintenant <input type="radio" name="choixenvoi" value="instant" onClick="displayBlock('instant');" /> </label>
            <label> &nbsp;&nbsp;Plus tard <input type="radio" name="choixenvoi" value="today" onClick="displayBlock('today');" /> </label>
            <label> &nbsp;&nbsp;Sur plusieurs jours <input type="radio" name="choixenvoi" value="days" onClick="displayBlock('days');" /> </label>
        </div>

        <div id='envoiBlock' style='display:none'>
            <p id='envoiBlock1'>
                <label id='start_datelab' > <input width="175" type="datetime-local" name="start_date" /> </label>
                <label id='end_datelab' style='display:none'> au <input width="175" type="datetime-local" name="end_date" /> </label>
                <br/>

                <label> Plateforme <span style='color:red'>*</span> <select id='plateforme1' name='plateforme[1]' class='plateforme' onchange='changeSenders(1);'> </select> </label>
                <label class='labelSend'> Sender <span style='color:red'>*</span> <select multiple="multiple" id='sender1' name="sender[1][]" class="sender" > </select>  </label> <span onclick='more();' class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
            </p>
        </div>

        <input type='hidden' id="nbMore" value='1'>

        <hr>

        <label class="title"> FAI(s) <span style='color:red'>*</span> </label>
        <p class='details'> <a target='_blank' href='setpressionM.php' style='font-size:11px;' > Modifier config. pression marketing</a> <br/> <em style='display:none;'> (Lorsqu'il y a plus d'un FAI, merci d'indiquer le nombre de mails souhait&eacute; par FAI, <br/> si ce n'est pas pr&eacute;cis&eacute; le nombre de mails/FAI est al&eacute;atoire.) </em> </p>

        <table>
            <tr>
                <td>
                    <label>Orange  <input class='fais' type="checkbox" name="fais[]" value="orange" <?php if( !is_null($_POST) and array_key_exists("fais", $_POST) and (in_array("orange", $_POST["fais"]) or in_array("tout", $_POST["fais"])) ) echo "checked" ?> /> &nbsp; 	</label> <br/>
                    <input class='pourFai' type="number" name="pourFai[orange]" style='width:70px;height:20px;' />
                </td>

                <td>
                    <label>Free  <input class='fais' type="checkbox" name="fais[]" value="free" <?php if( !is_null($_POST) and array_key_exists("fais", $_POST) and (in_array("free", $_POST["fais"]) or in_array("tout", $_POST["fais"])) ) echo "checked" ?>  /> &nbsp; </label> <br/>
                    <input class='pourFai' type="number" name="pourFai[free]" style='width:70px;height:20px;' />
                </td>

                <td>
                    <label>Hotmail  <input class='fais' type="checkbox" name="fais[]" value="hotmail" <?php if( !is_null($_POST) and array_key_exists("fais", $_POST) and (in_array("hotmail", $_POST["fais"]) or in_array("tout", $_POST["fais"])) ) echo "checked" ?> /> &nbsp; </label><br/>
                    <input class='pourFai' type="number" name="pourFai[hotmail]" style='width:70px;height:20px;' />
                </td>

                <td>
                    <label>Yahoo <input class='fais' type="checkbox" name="fais[]" value="yahoo" <?php if( !is_null($_POST) and array_key_exists("fais", $_POST) and (in_array("yahoo", $_POST["fais"]) or in_array("tout", $_POST["fais"])) ) echo "checked" ?> /> &nbsp; </label><br/>
                    <input class='pourFai' type="number" name="pourFai[yahoo]" style='width:70px;height:20px;' />
                </td>

                <br/>

                <td>
                    <label>Gmail <input class='fais' type="checkbox" name="fais[]" value="gmail" <?php if( !is_null($_POST) and array_key_exists("fais", $_POST) and (in_array("gmail", $_POST["fais"]) or in_array("tout", $_POST["fais"])) ) echo "checked" ?> /> &nbsp; </label><br/>
                    <input class='pourFai' type="number" name="pourFai[gmail]" style='width:70px;height:20px;' />
                </td>

                <td>
                    <label>SFR <input class='fais' type="checkbox" name="fais[]" value="sfr" <?php if( !is_null($_POST) and array_key_exists("fais", $_POST) and (in_array("sfr", $_POST["fais"]) or in_array("tout", $_POST["fais"])) ) echo "checked" ?> /> &nbsp; </label><br/>
                    <input class='pourFai' type="number" name="pourFai[sfr]" style='width:70px;height:20px;' />
                </td>

                <td>
                    <label> Laposte <input class='fais' type="checkbox" name="fais[]" value="laposte" <?php if( !is_null($_POST) and array_key_exists("fais", $_POST) and (in_array("laposte", $_POST["fais"]) or in_array("tout", $_POST["fais"])) ) echo "checked" ?> /> &nbsp; </label><br/>
                    <input class='pourFai' type="number" name="pourFai[laposte]" style='width:70px;height:20px;' />
                </td>

                <td>
                    <label>Autre <input class='fais' type="checkbox" name="fais[]" value="autre" <?php if( !is_null($_POST) and array_key_exists("fais", $_POST) and (in_array("autre", $_POST["fais"]) or in_array("tout", $_POST["fais"])) ) echo "checked" ?> /> &nbsp; </label><br/>
                    <input class='pourFai' type="number" name="pourFai[autre]" style='width:70px;height:20px;' />
                </td>

                <td>
                    <label>Tout <input type="checkbox" name="fais[]" value="tout" <?php if( !is_null($_POST) and array_key_exists("fais", $_POST) and in_array("tout", $_POST["fais"]) ) echo "checked" ?> onClick='checkAll();'/> &nbsp; </label>
                </td>
            </tr>

        </table>
        <br/>
        <hr>

        <label class="title"> Activit&eacute; des mails</label>
        <p class='details'> <em> (Facultatif) </em> </p>


        <br/>
        <label>Ouvreurs  <input name='statutMail' type="radio" value="ouvreurs"<?php if( isset($_POST["statutMail"]) && $_POST["statutMail"] == "ouvreurs" ) echo "checked"; ?> onClick="afficherJours();" /> &nbsp; </label>
        <label>Cliqueurs <input name='statutMail' type="radio" value="cliqueurs"<?php if( isset($_POST["statutMail"]) && $_POST["statutMail"] == "cliqueurs"  ) echo "checked"; ?> onClick="afficherJours();" /> &nbsp; </label>

        <div id='jours' <?php if( empty($_POST["nbDays"]) ) echo "style='display:none;'"; else echo "style='display:inline;'";?>>
            <label>de moins de <input name='nbDays' type="number" min="1" max="365" value="<?php if( isset($_POST["nbDays"]) ) echo $_POST["nbDays"];?>" /> jours &nbsp; </label>
        </div>

        <br/><br/>

        <input type="hidden" name="envoi" value="generation" />
        <input type="submit" name="test" value="Chercher des mails" />

    </form>
    <br/>

    <?php

    include("inc/parametres_5.php");
    include("PG_conf.php");

    include("conf_pressionM.php");
    include("functions/progression.php");

    include("functions/ct.php");
    include("lib/utils.php");

    ini_set('memory_limit', '20000M');
    set_time_limit(-1);
    ini_set('error_reporting', E_ALL);

    /*	==FONCTION==
    =	tri_fais :
    =		-	fonction :		tri un tableau de mails en fonction du %/FAI et du nb/envoi/J
    =		- 	paramÃ¨tres :	3 tableaux (d'emails, de FAIs voulus, et de quantitÃ© de mails/FAI)
    =							et le nombre total de mails souhaitÃ©
    =		- 	retour :		un tableau filtrÃ© selon les FAIs
    */

    function tri_fais_old($emails, $pourFais, $nb_mail_total )
    {

        $_FAIS = $GLOBALS['FAIS_grep'];
        $quotaFAI = $GLOBALS['quotaFAIS'];
        $quotaDefault = $GLOBALS['quotaDefault'];
        $infos = array();

        $compteurMails = $nb_mail_total;
        $ouv_filter = array();
        $pourFAI = array();

        foreach($emails as $unMail=>$nbEnvoiparJ)
        {
            //echo "\nMail -- $unMail -- NbEnvoiParJour -- $nbEnvoiparJ";
            $nbEnvoiparJ = 0;

            if($compteurMails == 0) break;

            //echo "<br/> *** Debut fais";
            foreach($pourFais as $unFai => $pour2)
            {
                //$pour = $pour2;
                if( !isset ($pourFAI[$unFai] ) ) $pourFAI[$unFai] = $pour2;

                if($pourFAI[$unFai] == 0){ continue;}

                if( array_key_exists($unFai,$_FAIS) )
                {
                    foreach($_FAIS[$unFai] as $f)
                    {
                        //if(stripos("@$f", $unMail) !== false)
                        if(preg_match("/@$f/i", $unMail))
                        {
                            //if($unFai == 'hotmail') echo "<<= HOTMAIL =>>";

                            if($pourFAI[$unFai] == 0) break;
                            //echo "<br/> Nbenvoi : $nbEnvoiparJ -- QuotaFAI : ".$quotaFAI[$unFai];
                            if(isset($quotaFAI[$unFai])) $leQuota = $quotaFAI[$unFai];
                            else $leQuota = $quotaDefault;

                            if($nbEnvoiparJ >= $leQuota )
                            {
                                //echo "<hr> quota atteint pour $unMail -- leQuota = $leQuota";
                                continue;
                            }

                            if(!isset($ouv_filter[$unFai]))	$ouv_filter[$unFai] = array();

                            array_push($ouv_filter[$unFai], $unMail);
                            //array_push($ouv_filter, $unMail);

                            if(!isset($infos[$unFai])) $infos[$unFai]=0;
                            $infos[$unFai]+=1;
                            //$ouv_filter[$unMail]=$id_mail;
                            //$ouv_filter[$unMail]="";
                            $compteurMails--;
                            $pourFAI[$unFai] = $pourFAI[$unFai] - 1;
                            //$pour = $pour + 1;
                            $pour = $pour - 1;
                            break 2;
                        }
                    }
                }//fin if( array_key_exists($unFai,$_FAIS) )
                else
                {
                    if($unFai == 'autre')
                    {
                        //echo "Autre--";
                        //Quota pr les autre
                        $leQuota = $quotaDefault;

                        if($nbEnvoiparJ >= $leQuota )
                        {
                            continue;
                        }

                        foreach($_FAIS as $f)
                        {
                            foreach($f as $v)
                            {
                                //if(stripos("@$f", $unMail) !== false)
                                if(preg_match("/@$v/i", $unMail))
                                {
                                    continue 4;
                                }
                            }
                        }
                    }

                    if(!isset($ouv_filter[$unFai]))	$ouv_filter[$unFai] = array();

                    array_push($ouv_filter[$unFai], $unMail);

                    //array_push($ouv_filter, $unMail);
                    $infos[$unFai]+=1;
                    $pourFAI[$unFai]--;
                    //$ouv_filter[$unMail]="";
                    $compteurMails--;
                    //$pour = $pour - 1;
                }//fin de else 1
            }//Fin du foreach($fais as $unFai)
        }//foreach($emails as $unEmail)
        $ouv_filter['infos']=$infos;
        return $ouv_filter;
    }//fin de la fonction


    function tri_fais($emails, $pourFais, $nb_mail_total )
    {

        $_FAIS = $GLOBALS['FAIS'];
        $quotaFAI = $GLOBALS['quotaFAIS'];
        $quotaDefault = $GLOBALS['quotaDefault'];

        $compteurMails = $nb_mail_total;
        $ouv_filter = array();
        $pourFAI = array();
        $infos = array();


        foreach($emails as $unMail=>$nbEnvoiparJ)
        {
            $nbEnvoiparJ = 0;

            if($compteurMails == 0) break;

            foreach($pourFais as $unFai => $pour2)
            {
                if( !isset ($pourFAI[$unFai] ) ) $pourFAI[$unFai] = $pour2;

                if($pourFAI[$unFai] == 0){ continue;}

                if( array_key_exists($unFai,$_FAIS) )
                {
                    if(findFAI($unMail)==$unFai)
                    {
                        if($pourFAI[$unFai] == 0) break;

                        if(isset($quotaFAI[$unFai])) $leQuota = $quotaFAI[$unFai];
                        else $leQuota = $quotaDefault;

                        if($nbEnvoiparJ >= $leQuota )
                        {
                            continue;
                        }

                        if(!isset($ouv_filter[$unFai]))	$ouv_filter[$unFai] = array();

                        array_push($ouv_filter[$unFai], $unMail);

                        if(!isset($infos[$unFai])) $infos[$unFai]=0;
                        $infos[$unFai]+=1;

                        $compteurMails--;
                        $pourFAI[$unFai] = $pourFAI[$unFai] - 1;
                        break;
                    }
                }//fin if( array_key_exists($unFai,$_FAIS) )

            }//Fin du foreach($fais as $unFai)
        }//foreach($emails as $unEmail)

        $ouv_filter['infos']=$infos;
        return $ouv_filter;

    }//fin de la fonction


    /*================================================
        ================================================
        ================================
        ================================================
        ================================================

        P_A_R_T_I_E 1 	: 	G_E_N_E_R_A_T_I_O_N

        ================================================
        ================================================
        ================================
        ================================================
    ================================================*/


    if(!is_null($_POST) && isset($_POST['test']) && $_POST['envoi']='generation' && isset($_POST["sender"]) && isset($_POST["plateforme"]) && count($_POST["pourFai"]) > 0 )
    {
        $TOTAL_COUNT = 0;
        $choixenvoi = $_POST["choixenvoi"];
        $senders = $_POST["sender"];
        $plateformes = $_POST["plateforme"];
        $timestamp = date("Y-m-d-H-i-s");

        $allsenders = array();
        $pourcentageFAIS = array();
        $corrMailID=array(); //tableau de correspondance entre ID et mail
        $uni_emails = array(); //tableau sans doublons
        $toExclude = array(); // tableau des mails Ã  exclure pour les envois sur plusieurs jours

        //RÃ©cupÃ©ration et vÃ©rifications des entrÃ©es saisies obligatoires
        if( !isset($_POST["base"]) or empty($_POST["base"]) ){ echo "<b style='color:red'> Merci de choisir une base. </b><br/>"; exit;}
        $base=$_POST["base"];

        if( !isset($_POST["campagne"]) or empty($_POST["campagne"]) ){ echo "<b style='color:red'> Merci de saisir un nom de campagne. </b><br/>"; exit;}
        $campagne = $_POST["campagne"];

        if( !isset($_POST["mailNb"]) or empty($_POST["mailNb"]) ){ echo "<b style='color:red'> Merci d'indiquer le nombre de mails souhaitÃ©. </b><br/>"; exit;}
        $nbmails = $_POST["mailNb"];

        foreach($plateformes as $cle=>$uneplateforme)
        {
            $compt = 0;
            while($compt<count($senders[$cle]))
            {
                $sen = $senders[$cle][$compt];
                array_push($allsenders, $sen);
                $compt ++;
            }
        }

        if($choixenvoi == 'instant')
        {
            $startdate = date('Y-m-d H:i');
        }
        else
        {
            if($choixenvoi == 'days' || $choixenvoi == 'today')
            {
                $startdate = $_POST["start_date"];
                $startdate = date("Y-m-d H:i", strtotime($startdate));

                if(isset($_POST["end_date"]) and !empty($_POST['end_date']))
                {
                    $enddate = $_POST["end_date"];
                    $enddate = date("Y-m-d H:i", strtotime($enddate));
                }

            }
        }

        $dates_envois = array($startdate);

        $duree = 1;

        if(isset($enddate))
        {
            $date1=  new DateTime($startdate);
            $date2= new DateTime($enddate);
            $duree = $date1->diff($date2)->format("%d");

            $k=1;
            while($k<$duree)
            {
                $var =  $date1->add(new DateInterval('P1D'))->format("Y-m-d H:i");
                array_push($dates_envois, $var);
                $k++;
            }
            array_push($dates_envois, $enddate); //date de fin
        }

        $nbDays = $duree + 1;

        $calculDays = round( $nbmails / ($duree+1) );
        $countDays = $calculDays;
        if($choixenvoi == 'instant' or $choixenvoi = 'today')
        {
            $countDays = $nbmails;
            $nbDays = 1;
        }

        if( isset($_POST["pourFai"]) and !empty($_POST["pourFai"]) )
        {
            $requeteFAI = "";

            $nbFAI = 0;
            foreach( $_POST["pourFai"] as $unFai=>$unPour )
            {
                if($unPour > 0)
                {
                    $nbFAI++;

                    $pourcentageFAIS[$unFai] = round($unPour/$nbDays); //si jamais on a un envoi sur plusieurs jours

                    if($nbFAI==1)
                    {
                        $requeteFAI .= " and (m_fai='$unFai'";
                    }
                    else
                    {
                        $requeteFAI .= " or m_fai='$unFai'";
                    }
                }


                //echo "<br> Pour$unFai --- ".round($unPour/$nbDays);
            }
            $requeteFAI = $requeteFAI.")";
        }

        //var_dump($pourcentageFAIS);

        if( isset($_POST["statutMail"]) ) $statut = $_POST["statutMail"];


        echo "<pre>";
        /* Etape 1: Requetes SQL _________________________________________________________________________________
        *																								=========
        *	RÃ©cupÃ©ration :																				========
        *		- de tous les mails de la base concernÃ©e (cdd ou pdf)									=======
        *		-	mails dÃ©jÃ  reÃ§u la campagne dans la semaine											======
        *		- 	mails qui ont dÃ©jÃ  reÃ§u pour les jours d'envoi concernÃ©e							=====
        *																								====
        *																								===
        * ______________________________________________________________________________________________==
        */

        //1Ã¨re requÃªte : RÃ©cupÃ©ration des mails en fonction de la base, et/ou de leur activitÃ©s(ouvreurs,cliqueurs)
        $requete1 = "SELECT m_mail,m_id FROM mail WHERE m_base = '$base' AND m_statut != 2 AND m_statut != 6";//AND m_statut != 2 AND m.m_last_ouv > '$ouv_date'
        $requete1 = $requete1 . $requeteFAI;
        if(isset($statut) && !empty($statut)) $requete1.=" AND m.m_statut > '$statut'";
        if(isset($ouv_date) && !empty($ouv_date)) $requete1.=" AND m.m_last_ouv > '$ouv_date'";
        $resultat = pg_query($requete1);
        echo "<br/> PremiÃ¨re requÃªte : <code class='language-sql'> $requete1 </code>";
        if(!$resultat) {echo " -- RÃ©sultat requÃªte 1 :  false"; exit;}
        $base_emails = pg_fetch_all($resultat);
        echo "<br/> Nombre de mails dans la base $base : ".count($base_emails);

        //Initialisation du nb d'envoi/J Ã  0 par dÃ©faut
        foreach($base_emails as $e)
        {
            $uni_emails[decrypter($e["m_mail"])]=0;
            $corrMailID[decrypter($e["m_mail"])]=$e["m_id"];
        }

        //On re-itere l'operation suivant la duree de l'envoi
        for($i=0;$i<count($dates_envois);$i++)
        {
            $retour_mails = array();
            //$mails_a_retourner = $uni_emails ;
            $mails_a_retourner = array();

            $mails_a_retourner = array_diff_key($uni_emails, $toExclude);

            $d = $dates_envois[$i];

            $newdate = strtotime ( '-7 days' , strtotime ( $dates_envois[$i] ) ) ;
            $date_c = date ('Y-m-d', $newdate);
            $retour_mails = array();
            $mails_today = array();

            //2Ã¨me requÃªte : RÃ©cupÃ©ration des mails qui ont dÃ©jÃ  recu la campagne dans la semaine par rapport Ã  la date d'envoi concernÃ©e
            $requete2 = "SELECT mail
		FROM temp t 
		WHERE 
		t.base = '$base' AND t.campagne = '$campagne' AND t.date_envoi > '$date_c'";

            /*$requete2 = "SELECT m_mail
            FROM mail m
            INNER JOIN temp t ON t.id_mail = m.m_id
            WHERE
            m.m_base = '$base' AND t.campagne = '$campagne' AND t.date_envoi > '$date_c'";*/


            $resultat = pg_query($requete2);
            //echo "<br/><br/> DeuxiÃ¨me requÃªte : <code class='language-sql'> $requete2 </code>";
            if(!$resultat) {echo " -- RÃ©sultat requÃªte 2 :  false"; exit;}
            $emails_already = pg_fetch_all($resultat);

            if($emails_already !== false)
            {
                $emails_already_count = count($emails_already);
                //On retire les mails qui ont deja reÃ§u des mails potentiels pour l'envoi
                foreach($emails_already as $unE)
                {
                    //$mail = decrypter($unE["m_mail"]);
                    $mail = decrypter($unE["mail"]);
                    unset($mails_a_retourner[$mail]);
                }
            }
            else { $emails_already_count = 0;}

            echo "<br/>Nombre de mails ayant dÃ©jÃ  reÃ§u la campagne : ".$emails_already_count;
            echo "<br/><br/> RÃ©sultat aprÃ¨s retrait des mails qui ont dÃ©jÃ  reÃ§u la campagne : ".count($mails_a_retourner);

            //3Ã¨me requÃªte : RÃ©cupÃ©ration des mails qui ont dÃ©jÃ  recu le(s) J d'envoi
            $requete3 = "SELECT mail, date_envoi FROM temp t
		WHERE 
		t.date_envoi = '$d'";// OR t.date_envoi='demain' si campagne programmee sur plusieurs j
            //echo "<br/>  3Ã¨me requÃªte : <code class='language-sql'> $requete3 </code>";
            $resultat = pg_query($requete3);
            if(!$resultat) {echo " -- RÃ©sultat requÃªte 3 :  false"; exit;}
            $mails_today = pg_fetch_all($resultat);

            if($mails_today === false )$mails_today_count = 0;
            else $mails_today_count = count($mails_today);

            echo "<br/> Mails Sent Today : ".$mails_today_count;

            //Ajout information du quota/J pour un mail
            foreach($mails_today as $mail)
            {
                $e = decrypter($mail["m_mail"]);
                if(empty($e)) continue;
                $mails_a_retourner[$e]+=1;
            }

            /* Etape 2: TRI / FAIS __________________________________________________________________________________
            *																								========
            *	Tri des mails en fonction des FAIS et du quota FAI/J (ex : @orange ne recoit que 3x/j)		=======
            *		-	fonction :		tri un tableau de mails en fonction du %/FAI et du nb/envoi/J		======
            *		- 	paramÃ¨tres :	3 tableaux (d'emails, de FAIs voulus, et de quantitÃ© de mails/FAI)	=====
            *							et le nombre total de mails souhaitÃ©								====
            *		- 	retour :		un tableau filtrÃ© selon les FAIs									===
            * ______________________________________________________________________________________________==
            */
            //var_dump($pourcentageFAIS);
            if($i == count($dates_envois)-1)
            {
                $countDays = $nbmails -( $countDays * $i );
            }

            //echo "<br> $i -- Count Days -- $countDays";
            $retour_mails = tri_fais($mails_a_retourner, $pourcentageFAIS, $countDays);

            //$retour_mails2 = tri_fais($mails_a_retourner, $pourcentageFAIS, $countDays);

            var_dump($retour_mails["infos"]);

            //exit;
            //var_dump($retour_mails['infos']);

            /* Etape 2: GÃ©nÃ©ration de liste _________________________________________________________________________
            *																								========
            *					Decoupage lineaire prealable en fonction de sender et FAIs					=======
            *																								======
            *						GENERATION D'UN FICHIER													=====
            *																								====
            *																								===
            * ______________________________________________________________________________________________==
            */
            $filename_base = "/home/web/tor.sc2consulting.fr/Emails/LISTES_BASES/GenEmails/$base/Liste_$timestamp-";;
            // on decoupe lineairement le tableau retour_mails de facon a avoir autant de fai dans un sender que dans un autre
            $filename = "/home/web/tor.sc2consulting.fr/Emails/LISTES_BASES/GenEmails/$base/Liste_$timestamp-$i.txt";
            $ecrire = fopen($filename, "w");

            foreach($retour_mails as $unF => $tabmails)
            {
                if($unF == 'infos') continue;

                $calcul = count($tabmails) / count($allsenders);
                $count = $calcul;

                $strr = strrpos($calcul, '.');
                if($strr !== false)
                {
                    $rajout = substr($calcul, $strr+1);
                    $count = round($calcul) + round($rajout);
                }

                $mailsDecoup = array_chunk($tabmails, $count);
                foreach($mailsDecoup as $n => $tabM)
                {
                    foreach($tabM as $unM)
                    {
                        $mval2 = $corrMailID[$unM];
                        if( array_key_exists($mval2, $toExclude) ) continue;
                        //$crypter = crypter($unM);
                        $crypter = $unM;
                        $toExclude[$unM]="";

                        if( !empty($mval2) && !empty($crypter)  )
                        {
                            fwrite($ecrire, $mval2.";".$startdate.";".$allsenders[$n].";".$crypter."\n");
                            $TOTAL_COUNT++;
                        }
                    }
                }
            }
            fclose($ecrire);
        }

        pg_close($dbconn);


        if($TOTAL_COUNT >= $nbmails) echo "<b style='font-size:14px; color:green'>";
        else echo "<b style='font-size:14px; color:red'>";
        echo "<br/><br/> Nombre de mails trouv&eacute;s : ".$TOTAL_COUNT."</b>";

        echo "</pre>";

        echo "<form action='GenEnvoi.php' method='post' >
			<input type='hidden' name='campagne' value='$campagne' />
			<input type='hidden' name='base' value='$base' />
			<input type='hidden' name='choixenvoi' value='$choixenvoi' />
			<input type='hidden' name='listFilename' value='$filename_base' />
			<input type='hidden' name='duree' value='$duree' />
			<input type='hidden' name='envoi' value='generation' />
			<input type='submit' name='validation' value='Valider g&eacute;n&eacute;ration' />	
		</form> <br/> <br/> ";

        //exit;
    }

    //exit;

    if(!is_null($_POST) && isset($_POST['validation']) && $_POST['envoi']='generation' && isset($_POST["listFilename"])  )
    {
        $choixenvoi = $_POST['choixenvoi'];
        $base_compte = $_POST['base'];
        /* Etape 2: GÃ©nÃ©ration de liste _________________________________________________________________________
        *																								========
        *																								=======
        *																								======
        *				GENERATION DANS LA BASE PG (table tmp)											=====
        *																								====
        *																								===
        * ______________________________________________________________________________________________==
        */
        $duree =  $_POST["duree"];
        $campagne = $_POST["campagne"];
        //remplacer le if par une boucle sur la duree de l'envoi; 1j/2j/3j
        /*
            if($choixenvoi=='instant' or $choixenvoi == 'today')
            {
        */
        $MailsBySenders = array();
        $indice=0;

        while($indice < $duree)
        {
            $base = $_POST["listFilename"];

            $filename = $base . "$indice.txt";
            //$base = $_POST["base"];
            $lire = fopen($filename, 'r');
            var_dump($filename);


            $template = "INSERT INTO temp (id_mail, campagne, sender, date_envoi, base, mail) VALUES %batch";
            $batchrows = 1;
            $batch ="";
            $batchsize=500;
            $numrows=0;


            if(!$lire){ echo "<br/> <b style='font-size:14px; color:red'> Oops : Echec de la validation, il faut refaire la g&eacute;n&eacute;ration et vaider. </b>"; exit;}

            while($ligne = fgets($lire) )
            {
                $explode = explode(';', $ligne);

                if( count($explode) < 3) continue;

                $id = $explode[0];
                $date = $explode[1];
                $sender = $explode[2];

                $mail = crypter(trim($explode[3]));

                //echo "MAIL TO INSERT -- $mail";
                //var_dump($explode);
                //exit;

                if($batchrows < $batchsize)
                {
                    $batch .= "($id, '$campagne', '$sender', '$date','$base_compte', '$mail'),";
                    $batchrows++;
                }
                elseif($batchrows == $batchsize)
                {
                    $batch .= "($id, '$campagne', '$sender', '$date','$base_compte','$mail'),";
                    $batch = substr($batch, 0, strlen($batch)-1); // On supprime la virgule
                    $query = str_replace("%batch", $batch, $template);
                    $retour = pg_query($query);
                    if(!$retour){ echo "<pre style='color:red'> Erreur d'association. </pre>"; exit; }
                    $batch = "";
                    $batchrows = 1;
                }

                if(!isset( $MailsBySenders[$sender][$date]))  $MailsBySenders[$sender][$date]=array();
                array_push($MailsBySenders[$sender][$date],$mail);

                $numrows++;

            }

            if(!empty($batch)) // Derniere requete en memoire
            {
                $batch = substr($batch, 0, strlen($batch)-1); // On supprime la virgule
                $query = str_replace("%batch", $batch, $template);
                $retour = pg_query($query);
                if(!$retour){ echo "<pre style='color:red'> Erreur d'association. </pre>"; exit; }
            }

            $indice++;

        }

        //}
        //unlink($filename);

        echo "<h4 style='font-size:14px; color:green'> La g&eacute;n&eacute;ration de liste a bien &eacute;t&eacute; prise en compte. </h4>";

        /*================================================
        ================================================
        ================================
        ================================================
        ================================================

        P_A_R_T_I_E 2 	: 	E_N_V_O_I_S

        ================================================
        ================================================
        ================================
        ================================================
        ================================================*/
        echo "<h2> Partie 2 -- Envoi (en cours de rÃ©alisation) </h2>";
        exit;

        //**********************************************
        //Creation de la campagne dans chaque compte
        //**********************************************
        foreach($MailsBySenders as $Sender => $MailsTab)
        {
            $recupPlate = "SELECT r_plateforme FROM routeurs WHERE r_compte = '$Sender'";
            $resultat = pg_query($recupPlate);
            $plateRes = pg_fetch_object($resultat);
            $plateforme = $plateRes->r_plateforme;

            //var_dump($MailsTab);

            //exit;

            if($plateforme == 'ediware')
            {
                echo "<br> It's Ediware";

                $location = "https://www.eml-srv.com/_soap/control.php";
                $uri = "https://www.eml-srv.com";

                if(strpos($Sender, '-'))
                {
                    echo " -- It's Smessage";
                    $location = "http://production.smessage.net/_soap/control.php";
                    $uri = "http://production.smessage.net";

                }

                foreach($MailsTab as $dateEnvoi => $Mails)
                {

                    echo "Date envoi -- $dateEnvoi -- ";

                    try
                    {
                        $client = new SoapClient(null,
                            array(
                                'location' => $location,
                                'uri'      => $uri,
                                'encoding'=>'ISO-8859-1'
                            ));

                        if($Sender != "sc2") $mwd = "Dg5436pax";
                        else $mwd = '6v3sMDW';

                        $variable = $client->Ajout_Campagne($Sender,$mwd);
                        //print_r($variable);
                        $id = $variable[0]["id"];

                    }
                    catch (SoapFault $fault)
                    {
                        trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
                    }

                    $dupli[0]='une ref';
                    $dupli[1]='un obj';
                    $dupli[2]="un expediteur";

                    $str_ref = strrpos($campagne,'_');
                    $reference = substr($campagne, 0,$str_ref);
                    $str_dos = strrpos($reference,'_');

                    $dupli[0] = $reference;

                    $dossier = substr($campagne, 0,$str_dos);
                    $deb = "/home/web/tor.sc2consulting.fr/Campagnes/$base_compte/$dossier/$reference/";
                    $deb2 = "http://ns3324481.ovh.net/tor.sc2consulting.fr/Campagnes/$base_compte/$dossier/$reference/";
                    $fileCamp = $deb.$campagne."_expe.txt";

                    $lireCampagne = fopen($fileCamp, 'r');

                    if(!$lireCampagne){ echo "<p style='color:red'> La campagne n'a pas Ã©tÃ© crÃ©Ã©. Les informations de la campagne n'ont pas Ã©tÃ© trouvÃ©. </p>"; exit;}

                    $numLi = 1;
                    while($ligne = fgets($lireCampagne))
                    {
                        //echo "$ligne <br/>";
                        if($numLi > 5 ) break;
                        if($numLi == 4 ) $dupli[2] = trim($ligne);
                        if($numLi == 5 ) $dupli[1] = trim($ligne);
                        $numLi++;
                    }


                    for ($p = 0 ; $p < 3 ; $p ++ )
                    {
                        try
                        {
                            $client = new SoapClient(null, array('location' => $location,

                                'uri'      => $uri,
                                'encoding'=>'ISO-8859-1'
                            ));
                            $variable = $client->Modif_Parametres_Campagne($id,$Sender,$mwd,$param[$p],$dupli[$p] );
                            //print_r($variable);
                        }
                        catch (SoapFault $fault)
                        {
                            trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
                        }
                    }

                    $message = $deb2 . $campagne."_MDW.html";

                    try
                    {
                        $client = new SoapClient(null, array('location' => $location,

                            'uri'      => $uri,
                            'encoding'=>'ISO-8859-1'
                        ));
                        $variable = $client->Modif_Message_Campagne($id,$Sender,$mwd,$message);
                        //print_r($variable);
                    }
                    catch (SoapFault $fault)
                    {

                        trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
                    }


                    foreach($Mails as $OneMail)
                    {
                        continue;
                        $ligne_mail=array($OneMail);

                        try {
                            $client = new SoapClient(null, array('location' => $location,
                                'uri'      => $uri,
                                'encoding'=>'ISO-8859-1'
                            ));
                            $variable = $client->Ajout_Adresse_Campagne($Sender,$mwd,$id,$ligne_mail);
                            //$variable=unserialize(rawurldecode($variable)); print_r($variable);
                        }
                        catch (SoapFault $fault)
                        {

                            trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
                        }
                    }
                    $insererEnvoi = "INSERT INTO envoi(e_id_campagne,e_date) VALUES ($id, '$dateEnvoi')";
                    $resultat = pg_query($insererEnvoi);
                }

            }

        }

        pg_close($dbconn);
        exit;

    }

    ?>
</div>
</center>
</body>
</html>