@extends('common.layout')

@section('content')

    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">
                    Mettre à jour un FAI
                </span>
            </div>
        </div>
        <div class="portlet-body">

            {!! Form::model($fai, array('method'=> 'PATCH', 'route' => array('fai.update', $fai->id), 'class'=>'form-horizontal')) !!}
            @include('fai.form')
            {!! Form::close() !!}

        </div>
    </div>

@endsection