@extends('template')

@section('content')

      <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">

                  <div class="x_title">
                    <h2>Pression marketing par FAI</h2>
                    <ul class="nav navbar-right panel_toolbox">

                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="row">

                      @if( \Auth::User()->user_group->name == 'Super Admin' || \Auth::User()->user_group->name == 'Admin' )
                          <div class="btn-group btn-group-devided">
                              <a title="Ajouter un FAI" class="btn btn-success" href="/fai/create"><i class="fa fa-plus" aria-hidden="true"></i></a>
                          </div>
                      @endif

                    </div>
                    <br>
                    <div class="row">

                      <table class="table table-bordered table-striped table-hover">
                          <tr>
                              <th>Nom</th>
                              <th>Pression marketing*</th>
                              <th>Maximum par envoi</th>
                              <th>Pourcentage par défaut par envoi</th>
                              <th>Actions</th>
                          </tr>
                      @foreach($fais as $f)
                          <tr>
                              <td>{{ $f->nom }}</td>
                              <td>{{ $f->quota_pression }}</td>
                              <td>{{ $f->quota_campagne }}</td>
                              <td>{{ $f->default_planning_percent }}</td>
                              <td>
                                  <a title="Modifier" class="btn btn-primary" href="/fai/{{$f->id}}/edit" !!}><i class="fa fa-pencil" aria-hidden="true"></i></a>
                              </td>
                              {{--<td>--}}
                                  {{--{!! Form::open(array('route' => array('fai.destroy', $f->id), 'method' => 'delete')) !!}--}}
                                  {{--<button class="btn default red" type="submit">Supprimer</button>--}}
                                  {{--{!! Form::close() !!}--}}
                              {{--</td>--}}
                          </tr>
                      @endforeach
                      </table>
                  </div>
                  <i>*Nombre maximum par jour de campagnes différentes que peut recevoir une adresse</i>

                  </div>


      </div>
      </div>
      </div>

          @endsection
