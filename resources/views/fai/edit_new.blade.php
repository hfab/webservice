@extends('template')

@section('content')

      <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">

                  <div class="x_title">
                    <h2>Modifier Pression Marketing</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="row">


                    </div>
                    <br>
                    <div class="row">

                      {!! Form::model($fai, array('method'=> 'PATCH', 'route' => array('fai.update', $fai->id), 'class'=>'form-horizontal')) !!}
                      @include('fai.form')
                      {!! Form::close() !!}

                  </div>


      </div>
      </div>
      </div>

          @endsection
