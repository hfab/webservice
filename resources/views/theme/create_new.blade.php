@extends('template')

@section('content')

      <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">

                  <div class="x_title">
                    <h2>Ajouter un thème</h2>
                    <ul class="nav navbar-right panel_toolbox">

                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br>
                    <div class="row">

                      {!! Form::model(new \App\Models\Theme, array('route' => array('theme.store'), 'method'=> 'post')) !!}
                      @include('theme.form')
                      {!! Form::close() !!}


                  </div>


      </div>
      </div>
      </div>

          @endsection
