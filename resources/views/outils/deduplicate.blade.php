@extends('common.layout')

@section('content')

    <h1>Déduplication</h1>

    <h2>Importer un fichier</h2>

    <form id="formUpload" class="form-horizontal">
        <div class="form-group">
            <label class="col-sm-3 control-label">Base destination :</label>
            <div class="col-sm-9">
                {!! Form::select('base_id', $bases, null, ['id'=>'base_id', 'class'=>'form-control'] ) !!}
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-9 col-sm-offset-3">
                <button id="import" class="btn btn-danger" href="">Importer</button>
            </div>
        </div>

        <div class="form-group" id="upload-progress">
            <label class="col-sm-3 control-label">Upload en cours...</label>
            <div class="col-sm-9">
                <div class="fileupload-process">
                    <div id="total-progress" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                        <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                    </div>
                </div>
            </div>
        </div>

        <input type="hidden" id="_token" value="{{ csrf_token() }}" />
    </form>


@endsection

@section('footer')

    <script src="/js/dropzone.js"></script>

    <script>
        var myDropzone = new Dropzone(document.body, {
            url: "/blacklist/import",
            acceptedFiles:".csv,.txt",
            createImageThumbnails: false,
            parallelUploads: 1,
            previewTemplate: '<div style="display:none"></div>',
            autoQueue: true,
            clickable: "#import",
            init: function() {
                this.on("success", function(file, responseText) {
                    var response = JSON.parse(responseText);

                    toastr['success']('Déduplcation réalisée');
                });
            }
        });

        myDropzone.on("totaluploadprogress", function(progress) {
            document.querySelector("#total-progress .progress-bar").style.width = progress + "%";
            if (progress == 100) {
                toastr['success']('Dédup en cours.');
            }
        });

        myDropzone.on("sending", function(file, xhr, formData) {
            formData.append("base_id", $('#base_id').val());
            formData.append("_token", $('#_token').val());
            document.querySelector("#upload-progress").style.opacity = "1";
        });

        myDropzone.on("queuecomplete", function(progress) {
            document.querySelector("#upload-progress").style.opacity = "0";
        });

        $('#import').click(function(e) {
            e.preventDefault();
        })

        $('#formUpload').submit(function() {
            return false;
        })

    </script>

@endsection
