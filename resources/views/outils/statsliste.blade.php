@extends('common.layout')

@section('content')

<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs font-green-sharp"></i>
            <span class="caption-subject font-green-sharp bold uppercase">Statistiques des listes</span>
        </div>
        <div class="actions">
          <div class="btn-group btn-group-devided">
              <a href="/outils/listemanager"><button class="btn btn-primary" href="">Menu ListManager</button></a>
          </div>
        </div>
    </div>
    <div class="portlet-body">

      {{-- varstats --}}
      <div class="row">
        Nombre d'adresses Free : {{$nombrefree}}
      </div>
      <div class="row">
        Nombre d'adresses SFR : {{$nombresfr}}
      </div>
      <div class="row">
        Nombre d'adresses LaPoste : {{$nombrelaposte}}
      </div>
      <div class="row">
        Nombre d'adresses Hotmail : {{$nombrehotmail}}
      </div>
      <div class="row">
        Nombre d'adresses Yahoo : {{$nombreyahoo}}
      </div>
      <div class="row">
        Nombre d'adresses Aol : {{$nombreaol}}
      </div>
      <div class="row">
        Nombre d'adresses Gmail : {{$nombregmail}}
      </div>
      <div class="row">
        Nombre d'adresses Orange : {{$nombreorange}}
      </div>
      <div class="row">
        Nombre d'adresses Autre : {{$nombreautre}}
      </div>

      <div class="row">
        Nombre d'adresses Total : {{$totalmail}}
      </div>

    </div>
</div>
</form>


@endsection

@section('footer')


@endsection
