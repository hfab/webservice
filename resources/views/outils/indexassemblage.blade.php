@extends('template')

@section('content')
     <div class="row">
         <div class="col-xs-12">
               <i class="fa fa-cogs fa-2x text-success"></i>
               <span class="lead text-success">Assembler fichiers liste</span>
               <br>
               <br>
          </div>
          <div class="col-xs-12">
               <a href="/outils/listemanager"><button class="btn btn-primary" href="">Menu ListManager</button></a>
               <hr>
          </div>

          <div class="col-xs-12">
               {!! Form::open(array('url' => 'outils/listemanager/assembler','files' => true, 'method' => 'post')) !!}

               <div class="col-xs-12 form-group">
                    <label for="listename">Nom de la liste :</label>
                    <input type="text" class="form-control" id="listename" name="listename">
               </div>
               @foreach ($fichiers as $fichier)
                    <div class="checkbox">
                         <label><input type="checkbox" value="{{basename($fichier)}}" name="file[]">{{\File::name($fichier)}}</label>
                         <br>
                    </div>
               @endforeach
               <hr>
               <div class="col-xs-12">
                    <button type="submit" class="btn btn-success">Assembler</button>
                    {!! Form::close() !!}
               </div>
          </div>
     </div>
@endsection

@section('footer')
@endsection
