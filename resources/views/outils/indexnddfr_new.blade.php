@extends('template')

@section('content')
     <div class="row">
         <div class="col-xs-12">
               <i class="fa fa-cogs fa-2x text-success"></i>
               <span class="lead text-success">Doublon fichiers liste</span>
               <br>
               <br>
          </div>
          <div class="col-xs-12">
               <a href="/outils/listemanager"><button class="btn btn-primary" href="">Menu ListManager</button></a>
               <hr>
          </div>

          <div class="col-xs-12">
               {!! Form::open(array('url' => 'outils/listemanager/nddfr','files' => true, 'method' => 'post')) !!}
               @foreach ($fichiers as $fichier)
                    <div class="checkbox">
                         <label><input type="radio" value="{{basename($fichier)}}" name="file">{{\File::name($fichier)}}</label>
                         <br>
                    </div>
               @endforeach
               <hr>
          </div>
          <div class="col-xs-12">
               <button type="submit" class="btn btn-success">Supprimer les étrangers</button>
               {!! Form::close() !!}
          </div>
     </div>
@endsection

@section('footer')
@endsection
