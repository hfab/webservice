@extends('template')

@section('content')

     <div class="row">
          <div class="col-xs-12">
               <i class="fa fa-cogs fa-2x text-success"></i>
               <span class="lead text-success">Traiter un fichier</span>
               <br>
               <br>
          </div>
          <div class="col-xs-12">
               <a href="/outils/listemanager"><button class="btn btn-primary" href="">Menu ListManager</button></a>
               <br>
          </div>
          <hr>
          <div class="col-xs-12">
               {!! Form::open(array('url' => 'outils/listemanager/modifier','files' => true, 'method' => 'post')) !!}
               @foreach ($fichiers as $fichier)
                    <div class="checkbox">
                         <label><input type="radio" value="{{basename($fichier)}}" name="file[]">{{\File::name($fichier)}}</label>
                    </div>
               @endforeach
               <hr>
               <div class="col-xs-12">
                    <label><input type="radio" value="supprimeradresse" name="choix"> Supprimer une adresse</label>
               </div>
               <div class="col-xs-12">
                    <label><input type="radio" value="supprimerfai" name="choix"> Supprimer un FAI</label>
               </div>
               <div class="col-xs-12">
                    <label><input type="radio" value="extrairefai" name="choix"> Extraire un FAI</label>
               </div>
               <div class="col-xs-12">
                    <label><input type="radio" value="filtrerliste" name="choix"> Filtrer / autre fichier </label>
               </div>
               <div class="col-xs-12">
                    <label><input type="radio" value="filtrerlistedb" name="choix"> Filtrer / BDD</label>
               </div>
               <div class="col-xs-12">
                    <label><input type="radio" value="filtrerblacklistglobal" name="choix"> Filtrer / blacklist</label>
               </div>
               <div class="col-xs-12">
                    <label><input type="radio" value="convertmd5" name="choix"> Convertir au format MD5</label>
               </div>
               <div class="col-xs-12">
                    <br>
                    <button type="submit" class="btn btn-success">Modifier la liste</button>
                    {!! Form::close() !!}
               </div>
          </div>
     </div>
@endsection
@section('footer')
@endsection
