@extends('common.layout')

@section('content')

<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs font-green-sharp"></i>
            <span class="caption-subject font-green-sharp bold uppercase">Modifier une liste</span>
        </div>
        <div class="actions">
          <div class="btn-group btn-group-devided">
              <a href="/outils/listemanager"><button class="btn btn-primary" href="">Menu ListManager</button></a>
          </div>
        </div>
    </div>
    <div class="portlet-body">

      <div class="form-group">
        <label for="listename">{{implode($fichier)}}</label>
      </div>

      {!! Form::open(array('url' => 'outils/listemanager/modifier','files' => true, 'method' => 'post')) !!}

      <div class="checkbox">
        <label><input type="radio" value="orange" name="lefai">Orange / Wanadoo</label>
      </div>
      <div class="checkbox">
        <label><input type="radio" value="free" name="lefai">Free</label>
      </div>
      <div class="checkbox">
        <label><input type="radio" value="sfr" name="lefai">SFR / Neuf</label>
      </div>
      <div class="checkbox">
        <label><input type="radio" value="laposte" name="lefai">Laposte</label>
      </div>
      <div class="checkbox">
        <label><input type="radio" value="aol" name="lefai">Aol</label>
      </div>
      <div class="checkbox">
        <label><input type="radio" value="gmail" name="lefai">Gmail</label>
      </div>
      <div class="checkbox">
        <label><input type="radio" value="hotmail" name="lefai">Hotmail</label>
      </div>
      <div class="checkbox">
        <label><input type="radio" value="yahoo" name="lefai">Yahoo</label>
      </div>

        <hr>

        <input type="hidden" name="choix" value="faiparam">
        <input type="hidden" name="varfichier" value="{{implode($fichier)}}">
       <button type="submit" class="btn btn-success">Valider</button>
      {!! Form::close() !!}

    </div>
</div>
</form>


@endsection

@section('footer')


@endsection
