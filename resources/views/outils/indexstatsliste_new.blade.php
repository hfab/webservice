@extends('template')

@section('content')
     <div class="row">
          <div class="col-xs-12">
               <i class="fa fa-cogs fa-2x text-success"></i>
               <span class="lead text-success">Statistiques des listes</span>
               <br>
               <br>
          </div>
          <div class="col-xs-12">
               <div class="btn-group btn-group-devided">
                   <a href="/outils/listemanager"><button class="btn btn-primary" href="">Menu ListManager</button></a>
               </div>
               <hr>
          </div>
          <div class="col-xs-12">
               {!! Form::open(array('url' => 'outils/listemanager/statliste','files' => true, 'method' => 'post')) !!}
               @foreach ($fichiers as $fichier)
                    <div class="col-xs-12">
                         <label><input type="radio" value="{{basename($fichier)}}" name="file[]">{{\File::name($fichier)}}</label>
                         <br>
                    </div>
               @endforeach               
               <div class="col-xs-12">
                    <hr>
                    <button type="submit" class="btn btn-success">Consulter statistiques</button>
                    {!! Form::close() !!}
               </div>
          </div>
     </div>
@endsection

@section('footer')
@endsection
