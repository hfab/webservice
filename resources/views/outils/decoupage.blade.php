@extends('common.layout')

@section('content')

<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs font-green-sharp"></i>
            <span class="caption-subject font-green-sharp bold uppercase">Découper une liste d'email</span>
        </div>
        <div class="actions">
            <div class="btn-group btn-group-devided">
                <a href="/outils/listemanager"><button class="btn btn-primary" href="">Menu ListManager</button></a>
            </div>
        </div>
    </div>
    <div class="portlet-body">

      <div class="portlet-body">
        <table class="table table-striped table-bordered">
          <tr>
              <th>Nom fichier</th>
              <th>Action</th>
          </tr>

      @foreach ($fichierdecoupe as $fichier)

      <tr>
        <td>{{basename($fichier)}}</td>
        <td>
          <a href="supprimer/{{basename($fichier)}}"><span class="label label-danger">Supprimer</span></a> -
          <a href="telecharger/{{basename($fichier)}}"><span class="label label-success">Télécharger</span></a>
        </td>
      </tr>

      @endforeach
      </table>
    </div>
</div>
</form>


@endsection

@section('footer')


@endsection
