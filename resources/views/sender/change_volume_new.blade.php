@extends('template')

@section('content')

    <div class="container">
         <div class="row">
              <div class="col-xs-12">
                    <i class="fa fa-2x fa-cogs text-success"></i>
                    <span class="lead text-success">
                         Modifier le volume de plusieurs Senders
                    </span>
                    <div class="btn-group btn-group-devided">
                         <a title="Retour" href="/sender" class="btn btn-success">Retour</a>
                    </div>
               </div>
          </div>
          <br>
          <div class="row">
               <div class="col-xs-12">
                    @foreach ($routeurinfo as $routeur)
                         <a href="/sender/changevolume/{{$routeur->id}}" class="btn btn-primary">{{$routeur->nom}}</a>
                    @endforeach
               </div>
          </div>
          <br>
          <div class="row">
               {!! Form::open(array('url' => 'sender/changevolume')) !!}
               <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    {!! Form::label('Choisir un FAI', null, ['class' => 'control-label']) !!}
                    {!! Form::select('fai_id', $faiselect, null, ['class' => 'form-control', 'id' => 'selectfai']) !!}
               </div>
               <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    {!! Form::label('Choisir le volume pour le FAI', null, ['class' => 'control-label']) !!}
                    {!! Form::text('fai_volume', '0', array_merge(['class' => 'form-control'])) !!}
               </div>
               <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    {!! Form::label('Choisir la capacité quotidienne', null, ['class' => 'control-label']) !!}
                    {!! Form::text('volume_global', '0', array_merge(['class' => 'form-control'])) !!}
               </div>
          </div>
          <br>
          <br>
          <table class="table table-hover table-advance table-striped">
               <tr>
                    <th width="2%">ID</th>
                    <th>Routeur</th>
                    <th>Nom</th>
                    <th>From</th>
                    <th>Informations</th>
                    <th>Choisir</th>
               </tr>

               @foreach($senders as $sender)
                    <tr>
                         <td>{{ $sender->id }}</td>
                         @if($tri)
                         <td>{{ $routeurtableau->nom }}</td>
                         @else
                         <td>{{ $sender->routeur->nom }}</td>
                         @endif
                         <td>{{ $sender->nom }}</td>
                         <td>{{ $sender->domaine }}</td>
                         <td>Volume Global : {{$sender->quota}} <br>
                             <?php
                                   $info_fai = \DB::table('fai_sender')->where('sender_id',$sender->id)->get();
                                   foreach ($info_fai as $i) {
                                   $le_fai = \DB::table('fais')->where('id',$i->fai_id)->first();
                                   echo $le_fai->nom . ' : ' ;
                                   echo $i->quota . '<br />';
                                   }
                              ?>
                         </td>
                         <td>{!! Form::checkbox('sender_id[]', $sender->id) !!}</td>
                    </tr>
                @endforeach
          </table>
          <button type="submit" class="btn btn-success" id="valider" style="margin-left:0px;">Modifier volume</button>
          {!! Form::close() !!}
     </div>

     <!-- <script>
     // de base le bouton est grisé
     $("#valider").attr("disabled", true);
     // si 1 val != 0 je grise val 2 je degrise le bouton submit
     // si 2 val!= 0 je grise val 1 je degrise le bouton submit
     </script> -->
     <!-- <script>
     function verif(){
     var valfai   = document.getElementById("selectfai").value;
     if(valfai != ''){
     $("#envoi").attr("disabled", false);
     }    </script> -->
@endsection
