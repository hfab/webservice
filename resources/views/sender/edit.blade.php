@extends('common.layout')

@section('content')

    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">
                    Editer un Sender
                </span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                    @if($sender->routeur->nom == 'Mindbaz')
                        <a href="/sender/{{$sender->id}}/lists" class="btn red">Listes</a>
                    @endif
                </div>
            </div>
        </div>
        <div class="portlet-body">
          <?php
              $pattern = '/error/';
              if(preg_match($pattern, $_SERVER['REQUEST_URI'])){
                  echo '<div class="alert alert-dismissable alert-danger">
                  <strong> Il y a une erreur dans la modification du routeur Maildrop </strong>
                  </div>';
              }
          ?>

            {!! Form::model($sender, array('method' => 'PATCH', 'route' => array('sender.update', $sender->id), 'class'=>'form-horizontal')) !!}

            @include('sender.partial-fields')

            <button class="col-md-offset-2 btn btn-primary">Sauvegarder</button>

            {!! Form::close() !!}
        </div>
    </div>

@endsection
