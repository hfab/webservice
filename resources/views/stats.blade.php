@extends('common.layout')

@section('content')
    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">Tokens</span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                    <a href="/stats/routeur" class="btn btn-danger">Statistiques des campagnes</a>
                </div>
            </div>
        </div>
        <div class="portlet-body">
            <table class="table table-hover table-bordered table-striped">
                <tr>
                    <th>Global</th>
                    <th width="15%">Total</th>
                    <th width="15%">Utilisés</th>
                    <th width="15%">Restant</th>
                    <th>%</th>
                </tr>

                <tr>
                    <td>Aujourd'hui</td>

                    <td>{{ big_number($tokensTotal) }}</td>
                    <td>{{ big_number($tokensUsed->total_used) }}</td>
                    <td>{{ big_number($tokensTotal - $tokensUsed->total_used) }}</td>
                    <td>
                      <?php
                        if ($tokensTotal == 0) {
                            $percent = 0;
                        } else {
                            $percent = round( ($tokensTotal - $tokensUsed->total_used) * 100 / $tokensTotal);
                        }
                        if ($percent > 50) {
                            $class = "success";
                        } elseif ($percent <= 50 && $percent > 25) {
                            $class = 'warning';
                        } else {
                            $class = 'danger';
                        }
                        ?>
                        <div class="progress">
                            <div class="progress-bar progress-bar-{{$class}}" role="progressbar" aria-valuenow="{{ $tokensTotal - $tokensUsed->total_used }}"
                                 aria-valuemin="0" aria-valuemax="{{ $tokensTotal }}" style="width:{{ $percent }}%">
                            </div>
                        </div>

                    </td>
                </tr>
            </table>
            <table class="table table-hover table-bordered table-striped">
                <tr>
                    <th>Par base</th>
                    <th>Tokens totaux</th>
                    <th>Tokens utilisés</th>
                    <th>Tokens disponibles</th>
                    <th>Action</th>
                    <th width="15%">%</th>
                </tr>

                @foreach($statstokens as $stats)
                <tr>
                    <td>
                     <?php
                        $basenom = \DB::table('bases')->where('id',$stats->base_id)->first();
                        // var_dump();
                        echo $basenom->nom;
                     ?>
                   </td>

                    <td>
                        {{ big_number($stats->volumetotal) }}
                    </td>
                    <td>
                        {{ big_number($stats->volumeused) }}
                    </td>
                    <td>
                        {{ big_number($stats->volumetotal - $stats->volumeused) }}
                    </td>
                    <td>
                      <a href='stats/fai/{{$stats->base_id}}'><button type="button" class="btn btn-primary btn-xs">Statistiques par FAI</button></a>
                    </td>

                    <td>

                      <?php

                      $restant = $stats->volumetotal - $stats->volumeused;
                      if ($stats->volumetotal == 0) {
                          $percent = 0;
                      } else {
                          $percent = round($restant * 100 / $stats->volumetotal);
                      }
                      if ($percent > 50) {
                          $class = "success";
                      } elseif ($percent <= 50 && $percent > 25) {
                          $class = 'warning';
                      } else {
                          $class = 'danger';
                      }
                      ?>

                      <div class="progress">
                          <div class="progress-bar progress-bar-{{$class}}" role="progressbar" aria-valuenow="{{ $restant }}"
                               aria-valuemin="0" aria-valuemax="{{ $stats->volumetotal }}" style="width:{{ $percent }}%">
                          </div>
                      </div>

                    </td>
                </tr>
                @endforeach
            </table>

        </div>
    </div>
    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">Routeurs</span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                    <a href="/stats/routeurs" class="btn btn-success btn-xs">+ de détails</a>
                </div>
            </div>
        </div>
        <div class="portlet-body">
            <table class="table table-hover table-bordered table-striped">
                <tr>
                    <th>Date - {{ date('Y-m-d') }} - Données sur les dernières 24 heures</th>
                    <th width="20%">Routeur</th>
                    <th width="40%">Total routé</th>
                </tr>

                @foreach($planningsrouteurs_24 as $pr)
                <tr>
                    <td></td>
                    <td>{{$pr->routeur->nom}}</td>
                    <td>{{big_number($pr->total)}}</td>
                </tr>
                @endforeach

            </table>
        </div>
    </div>
    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">Ouvertures</span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                    <a href="/stats/ouvertures" class="btn btn-success btn-xs">+ de détails</a>
                </div>
            </div>
        </div>
        <div class="portlet-body">
            <table class="table table-hover table-bordered table-striped">
                <tr>
                    <th>Date - {{ date('Y-m-d') }} - Données sur les dernières 24 heures</th>
                    <th width="10%">Ouvertures</th>
                </tr>

                <tr>
                    <td></td>
                    @if(isset($ouv_24))
                    <td width="20%">{{ big_number($ouv_24->count) }} </td>
                    @else
                    <td width="20%"> - </td>
                    @endif
                </tr>

            </table>
        </div>
    </div>
    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">Erosion</span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                    <a href="/stats/optout" class="btn btn-success btn-xs">+ de détails</a>
                </div>
            </div>
        </div>
        <div class="portlet-body">
            <table class="table table-hover table-bordered table-striped">
                <tr>
                    <th>Date - {{ date('Y-m-d') }}</th>
                    <th width="10%">Total inactifs </th>
                </tr>

                <tr>
                    <td></td>
                    <td width="20%">{{ big_number($optout_24->total_optout) }}</td>



                </tr>

            </table>
        </div>
    </div>
@endsection
