<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableIpBl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('ip_check', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('sender_id');
          $table->string('ip');
          $table->string('hostname');
          $table->string('provider_bl');
          $table->string('date_maj');
          $table->string('type_check')->nullable();

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ip_check');
    }
}
