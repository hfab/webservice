<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatsMindbazTotalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stats_mindbaz_total', function (Blueprint $table) {
            $table->increments('id');

            $table->string('reference');
            $table->integer('total_sent');
            $table->integer('openers');
            $table->integer('clickers');
            $table->integer('soft_bounces');
            $table->integer('hard_bounces');
            $table->integer('spam_bounces');
            $table->integer('spam_complaints');
            $table->integer('unsubscribers');
            $table->date('date_maj');
            $table->integer('bloc_maj');
//            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stats_mindbaz_total');
    }
}
