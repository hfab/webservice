<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampagneTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

        Schema::create('campagnes', function($table)
        {
            $table->increments('id');

            $table->text('html')->nullable();
            $table->integer('base_id');
            $table->integer('theme_id');

            $table->string('nom');
            $table->date('date_crea');
            $table->string('ref');
            $table->string('expediteur');
            $table->string('objet');
            $table->string('cle');

            $table->text('toptext')->nullable();
            $table->text('bottomtext')->nullable();
            $table->text('belowtext')->nullable();
            $table->text('deletetext')->nullable();

            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('campagnes');
	}

}
