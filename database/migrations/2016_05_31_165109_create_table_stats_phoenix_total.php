<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableStatsPhoenixTotal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('stats_phoenix_total', function (Blueprint $table) {
          $table->increments('id');

          $table->integer('volume_total');
          $table->integer('ouvreurs');
          $table->integer('cliqueurs');
          $table->integer('erreurs');
          $table->integer('desabo');
          $table->integer('date_maj');
          $table->integer('bloc_maj');
          // $table->timestamps();
          $table->string('reference');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stats_phoenix_total');
    }
}
