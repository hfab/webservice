<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyColumnMailIdInRepoussoirsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('repoussoirs', function (Blueprint $table) {
            \DB::statement("ALTER TABLE repoussoirs CHANGE COLUMN mail_id destinataire_id INT NOT NULL");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('repoussoirs', function (Blueprint $table) {
            //
        });
    }
}
