<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPlanning extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plannings', function($table) {
            $table->dropColumn('is_prepared');
            $table->dropColumn('is_sent');
            $table->dropColumn('is_tokens');
            $table->dropColumn('is_senders');
            $table->dropColumn('is_uploaded');

            $table->datetime('tokens_at')->nullable()->default(null);
            $table->datetime('segments_at')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
