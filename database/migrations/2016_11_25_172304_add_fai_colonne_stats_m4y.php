<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFaiColonneStatsM4y extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('stats_m4y', function (Blueprint $table) {

          $table->integer('nombrebfree')->default(null);
          $table->integer('nombrebsfr')->default(null);
          $table->integer('nombreblaposte')->default(null);
          $table->integer('nombrebautre')->default(null);

          $table->integer('nombretfree')->default(null);
          $table->integer('nombretsfr')->default(null);
          $table->integer('nombretlaposte')->default(null);
          $table->integer('nombretautre')->default(null);

          $table->float('aboutis_free',6,2)->default(null);
          $table->float('aboutis_sfr',6,2)->default(null);
          $table->float('aboutis_laposte',6,2)->default(null);
          $table->float('aboutis_autre',6,2)->default(null);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
