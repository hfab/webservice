<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOuvV2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('ouv_api', function (Blueprint $table) {
          $table->increments('id');

          $table->string('mail');
          $table->integer('sender_id');
          $table->integer('campagne_id');
          $table->integer('theme_id');

          $table->timestamps();

          $table->unique(['campagne_id', 'mail']);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ouv_api');
    }
}
