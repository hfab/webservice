<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewParameterToSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('settings', function (Blueprint $table) {
            $table->index(['parameter']);
        });

        \DB::table('settings')->insert(['parameter'=>'app_major_version', 'value' => '1']);
        \DB::table('settings')->insert(['parameter'=>'app_minor_version', 'value' => '2']);
        \DB::table('settings')->insert(['parameter'=>'app_patch_version', 'value' => '12']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
