<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEtatEnvoiM4Y extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('etatenvoim4y', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('planning_id');
          $table->integer('sender_id');
          $table->integer('campagne_id');
          $table->integer('cid_routeur');
          $table->integer('bloc_maj');
          $table->integer('tosend');
          $table->integer('send');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('etat_envoi_m4y');
    }
}
