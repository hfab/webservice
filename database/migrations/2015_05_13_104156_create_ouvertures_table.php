<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOuverturesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('ouvertures', function($table)
        {
            $table->increments('id');

            $table->integer('campagne_id');
            $table->integer('destinataire_id');

            $table->text('ip')->nullable();

            $table->boolean('isMobile');
            $table->boolean('isTablet');
            $table->boolean('isDesktop');

            $table->string('browserFamily');
            $table->string('browserVersionMajor');
            $table->string('browserVersionMinor');
            $table->string('browserVersionPatch');

            $table->string('osFamily');
            $table->string('osVersionMajor');
            $table->string('osVersionMinor');
            $table->string('osVersionPatch');

            $table->string('deviceFamily');
            $table->string('deviceModel');

            $table->string('mobileGrade');
            $table->string('cssVersion');
            $table->string('javaScriptSupport');

            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
            Schema::drop('ouvertures');
	}

}
