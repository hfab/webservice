<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLastStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('last_stats', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('planning_id');
            $table->integer('sender_id');
            $table->string('type'); // si bounces, desinscsrits, clics, ouvreurs, etc..
            // PERMET DE RECUPERER SEULEMENT A PARTIR DE LA DERNIERE OCCURENCE RECUPEREE (pour MD pr le moment)
            $table->string('since_id');
            $table->timestamps();
            
            $table->unique(['planning_id','sender_id','type']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('last_stats');
    }
}
