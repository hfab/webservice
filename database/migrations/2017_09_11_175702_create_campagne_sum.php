<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampagneSum extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

      // a supprimer après ??.
      Schema::create('campagnes_ca_total', function (Blueprint $table) {

        $table->increments('id');
        $table->string('nom_campagne');
        $table->integer('base_id')->nullable();
        $table->float('ca_brut');
        $table->float('ca_net');
        $table->text('commentaire')->nullable();

        $table->integer('periode_compta');
        $table->integer('bloc_maj'); // gerer ca

        $table->timestamps();

        // bloc maj

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('campagnes_ca_total');
    }
}
