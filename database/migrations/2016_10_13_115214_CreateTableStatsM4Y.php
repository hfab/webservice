<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableStatsM4Y extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('stats_m4y', function (Blueprint $table) {

          $table->increments('id');
          $table->integer('nb_send');
          $table->integer('desinscriptions');
          $table->integer('ouvreurs');
          $table->integer('cliqueurs');
          $table->integer('soft_bounces');
          $table->integer('hard_bounces');
          $table->integer('date_maj');
          $table->integer('bloc_maj');
          $table->string('reference');
          $table->string('planning_id');
          // $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stats_m4y');
    }
}
