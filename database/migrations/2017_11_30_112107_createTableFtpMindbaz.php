<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableFtpMindbaz extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('ftp_mindbaz', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('sender_id');
          $table->text('ftp_host');
          $table->text('ftp_login');
          $table->text('ftp_pass');
          $table->text('ftp_folder');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ftp_mindbaz');
    }
}
