<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumnsToRouteursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('routeurs', function (Blueprint $table) {
            $table->string('variable_unsubscribe');
            $table->string('variable_mirror');
            $table->string('variable_email_sha1')->default('');
            $table->string('variable_email_md5')->default('');
            $table->string('variable_prenom')->default('');
            $table->string('variable_nom')->default('');
            $table->string('variable_tor_id')->default('');
        });

        $routeurs = \App\Models\Routeur::all();

        foreach($routeurs as $r)
        {
            if($r->nom == 'Ediware')
            {
                $r->variable_unsubscribe = "@@@";
                $r->variable_mirror = "&&&";
                $r->variable_email_sha1 = "";
                $r->variable_email_md5 = "";
                $r->variable_prenom = "%prenom%";
                $r->variable_nom = "%nom%";
                $r->variable_tor_id = "%p1%";
                $r->save();
            }

            if($r->nom == 'Maildrop')
            {
                $r->variable_unsubscribe = '{$Unsubscribe_Link}';
                $r->variable_mirror = '{$Web_View_Link}';
                $r->variable_email_sha1 = '{$Member.email|sha1}';
                $r->variable_email_md5 = '{$Member.email|md5}';
                $r->variable_prenom = '{$Member.first_name}';
                $r->variable_nom = '{$Member.last_name}';
                $r->variable_tor_id = '{$Member.tor_ID}';
                $r->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}