<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePlanningGeoloc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('planning_geoloc', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('base_id')->nullable();
            $table->string('departement')->nullable();
            $table->integer('nombre_total_mail')->nullable();
            $table->integer('nombre_select_mail')->nullable();
            $table->integer('nombre_restant')->nullable();
            $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('planning_geoloc');
    }
}
