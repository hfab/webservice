<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableStatsMaildrop extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('stats_maildrop', function (Blueprint $table) {
          $table->increments('id');

          $table->integer('id_maildrop');
          $table->integer('desinscriptions');
          $table->integer('ouvreurs');
          $table->integer('cliqueurs');
          $table->integer('erreurs');
          $table->integer('date_maj');
          $table->integer('bloc_maj');
          // $table->timestamps();
          $table->string('reference');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stats_maildrop');
    }
}
