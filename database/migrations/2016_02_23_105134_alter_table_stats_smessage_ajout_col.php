<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableStatsSmessageAjoutCol extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('stats_smessage', function (Blueprint $table) {
          $table->integer("desinscrits");
      });

      Schema::table('stats_smessage_total', function (Blueprint $table) {
          $table->integer("desinscrits");
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
