<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMailForYou extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      \DB::statement("INSERT INTO routeurs (nom, created_at, updated_at, variable_email, variable_unsubscribe, variable_mirror) VALUES ('MailForYou','2016-09-15 12:00:00','2016-09-15 12:00:00','[Email]','[Balise_Desabonnement]','[Balise_Message_En_Ligne]')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
