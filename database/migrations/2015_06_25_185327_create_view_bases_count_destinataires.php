<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewBasesCountDestinataires extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("create DEFINER = ".env('DB_USERNAME')." view bases_count_destinataires as select b.id as base_id,count(*) as num from bases b left join destinataires d on b.id = d.base_id group by b.id");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement('drop view bases_count_destinataires');
    }
}
