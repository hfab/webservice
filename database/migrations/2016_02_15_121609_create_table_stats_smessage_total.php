<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableStatsSmessageTotal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('stats_smessage_total', function (Blueprint $table) {

        $table->increments('id');

        $table->integer('id_smessage');
        /*
        $table->string('date_creation');
        $table->string('date_envoi');
        */
        $table->integer('total_mail');

        $table->integer('npai');
        $table->integer('npai_soft');
        $table->integer('ouvreurs');
        $table->integer('cliqueurs');
        $table->integer('inactifs');
        $table->integer('date_maj');
        $table->integer('bloc_maj');
        $table->string('reference');
        $table->string('date_creation');

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stats_smessage_total');
    }
}
