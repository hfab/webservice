<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Createcampagnecabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('campagnes_ca_base', function (Blueprint $table) {

        $table->increments('id');
        $table->integer('base_id');
        $table->float('ca_brut');
        $table->float('ca_net');
        $table->integer('periode_compta');

        $table->integer('base_volume_total');
        $table->float('base_cout_total');
        // rejouter cummul des volumes
        $table->timestamps();

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('campagnes_ca_base');
    }
}
