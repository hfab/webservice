<?php
use Illuminate\Database\Seeder;
use App\Models\Campagne;

class CampagnesTableSeeder extends Seeder {

    public function run()
    {
        Campagne::flushEventListeners();

        $items[] =
            [
                'id' => 3,
                'html' => '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
    <!-- Facebook sharing information tags -->
	<meta property="og:title" content="%%subject%%" />

    <title>Legendre Patrimoine</title>
</head>

<body bgcolor="#FFFFFF" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; height: 100% !important; width: 100% !important; background-color: #FAFAFA; margin: 0; padding: 0;">
<style type="text/css">#outlook a {
          padding: 0;
      }
      .body{
          width: 100% !important;
          -webkit-text-size-adjust: 100%;
          -ms-text-size-adjust: 100%;
          margin: 0;
          padding: 0;
      }
      .ExternalClass {
          width:100%;
      }
      .ExternalClass,
      .ExternalClass p,
      .ExternalClass span,
      .ExternalClass font,
      .ExternalClass td,
      .ExternalClass div {
          line-height: 100%;
      }
      img {
          outline: none;
          text-decoration: none;
          -ms-interpolation-mode: bicubic;
      }
      a img {
          border: none;
      }
      p {
          margin: 1em 0;
      }
      table td {
          border-collapse: collapse;
      }
      /* hide unsubscribe from forwards*/
      blockquote .original-only, .WordSection1 .original-only {
        display: none !important;
      }

      @media only screen and (max-width: 480px){
        body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:none !important;} /* Prevent Webkit platforms from changing default text sizes */
                body{width:100% !important; min-width:100% !important;} /* Prevent iOS Mail from adding padding to the body */

        #bodyCell{padding:10px !important;}

        #templateContainer{
          max-width:600px !important;
          width:100% !important;
        }

        h1{
          font-size:24px !important;
          line-height:100% !important;
        }

        h2{
          font-size:20px !important;
          line-height:100% !important;
        }

        h3{
          font-size:18px !important;
          line-height:100% !important;
        }

        h4{
          font-size:16px !important;
          line-height:100% !important;
        }

        #templatePreheader{display:none !important;} /* Hide the template preheader to save space */

        #headerImage{
          height:auto !important;
          max-width:600px !important;
          width:100% !important;
        }

        .headerContent{
          font-size:20px !important;
          line-height:125% !important;
        }

        .bodyContent{
          font-size:18px !important;
          line-height:125% !important;
        }

        .footerContent{
          font-size:14px !important;
          line-height:115% !important;
        }

        .footerContent a{display:block !important;} /* Place footer social and utility links on their own lines, for easier access */
      }
</style>
<table align="center" border="0" cellpadding="0" cellspacing="0" id="bodyTable" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #FFFFFF; border-collapse: collapse !important; height: 100% !important; margin: 0; mso-table-lspace: 0pt; mso-table-rspace: 0pt; padding: 0; width: 100% !important" width="100%">
	<tbody>
		<tr>
			<td align="center" id="bodyCell" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; height: 100% !important; width: 100% !important; border-top-width: 4px; border-top-color: #FFFFFF; border-top-style: solid; margin: 0; padding: 20px;" valign="top"><!-- BEGIN TEMPLATE // -->
			<table border="0" cellpadding="0" cellspacing="0" id="templateContainer" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse !important; width: 600px; border: 1px solid #FFFFFF;">
				<tbody>
					<tr>
						<td align="center" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" valign="top"><!-- BEGIN PREHEADER // -->
						<table border="0" cellpadding="0" cellspacing="0" id="templatePreheader" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #FFFFFF; border-bottom-color: #FFFFFF; border-bottom-style: solid; border-bottom-width: 1px; border-collapse: collapse !important; mso-table-lspace: 0pt; mso-table-rspace: 0pt" width="100%">
							<tbody>
								<tr>
									<td align="left" class="preheaderContent" pardot-region="preheader_content00" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #CCCCCC; font-family: Helvetica; font-size: 10px; line-height: 12.5px; text-align: left; padding: 10px 20px;" valign="top"><span style="font-size:11px;"><span style="color:#465564;">6% par an pendant 7 ans en investissant écologique.</span></span></td>
									<td align="left" class="preheaderContent" pardot-region="preheader_content01" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #CCCCCC; font-family: Helvetica; font-size: 10px; line-height: 12.5px; text-align: left; padding: 10px 20px 10px 0;" valign="top" width="180"><span style="font-size:11px;"><span style="color:#465564;">Cet email ne s\'affiche pas ?</span><br>
									<a href="http://bayspringpartners.solution.weborama.fr/fcgi-bin/performance.fcgi?ID=291403&A=1&L=1137327&C=47843&f=13&P=75486&T=E&URL=https%3A%2F%2Fwww.legendre-patrimoine.com%2Fgo%2Fclicklab-solaire-ap" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; color: #606060; font-weight: normal; text-decoration: underline;" target="_blank"><span style="color:#465564;">Voir dans mon navigateur</span></a><span style="color:#465564;">.</span></span></td>
								</tr>
							</tbody>
						</table>
						<!-- // END PREHEADER --></td>
					</tr>
					<tr>
						<td align="center" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" valign="top"><!-- BEGIN HEADER // -->
    <table border="0" cellpadding="0" cellspacing="0" id="templateHeader" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #FFFFFF; border-bottom-color: #FFFFFF; border-bottom-style: solid; border-bottom-width: 1px; border-collapse: collapse !important; mso-table-lspace: 0pt; mso-table-rspace: 0pt" width="100%">
							<tbody>
								<tr>
									<td align="left" class="headerContent" pardot-region="header_image" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #505050; font-family: Helvetica; font-size: 20px; font-weight: bold; line-height: 20px; text-align: left; vertical-align: middle; padding: 0;" valign="top">&nbsp;&nbsp;&nbsp;<a href="http://bayspringpartners.solution.weborama.fr/fcgi-bin/performance.fcgi?ID=291403&A=1&L=1137327&C=47843&f=13&P=75486&T=E&URL=https%3A%2F%2Fwww.legendre-patrimoine.com%2Fgo%2Fclicklab-solaire-ap"><img align="left" alt="" border="0" height="38" src="http://go.quaters.fr/l/54732/2015-01-16/cxxt/54732/16036/legendre_patrimoine_investissement.jpg" style="width: 150px; float: left; margin: 10px 0px; height: 38px; border-width: 0px; border-style: solid;" width="150"></a><br>
<br>
<a href="http://bayspringpartners.solution.weborama.fr/fcgi-bin/performance.fcgi?ID=291403&A=1&L=1137327&C=47843&f=13&P=75486&T=E&URL=https%3A%2F%2Fwww.legendre-patrimoine.com%2Fgo%2Fclicklab-solaire-ap"><img alt="" border="0" height="326" src="http://go.pardot.com/l/54732/2015-05-26/yc1br/54732/41794/visuel_photovoltaique_emailing_quatersV3.jpg" style="height: 326px; border-width: 0px; border-style: solid; width: 733px;" width="733"></a></td>
								</tr>
							</tbody>
						</table>
						<!-- // END HEADER --></td>
					</tr>
					<tr>
						<td align="center" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" valign="top"><!-- BEGIN BODY // -->
    <table border="0" cellpadding="0" cellspacing="0" id="templateBody" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #FFFFFF; border-bottom-color: #FFFFFF; border-bottom-style: solid; border-bottom-width: 1px; border-collapse: collapse !important; border-top-color: #FFFFFF; border-top-style: solid; border-top-width: 1px; mso-table-lspace: 0pt; mso-table-rspace: 0pt" width="100%">
							<tbody>
								<tr>
									<td align="left" class="bodyContent" pardot-region="body_content" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #505050; font-family: Helvetica; font-size: 14px; line-height: 21px; text-align: left; padding: 20px;" valign="top"><a href="http://bayspringpartners.solution.weborama.fr/fcgi-bin/performance.fcgi?ID=291403&A=1&L=1137327&C=47843&f=13&P=75486&T=E&URL=https%3A%2F%2Fwww.legendre-patrimoine.com%2Fgo%2Fclicklab-solaire-ap"><img align="left" alt="" border="0" height="120" src="http://go.quaters.fr/l/54732/2014-12-30/9dzh/54732/13416/securite.png" style="opacity: 0.9; color: rgb(70, 85, 100); font-size: 16px; line-height: 25.6000003814697px; text-align: justify; width: 120px; height: 120px; float: left; margin: 50px 70px; border-width: 0px; border-style: solid;" width="120"></a><br>
<span style="color: rgb(70, 85, 100); font-family: arial, helvetica, sans-serif; font-size: 20px; line-height: 1.5em;">Protégez votre capital en investissant écologique.</span>

<p style="text-align: justify;"><span style="color:#465564;"><span style="font-size:18px;"><span style="font-size:16px;line-height:1.6em;">Votre investissement bénéficie d’une <a href="http://bayspringpartners.solution.weborama.fr/fcgi-bin/performance.fcgi?ID=291403&A=1&L=1137327&C=47843&f=13&P=75486&T=E&URL=https%3A%2F%2Fwww.legendre-patrimoine.com%2Fgo%2Fclicklab-solaire-ap">sécurité optimale</a><a href="http://bayspringpartners.solution.weborama.fr/fcgi-bin/performance.fcgi?ID=291403&A=1&L=1137327&C=47843&f=13&P=75486&T=E&URL=https%3A%2F%2Fwww.legendre-patrimoine.com%2Fgo%2Fclicklab-solaire-ap"> </a>grâce à un contrat avec EDF de 20 ans. De plus, les &nbsp;centrales solaires sont couvertes contractuellement par des assurances contre les risques et la perte d\'exploitation, que ce soit lors de son installation ou de son exploitation déjà initiée.</span></span></span><br>
<br>
<br>
<a href="http://bayspringpartners.solution.weborama.fr/fcgi-bin/performance.fcgi?ID=291403&A=1&L=1137327&C=47843&f=13&P=75486&T=E&URL=https%3A%2F%2Fwww.legendre-patrimoine.com%2Fgo%2Fclicklab-solaire-ap"><img align="left" alt="" border="0" height="450" src="http://go.pardot.com/l/54732/2015-05-26/yc1hw/54732/41798/brochure_mock_up_solaire_5.png" style="width: 300px; height: 450px; float: left; margin-left: 30px; margin-right: 30px; border-width: 0px; border-style: solid;" width="300"></a></p>

<p><br>
<span style="color: rgb(70, 85, 100); font-family: arial, helvetica, sans-serif; font-size: 20px; line-height: 1.5em;">Déçus par le faible rendement de votre livret A ou de votre assurance-vie ?</span><br>
<br>
<font color="#465564"><span style="font-size: 16px; line-height: 25.6000003814697px;">L\'opportunité de vous créer<br>
<a href="http://bayspringpartners.solution.weborama.fr/fcgi-bin/performance.fcgi?ID=291403&A=1&L=1137327&C=47843&f=13&P=75486&T=E&URL=https%3A%2F%2Fwww.legendre-patrimoine.com%2Fgo%2Fclicklab-solaire-ap"><strong>un complément de revenu effectif&nbsp;à 6%&nbsp;</strong></a>par an pendant 7 ans.<br>
<br>
<a href="http://bayspringpartners.solution.weborama.fr/fcgi-bin/performance.fcgi?ID=291403&A=1&L=1137327&C=47843&f=13&P=75486&T=E&URL=https%3A%2F%2Fwww.legendre-patrimoine.com%2Fgo%2Fclicklab-solaire-ap"><img alt="" border="0" height="68" src="http://go.pardot.com/l/54732/2015-03-05/2d21p/54732/26168/bouton_telecharger.png" style="width: 278px; height: 68px; border-width: 0px; border-style: solid;" width="278"></a></span></font></p>
</td>
								</tr>
							</tbody>
						</table>
						<!-- // END BODY --></td>
					</tr>
					<tr>
						<td align="center" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" valign="top"><!-- BEGIN FOOTER // -->
    <table border="0" cellpadding="0" cellspacing="0" id="templateFooter" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #FFFFFF; border-collapse: collapse !important; border-top-color: #FFFFFF; border-top-style: solid; border-top-width: 1px; mso-table-lspace: 0pt; mso-table-rspace: 0pt" width="100%">
							<tbody>
								<tr pardot-removable="" class="">
									<td align="left" class="footerContent" pardot-region="footer_content00" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #808080; font-family: Helvetica; font-size: 10px; line-height: 15px; text-align: left; padding: 20px;" valign="top"><a href="http://bayspringpartners.solution.weborama.fr/fcgi-bin/performance.fcgi?ID=291403&A=1&L=1137327&C=47843&f=13&P=75486&T=E&URL=https%3A%2F%2Fwww.legendre-patrimoine.com%2Fgo%2Fclicklab-solaire-ap"><img alt="" border="0" height="138" src="http://go.pardot.com/l/54732/2015-04-27/d14dc/54732/35884/Stats_Legendre.jpg" style="width: 730px; height: 138px; border-width: 0px; border-style: solid;" width="730"></a></td>
								</tr>
								<tr>
									<td align="left" class="footerContent" pardot-region="footer_content01" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #808080; font-family: Helvetica; font-size: 10px; line-height: 15px; text-align: left; padding: 0 20px 20px;" valign="top"><div style="text-align: justify;"><span style="text-align: justify;"><span style="font-size:11px;">* Possibilité de sortir de votre investissement à partir de la fin de la troisième année.</span><br>
<br>
    Legendre Patrimoine © est une marque déposée à l’INPI et appartenant à Global Patrimoine investissement,&nbsp;société par actions simplifiée au capital de € 200.000, immatriculée sous le numéro RCS Paris 521 242 107, dont le siège se trouve 34 avenue des Champs-Elysées 75008 Paris&nbsp;.&nbsp;Courtier en assurance enregistré à l’ORIAS sous le N°12068090.&nbsp;Conseiller financier : Agrément N° E003303 à l’ANACOFI-CIF.&nbsp;Carte professionnelle N° T13739 délivrée par la préfecture de Paris&nbsp;521 242 107 RCS PARIS. Garantie financière : QBE – Etoile Saint Honoré – 21 rue Balzac – 75406 Paris cedex 08. Dans un souci de protection absolue de notre clientèle, il n’est reçu&nbsp;aucun fonds, en dehors des honoraires.</span></div>
</td>
								</tr>
								<tr>
									<td align="left" class="footerContent original-only" pardot-region="footer_content02" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #808080; font-family: Helvetica; font-size: 10px; line-height: 15px; text-align: left; padding: 0 20px 40px;" valign="top"><div style="text-align: center;"><a href="http://bayspringpartners.solution.weborama.fr/fcgi-bin/performance.fcgi?ID=291403&A=1&L=1137327&C=47843&f=13&P=75486&T=E&URL=https%3A%2F%2Fwww.legendre-patrimoine.com%2Fgo%2Fclicklab-solaire-ap" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; color: #606060; font-weight: normal; text-decoration: underline;">Mettre à jour mes préférences</a><br>
<br>
    Siège social :<br>
    34, avenue des Champs-Élysées<br>
75008, Paris - France<br>
© 2014 Legendre Patrimoine- Tous droits réservés</div>
</td>
								</tr>
							</tbody>
						</table>
						<!-- // END FOOTER --></td>
					</tr>
				</tbody>
			</table>
			<!-- // END TEMPLATE --></td>
		</tr>
	</tbody>
</table>
<br>
<!--
    This email was originally designed by the wonderful folks at MailChimp and remixed by Pardot.
    It is licensed under CC BY-SA 3.0
    -->
</body>
</html>',
                'base_id' => 6,
                'theme_id' => 80,
                'nom' => 'Legendre Solaire',
                'ref' => 'Legendre_Solaire-2015-06-19_pdm',
                'expediteur' => 'Legendre Solaire',
                'objet' => "Profitez d'un investissement fiable à 6 pourcent de rendement par an|Profitez vite d'un investissement fiable à 6 pourcent de rendement par an|Profitez maintenant d'un investissement fiable à 6 pourcent de rendement par année",
                'cle' => 'test',
                'toptext' => '',
                'bottomtext' => '',
                'belowtext' => '',
                'deletetext' => '',
            ];

        foreach($items as $item)
        {
            Campagne::create($item);
        }
    }
}
