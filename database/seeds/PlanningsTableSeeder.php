<?php
use Illuminate\Database\Seeder;
use App\Models\Planning;

class PlanningsTableSeeder extends Seeder {

    public function run()
    {
        \DB::table('plannings')->delete();

        //$items[] = ['id' => 5, 'campagne_id' => 1, 'volume' => 100, 'date_campagne' => date('Y-m-d')];
        //$items[] = ['id' => 6, 'campagne_id' => 1, 'volume' => 10, 'date_campagne' => date('Y-m-d')];
        $items[] = ['id' => 7, 'campagne_id' => 3, 'volume' => 1005, 'date_campagne' => date('Y-m-d'), 'routeur_id' => 1] ;

        foreach($items as $item)
        {
            Planning::create($item);
        }
    }
}
