<?php

use Illuminate\Database\Seeder;
use App\Models\Destinataire;

class DestinatairesTableSeeder extends Seeder {

    public function run()
    {
        $destinataires[] = [
            'mail'             => 'fabien@lead-factory.net',
            'base_id'          =>   1,
            'origine'           => 'Administrateurs'
        ];

        $destinataires[] = [
            'mail'             => 'adeline@sc2consulting.fr',
            'base_id'          =>   1,
            'origine'           => 'Administrateurs'
        ];

        $destinataires[] = [
            'mail'             => 'yves@sc2consulting.fr',
            'base_id'          =>   1,
            'origine'           => 'Administrateurs'
        ];

        $destinataires[] = [
            'mail'             => 'olivier@sc2consulting.fr',
            'base_id'          =>   1,
            'origine'           => 'Administrateurs'
        ];

        foreach($destinataires as $destinataire)
        {
            Destinataire::create($destinataire);
        }
    }
}