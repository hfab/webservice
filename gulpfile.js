var gulp = require('gulp');
var sass = require('gulp-sass');
var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {

    mix.sass("*.scss","public/css/global.css");

    mix.styles(
        [
            "../metronic/global/plugins/font-awesome/css/font-awesome.min.css",
            "../metronic/global/plugins/simple-line-icons/simple-line-icons.min.css",
            "../metronic/global/plugins/bootstrap/css/bootstrap.min.css",
            "../metronic/global/plugins/uniform/css/uniform.default.css",
            "../metronic/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css",
        ],
        'public/css/bootstrap.css'
    );

    mix.styles(
        [
            "../metronic/admin/pages/css/login.css",
        ],
        'public/css/login.css'
    );

    mix.styles(
        [
            "../metronic/global/css/components-rounded.css",
            "../metronic/global/css/plugins.css",
            "../metronic/admin/layout3/css/layout.css",
            "../metronic/admin/layout3/css/custom.css",
            "../metronic/admin/pages/css/timeline.css",
            "../dist/sweetalert.css",
            "../dist/toastr.min.css",
            "../css/global.css",
        ],
        'public/css/layout.css'
    );

    mix.scripts(
        [
            "../metronic/global/plugins/jquery.min.js",
            "../metronic/global/plugins/jquery-migrate.min.js",
            "../metronic/global/plugins/bootstrap/js/bootstrap.min.js",
            "../metronic/frontend/layout/scripts/back-to-top.js",
            "../metronic/global/scripts/metronic.js",
            "../metronic/admin/layout3/scripts/layout.js",
            "../dist/stacktable.min.js",
            "../dist/sweetalert.min.js",
            "../dist/toastr.min.js",
            "../global.js",
        ],
        'public/js/base.js'
    );

    mix.scripts(
        [
            "../metronic/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js",
            "../metronic/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js",
            "../metronic/global/plugins/jquery.blockui.min.js",
            "../metronic/global/plugins/uniform/jquery.uniform.min.js",
            "../metronic/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js",
        ],
        'public/js/layout.js'
    );

    mix.scripts(
        [
            '../metronic/global/plugins/jquery-validation/js/jquery.validate.min.js',
            '../metronic/admin/pages/scripts/login.js'
        ],
        'public/js/login.js'
    );

    mix.scripts(
        [
            '../metronic/global/plugins/flot/jquery.flot.min.js',
            '../metronic/global/plugins/flot/jquery.flot.resize.min.js',
            '../metronic/global/plugins/flot/jquery.flot.pie.min.js',
            '../metronic/global/plugins/flot/jquery.flot.stack.min.js',
            '../metronic/global/plugins/flot/jquery.flot.crosshair.min.js',
            '../metronic/global/plugins/flot/jquery.flot.categories.min.js',
            //'../metronic/admin/pages/scripts/charts-flotcharts.js'
        ],
        'public/js/charts.js'
    );

    mix.copy('../metronic/global/plugins/font-awesome/fonts', 'public/fonts');

});

// console.log(elixir);